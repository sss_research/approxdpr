library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
generic (N : integer := 8; M : integer := 8);
port( 
topa : in std_logic_vector(N-1 downto 0);
topb : in std_logic_vector(M-1 downto 0);
topd : in std_logic_vector(N downto 0);
topp : out std_logic_vector(N+M-1 downto 0)
);
end top;

architecture Behavioral of top is

begin

mul : entity work.approximate
generic map(
    N => N,
    M => M
)    
port map(
    a => topa,
    b => topb,
    d => topd,
    p => topp
);
    
end Behavioral;
