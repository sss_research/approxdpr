library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

use IEEE.MATH_REAL.ALL;
use STD.TEXTIO.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
-- library UNISIM;
-- use UNISIM.VComponents.all;

entity top_tb is 
end top_tb;

architecture Behavioral of top_tb is
constant N : integer := 8; --width
constant M : integer := 8; --height

signal a : std_logic_vector(N-1 downto 0) := '1' & (N-2 downto 0 => '0'); --width
signal b : std_logic_vector(M-1 downto 0) := '1' & (M-2 downto 0 => '0'); --height
signal d : std_logic_vector(N downto 0) := (N downto 0 => '0');
signal p : std_logic_vector(N+M-1 downto 0);

signal var_a : signed(N-1 downto 0);
signal var_b : signed(M-1 downto 0);
signal var_c : signed(N+M-1 downto 0);
signal rand_a : integer := 0;
signal rand_b : integer := 0;
signal p1 : std_logic_vector(N+M-1 downto 0);

begin

DUT : entity work.top
port map(
topa => a,
topb => b,
topd => d,
topp => p
);

Stimuli : process
    variable seed1, seed2 : positive;
    variable rand : real;
    variable range_of_rand : real := 2.0**N;
    
    file file_results : text;
    file file_input1, file_input2 : text;
    variable file_oline : line;
    variable file_iline1, file_iline2 : line;
    variable input_a : integer;
    variable input_b : integer;
    
begin

d <= (N downto 0 => '0');

--------------------------------------------------------------------------------------
--full simulation for accuracy estimation of approx multiplier including error alert
--------------------------------------------------------------------------------------

--file_open(file_results, "mul_results.csv", write_mode);

--write(file_oline, string'("a, b, bin_a, bin_b, accu, appr, bin_accu, bin_appr, error, rel_err"));
--writeline(file_results, file_oline);

--for I in -128 to 127 loop
--    for J in -128 to 127 loop
--            var_a <= to_signed(I,N);
--            var_b <= to_signed(J,N);
--            wait for 1 ns;
--            a <= std_logic_vector(var_a);
--            b <= std_logic_vector(var_b);
--            wait for 1 ns;
--            var_c <= var_a*var_b;
--            p1 <= std_logic_vector(var_a*var_b);
--            wait for 1 ns;
--            assert std_logic_vector(var_c) = p 
--                report "Wrong for: " & integer'image(to_integer(var_a)) & " * " & integer'image(to_integer(var_b)) & 
--                    " != " & integer'image(to_integer(var_c)) & " Given result: " & integer'image(to_integer(signed(p))) 
--                severity error;
--            write(file_oline, integer'image(I));
--            write(file_oline, string'(", "));
--            write(file_oline, integer'image(J));
--            write(file_oline, string'(", "));
--            write(file_oline, a);
--            write(file_oline, string'(", "));
--            write(file_oline, b);
--            write(file_oline, string'(", "));
--            write(file_oline, integer'image(to_integer(var_c)));--, J, to_integer(signed(p)));
--            write(file_oline, string'(", "));
--            write(file_oline, integer'image(to_integer(signed(p))));
--            write(file_oline, string'(", "));
----            write(file_line, string'(std_logic_vector(var_c));
--            write(file_oline, p1);
--            write(file_oline, string'(", "));
--            write(file_oline, p);
--            write(file_oline, string'(","));
--            writeline(file_results, file_oline);
--    end loop;
--end loop;

--file_close(file_results);

--------------------------------------------------------------------------------------
--new random simulation for power and delay estimation
--------------------------------------------------------------------------------------

file_open(file_results, "mul_results.csv", write_mode);

write(file_oline, string'("a, b, bin_a, bin_b, accu, appr, bin_accu, bin_appr, error, rel_err"));
writeline(file_results, file_oline);


file_open(file_input1, "/afs/pd.inf.tu-dresden.de/users/s1780256/tb_in_a0.txt", read_mode);

for i in 0 to 255 loop --65535
    readline (file_input1, file_iline1);
    read(file_iline1, input_a);
    a <= std_logic_vector(to_signed(input_a, a'length));
        
    file_open(file_input2, "/afs/pd.inf.tu-dresden.de/users/s1780256/tb_in_b0.txt", read_mode);    
        
    for j in 0 to 255 loop
        readline (file_input2, file_iline2);
        read(file_iline2, input_b);
        b <= std_logic_vector(to_signed(input_b, b'length));

        wait for 1 ns; 
        p1 <= std_logic_vector(signed(a)*signed(b));
        var_c <= signed(a)*signed(b);
        wait for 1 ns;
        
        write(file_oline, integer'image(input_a));
        write(file_oline, string'(", "));
        write(file_oline, integer'image(input_b));
        write(file_oline, string'(", "));
        write(file_oline, a);
        write(file_oline, string'(", "));
        write(file_oline, b);
        write(file_oline, string'(", "));
        write(file_oline, integer'image(to_integer(var_c)));--, J, to_integer(signed(p)));
        write(file_oline, string'(", "));
        write(file_oline, integer'image(to_integer(signed(p))));
        write(file_oline, string'(", "));
--        write(file_line, string'(std_logic_vector(var_c));
        write(file_oline, p1);
        write(file_oline, string'(", "));
        write(file_oline, p);
        write(file_oline, string'(","));
        writeline(file_results, file_oline);
        
    end loop; 

    file_close(file_input2);
    
    wait for 1 ns;

end loop;

file_close(file_input1);

file_close(file_results);

--------------------------------------------------------------------------------------
--old random simulation including error alert
--------------------------------------------------------------------------------------

--for I in -8 to 8 loop
--    for J in -8 to 8 loop
--            var_a <= to_signed(I,N);
--            var_b <= to_signed(J,N);
--            wait for 1 ns;
--            a <= std_logic_vector(var_a);
--            b <= std_logic_vector(var_b);
--            wait for 1 ns;
--            var_c <= var_a*var_b;
--            wait for 1 ns;
--            assert std_logic_vector(var_c) = p report "Wrong for: " & integer'image(to_integer(var_a)) & " * " & integer'image(to_integer(var_b)) & " != " & integer'image(to_integer(var_c)) & " Given result: " & integer'image(to_integer(signed(p))) severity error;
--    end loop;
--end loop;

--a <= '0' & (N-2 downto 0 => '1');
--b <= '0' & (N-2 downto 0 => '1');
--wait for 1 ns;
--var_a <= signed(a);
--var_b <= signed(b);
--wait for 1 ns;
--var_c <= var_a*var_b;
--wait for 1 ns;
--assert std_logic_vector(var_c) = p report "Wrong for: " & integer'image(to_integer(var_a)) & " * " & integer'image(to_integer(var_b)) & " != " & integer'image(to_integer(var_c)) & " Given result: " & integer'image(to_integer(signed(p))) severity error;

--a <= '0' & (N-2 downto 0 => '1');
--b <= '1' & (N-2 downto 0 => '0');
--wait for 1 ns;
--var_a <= signed(a);
--var_b <= signed(b);
--wait for 1 ns;
--var_c <= var_a*var_b;
--wait for 1 ns;
--assert std_logic_vector(var_c) = p report "Wrong for: " & integer'image(to_integer(var_a)) & " * " & integer'image(to_integer(var_b)) & " != " & integer'image(to_integer(var_c)) & " Given result: " & integer'image(to_integer(signed(p))) severity error;

--a <= '1' & (N-2 downto 0 => '0');
--b <= '0' & (N-2 downto 0 => '1');
--wait for 1 ns;
--var_a <= signed(a);
--var_b <= signed(b);
--wait for 1 ns;
--var_c <= var_a*var_b;
--wait for 1 ns;
--assert std_logic_vector(var_c) = p report "Wrong for: " & integer'image(to_integer(var_a)) & " * " & integer'image(to_integer(var_b)) & " != " & integer'image(to_integer(var_c)) & " Given result: " & integer'image(to_integer(signed(p))) severity error;

--a <= '1' & (N-2 downto 0 => '0');
--b <= '1' & (N-2 downto 0 => '0');
--wait for 1 ns;
--var_a <= signed(a);
--var_b <= signed(b);
--wait for 1 ns;
--var_c <= var_a*var_b;
--wait for 1 ns;
--assert std_logic_vector(var_c) = p report "Wrong for: " & integer'image(to_integer(var_a)) & " * " & integer'image(to_integer(var_b)) & " != " & integer'image(to_integer(var_c)) & " Given result: " & integer'image(to_integer(signed(p))) severity error;

--a <= (N-1 downto 0 => '1');
--b <= (N-1 downto 0 => '1');
--wait for 1 ns;
--var_a <= signed(a);
--var_b <= signed(b);
--wait for 1 ns;
--var_c <= var_a*var_b;
--wait for 1 ns;
--assert std_logic_vector(var_c) = p report "Wrong for: " & integer'image(to_integer(var_a)) & " * " & integer'image(to_integer(var_b)) & " != " & integer'image(to_integer(var_c)) & " Given result: " & integer'image(to_integer(signed(p))) severity error;

--for k in 0 to 50000 loop
--    uniform(seed1, seed2, rand);
--    rand_a <= integer(rand*range_of_rand);
--    wait for 1 ns;
--    uniform(seed1, seed2, rand);
--    rand_b <= integer(rand*range_of_rand);
--    wait for 1 ns;
--    var_a <= to_signed(rand_a, a'length);
--    var_b <= to_signed(rand_b, b'length);
--    wait for 1 ns;
--    a <= std_logic_vector(var_a);
--    b <= std_logic_vector(var_b);
--    wait for 1 ns;
--    var_c <= var_a * var_b;
--    wait for 1 ns;
--    assert std_logic_vector(var_c) = p report "Wrong for: " & integer'image(to_integer(var_a)) & " * " & integer'image(to_integer(var_b)) & " != " & integer'image(to_integer(var_c)) & " Given result: " & integer'image(to_integer(signed(p))) severity error;
--end loop;

wait;
end process;

end Behavioral;