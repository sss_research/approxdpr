
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity basic_accurate is
generic (N : integer);
port(
    a : in std_logic_vector(N-1 downto 0);
    b : in std_logic_vector(N-1 downto 0);
    d : in std_logic_vector(N downto 0);
    p : out std_logic_vector(2*N-1 downto 0)
);
end basic_accurate;

architecture Behavioral of basic_accurate is
component LUT6_2
generic (INIT : bit_vector(63 downto 0) := X"0000000000000000");
port(
    O5 : out std_logic;
    O6 : out std_logic;
    I0 : in std_logic;
    I1 : in std_logic;
    I2 : in std_logic;
    I3 : in std_logic;
    I4 : in std_logic;
    I5 : in std_logic
);
end component;

component CARRY4
port(
    CO : out std_logic_vector(3 downto 0); 
    O : out std_logic_vector(3 downto 0);
    CI : in std_logic;
    CYINIT : in std_logic;
    DI : in std_logic_vector(3 downto 0);
    S : in std_logic_vector(3 downto 0)
);
end component;

--lut_o(stage)(lut position in stage), output of all luts, summarized per stage
type tLut_o is array(N/2-1 downto 0) of std_logic_vector(N+7 downto 0);--std_logic_vector(N+3 downto 0);
signal lut_o : tLut_o;
--carry(stage)(bit position in stage), carry word per stage including the carry in at position carry(stage)(0)
type tCarry is array(N/2-1 downto 0) of std_logic_vector(N+8 downto 0);--std_logic_vector(N+4 downto 0);
signal carry : tCarry;
--ccout(stage)(bit position in stage), carry chain output word per stage
type tOut is array(N/2-1 downto 0) of std_logic_vector(N+7 downto 0);--std_logic_vector(N+3 downto 0);
signal ccout : tOut;
--din(stage)(lut input), signal that is propagated to LUT AND carry chain, therefore additional word for generic carry chain generation
type tDin is array(N/2-1 downto 0) of std_logic_vector(N+7 downto 0);--std_logic_vector(N+3 downto 0);
signal din : tDin;

--approximation testing:
type tAppcarry is array(1 downto 0) of std_logic_vector(1 downto 0);
signal appcarry :tAppcarry;

begin
--generic LUT A generation, special cases:
--row zero has no LUT at position zero
--row zero b(m-1) input is static '0'
--every other row LUT at position zero has all '0' as input
--every row LUT at position one input for a(n-1) is static '0'
--every row LUT at position N input for a(n) and a(n-1) is static a(N-1) 
GEN_LUTA :
for stage in 0 to N/2-1 generate
    LUTA0 :
    if stage = 0 generate
    GEN_LUTA0 : 
    for nr in 1 to N+1 generate
        LUTA01 : 
        if nr = 1 generate
            LUTA01X : LUT6_2
            generic map(
                INIT => X"AAA9655AA5596AAA"
            )
            port map(
                I0 => d(nr-1),
                I1 => '0',
                I2 => '0',
                I3 => b(2*stage),
                I4 => b(2*stage+1),
                I5 => a(nr-1),
                O6 => lut_o(stage)(nr)
            );
            end generate LUTA01;            
        LUTA0Y :
        if nr > 1 and nr < N+1 generate
            LUTA0YX : LUT6_2
            generic map(
                INIT => X"AAA9655AA5596AAA"
            )
            port map(
                I0 => d(nr-1),
                I1 => a(nr-2),
                I2 => '0',
                I3 => b(2*stage),
                I4 => b(2*stage+1),
                I5 => a(nr-1),
                O6 => lut_o(stage)(nr)
             );
        end generate LUTA0Y;
        LUTA0N :
        if nr = N+1 generate
            LUTA0NX : LUT6_2
            generic map(
                INIT => X"AAA9655AA5596AAA"
            )
            port map(
                I0 => d(nr-1),
                I1 => a(nr-2),
                I2 => '0',
                I3 => b(2*stage),
                I4 => b(2*stage+1),
                I5 => a(nr-2),
                O6 => lut_o(stage)(nr)
             );
        end generate LUTA0N;        
    end generate GEN_LUTA0;
    end generate LUTA0;
    LUTAY :
    if stage > 0 and stage < N/2 generate
    GEN_LUTAY : 
    for nr in 0 to N+1 generate
        LUTAY0 :
        if nr = 0 generate
            LUTAY0X : LUT6_2
            generic map(
                INIT => X"AAA9655AA5596AAA"
            )
            port map(
                I0 => '0',
                I1 => '0',
                I2 => b(2*stage-1),
                I3 => b(2*stage),
                I4 => b(2*stage+1),
                I5 => '0',
                O6 => lut_o(stage)(nr)
            );
        end generate LUTAY0;
        LUTAY1 :
        if nr = 1 generate
            LUTAY1X : LUT6_2
            generic map(
                INIT => X"AAA9655AA5596AAA"
            )
            port map(
                I0 => ccout(stage-1)(nr+2),
                I1 => '0',
                I2 => b(2*stage-1),
                I3 => b(2*stage),
                I4 => b(2*stage+1),
                I5 => a(nr-1),
                O6 => lut_o(stage)(nr)
            );
        end generate LUTAY1;       
        LUTAYY :
        if nr > 1 and nr < N+1 generate 
            LUTAYYX : LUT6_2
            generic map(
                INIT => X"AAA9655AA5596AAA"
            )
            port map(
                I0 => ccout(stage-1)(nr+2),
                I1 => a(nr-2),
                I2 => b(2*stage-1),
                I3 => b(2*stage),
                I4 => b(2*stage+1),
                I5 => a(nr-1),
                O6 => lut_o(stage)(nr)
            );
         end generate LUTAYY;
         LUTAYN :
         if  nr = N+1 generate
         LUTAYNX : LUT6_2
            generic map(
             INIT => X"AAA9655AA5596AAA"
         )
         port map(
             I0 => ccout(stage-1)(nr+2),
             I1 => a(nr-2),
             I2 => b(2*stage-1),
             I3 => b(2*stage),
             I4 => b(2*stage+1),
             I5 => a(nr-2),
             O6 => lut_o(stage)(nr)
         );
        end generate LUTAYN;         
     end generate GEN_LUTAY;
     end generate LUTAY;
end generate GEN_LUTA;

--generic LUT C generation, special cases:
--row zero b(m-1) input is static '0'
--last row has no LUT C
GEN_LUTC :
for stage in 0 to N/2-1 generate
    LUTC0 :
    if stage = 0 generate
    LUTC0X : LUT6_2
    generic map(
        INIT => X"0000000056669995"
    )
    port map(
        I0 => '1',
        I1 => a(N-1),
        I2 => '0',
        I3 => b(2*stage),
        I4 => b(2*stage+1),
        I5 => '0',
        O6 => lut_o(stage)(N+2)
    );
    end generate LUTC0;
    LUTCY :
    if stage > 0 generate
    LUTCYX : LUT6_2
    generic map(
        INIT => X"0000000056669995"
    )
    port map(
        I0 => carry(stage-1)(N+4),
        I1 => a(N-1),
        I2 => b(2*stage-1),
        I3 => b(2*stage),
        I4 => b(2*stage+1),
        I5 => '0',
        O6 => lut_o(stage)(N+2)
    );
    end generate LUTCY;
end generate GEN_LUTC;

GEN_CARRY :
for stage in 0 to N/2-1 generate
    GEN_CARRYY : 
    for nr in 0 to (N+4)/4-1 generate --(N+4)/4-1 generate
    CARRYYX : CARRY4
    port map(
        CO => carry(stage) (nr*4+4 downto nr*4+1),
        O => ccout(stage) (nr*4+3 downto nr*4),
        CI => carry(stage) (nr*4),
        CYINIT => '0',
        DI => din(stage) (nr*4+3 downto nr*4),
        S => lut_o(stage) (nr*4+3 downto nr*4)                
    );
    end generate GEN_CARRYY;    
end generate GEN_CARRY;    

------------------------------------------------------------------------------------------
--Input & output configuration for 4x4
------------------------------------------------------------------------------------------
--carry(0)(0) <= '1';
--carry(1)(0) <= '1';

--lut_o(0)(0) <= b(1);
--lut_o(0)(N+3) <= '1';

--din(0)(N+3 downto 0) <= (b"01" & d & '0');
--din(1)(N+3 downto 0) <= ('1' & carry(0)(N+4) & ccout(0)(N+3 downto 3) & '0');

--p <= (ccout(1)(N+2 downto 1) & ccout(0)(2 downto 1));
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--Input & output configuration for 8x8
------------------------------------------------------------------------------------------
carry(0)(0) <= '1';
carry(1)(0) <= '1';
carry(2)(0) <= '1';
carry(3)(0) <= '1';
--carry(4)(0) <= '1';

lut_o(0)(0) <= b(1);
lut_o(0)(N+3) <= '1';
lut_o(1)(N+3) <= '1';
lut_o(2)(N+3) <= '1';

din(0)(N+3 downto 0) <= (b"01" & d & '0');
din(1)(N+3 downto 0) <= ('1' & carry(0)(N+4) & ccout(0)(N+3 downto 3) & '0');
din(2)(N+3 downto 0) <= ('1' & carry(1)(N+4) & ccout(1)(N+3 downto 3) & '0');
din(3)(N+2 downto 0) <= (carry(2)(N+4) & ccout(2)(N+3 downto 3) & '0');
--din(4)(N downto 0) <= (ccout(3)(N+2 downto 3) & '0');

p <= (ccout(3)(N+2 downto 1) & ccout(2)(2 downto 1) & ccout(1)(2 downto 1) & ccout(0)(2 downto 1));
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--Input & output configuration for 16x16
------------------------------------------------------------------------------------------
--carry(0)(0) <= '1';
--carry(1)(0) <= '1';
--carry(2)(0) <= '1';
--carry(3)(0) <= '1';
--carry(4)(0) <= '1';
--carry(5)(0) <= '1';
--carry(6)(0) <= '1';
--carry(7)(0) <= '1';
----carry(8)(0) <= '1';

--lut_o(0)(0) <= b(1);
--lut_o(0)(N+3) <= '1';
--lut_o(1)(N+3) <= '1';
--lut_o(2)(N+3) <= '1';
--lut_o(3)(N+3) <= '1';
--lut_o(4)(N+3) <= '1';
--lut_o(5)(N+3) <= '1';
--lut_o(6)(N+3) <= '1';

--din(0)(N+3 downto 0) <= (b"01" & d & '0');
--din(1)(N+3 downto 0) <= ('1' & carry(0)(N+4) & ccout(0)(N+3 downto 3) & '0');
--din(2)(N+3 downto 0) <= ('1' & carry(1)(N+4) & ccout(1)(N+3 downto 3) & '0');
--din(3)(N+3 downto 0) <= ('1' & carry(2)(N+4) & ccout(2)(N+3 downto 3) & '0');
--din(4)(N+3 downto 0) <= ('1' & carry(3)(N+4) & ccout(3)(N+3 downto 3) & '0');
--din(5)(N+3 downto 0) <= ('1' & carry(4)(N+4) & ccout(4)(N+3 downto 3) & '0');
--din(6)(N+3 downto 0) <= ('1' & carry(5)(N+4) & ccout(5)(N+3 downto 3) & '0');
--din(7)(N+2 downto 0) <= (carry(6)(N+4) & ccout(6)(N+3 downto 3) & '0');
----din(8)(N downto 0) <= (ccout(7)(N+2 downto 3) & '0');

--p <= (ccout(7)(N+2 downto 1) & ccout(6)(2 downto 1) & ccout(5)(2 downto 1) & ccout(4)(2 downto 1) & 
--    ccout(3)(2 downto 1) & ccout(2)(2 downto 1) & ccout(1)(2 downto 1) & ccout(0)(2 downto 1));
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--Input & output configuration for 32x32
------------------------------------------------------------------------------------------
--carry(0)(0) <= '1';
--carry(1)(0) <= '1';
--carry(2)(0) <= '1';
--carry(3)(0) <= '1';
--carry(4)(0) <= '1';
--carry(5)(0) <= '1';
--carry(6)(0) <= '1';
--carry(7)(0) <= '1';
--carry(8)(0) <= '1';
--carry(9)(0) <= '1';
--carry(10)(0) <= '1';
--carry(11)(0) <= '1';
--carry(12)(0) <= '1';
--carry(13)(0) <= '1';
--carry(14)(0) <= '1';
--carry(15)(0) <= '1';
----carry(16)(0) <= '1';

--lut_o(0)(0) <= b(1);
--lut_o(0)(N+3) <= '1';
--lut_o(1)(N+3) <= '1';
--lut_o(2)(N+3) <= '1';
--lut_o(3)(N+3) <= '1';
--lut_o(4)(N+3) <= '1';
--lut_o(5)(N+3) <= '1';
--lut_o(6)(N+3) <= '1';
--lut_o(7)(N+3) <= '1';
--lut_o(8)(N+3) <= '1';
--lut_o(9)(N+3) <= '1';
--lut_o(10)(N+3) <= '1';
--lut_o(11)(N+3) <= '1';
--lut_o(12)(N+3) <= '1';
--lut_o(13)(N+3) <= '1';
--lut_o(14)(N+3) <= '1';

--din(0)(N+3 downto 0) <= (b"01" & d & '0');
--din(1)(N+3 downto 0) <= ('1' & carry(0)(N+4) & ccout(0)(N+3 downto 3) & '0');
--din(2)(N+3 downto 0) <= ('1' & carry(1)(N+4) & ccout(1)(N+3 downto 3) & '0');
--din(3)(N+3 downto 0) <= ('1' & carry(2)(N+4) & ccout(2)(N+3 downto 3) & '0');
--din(4)(N+3 downto 0) <= ('1' & carry(3)(N+4) & ccout(3)(N+3 downto 3) & '0');
--din(5)(N+3 downto 0) <= ('1' & carry(4)(N+4) & ccout(4)(N+3 downto 3) & '0');
--din(6)(N+3 downto 0) <= ('1' & carry(5)(N+4) & ccout(5)(N+3 downto 3) & '0');
--din(7)(N+3 downto 0) <= ('1' & carry(6)(N+4) & ccout(6)(N+3 downto 3) & '0');
--din(8)(N+3 downto 0) <= ('1' & carry(7)(N+4) & ccout(7)(N+3 downto 3) & '0');
--din(9)(N+3 downto 0) <= ('1' & carry(8)(N+4) & ccout(8)(N+3 downto 3) & '0');
--din(10)(N+3 downto 0) <= ('1' & carry(9)(N+4) & ccout(9)(N+3 downto 3) & '0');
--din(11)(N+3 downto 0) <= ('1' & carry(10)(N+4) & ccout(10)(N+3 downto 3) & '0');
--din(12)(N+3 downto 0) <= ('1' & carry(11)(N+4) & ccout(11)(N+3 downto 3) & '0');
--din(13)(N+3 downto 0) <= ('1' & carry(12)(N+4) & ccout(12)(N+3 downto 3) & '0');
--din(14)(N+3 downto 0) <= ('1' & carry(13)(N+4) & ccout(13)(N+3 downto 3) & '0');
--din(15)(N+3 downto 0) <= ('1' & carry(14)(N+4) & ccout(14)(N+3 downto 3) & '0');
----din(16)(N downto 0) <= (ccout(15)(N+2 downto 3) & '0');


--p <= (ccout(15)(N+2 downto 1) & ccout(14)(2 downto 1) & ccout(13)(2 downto 1) & ccout(12)(2 downto 1) & 
--    ccout(11)(2 downto 1) & ccout(10)(2 downto 1) & ccout(9)(2 downto 1) & ccout(8)(2 downto 1) & ccout(7)(2 downto 1) & 
--    ccout(6)(2 downto 1) & ccout(5)(2 downto 1) & ccout(4)(2 downto 1) & ccout(3)(2 downto 1) & ccout(2)(2 downto 1) & 
--    ccout(1)(2 downto 1) & ccout(0)(2 downto 1));
end Behavioral;