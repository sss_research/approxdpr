#!/usr/bin/python3

import csv
#input size
n = 8
m = 8
#columns in csv
acc = 4
app = 5
acc_bin = 6
app_bin = 7
error = 8
rel_error = 9
#variables
max_error = 0
min_error = 9999
max_rel_error = 0.0

min_cnt = 0
max_cnt = 0
correct_max = 0
correct_min = 9999
err_cnt = 0
err_sum = 0
avg_err = 0 #before division!
mean_err = 0 #before division!
mse = 0 #before division!
nmed = 0 #before division!

bit = dict()

for i in range(0, n+m):
  bit[i] = 0

with open ('mul_results.csv') as csvfile:
  readCSV = csv.reader(csvfile, delimiter=',')
  header = next(readCSV)
  rowcount = sum(1 for row in readCSV)

file_new = open("results.csv","w")

#prepare new output file
with open ('mul_results.csv') as csvfile:
  readCSV = csv.reader(csvfile, delimiter=',')
  header = next(readCSV)
  writer = csv.writer(file_new)
  writer.writerow(header)
  for row in readCSV:
    if int(row[acc]) != 0:
      writer.writerow(row + [abs(int(row[acc])-int(row[app]))] + [abs((int(row[acc])-int(row[app]))/int(row[acc]))])
    else:
      writer.writerow(row + [abs(int(row[acc])-int(row[app]))] + [abs(int(row[acc])-int(row[app]))])

file_new.close()
print("finished error calculation")

#prepare variables min_error and max_error
with open ('results.csv') as csvfile:
  readCSV = csv.reader(csvfile, delimiter=',')
  header = next(readCSV)
  for row in readCSV:
    if int(row[error]) > max_error:
      max_error = int(row[error])
    if int(row[error]) < min_error:
      min_error = int(row[error])

print("found min and max error")

#create textfile with all metrics

file_metrics = open("results.txt","w")

with open ('results.csv') as csvfile:
  readCSV = csv.reader(csvfile, delimiter=',')
  header = next(readCSV)
  for row in readCSV:
    if int(row[error]) == min_error:
      min_cnt += 1
    if int(row[error]) == max_error:
      max_cnt += 1
    if int(row[acc]) > correct_max:
      correct_max = int(row[acc])
    if int(row[acc]) < correct_min:
      correct_min = int(row[acc])
    if float(row[rel_error]) > max_rel_error:
      max_rel_error = float(row[rel_error])
    if int(row[error]) != 0:
      err_cnt += 1
    err_sum += int(row[error])
    if int(row[acc]) != 0:
      avg_err += int(row[error]) / abs(int(row[acc]))
      mean_err += int(row[error]) / int(row[acc])
    mse += int(row[error]) * int(row[error])
    nmed += abs(int(row[app])-int(row[acc]))

file_metrics.write("Min error : " + str(min_error) + "\n") 
file_metrics.write("Max error : " + str(max_error) + "\n")
file_metrics.write("Min count : " + str(min_cnt) + " (" + str((min_cnt/rowcount)*100) + "%)" + "\n")
file_metrics.write("Max count : " + str(max_cnt) + " (" + str((max_cnt/rowcount)*100) + "%)" + "\n")
file_metrics.write("Max accurate result: " + str(correct_max) + "\n")
file_metrics.write("Min accurate result: " + str(correct_min) + "\n")
file_metrics.write("Error occurences: " + str(err_cnt) + " (" + str((err_cnt/rowcount)*100) + "%)" + "\n")
file_metrics.write("Average error: " + str(err_sum/rowcount) + "\n")
file_metrics.write("Average relative error: " + str(avg_err/rowcount) + "\n")
file_metrics.write("Maximum relative error: " + str(max_rel_error) + "\n")
file_metrics.write("Mean error: " + str((mean_err*100)/rowcount) + "\n")
file_metrics.write("Mean squared error: " + str(mse/rowcount) + "\n")
file_metrics.write("Normalized mean squared error : " + str((nmed/rowcount)/((2**(n-1))*(2**(m-1)))) + "\n")

file_metrics.close()

print("calculated metrics")

#calculate bit accuracy

file_bit = open("bit.csv","w")

with open ('results.csv') as csvfile:
  readCSV = csv.reader(csvfile, delimiter=',')
  header = next(readCSV)
  for row in readCSV:
    for i in range(0, n+m):
      if (str(row[acc_bin])[i:i+1:] != str(row[app_bin])[i:i+1:]):
        bit[n+m-1-i] += 1

for i in range(0, n+m):
  file_bit.write(str((bit[i]/rowcount)*100))  
  file_bit.write("\n")

file_bit.close()

print("calculated bit accuracy")