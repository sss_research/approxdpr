#!/bin/bash

#*******************************************
#run ./rand_gen.sh varA varB to get three randomized input sets for limited number of input configurations, change $length!
#*******************************************

length=4096

n=$1
nn=$((2**$n))
nnn=$(($nn-1))
nnnn=$(($nn/2))

m=$2
mm=$((2**$m))
mmm=$(($mm-1))
mmmm=$(($mm/2))

> tb_in_a0.txt
nnr=($(shuf -i 0-$nnn -n $length))
for (( j=0; j<=(($length-1)); j++ ))
do
  nnr[$j]=$((nnr[$j]-$nnnn))
  echo "${nnr[$j]}" >> tb_in_a0.txt
done

> tb_in_a1.txt
nnr=($(shuf -i 0-$nnn -n $length))
for (( j=0; j<=(($length-1)); j++ ))
do
  nnr[$j]=$((nnr[$j]-$nnnn))
  echo "${nnr[$j]}" >> tb_in_a1.txt
done

> tb_in_a2.txt
nnr=($(shuf -i 0-$nnn -n $length))
for (( j=0; j<=(($length-1)); j++ ))
do
  nnr[$j]=$((nnr[$j]-$nnnn))
  echo "${nnr[$j]}" >> tb_in_a2.txt
done


> tb_in_b0.txt
nnr=($(shuf -i 0-$mmm -n $length))
for (( j=0; j<=(($length-1)); j++ ))
do
  nnr[$j]=$((nnr[$j]-$mmmm))
  echo "${nnr[$j]}" >> tb_in_b0.txt
done

> tb_in_b1.txt
nnr=($(shuf -i 0-$mmm -n $length))
for (( j=0; j<=(($length-1)); j++ ))
do
  nnr[$j]=$((nnr[$j]-$mmmm))
  echo "${nnr[$j]}" >> tb_in_b1.txt
done

> tb_in_b2.txt
nnr=($(shuf -i 0-$mmm -n $length))
for (( j=0; j<=(($length-1)); j++ ))
do
  nnr[$j]=$((nnr[$j]-$mmmm))
  echo "${nnr[$j]}" >> tb_in_b2.txt
done