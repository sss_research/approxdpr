#!/bin/bash

#*******************************************
#run ./rand_gen.sh varA varB to get three randomized input sets for all possible input combinations, varA, varB = bitlength
#*******************************************

n=$1
nn=$((2**$n))
nnn=$(($nn-1))

m=$2
mm=$((2**$m))
mmm=$(($mm-1))

negn=$((2**($n-1)*(-1)))
posn=$(((2**($n-1)-1)))

negm=$((2**($m-1)*(-1)))
posm=$(((2**($m-1)-1)))

> tb_in_a0.txt
nnr=($(seq $negn $posn | shuf -n $nn))
for (( j=0; j<=$nn; j++ ))
do
  echo "${nnr[$j]}" >> tb_in_a0.txt
done

> tb_in_a1.txt
nnr=($(seq $negn $posn | shuf -n $nn))
for (( j=0; j<=$nn; j++ ))
do
  echo "${nnr[$j]}" >> tb_in_a1.txt
done

> tb_in_a2.txt
nnr=($(seq $negn $posn | shuf -n $nn))
for (( j=0; j<=$nn; j++ ))
do
  echo "${nnr[$j]}" >> tb_in_a2.txt
done

> tb_in_b0.txt
nnr=($(seq $negm $posm | shuf -n $mm))
for (( j=0; j<=$mm; j++ ))
do
  echo "${nnr[$j]}" >> tb_in_b0.txt
done

> tb_in_b1.txt
nnr=($(seq $negm $posm | shuf -n $mm))
for (( j=0; j<=$mm; j++ ))
do
  echo "${nnr[$j]}" >> tb_in_b1.txt
done

> tb_in_b2.txt
nnr=($(seq $negm $posm | shuf -n $mm))
for (( j=0; j<=$mm; j++ ))
do
  echo "${nnr[$j]}" >> tb_in_b2.txt
done