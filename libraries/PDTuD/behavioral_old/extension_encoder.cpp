#include <iostream>
#include "behavioral.hpp"
using namespace std;

void Ee::setEeInput( bool bmnegin, bool bmin, bool bmposin, bool signin ) {
  bmneg = bmnegin;
  bm = bmin;
  bmpos = bmposin;
  sign = signin;
}

bool Ee::getEeOutput( void ) {
  //F0 = bm bmpos sign' + bm' bmpos' sign + bmneg' sign' + bmneg sign;
  //ee = (bm & bmpos & !sign) | (!bm & !bmpos & sign) | (!bmneg & !sign) | (bmneg & sign);
  //F0 = bm1 sign + bm1' sign' + bm' bm-1' sign + bm bm-1 sign';
  ee = (bmpos & sign) | (!bmpos & !sign) | (!bm & !bmneg & sign) | (bm & bmneg & !sign);
  return ee;
}
