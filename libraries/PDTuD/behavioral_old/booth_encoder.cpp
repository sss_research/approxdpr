#include <iostream>
#include "behavioral.hpp"
using namespace std;

void Be::setBeInput( bool bmnegin, bool bmin, bool bmposin ) {
  bmneg = bmnegin;
  bm = bmin;
  bmpos = bmposin;
}

bool Be::getS( void ) {
  s = (!bmpos & bm & bmneg) | (bmpos & !bm & !bmneg);
  return s;
}

bool Be::getC( void ) {
  c = bmpos;
  return c;
}

bool Be::getZ( void ) {
  z = (bmpos & bm & bmneg) | (!bmpos & !bm & !bmneg);
  return z;
}

bool Be::getM( void ) {
  //m = bm1 bm' bm-1' + bm1 bm' bm-1 + bm1 bm bm-1';
  m = (bmpos & !bm & !bmneg) | (bmpos & !bm & bmneg) | (bmpos & bm & !bmneg);
  return m;
}

void Be::printBeInput( void ) {
  std::cout << "bmneg: " << !bmneg << std::endl;
  std::cout << "bm: " << !bm << std::endl;
  std::cout << "bmpos: " << !bmpos << std::endl;
}
