#include <iostream>
#include "behavioral.hpp"
using namespace std;

void Lut::setLutInput( bool in0, bool in1, bool in2, bool in3, bool in4, bool in5 ) {
  i0 = in0; //tn
  i1 = in1; //bm-1
  i2 = in2; //bm
  i3 = in3; //bm+1
  i4 = in4; //an-1
  i5 = in5; //an
}

bool Lut::getAmO6( void ) {
  Be Be0;
  bool s, c, z, m, s1, c1, z1, xor1;
  Be0.setBeInput(i1, i2, i3);
  s = Be0.getS();
  c = Be0.getC();
  z = Be0.getZ();
  m = Be0.getM();
  s1 = (i5 & !s) | (i4 & s);
  c1 = (s1 & !c) | (!s1 & c);
  z1 = (c1 & !z) | (0 & z);
  xor1 = z1 ^ i0;
  o6 = m ^ xor1;
  return o6;
  }

bool Lut::getAO6( void ) {
  Be Be0;
  bool s, c, z, s1, c1, z1;
  Be0.setBeInput(i1, i2, i3);
  s = Be0.getS();
  c = Be0.getC();
  z = Be0.getZ();
  s1 = (i5 & !s) | (i4 & s);
  c1 = (s1 & !c) | (!s1 & c);
  z1 = (c1 & !z) | (0 & z);
  o6 = z1 ^ i0;
  return o6;
}

bool Lut::getA5O5( void ) {
  o5 = !i0;
  return o5;
}

bool Lut::getA5O6( void ) {
  Be Be0;
  bool c, z, c1, z1;
  Be0.setBeInput(i1, i2, i3);
  c = Be0.getC();
  z = Be0.getZ();
  c1 = (i5 & !c) | (!i5 & c);
  z1 = (c1 & !z) | (0 & z);
  o6 = !i0 ^ z1;
  return o6;
}

bool Lut::getA6O5( void ) {
  o5 = i0;
  return o5;
}

bool Lut::getA6O6( void ) {
  Be Be0;
  bool c, z, c1, z1;
  Be0.setBeInput(i1, i2, i3);
  c = Be0.getC();
  z = Be0.getZ();
  c1 = (i5 & !c) | (!i5 & c);
  z1 = (c1 & !z) | (0 & z);
  o6 = i0 ^ z1;
  return o6;
}

bool Lut::getBO6( void ) {
 Ee Ee0;
 bool ee;
 Ee0.setEeInput(i1, i2, i3, i5);
 ee = Ee0.getEeOutput();
 o6 = i0 ^ ee;
 return o6;
}

bool Lut::getCarryGenO6( void ) {
  Be Be0;
  bool s, c, z, s1, c1, z1, c2, z2, xor1, cint;
  Be0.setBeInput(i1, i2, i3);
  s = Be0.getS();
  c = Be0.getC();
  z = Be0.getZ();
  s1 = (i5 & !s) | (0 & s);
  c1 = (s1 & !c) | (!s1 & c);
  z1 = (c1 & !z) | (0 & z);
  //c2 = (1 & !c) | (0 & c);
  c2 = (0 & !c) | (1 & c);
  z2 = (c2 & !z) | (0 & z);
  xor1 = i0 ^ z1;
  o6 = (i0 & !xor1) | (z2 & xor1);
  return o6;
}
