//booth encoder
class Be {
public:
  bool bmneg, bm, bmpos;
  bool s, c, z, m;

  void setBeInput( bool bmnegin, bool bmin, bool bmposin );
  bool getS( void );
  bool getC( void );
  bool getZ( void );
  bool getM( void );
  void printBeInput( void );
};

//extention encoder
class Ee {
public:
  bool bmneg, bm, bmpos, sign;
  bool ee;

  void setEeInput( bool bmnegin, bool bmin, bool bmposin, bool signin );
  bool getEeOutput ( void );
};

//carry chain element (single!!!)
class Carry {
public:
  bool inc, ino5, ino6;
  bool xout, cout;

  void setCarryInput( bool incin, bool ino5in, bool ino6in );
  bool getCarryCOut( void );
  bool getCarryXOut( void );
};

//Lut
class Lut {
public:
  bool i0, i1, i2, i3, i4, i5; //inputs
  bool o5, o6; //output

  void setLutInput( bool in0, bool in1, bool in2, bool in3, bool in4, bool in5 );
  bool getAmO6( void );
  bool getAO6( void );
  bool getA5O5( void );
  bool getA5O6( void );
  bool getA6O5( void );
  bool getA6O6( void );
  bool getBO6( void );
  bool getCarryGenO6( void );
};
