#include <iostream>
#include "math.h"
#include <cmath>
#include <fstream>
#include "behavioral.hpp"
using namespace std;

int main() {
  //***insert size of multiplier here***
  int N = 8; //width
  int M = 8; //heigth
  bool a[N]; //width
  bool b[M]; //height
  bool p[N+M]; //product
  bool acc_p[N+M]; //accurate product
  Lut Lut0[M/2][N+2];
  Carry Carry0[M/2][N+2];
  signed int aa, bb, saveaa, savebb, saveacc, error;
  signed int acc = 0;
  signed int pp = 0;

  //uncomment this for console input from here -->
  std::cout << "Input a: " << endl;
  std::cin >> saveaa;
  std::cout << "Input b: " << endl;
  std::cin >> savebb;
  //<-- till here

  //uncomment this for reading from 2 .txt files and write into a .csv from here -->
  //std::ofstream ofile;
  //ofile.open("mul_results.csv", ios::out | ios::trunc);

  //ofile << "a, b, bin_a, bin_b, accu, appr, bin_accu, bin_appr, error, rel_err" << endl;

  //ofile.close();

  //std::ifstream infilea("tb_in_a0.txt");
  //while (infilea >> saveaa){
  //std::ifstream infileb("tb_in_b0.txt");
  //while (infileb >> savebb){
  //<-- till here

  //convert int to binary array

  //saveaa = aa;
  //savebb = bb;
  aa = saveaa;
  bb = savebb;
  acc = aa*bb;
  saveacc = acc;

  for (int i=0; i < N; i++){
    a[i] = aa & 0x1;
    aa = aa >> 1;
  }

  for (int i=0; i < M; i++){
    b[i] = bb & 0x1;
    bb = bb >> 1;
  }

  for (int i=0; i < N+M; i++){
    acc_p[i] = acc & 0x1;
    acc = acc >> 1;
  }

//multiplier functionality

  for (int i = 0; i < M/2; i++){
    for (int j = 1; j < N+2; j++){
      if (i == 0){
        if (j == 1) {
          Lut0[i][j].setLutInput(0, 0, b[2*i], b[2*i+1], a[j-1], a[j]);
          p[2*i] = 0;
          p[2*i+j] = Lut0[i][j].getAmO6();
        }
        if (j == 2){
          Lut0[i][j].setLutInput(0, 0, b[2*i], b[2*i+1], a[j-1], a[j]);
          Carry0[i][j].setCarryInput(1, 0, Lut0[i][j].getAO6());
        }
        if ((j > 2) && (j < N)){
          Lut0[i][j].setLutInput(0, 0, b[2*i], b[2*i+1], a[j-1], a[j]);
          Carry0[i][j].setCarryInput(Carry0[i][j-1].getCarryCOut(), 0, Lut0[i][j].getAO6());
        }
        if (j == N){
          Lut0[i][j].setLutInput(0, 0, b[2*i], b[2*i+1], 0, a[j-1]);
          Carry0[i][j].setCarryInput(Carry0[i][j-1].getCarryCOut(), Lut0[i][j].getA6O5(), Lut0[i][j].getA6O6());
        }
        if (j == N+1){
          Lut0[i][j].setLutInput(1, 0, b[2*i], b[2*i+1], 0, a[j-2]);
          Carry0[i][j].setCarryInput(Carry0[i][j-1].getCarryCOut(), 1, Lut0[i][j].getBO6());
        }
      }
      if ((i > 0) && (i < M/2-1)){
        if (j == 1){
          Lut0[i][j].setLutInput(Carry0[i-1][j+2].getCarryXOut(), b[2*i-1], b[2*i], b[2*i+1], a[j-1], a[j]);
          p[2*i] = 0;
          p[2*i+j] = Lut0[i][j].getAmO6();
        }
        if (j == 2){
          Lut0[i][j].setLutInput(Carry0[i-1][j+2].getCarryXOut(), b[2*i-1], b[2*i], b[2*i+1], a[j-1], a[j]);
          Carry0[i][j].setCarryInput(1, Carry0[i-1][j+2].getCarryXOut(), Lut0[i][j].getAO6());
        }
        if ((j > 2) && (j < N)){
          Lut0[i][j].setLutInput(Carry0[i-1][j+2].getCarryXOut(), b[2*i-1], b[2*i], b[2*i+1], a[j-1], a[j]);
          Carry0[i][j].setCarryInput(Carry0[i][j-1].getCarryCOut(), Carry0[i-1][j+2].getCarryXOut(), Lut0[i][j].getAO6());
        }
        if (j == N){
          Lut0[i][j].setLutInput(Carry0[i-1][j+1].getCarryCOut(), b[2*i-1], b[2*i], b[2*i+1], 0, a[j-1]);
          Carry0[i][j].setCarryInput(Carry0[i][j-1].getCarryCOut(), Lut0[i][j].getA5O5(), Lut0[i][j].getA5O6());
        }
        if (j == N+1){
          Lut0[i][j].setLutInput(Carry0[i-1][j].getCarryCOut(), b[2*i-1], b[2*i], b[2*i+1], 0, a[j-2]);
          Carry0[i][j].setCarryInput(Carry0[i][j-1].getCarryCOut(), Carry0[i-1][j].getCarryCOut(), Lut0[i][j].getBO6());
        }
      }
      if (i == M/2-1){
        if (j == 1){
          Lut0[i][j].setLutInput(Carry0[i-1][j+2].getCarryXOut(), b[2*i-1], b[2*i], b[2*i+1], a[j-1], a[j]);
          p[2*i] = 0;
          p[2*i+j]= Lut0[i][j].getAmO6();
        }
        if (j == 2){
          Lut0[i][0].setLutInput(Carry0[i-1][2].getCarryXOut(), b[2*i-1], b[2*i], b[2*i+1], 0, a[0]);
          Lut0[i][j].setLutInput(Carry0[i-1][j+2].getCarryXOut(), b[2*i-1], b[2*i], b[2*i+1], a[j-1], a[j]);
          Carry0[i][j].setCarryInput(Lut0[i][0].getCarryGenO6(), Carry0[i-1][j+2].getCarryXOut(), Lut0[i][j].getAO6());
          p[2*i+j] = Carry0[i][j].getCarryXOut();
        }
        if ((j > 2) && (j < N)){
          Lut0[i][j].setLutInput(Carry0[i-1][j+2].getCarryXOut(), b[2*i-1], b[2*i], b[2*i+1], a[j-1], a[j]);
          Carry0[i][j].setCarryInput(Carry0[i][j-1].getCarryCOut(), Carry0[i-1][j+2].getCarryXOut(), Lut0[i][j].getAO6());
          p[2*i+j] = Carry0[i][j].getCarryXOut();
        }
        if (j == N){
          Lut0[i][j].setLutInput(Carry0[i-1][j+1].getCarryCOut(), b[2*i-1], b[2*i], b[2*i+1], 0, a[j-1]);
          Carry0[i][j].setCarryInput(Carry0[i][j-1].getCarryCOut(), Lut0[i][j].getA5O5(), Lut0[i][j].getA5O6());
          p[2*i+j] = Carry0[i][j].getCarryXOut();
        }
        if (j == N+1){
          Lut0[i][j].setLutInput(Carry0[i-1][j].getCarryCOut(), b[2*i-1], b[2*i], b[2*i+1], 0, a[j-2]);
          Carry0[i][j].setCarryInput(Carry0[i][j-1].getCarryCOut(), Carry0[i-1][j].getCarryCOut(), Lut0[i][j].getBO6());
          p[2*i+j] = Carry0[i][j].getCarryXOut();
        }
      }
    }
  }


pp = 0;
for (int i = 0; i < N+M; i++){
  pp += p[i]*(pow(2,i));
}

if (p[N+M-1]){
  int tmp;
  tmp = pow(2, N+M);
  tmp = tmp - 1;
  pp = ~pp & tmp;
  pp = (pp + 1) * -1;
}

//regular console output, uncomment from here -->
std::cout << "Result: " << endl;
std::cout << pp << endl;
//<-- till here

//give .csv conform output, uncomment from here -->

//ofile.open("mul_results.csv", ios::out | ios::app);

//ofile << saveaa << ", " << savebb << ", ";
//for (int i = N-1; i >= 0; i--){
//  ofile << a[i];
//}
//ofile << ", ";
//for (int i = M-1; i >= 0; i--){
//  ofile << b[i];
//}
//ofile << ", ";
//ofile << saveacc << ", " << pp << ", ";
//for (int i = N+M-1; i >= 0; i--){
//  ofile << acc_p[i];
//}
//ofile << ", ";
//for (int i = N+M-1; i >= 0; i--){
//  ofile << p[i];
//}
//ofile << "," << endl;
//ofile.close();
//}
//}
//<-- till here (sorry for the bracket layout of the output :D)
return 0;
}
