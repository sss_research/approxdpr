#!/usr/bin/env python3.5
import numpy as np
import re as re
filesDIR = "./Decimal_Simulation_EvoLib/"
numDesigns = 500
val2DArray = np.zeros(shape=(65536,numDesigns))
for dsgnID in range(0,numDesigns):
    fileName = filesDIR + "mult_8x8_"+str(dsgnID).zfill(3)+"_evolib_decimal.txt"
    #print(fileName)
    valsfromFile = np.loadtxt(fileName,dtype=int,delimiter=' ',usecols=0)
    val2DArray[:,dsgnID] = valsfromFile

np.savetxt("LuT_8x8_Multiplier.csv",\
                (val2DArray), \
                delimiter=',',\
                fmt="%d",\
                comments=""\
                )
