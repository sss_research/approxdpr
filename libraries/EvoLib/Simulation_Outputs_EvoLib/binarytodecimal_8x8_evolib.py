from argparse import ArgumentParser
import sys

#Parse command line to determine name of outputfile, type of filter, number of stages,
#And name of the text file from which coefficients for the multipliers are inputted

parser = ArgumentParser()
parser.add_argument("-o", "--outputfile", help="Name of .txt file (to create)")
parser.add_argument("-i", "--inputfile", help="Name of .txt file with 32bit input")


args = parser.parse_args()
#outfile = args.outputfile
#infile = args.inputfile
for i1 in range(0,5):
	for i2 in range (0,10):
		for i3 in range(0,10):
			s=str(i1)+str(i2)+str(i3)
			infile = "C:\\Users\\sanjeev\\Desktop\\Simulation_Outputs_EvoLib\\mult_8x8_" + s + "_evolib.txt"
			outfile = "C:\\Users\\sanjeev\\Desktop\\Decimal_Simulation_EvoLib\\mult_8x8_" + s + "_evolib_decimal.txt"
						#Convert 32 bit .txt output file to .txt decimal file
			with open(outfile, 'w+') as outie:
			    with open(infile, 'r') as innie:
			        for line in innie:
			            bintemp = int(line, 2)
			            print(bintemp, file=outie)


