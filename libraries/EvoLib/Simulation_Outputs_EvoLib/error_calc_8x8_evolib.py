#Extract the Relative and maximum error magnitude for a given filter with respect to it's accurate version

#Function takes in command line input (.txt files, accurate and approximate) and determines the error for each input, the maximum error magnitude and the relative error magnitude

#Import Command line parser and sys for printing to the output file
from argparse import ArgumentParser
import sys

#Argument Parser
#parser = ArgumentParser()

#parser.add_argument("-i1", "--inputaccuratefile", help = "Name of the .txt file with outputs for accurate version of the filter")
#parser.add_argument("-i2", "--inputapproximatefile", help = "Name of the .txt file with outputs for the approximate version of the filter")
#parser.add_argument("-o", "--outputfile", help = "Name of the .txt file to be created with the respective errors")

#args = parser.parse_args()
#inputacc = args.inputaccuratefile
#inputapp = args.inputapproximatefile
#my_outfile = args.outputfile

matrix = ["Acc","o","d","a"]
with open("main_script.tcl","w") as fh1 :
	for i1 in range(0,5):
		for i2 in range (0,10):
			for i3 in range(0,10):
				s=str(i1)+str(i2)+str(i3)
				inputacc = "C:\\Users\\sanjeev\\Desktop\\8x8_accurate_decimal.txt"
				inputapp = "C:\\Users\\sanjeev\\Desktop\\Decimal_Simulation_EvoLib\\mult_8x8_" + s + "_evolib_decimal.txt"
				my_outfile = "C:\\Users\\sanjeev\\Desktop\\Error_Reports_EvoLib\\error_mult_8x8_" + s + "_evolib.txt"

#Values to be printedreturned
				error = []
				rel_err = []
				max_error = 0
				avg_error = 0
				avg_rel_err = 0
				error_count = 0

					#Extract information from the file
				input1 = open(inputacc,'r')
				input2 = open(inputapp,'r')

				temp_var = 0
				temp_var2 = 0
				temp_var3 = 0
				count = 0

					#List which store the values from the .txt file
				value_acc = []
				value_app = []
				val_list = []

					#Storing the values of the file in in the lists
				while temp_var==0:

					temp1 = input1.readline()
					temp2 = input2.readline()

					if (temp1 != '' and temp2 != ''):
						value_acc.append(int(temp1[:-1]))
						value_app.append(int(temp2[:-1]))
					else:
						temp_var = 1

					#Calculating the error and relative error values
				for value in range(0,len(value_acc)):
					if int(abs(value_acc[value]-value_app[value])) != 0:
						val_list.append(value+1)
					error.append(int(abs(value_acc[value]-value_app[value])))
					if (int(abs(value_acc[value]-value_app[value])) != 0):
						error_count += 1
					if (value_acc[value]!=0):
						rel_err.append(float(abs((value_acc[value]-value_app[value])/value_acc[value])))
						temp_var2 += float(abs((value_acc[value]-value_app[value])/value_acc[value]))
					temp_var3 += int(abs(value_acc[value]-value_app[value]))

				max_error = max(error)

				for val in error:
					if val == max_error:
						count += 1
				avg_error = temp_var3/len(value_acc)
				avg_rel_err = temp_var2/len(value_acc)

					#Writing the values to the file
				with open(my_outfile,'w+') as outfile:

					sys.stdout = outfile

					print ('Max_Error_Magnitude : ' + str (max_error))

					print ('Avg_Error : ' + str (avg_error))

					print ('Avg_Rel_Error : ' + str (avg_rel_err))

					print('Error Occurences : ' + str(error_count))

					print('Max. Error Occurences : ' + str(count))

					for vals in val_list:
						print (vals)