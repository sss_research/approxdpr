#!/usr/bin/env python3.5
import numpy as np
import re as re

numDesignPoints = 500
totalPower_re           = re.compile(r'^Total\ Power')      #Total Power : 0.215
dynamicPower_re         = re.compile(r'^Dynamic\ Power')    #Dynamic Power : 0.036
staticPower_re          = re.compile(r'^Static\ Power')     #Static Power : 0.179
delay_re                = re.compile(r'^Delay')             #Delay : 8.309
pdp_re                  = re.compile(r'^PDP')               #PDP : 1.7864349999999998
edp_re                  = re.compile(r'^EDP')               #EDP : 14.843488414999996
usageLUTs_re            = re.compile(r'^LUTs')              #LUTs :  82
usageSlices_re          = re.compile(r'^Slices')             #Slices : 24
maxErrMag_re            = re.compile(r'^Max\ Error\ Magnitude')             #Max Error Magnitude : 820
avgErr_re               = re.compile(r'^Avg\ Error')                        #Avg Error : 98.527099609375
avgRelErr_re            = re.compile(r'^Avg\ Relative\ Error')              #Avg Relative Error : 0.01987917744147997
errOccur_re             = re.compile(r'^Error\ Occurences')                 #Error Occurences : 56682
maxErrOccur_re          = re.compile(r'^Max\ Error\ Occurences')            #Max Error Occurences : 1

designID_array             = np.arange(0,numDesignPoints,1)
totalPower_array           = np.zeros(numDesignPoints)
dynamicPower_array         = np.zeros(numDesignPoints)
staticPower_array          = np.zeros(numDesignPoints)
delay_array                = np.zeros(numDesignPoints)
pdp_array                  = np.zeros(numDesignPoints)
edp_array                  = np.zeros(numDesignPoints)
usageLUTs_array            = np.zeros(numDesignPoints)
usageSlices_array          = np.zeros(numDesignPoints)
maxErrMag_array            = np.zeros(numDesignPoints)
avgErr_array               = np.zeros(numDesignPoints)
avgRelErr_array            = np.zeros(numDesignPoints)
errOccur_array             = np.zeros(numDesignPoints)
maxErrOccur_array          = np.zeros(numDesignPoints)
with open('design_space_points_evolib.txt') as infile:
    totalPower_cntr           = 0
    dynamicPower_cntr         = 0
    staticPower_cntr          = 0
    delay_cntr                = 0
    pdp_cntr                  = 0
    edp_cntr                  = 0
    usageLUTs_cntr            = 0
    usageSlices_cntr          = 0
    maxErrMag_cntr            = 0
    avgErr_cntr               = 0
    avgRelErr_cntr            = 0
    errOccur_cntr             = 0
    maxErrOccur_cntr          = 0
    for line in infile:
        lineNoCR = line.strip('\n')
        val = lineNoCR.split(':')[-1]
        if totalPower_re.match(line):
            totalPower_array[totalPower_cntr] = val
            totalPower_cntr = totalPower_cntr + 1
            continue
        if dynamicPower_re.match(line):
            dynamicPower_array[dynamicPower_cntr] = val
            dynamicPower_cntr = dynamicPower_cntr + 1
            continue
        if staticPower_re.match(line):
            staticPower_array[staticPower_cntr] = val
            staticPower_cntr = staticPower_cntr + 1
            continue
        if delay_re.match(line):
            delay_array[delay_cntr] = val
            delay_cntr = delay_cntr + 1
            continue
        if pdp_re.match(line):
            pdp_array[pdp_cntr] = val
            pdp_cntr = pdp_cntr + 1
            continue
        if edp_re.match(line):
            edp_array[edp_cntr] = val
            edp_cntr = edp_cntr + 1
            continue
        if usageLUTs_re.match(line):
            usageLUTs_array[usageLUTs_cntr] = val
            usageLUTs_cntr = usageLUTs_cntr + 1
            continue
        if usageSlices_re.match(line):
            usageSlices_array[usageSlices_cntr] = val
            usageSlices_cntr = usageSlices_cntr + 1
            continue
        if maxErrMag_re.match(line):
            maxErrMag_array[maxErrMag_cntr] = val
            maxErrMag_cntr = maxErrMag_cntr + 1
            continue
        if avgErr_re.match(line):
            avgErr_array[avgErr_cntr] = val
            avgErr_cntr = avgErr_cntr + 1
            continue
        if avgRelErr_re.match(line):
            avgRelErr_array[avgRelErr_cntr] = val
            avgRelErr_cntr = avgRelErr_cntr + 1
            continue
        if errOccur_re.match(line):
            errOccur_array[errOccur_cntr] = val
            errOccur_cntr = errOccur_cntr + 1
            continue
        if maxErrOccur_re.match(line):
            maxErrOccur_array[maxErrOccur_cntr] = val
            maxErrOccur_cntr = maxErrOccur_cntr + 1
            continue
##Write Data as Table in csv file
allDataVals = np.array([\
                    designID_array,\
                    totalPower_array,dynamicPower_array,staticPower_array,\
                    delay_array,pdp_array,edp_array,\
                    usageLUTs_array,usageSlices_array,\
                    maxErrMag_array,avgErr_array,avgRelErr_array,\
                    errOccur_array,maxErrOccur_array\
                    ])
allDataHeader = "dsgnID,\
                totPower,dynPower,statPower,\
                delay,pdp,edp,\
                useLUTs,useSlices,\
                maxErrmag,avgErr,avgRelErr,\
                errOccur,maxErrOccur\
                "
allDataFormat = "\
                 %d,\
                 %f,%f,%f,\
                 %f,%f,%f,\
                 %d,%d,\
                 %f,%f,%f,\
                 %d,%d\
                 "
#print(AllData)
np.savetxt("DesignPointsMetrics.csv",\
                np.transpose(allDataVals), \
                delimiter=',',\
                header=allDataHeader, \
                fmt=allDataFormat,\
                comments=""\
                )
