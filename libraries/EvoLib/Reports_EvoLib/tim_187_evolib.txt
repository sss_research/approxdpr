Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 22:52:48 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_187_evolib.txt -name O
| Design       : mul8_187
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.855ns  (logic 2.989ns (33.760%)  route 5.865ns (66.240%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=1 LUT5=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.395     1.982    A_IBUF[5]
    SLICE_X1Y64                                                       r  O_OBUF[9]_inst_i_13/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.043     2.025 f  O_OBUF[9]_inst_i_13/O
                         net (fo=4, routed)           0.377     2.402    O_OBUF[9]_inst_i_13_n_0
    SLICE_X3Y63                                                       f  O_OBUF[15]_inst_i_10/I5
    SLICE_X3Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.445 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.417     2.862    N_915
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X2Y62          LUT5 (Prop_lut5_I4_O)        0.052     2.914 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.356     3.270    N_948
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.132     3.402 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.434     3.836    N_1198
    SLICE_X4Y62                                                       r  O_OBUF[15]_inst_i_9/I4
    SLICE_X4Y62          LUT6 (Prop_lut6_I4_O)        0.127     3.963 r  O_OBUF[15]_inst_i_9/O
                         net (fo=2, routed)           0.427     4.390    N_1465
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_6/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     4.433 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.346     4.779    N_1482
    SLICE_X2Y62                                                       r  O_OBUF[13]_inst_i_4/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.043     4.822 r  O_OBUF[13]_inst_i_4/O
                         net (fo=2, routed)           0.429     5.251    N_1748
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_4/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     5.294 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.359     5.653    N_1999
    SLICE_X2Y63                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.043     5.696 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.326     7.022    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.855 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.855    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.754ns  (logic 2.992ns (34.181%)  route 5.762ns (65.819%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=1 LUT5=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.395     1.982    A_IBUF[5]
    SLICE_X1Y64                                                       r  O_OBUF[9]_inst_i_13/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.043     2.025 f  O_OBUF[9]_inst_i_13/O
                         net (fo=4, routed)           0.377     2.402    O_OBUF[9]_inst_i_13_n_0
    SLICE_X3Y63                                                       f  O_OBUF[15]_inst_i_10/I5
    SLICE_X3Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.445 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.417     2.862    N_915
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X2Y62          LUT5 (Prop_lut5_I4_O)        0.052     2.914 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.356     3.270    N_948
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.132     3.402 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.434     3.836    N_1198
    SLICE_X4Y62                                                       r  O_OBUF[15]_inst_i_9/I4
    SLICE_X4Y62          LUT6 (Prop_lut6_I4_O)        0.127     3.963 r  O_OBUF[15]_inst_i_9/O
                         net (fo=2, routed)           0.427     4.390    N_1465
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_6/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     4.433 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.346     4.779    N_1482
    SLICE_X2Y62                                                       r  O_OBUF[13]_inst_i_4/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.043     4.822 r  O_OBUF[13]_inst_i_4/O
                         net (fo=2, routed)           0.429     5.251    N_1748
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_4/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     5.294 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.159     5.453    N_1999
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_1/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     5.496 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.422     6.918    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.754 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.754    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.659ns  (logic 2.965ns (34.239%)  route 5.694ns (65.761%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.395     1.982    A_IBUF[5]
    SLICE_X1Y64                                                       f  O_OBUF[9]_inst_i_13/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.043     2.025 r  O_OBUF[9]_inst_i_13/O
                         net (fo=4, routed)           0.297     2.322    O_OBUF[9]_inst_i_13_n_0
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_16/I4
    SLICE_X1Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.365 r  O_OBUF[9]_inst_i_16/O
                         net (fo=2, routed)           0.343     2.708    N_932
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_10/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.751 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.440     3.191    N_1182
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_6/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.043     3.234 r  O_OBUF[11]_inst_i_6/O
                         net (fo=4, routed)           0.505     3.739    N_1433
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_10/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.043     3.782 r  O_OBUF[11]_inst_i_10/O
                         net (fo=2, routed)           0.409     4.191    N_1464
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_4/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.051     4.242 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.515     4.757    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_2/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.129     4.886 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.520     5.406    N_1965
    SLICE_X1Y64                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X1Y64          LUT4 (Prop_lut4_I0_O)        0.053     5.459 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.271     6.730    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.929     8.659 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.659    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.503ns  (logic 2.867ns (33.719%)  route 5.636ns (66.281%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.395     1.982    A_IBUF[5]
    SLICE_X1Y64                                                       f  O_OBUF[9]_inst_i_13/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.043     2.025 r  O_OBUF[9]_inst_i_13/O
                         net (fo=4, routed)           0.297     2.322    O_OBUF[9]_inst_i_13_n_0
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_16/I4
    SLICE_X1Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.365 r  O_OBUF[9]_inst_i_16/O
                         net (fo=2, routed)           0.343     2.708    N_932
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_10/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.751 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.440     3.191    N_1182
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_6/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.043     3.234 r  O_OBUF[11]_inst_i_6/O
                         net (fo=4, routed)           0.505     3.739    N_1433
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_10/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.043     3.782 r  O_OBUF[11]_inst_i_10/O
                         net (fo=2, routed)           0.409     4.191    N_1464
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_4/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.051     4.242 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.515     4.757    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_2/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.129     4.886 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.416     5.302    N_1965
    SLICE_X2Y64                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     5.345 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.316     6.662    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.503 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.503    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.662ns  (logic 2.813ns (36.712%)  route 4.849ns (63.288%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.395     1.982    A_IBUF[5]
    SLICE_X1Y64                                                       f  O_OBUF[9]_inst_i_13/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.043     2.025 r  O_OBUF[9]_inst_i_13/O
                         net (fo=4, routed)           0.297     2.322    O_OBUF[9]_inst_i_13_n_0
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_16/I4
    SLICE_X1Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.365 r  O_OBUF[9]_inst_i_16/O
                         net (fo=2, routed)           0.343     2.708    N_932
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_10/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.751 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.440     3.191    N_1182
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_6/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.043     3.234 r  O_OBUF[11]_inst_i_6/O
                         net (fo=4, routed)           0.505     3.739    N_1433
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_10/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.043     3.782 r  O_OBUF[11]_inst_i_10/O
                         net (fo=2, routed)           0.409     4.191    N_1464
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_4/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.051     4.242 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.176     4.418    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.129     4.547 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.285     5.832    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.662 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.662    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.534ns  (logic 2.859ns (37.950%)  route 4.675ns (62.050%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.395     1.982    A_IBUF[5]
    SLICE_X1Y64                                                       r  O_OBUF[9]_inst_i_13/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.043     2.025 f  O_OBUF[9]_inst_i_13/O
                         net (fo=4, routed)           0.377     2.402    O_OBUF[9]_inst_i_13_n_0
    SLICE_X3Y63                                                       f  O_OBUF[15]_inst_i_10/I5
    SLICE_X3Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.445 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.417     2.862    N_915
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X2Y62          LUT5 (Prop_lut5_I4_O)        0.052     2.914 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.356     3.270    N_948
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.132     3.402 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.521     3.923    N_1198
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_3/I2
    SLICE_X3Y63          LUT6 (Prop_lut6_I2_O)        0.127     4.050 r  O_OBUF[11]_inst_i_3/O
                         net (fo=3, routed)           0.393     4.442    N_1698
    SLICE_X0Y65                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y65          LUT4 (Prop_lut4_I1_O)        0.043     4.485 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.217     5.702    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.534 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.534    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.631ns  (logic 2.621ns (39.534%)  route 4.009ns (60.466%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.395     1.982    A_IBUF[5]
    SLICE_X1Y64                                                       f  O_OBUF[9]_inst_i_13/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.043     2.025 r  O_OBUF[9]_inst_i_13/O
                         net (fo=4, routed)           0.297     2.322    O_OBUF[9]_inst_i_13_n_0
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_16/I4
    SLICE_X1Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.365 r  O_OBUF[9]_inst_i_16/O
                         net (fo=2, routed)           0.343     2.708    N_932
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_10/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.751 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.405     3.156    N_1182
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_3/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.199 r  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.303     3.501    N_1682
    SLICE_X0Y64                                                       r  O_OBUF[9]_inst_i_1/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     3.544 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.268     4.812    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.631 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.631    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.088ns  (logic 2.576ns (42.309%)  route 3.512ns (57.691%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.395     1.982    A_IBUF[5]
    SLICE_X1Y64                                                       f  O_OBUF[9]_inst_i_13/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.043     2.025 r  O_OBUF[9]_inst_i_13/O
                         net (fo=4, routed)           0.175     2.201    O_OBUF[9]_inst_i_13_n_0
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.244 r  O_OBUF[9]_inst_i_6/O
                         net (fo=4, routed)           0.337     2.581    N_914
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_2/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.624 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.345     2.969    N_1414
    SLICE_X0Y64                                                       r  O_OBUF[8]_inst_i_1/I2
    SLICE_X0Y64          LUT5 (Prop_lut5_I2_O)        0.043     3.012 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.260     4.272    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.088 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.088    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.011ns  (logic 2.482ns (49.532%)  route 2.529ns (50.468%))
  Logic Levels:           4  (IBUF=1 LUT4=1 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              f  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 f  A_IBUF[3]_inst/O
                         net (fo=13, routed)          1.059     1.653    A_IBUF[3]
    SLICE_X1Y64                                                       f  O_OBUF[0]_inst_i_1/I2
    SLICE_X1Y64          LUT4 (Prop_lut4_I2_O)        0.043     1.696 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.348     2.044    O_OBUF[0]
    SLICE_X1Y64                                                       r  O_OBUF[5]_inst_i_1/I4
    SLICE_X1Y64          LUT5 (Prop_lut5_I4_O)        0.043     2.087 r  O_OBUF[5]_inst_i_1/O
                         net (fo=1, routed)           1.122     3.209    O_OBUF[5]
    AJ26                                                              r  O_OBUF[5]_inst/I
    AJ26                 OBUF (Prop_obuf_I_O)         1.802     5.011 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.011    O[5]
    AJ26                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[3]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.785ns  (logic 2.515ns (52.560%)  route 2.270ns (47.440%))
  Logic Levels:           3  (IBUF=1 LUT2=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.221     1.808    A_IBUF[6]
    SLICE_X1Y64                                                       r  O_OBUF[3]_inst_i_1/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.050     1.858 r  O_OBUF[3]_inst_i_1/O
                         net (fo=1, routed)           1.049     2.906    O_OBUF[3]
    AG25                                                              r  O_OBUF[3]_inst/I
    AG25                 OBUF (Prop_obuf_I_O)         1.878     4.785 r  O_OBUF[3]_inst/O
                         net (fo=0)                   0.000     4.785    O[3]
    AG25                                                              r  O[3] (OUT)
  -------------------------------------------------------------------    -------------------




