Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 01:50:45 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_248_evolib.txt -name O
| Design       : mul8_248
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.895ns  (logic 2.784ns (35.268%)  route 5.110ns (64.732%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=16, routed)          1.588     2.169    B_IBUF[0]
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_10/I4
    SLICE_X5Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.212 r  O_OBUF[6]_inst_i_10/O
                         net (fo=2, routed)           0.217     2.428    O_OBUF[6]_inst_i_10_n_0
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X5Y64          LUT3 (Prop_lut3_I2_O)        0.051     2.479 f  O_OBUF[6]_inst_i_3/O
                         net (fo=5, routed)           0.698     3.178    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y66                                                       f  O_OBUF[10]_inst_i_5/I0
    SLICE_X1Y66          LUT6 (Prop_lut6_I0_O)        0.129     3.307 f  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.501     3.808    O_OBUF[10]_inst_i_5_n_0
    SLICE_X2Y66                                                       f  O_OBUF[10]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.851 f  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.380     4.231    O_OBUF[10]_inst_i_3_n_0
    SLICE_X3Y67                                                       f  O_OBUF[15]_inst_i_6/I4
    SLICE_X3Y67          LUT6 (Prop_lut6_I4_O)        0.043     4.274 r  O_OBUF[15]_inst_i_6/O
                         net (fo=2, routed)           0.413     4.687    O_OBUF[15]_inst_i_6_n_0
    SLICE_X3Y66                                                       r  O_OBUF[15]_inst_i_1/I5
    SLICE_X3Y66          LUT6 (Prop_lut6_I5_O)        0.043     4.730 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.313     6.043    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.895 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.895    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.882ns  (logic 2.768ns (35.122%)  route 5.114ns (64.878%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=16, routed)          1.588     2.169    B_IBUF[0]
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_10/I4
    SLICE_X5Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.212 r  O_OBUF[6]_inst_i_10/O
                         net (fo=2, routed)           0.217     2.428    O_OBUF[6]_inst_i_10_n_0
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X5Y64          LUT3 (Prop_lut3_I2_O)        0.051     2.479 f  O_OBUF[6]_inst_i_3/O
                         net (fo=5, routed)           0.698     3.178    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y66                                                       f  O_OBUF[10]_inst_i_5/I0
    SLICE_X1Y66          LUT6 (Prop_lut6_I0_O)        0.129     3.307 f  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.501     3.808    O_OBUF[10]_inst_i_5_n_0
    SLICE_X2Y66                                                       f  O_OBUF[10]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.851 f  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.380     4.231    O_OBUF[10]_inst_i_3_n_0
    SLICE_X3Y67                                                       f  O_OBUF[15]_inst_i_6/I4
    SLICE_X3Y67          LUT6 (Prop_lut6_I4_O)        0.043     4.274 r  O_OBUF[15]_inst_i_6/O
                         net (fo=2, routed)           0.414     4.688    O_OBUF[15]_inst_i_6_n_0
    SLICE_X3Y66                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X3Y66          LUT6 (Prop_lut6_I1_O)        0.043     4.731 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.316     6.047    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.882 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.882    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.830ns  (logic 2.965ns (37.860%)  route 4.866ns (62.140%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.545     2.141    A_IBUF[2]
    SLICE_X3Y65                                                       r  O_OBUF[7]_inst_i_7/I4
    SLICE_X3Y65          LUT6 (Prop_lut6_I4_O)        0.043     2.184 r  O_OBUF[7]_inst_i_7/O
                         net (fo=6, routed)           0.349     2.533    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y65                                                       r  O_OBUF[6]_inst_i_13/I2
    SLICE_X2Y65          LUT3 (Prop_lut3_I2_O)        0.049     2.582 r  O_OBUF[6]_inst_i_13/O
                         net (fo=3, routed)           0.446     3.028    O_OBUF[6]_inst_i_13_n_0
    SLICE_X2Y65                                                       r  O_OBUF[11]_inst_i_12/I0
    SLICE_X2Y65          LUT3 (Prop_lut3_I0_O)        0.132     3.160 r  O_OBUF[11]_inst_i_12/O
                         net (fo=2, routed)           0.408     3.568    O_OBUF[11]_inst_i_12_n_0
    SLICE_X4Y66                                                       r  O_OBUF[10]_inst_i_2/I4
    SLICE_X4Y66          LUT6 (Prop_lut6_I4_O)        0.132     3.700 f  O_OBUF[10]_inst_i_2/O
                         net (fo=6, routed)           0.414     4.114    O_OBUF[10]_inst_i_2_n_0
    SLICE_X4Y67                                                       f  O_OBUF[13]_inst_i_6/I0
    SLICE_X4Y67          LUT4 (Prop_lut4_I0_O)        0.051     4.165 r  O_OBUF[13]_inst_i_6/O
                         net (fo=1, routed)           0.413     4.578    O_OBUF[13]_inst_i_6_n_0
    SLICE_X3Y67                                                       r  O_OBUF[13]_inst_i_1/I4
    SLICE_X3Y67          LUT6 (Prop_lut6_I4_O)        0.129     4.707 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.291     5.997    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.830 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.830    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.793ns  (logic 2.775ns (35.602%)  route 5.019ns (64.398%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=16, routed)          1.588     2.169    B_IBUF[0]
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_10/I4
    SLICE_X5Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.212 r  O_OBUF[6]_inst_i_10/O
                         net (fo=2, routed)           0.217     2.428    O_OBUF[6]_inst_i_10_n_0
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X5Y64          LUT3 (Prop_lut3_I2_O)        0.051     2.479 r  O_OBUF[6]_inst_i_3/O
                         net (fo=5, routed)           0.698     3.178    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y66                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X1Y66          LUT6 (Prop_lut6_I0_O)        0.129     3.307 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.501     3.808    O_OBUF[10]_inst_i_5_n_0
    SLICE_X2Y66                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.851 r  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.332     4.182    O_OBUF[10]_inst_i_3_n_0
    SLICE_X4Y66                                                       r  O_OBUF[12]_inst_i_5/I4
    SLICE_X4Y66          LUT5 (Prop_lut5_I4_O)        0.043     4.225 r  O_OBUF[12]_inst_i_5/O
                         net (fo=1, routed)           0.416     4.642    O_OBUF[12]_inst_i_5_n_0
    SLICE_X3Y66                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.043     4.685 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.267     5.952    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.793 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.793    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.661ns  (logic 2.764ns (36.081%)  route 4.897ns (63.919%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=16, routed)          1.588     2.169    B_IBUF[0]
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_10/I4
    SLICE_X5Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.212 r  O_OBUF[6]_inst_i_10/O
                         net (fo=2, routed)           0.217     2.428    O_OBUF[6]_inst_i_10_n_0
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X5Y64          LUT3 (Prop_lut3_I2_O)        0.051     2.479 r  O_OBUF[6]_inst_i_3/O
                         net (fo=5, routed)           0.698     3.178    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y66                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X1Y66          LUT6 (Prop_lut6_I0_O)        0.129     3.307 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.501     3.808    O_OBUF[10]_inst_i_5_n_0
    SLICE_X2Y66                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.851 r  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.185     4.035    O_OBUF[10]_inst_i_3_n_0
    SLICE_X4Y66                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X4Y66          LUT5 (Prop_lut5_I0_O)        0.043     4.078 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.505     4.583    O_OBUF[11]_inst_i_6_n_0
    SLICE_X4Y67                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X4Y67          LUT2 (Prop_lut2_I1_O)        0.043     4.626 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.204     5.830    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.661 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.661    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.416ns  (logic 2.774ns (37.405%)  route 4.642ns (62.595%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=16, routed)          1.588     2.169    B_IBUF[0]
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_10/I4
    SLICE_X5Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.212 r  O_OBUF[6]_inst_i_10/O
                         net (fo=2, routed)           0.217     2.428    O_OBUF[6]_inst_i_10_n_0
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X5Y64          LUT3 (Prop_lut3_I2_O)        0.051     2.479 r  O_OBUF[6]_inst_i_3/O
                         net (fo=5, routed)           0.698     3.178    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y66                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X1Y66          LUT6 (Prop_lut6_I0_O)        0.129     3.307 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.501     3.808    O_OBUF[10]_inst_i_5_n_0
    SLICE_X2Y66                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.851 r  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.185     4.035    O_OBUF[10]_inst_i_3_n_0
    SLICE_X4Y66                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X4Y66          LUT5 (Prop_lut5_I0_O)        0.043     4.078 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.184     4.262    O_OBUF[11]_inst_i_6_n_0
    SLICE_X3Y65                                                       r  O_OBUF[11]_inst_i_1/I5
    SLICE_X3Y65          LUT6 (Prop_lut6_I5_O)        0.043     4.305 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.270     5.575    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.416 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.416    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.314ns  (logic 2.804ns (38.342%)  route 4.510ns (61.658%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=16, routed)          1.588     2.169    B_IBUF[0]
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_10/I4
    SLICE_X5Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.212 r  O_OBUF[6]_inst_i_10/O
                         net (fo=2, routed)           0.217     2.428    O_OBUF[6]_inst_i_10_n_0
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X5Y64          LUT3 (Prop_lut3_I2_O)        0.051     2.479 r  O_OBUF[6]_inst_i_3/O
                         net (fo=5, routed)           0.698     3.178    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y66                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X1Y66          LUT6 (Prop_lut6_I0_O)        0.129     3.307 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.469     3.776    O_OBUF[10]_inst_i_5_n_0
    SLICE_X4Y67                                                       r  O_OBUF[7]_inst_i_3/I0
    SLICE_X4Y67          LUT6 (Prop_lut6_I0_O)        0.043     3.819 r  O_OBUF[7]_inst_i_3/O
                         net (fo=1, routed)           0.417     4.237    O_OBUF[7]_inst_i_3_n_0
    SLICE_X3Y67                                                       r  O_OBUF[7]_inst_i_1/I1
    SLICE_X3Y67          LUT2 (Prop_lut2_I1_O)        0.053     4.290 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.120     5.409    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.904     7.314 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     7.314    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.234ns  (logic 2.626ns (36.301%)  route 4.608ns (63.699%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=16, routed)          1.588     2.169    B_IBUF[0]
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_10/I4
    SLICE_X5Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.212 r  O_OBUF[6]_inst_i_10/O
                         net (fo=2, routed)           0.217     2.428    O_OBUF[6]_inst_i_10_n_0
    SLICE_X5Y64                                                       r  O_OBUF[7]_inst_i_5/I0
    SLICE_X5Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.471 r  O_OBUF[7]_inst_i_5/O
                         net (fo=7, routed)           0.685     3.156    O_OBUF[7]_inst_i_5_n_0
    SLICE_X5Y66                                                       r  O_OBUF[13]_inst_i_14/I4
    SLICE_X5Y66          LUT6 (Prop_lut6_I4_O)        0.043     3.199 f  O_OBUF[13]_inst_i_14/O
                         net (fo=4, routed)           0.442     3.642    O_OBUF[13]_inst_i_14_n_0
    SLICE_X5Y66                                                       f  O_OBUF[10]_inst_i_4/I0
    SLICE_X5Y66          LUT5 (Prop_lut5_I0_O)        0.043     3.685 r  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.411     4.095    O_OBUF[10]_inst_i_4_n_0
    SLICE_X3Y67                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X3Y67          LUT5 (Prop_lut5_I4_O)        0.043     4.138 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.266     5.404    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.234 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.234    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.171ns  (logic 2.801ns (39.052%)  route 4.371ns (60.948%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=16, routed)          1.588     2.169    B_IBUF[0]
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_10/I4
    SLICE_X5Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.212 r  O_OBUF[6]_inst_i_10/O
                         net (fo=2, routed)           0.217     2.428    O_OBUF[6]_inst_i_10_n_0
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X5Y64          LUT3 (Prop_lut3_I2_O)        0.051     2.479 r  O_OBUF[6]_inst_i_3/O
                         net (fo=5, routed)           0.698     3.178    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y66                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X1Y66          LUT6 (Prop_lut6_I0_O)        0.129     3.307 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.501     3.808    O_OBUF[10]_inst_i_5_n_0
    SLICE_X2Y66                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.851 r  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.220     4.070    O_OBUF[10]_inst_i_3_n_0
    SLICE_X3Y67                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X3Y67          LUT2 (Prop_lut2_I1_O)        0.049     4.119 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.147     5.267    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.905     7.171 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.171    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.807ns  (logic 2.712ns (39.850%)  route 4.094ns (60.150%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=16, routed)          1.588     2.169    B_IBUF[0]
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_10/I4
    SLICE_X5Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.212 r  O_OBUF[6]_inst_i_10/O
                         net (fo=2, routed)           0.217     2.428    O_OBUF[6]_inst_i_10_n_0
    SLICE_X5Y64                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X5Y64          LUT3 (Prop_lut3_I2_O)        0.051     2.479 r  O_OBUF[6]_inst_i_3/O
                         net (fo=5, routed)           0.698     3.178    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y66                                                       r  O_OBUF[5]_inst_i_2/I0
    SLICE_X1Y66          LUT6 (Prop_lut6_I0_O)        0.129     3.307 r  O_OBUF[5]_inst_i_2/O
                         net (fo=3, routed)           0.226     3.533    O_OBUF[5]_inst_i_2_n_0
    SLICE_X1Y66                                                       r  O_OBUF[6]_inst_i_7/I1
    SLICE_X1Y66          LUT3 (Prop_lut3_I1_O)        0.043     3.576 r  O_OBUF[6]_inst_i_7/O
                         net (fo=1, routed)           0.169     3.746    O_OBUF[6]_inst_i_7_n_0
    SLICE_X2Y66                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X2Y66          LUT6 (Prop_lut6_I5_O)        0.043     3.789 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.196     4.984    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.807 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.807    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




