Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 03:15:48 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_277_evolib.txt -name O
| Design       : mul8_277
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.150ns  (logic 2.883ns (35.372%)  route 5.267ns (64.628%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=21, routed)          1.640     2.229    A_IBUF[4]
    SLICE_X5Y63                                                       r  O_OBUF[13]_inst_i_16/I0
    SLICE_X5Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.280 r  O_OBUF[13]_inst_i_16/O
                         net (fo=2, routed)           0.425     2.705    O_OBUF[13]_inst_i_16_n_0
    SLICE_X5Y62                                                       r  O_OBUF[13]_inst_i_13/I1
    SLICE_X5Y62          LUT6 (Prop_lut6_I1_O)        0.129     2.834 r  O_OBUF[13]_inst_i_13/O
                         net (fo=4, routed)           0.423     3.257    O_OBUF[13]_inst_i_13_n_0
    SLICE_X6Y64                                                       r  O_OBUF[14]_inst_i_8/I0
    SLICE_X6Y64          LUT5 (Prop_lut5_I0_O)        0.049     3.306 r  O_OBUF[14]_inst_i_8/O
                         net (fo=3, routed)           0.514     3.820    O_OBUF[14]_inst_i_8_n_0
    SLICE_X3Y64                                                       r  O_OBUF[15]_inst_i_9/I1
    SLICE_X3Y64          LUT3 (Prop_lut3_I1_O)        0.127     3.947 r  O_OBUF[15]_inst_i_9/O
                         net (fo=2, routed)           0.428     4.375    O_OBUF[15]_inst_i_9_n_0
    SLICE_X4Y64                                                       r  O_OBUF[15]_inst_i_3/I1
    SLICE_X4Y64          LUT6 (Prop_lut6_I1_O)        0.043     4.418 r  O_OBUF[15]_inst_i_3/O
                         net (fo=1, routed)           0.428     4.846    O_OBUF[15]_inst_i_3_n_0
    SLICE_X3Y65                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X3Y65          LUT6 (Prop_lut6_I1_O)        0.043     4.889 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.410     6.299    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.150 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.150    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.081ns  (logic 2.869ns (35.505%)  route 5.212ns (64.495%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=21, routed)          1.640     2.229    A_IBUF[4]
    SLICE_X5Y63                                                       r  O_OBUF[13]_inst_i_16/I0
    SLICE_X5Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.280 r  O_OBUF[13]_inst_i_16/O
                         net (fo=2, routed)           0.413     2.694    O_OBUF[13]_inst_i_16_n_0
    SLICE_X5Y62                                                       r  O_OBUF[13]_inst_i_9/I0
    SLICE_X5Y62          LUT6 (Prop_lut6_I0_O)        0.129     2.823 r  O_OBUF[13]_inst_i_9/O
                         net (fo=4, routed)           0.409     3.232    O_OBUF[13]_inst_i_9_n_0
    SLICE_X5Y63                                                       r  O_OBUF[14]_inst_i_7/I1
    SLICE_X5Y63          LUT3 (Prop_lut3_I1_O)        0.043     3.275 r  O_OBUF[14]_inst_i_7/O
                         net (fo=5, routed)           0.404     3.678    O_OBUF[14]_inst_i_7_n_0
    SLICE_X3Y63                                                       r  O_OBUF[13]_inst_i_4/I0
    SLICE_X3Y63          LUT5 (Prop_lut5_I0_O)        0.043     3.721 r  O_OBUF[13]_inst_i_4/O
                         net (fo=3, routed)           0.409     4.130    O_OBUF[13]_inst_i_4_n_0
    SLICE_X4Y65                                                       r  O_OBUF[14]_inst_i_4/I2
    SLICE_X4Y65          LUT3 (Prop_lut3_I2_O)        0.049     4.179 r  O_OBUF[14]_inst_i_4/O
                         net (fo=2, routed)           0.615     4.794    O_OBUF[14]_inst_i_4_n_0
    SLICE_X2Y65                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X2Y65          LUT6 (Prop_lut6_I2_O)        0.129     4.923 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.322     6.245    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.081 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.081    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.817ns  (logic 2.774ns (35.490%)  route 5.043ns (64.510%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT3=1 LUT4=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=21, routed)          1.640     2.229    A_IBUF[4]
    SLICE_X5Y63                                                       r  O_OBUF[13]_inst_i_16/I0
    SLICE_X5Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.280 r  O_OBUF[13]_inst_i_16/O
                         net (fo=2, routed)           0.413     2.694    O_OBUF[13]_inst_i_16_n_0
    SLICE_X5Y62                                                       r  O_OBUF[13]_inst_i_9/I0
    SLICE_X5Y62          LUT6 (Prop_lut6_I0_O)        0.129     2.823 r  O_OBUF[13]_inst_i_9/O
                         net (fo=4, routed)           0.409     3.232    O_OBUF[13]_inst_i_9_n_0
    SLICE_X5Y63                                                       r  O_OBUF[14]_inst_i_7/I1
    SLICE_X5Y63          LUT3 (Prop_lut3_I1_O)        0.043     3.275 r  O_OBUF[14]_inst_i_7/O
                         net (fo=5, routed)           0.678     3.953    O_OBUF[14]_inst_i_7_n_0
    SLICE_X4Y65                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X4Y65          LUT2 (Prop_lut2_I0_O)        0.043     3.996 f  O_OBUF[11]_inst_i_3/O
                         net (fo=4, routed)           0.246     4.241    O_OBUF[11]_inst_i_3_n_0
    SLICE_X2Y66                                                       f  O_OBUF[15]_inst_i_7/I2
    SLICE_X2Y66          LUT4 (Prop_lut4_I2_O)        0.043     4.284 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.294     4.578    O_OBUF[15]_inst_i_7_n_0
    SLICE_X3Y65                                                       r  O_OBUF[13]_inst_i_1/I3
    SLICE_X3Y65          LUT6 (Prop_lut6_I3_O)        0.043     4.621 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.364     5.985    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.817 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.817    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.689ns  (logic 2.740ns (35.634%)  route 4.949ns (64.366%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=21, routed)          1.640     2.229    A_IBUF[4]
    SLICE_X5Y63                                                       r  O_OBUF[13]_inst_i_16/I0
    SLICE_X5Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.280 r  O_OBUF[13]_inst_i_16/O
                         net (fo=2, routed)           0.413     2.694    O_OBUF[13]_inst_i_16_n_0
    SLICE_X5Y62                                                       r  O_OBUF[13]_inst_i_9/I0
    SLICE_X5Y62          LUT6 (Prop_lut6_I0_O)        0.129     2.823 r  O_OBUF[13]_inst_i_9/O
                         net (fo=4, routed)           0.409     3.232    O_OBUF[13]_inst_i_9_n_0
    SLICE_X5Y63                                                       r  O_OBUF[14]_inst_i_7/I1
    SLICE_X5Y63          LUT3 (Prop_lut3_I1_O)        0.043     3.275 r  O_OBUF[14]_inst_i_7/O
                         net (fo=5, routed)           0.678     3.953    O_OBUF[14]_inst_i_7_n_0
    SLICE_X4Y65                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X4Y65          LUT2 (Prop_lut2_I0_O)        0.043     3.996 r  O_OBUF[11]_inst_i_3/O
                         net (fo=4, routed)           0.520     4.516    O_OBUF[11]_inst_i_3_n_0
    SLICE_X2Y66                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X2Y66          LUT6 (Prop_lut6_I2_O)        0.043     4.559 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.289     5.848    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.689 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.689    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.686ns  (logic 2.880ns (37.474%)  route 4.806ns (62.526%))
  Logic Levels:           8  (IBUF=1 LUT2=2 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=21, routed)          1.640     2.229    A_IBUF[4]
    SLICE_X5Y63                                                       r  O_OBUF[13]_inst_i_16/I0
    SLICE_X5Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.280 r  O_OBUF[13]_inst_i_16/O
                         net (fo=2, routed)           0.413     2.694    O_OBUF[13]_inst_i_16_n_0
    SLICE_X5Y62                                                       r  O_OBUF[13]_inst_i_9/I0
    SLICE_X5Y62          LUT6 (Prop_lut6_I0_O)        0.129     2.823 r  O_OBUF[13]_inst_i_9/O
                         net (fo=4, routed)           0.409     3.232    O_OBUF[13]_inst_i_9_n_0
    SLICE_X5Y63                                                       r  O_OBUF[14]_inst_i_7/I1
    SLICE_X5Y63          LUT3 (Prop_lut3_I1_O)        0.043     3.275 r  O_OBUF[14]_inst_i_7/O
                         net (fo=5, routed)           0.678     3.953    O_OBUF[14]_inst_i_7_n_0
    SLICE_X4Y65                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X4Y65          LUT2 (Prop_lut2_I0_O)        0.043     3.996 f  O_OBUF[11]_inst_i_3/O
                         net (fo=4, routed)           0.246     4.241    O_OBUF[11]_inst_i_3_n_0
    SLICE_X2Y66                                                       f  O_OBUF[12]_inst_i_2/I1
    SLICE_X2Y66          LUT2 (Prop_lut2_I1_O)        0.051     4.292 r  O_OBUF[12]_inst_i_2/O
                         net (fo=1, routed)           0.153     4.445    O_OBUF[12]_inst_i_2_n_0
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X2Y66          LUT6 (Prop_lut6_I1_O)        0.132     4.577 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.267     5.844    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.686 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.686    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.501ns  (logic 2.852ns (38.014%)  route 4.650ns (61.986%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT3=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=15, routed)          1.418     2.012    A_IBUF[3]
    SLICE_X6Y62                                                       r  O_OBUF[12]_inst_i_14/I3
    SLICE_X6Y62          LUT5 (Prop_lut5_I3_O)        0.043     2.055 r  O_OBUF[12]_inst_i_14/O
                         net (fo=4, routed)           0.349     2.405    O_OBUF[12]_inst_i_14_n_0
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_19/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.127     2.532 r  O_OBUF[12]_inst_i_19/O
                         net (fo=2, routed)           0.440     2.972    O_OBUF[12]_inst_i_19_n_0
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y62          LUT5 (Prop_lut5_I1_O)        0.043     3.015 f  O_OBUF[8]_inst_i_6/O
                         net (fo=6, routed)           0.439     3.454    O_OBUF[8]_inst_i_6_n_0
    SLICE_X2Y63                                                       f  O_OBUF[12]_inst_i_12/I0
    SLICE_X2Y63          LUT3 (Prop_lut3_I0_O)        0.043     3.497 f  O_OBUF[12]_inst_i_12/O
                         net (fo=1, routed)           0.341     3.839    O_OBUF[12]_inst_i_12_n_0
    SLICE_X2Y64                                                       f  O_OBUF[12]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.043     3.882 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.450     4.332    O_OBUF[12]_inst_i_4_n_0
    SLICE_X2Y66                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X2Y66          LUT2 (Prop_lut2_I1_O)        0.043     4.375 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.211     5.586    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.915     7.501 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.501    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.405ns  (logic 2.766ns (37.358%)  route 4.639ns (62.642%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=15, routed)          1.418     2.012    A_IBUF[3]
    SLICE_X6Y62                                                       r  O_OBUF[12]_inst_i_14/I3
    SLICE_X6Y62          LUT5 (Prop_lut5_I3_O)        0.043     2.055 r  O_OBUF[12]_inst_i_14/O
                         net (fo=4, routed)           0.349     2.405    O_OBUF[12]_inst_i_14_n_0
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_19/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.127     2.532 r  O_OBUF[12]_inst_i_19/O
                         net (fo=2, routed)           0.440     2.972    O_OBUF[12]_inst_i_19_n_0
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y62          LUT5 (Prop_lut5_I1_O)        0.043     3.015 f  O_OBUF[8]_inst_i_6/O
                         net (fo=6, routed)           0.439     3.454    O_OBUF[8]_inst_i_6_n_0
    SLICE_X2Y63                                                       f  O_OBUF[12]_inst_i_12/I0
    SLICE_X2Y63          LUT3 (Prop_lut3_I0_O)        0.043     3.497 f  O_OBUF[12]_inst_i_12/O
                         net (fo=1, routed)           0.341     3.839    O_OBUF[12]_inst_i_12_n_0
    SLICE_X2Y64                                                       f  O_OBUF[12]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.043     3.882 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.450     4.332    O_OBUF[12]_inst_i_4_n_0
    SLICE_X2Y66                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X2Y66          LUT4 (Prop_lut4_I2_O)        0.043     4.375 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.200     5.575    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.405 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.405    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.104ns  (logic 2.717ns (38.253%)  route 4.386ns (61.747%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 f  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              f  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 f  A_IBUF[4]_inst/O
                         net (fo=21, routed)          1.319     1.909    A_IBUF[4]
    SLICE_X2Y61                                                       f  O_OBUF[7]_inst_i_19/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     1.952 r  O_OBUF[7]_inst_i_19/O
                         net (fo=2, routed)           0.356     2.307    O_OBUF[7]_inst_i_19_n_0
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_14/I2
    SLICE_X2Y61          LUT3 (Prop_lut3_I2_O)        0.043     2.350 r  O_OBUF[7]_inst_i_14/O
                         net (fo=6, routed)           0.606     2.956    O_OBUF[7]_inst_i_14_n_0
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_8/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.999 f  O_OBUF[8]_inst_i_8/O
                         net (fo=1, routed)           0.423     3.423    O_OBUF[8]_inst_i_8_n_0
    SLICE_X1Y63                                                       f  O_OBUF[8]_inst_i_3/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.051     3.474 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.431     3.905    O_OBUF[8]_inst_i_3_n_0
    SLICE_X3Y65                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X3Y65          LUT2 (Prop_lut2_I1_O)        0.129     4.034 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.251     5.285    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.104 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.104    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[2]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.834ns  (logic 2.840ns (41.553%)  route 3.994ns (58.447%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=21, routed)          1.640     2.229    A_IBUF[4]
    SLICE_X5Y63                                                       r  O_OBUF[13]_inst_i_16/I0
    SLICE_X5Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.280 r  O_OBUF[13]_inst_i_16/O
                         net (fo=2, routed)           0.413     2.694    O_OBUF[13]_inst_i_16_n_0
    SLICE_X5Y62                                                       r  O_OBUF[13]_inst_i_9/I0
    SLICE_X5Y62          LUT6 (Prop_lut6_I0_O)        0.129     2.823 r  O_OBUF[13]_inst_i_9/O
                         net (fo=4, routed)           0.409     3.232    O_OBUF[13]_inst_i_9_n_0
    SLICE_X5Y63                                                       r  O_OBUF[2]_inst_i_2/I1
    SLICE_X5Y63          LUT3 (Prop_lut3_I1_O)        0.053     3.285 r  O_OBUF[2]_inst_i_2/O
                         net (fo=1, routed)           0.420     3.704    O_OBUF[2]_inst_i_2_n_0
    SLICE_X4Y64                                                       r  O_OBUF[2]_inst_i_1/I0
    SLICE_X4Y64          LUT5 (Prop_lut5_I0_O)        0.136     3.840 r  O_OBUF[2]_inst_i_1/O
                         net (fo=2, routed)           1.113     4.953    O_OBUF[2]
    AG25                                                              r  O_OBUF[2]_inst/I
    AG25                 OBUF (Prop_obuf_I_O)         1.881     6.834 r  O_OBUF[2]_inst/O
                         net (fo=0)                   0.000     6.834    O[2]
    AG25                                                              r  O[2] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.829ns  (logic 2.621ns (38.383%)  route 4.208ns (61.617%))
  Logic Levels:           7  (IBUF=1 LUT3=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 f  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              f  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 f  A_IBUF[4]_inst/O
                         net (fo=21, routed)          1.319     1.909    A_IBUF[4]
    SLICE_X2Y61                                                       f  O_OBUF[7]_inst_i_19/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     1.952 r  O_OBUF[7]_inst_i_19/O
                         net (fo=2, routed)           0.356     2.307    O_OBUF[7]_inst_i_19_n_0
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_14/I2
    SLICE_X2Y61          LUT3 (Prop_lut3_I2_O)        0.043     2.350 f  O_OBUF[7]_inst_i_14/O
                         net (fo=6, routed)           0.523     2.873    O_OBUF[7]_inst_i_14_n_0
    SLICE_X1Y62                                                       f  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y62          LUT3 (Prop_lut3_I2_O)        0.043     2.916 r  O_OBUF[8]_inst_i_4/O
                         net (fo=5, routed)           0.418     3.334    O_OBUF[8]_inst_i_4_n_0
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_2/I1
    SLICE_X1Y63          LUT3 (Prop_lut3_I1_O)        0.043     3.377 r  O_OBUF[7]_inst_i_2/O
                         net (fo=1, routed)           0.325     3.701    O_OBUF[7]_inst_i_2_n_0
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     3.744 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.268     5.012    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.829 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.829    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




