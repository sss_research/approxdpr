Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 05:43:38 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_327_evolib.txt -name O
| Design       : mul8_327
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.806ns  (logic 2.908ns (42.730%)  route 3.898ns (57.270%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          0.885     1.473    A_IBUF[5]
    SLICE_X1Y59                                                       r  O_OBUF[11]_inst_i_11/I0
    SLICE_X1Y59          LUT4 (Prop_lut4_I0_O)        0.048     1.521 r  O_OBUF[11]_inst_i_11/O
                         net (fo=3, routed)           0.532     2.053    N_1187
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_6/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.182 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.517     2.699    N_1603
    SLICE_X1Y62                                                       r  O_OBUF[13]_inst_i_3/I1
    SLICE_X1Y62          LUT3 (Prop_lut3_I1_O)        0.048     2.747 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.448     3.196    N_1781
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X0Y62          LUT5 (Prop_lut5_I2_O)        0.134     3.330 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.176     3.505    N_1973
    SLICE_X0Y63                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.129     3.634 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.339     4.974    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.806 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     6.806    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.792ns  (logic 2.911ns (42.864%)  route 3.881ns (57.136%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          0.885     1.473    A_IBUF[5]
    SLICE_X1Y59                                                       r  O_OBUF[11]_inst_i_11/I0
    SLICE_X1Y59          LUT4 (Prop_lut4_I0_O)        0.048     1.521 r  O_OBUF[11]_inst_i_11/O
                         net (fo=3, routed)           0.532     2.053    N_1187
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_6/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.182 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.517     2.699    N_1603
    SLICE_X1Y62                                                       r  O_OBUF[13]_inst_i_3/I1
    SLICE_X1Y62          LUT3 (Prop_lut3_I1_O)        0.048     2.747 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.448     3.196    N_1781
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X0Y62          LUT5 (Prop_lut5_I2_O)        0.134     3.330 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.175     3.505    N_1973
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.129     3.634 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.323     4.956    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     6.792 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     6.792    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.492ns  (logic 2.783ns (42.874%)  route 3.708ns (57.126%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          0.885     1.473    A_IBUF[5]
    SLICE_X1Y59                                                       r  O_OBUF[11]_inst_i_11/I0
    SLICE_X1Y59          LUT4 (Prop_lut4_I0_O)        0.048     1.521 r  O_OBUF[11]_inst_i_11/O
                         net (fo=3, routed)           0.532     2.053    N_1187
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_6/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.182 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.517     2.699    N_1603
    SLICE_X1Y62                                                       r  O_OBUF[13]_inst_i_3/I1
    SLICE_X1Y62          LUT3 (Prop_lut3_I1_O)        0.048     2.747 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.448     3.196    N_1781
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X0Y62          LUT5 (Prop_lut5_I1_O)        0.129     3.325 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.325     4.650    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.492 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.492    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.481ns  (logic 2.783ns (42.940%)  route 3.698ns (57.060%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          0.885     1.473    A_IBUF[5]
    SLICE_X1Y59                                                       r  O_OBUF[11]_inst_i_11/I0
    SLICE_X1Y59          LUT4 (Prop_lut4_I0_O)        0.048     1.521 r  O_OBUF[11]_inst_i_11/O
                         net (fo=3, routed)           0.532     2.053    N_1187
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_6/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.182 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.517     2.699    N_1603
    SLICE_X1Y62                                                       r  O_OBUF[13]_inst_i_3/I1
    SLICE_X1Y62          LUT3 (Prop_lut3_I1_O)        0.048     2.747 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.355     3.103    N_1781
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X1Y62          LUT3 (Prop_lut3_I0_O)        0.129     3.232 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.408     4.639    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.481 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.481    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.933ns  (logic 2.555ns (43.054%)  route 3.379ns (56.946%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              f  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 f  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.068     1.662    A_IBUF[3]
    SLICE_X1Y59                                                       f  O_OBUF[9]_inst_i_3/I5
    SLICE_X1Y59          LUT6 (Prop_lut6_I5_O)        0.043     1.705 f  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.426     2.131    N_1408
    SLICE_X2Y59                                                       f  O_OBUF[11]_inst_i_2/I4
    SLICE_X2Y59          LUT5 (Prop_lut5_I4_O)        0.043     2.174 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.590     2.765    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.043     2.808 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.294     4.102    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.933 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.933    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.921ns  (logic 2.553ns (43.124%)  route 3.368ns (56.876%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              f  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 f  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.068     1.662    A_IBUF[3]
    SLICE_X1Y59                                                       f  O_OBUF[9]_inst_i_3/I5
    SLICE_X1Y59          LUT6 (Prop_lut6_I5_O)        0.043     1.705 f  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.426     2.131    N_1408
    SLICE_X2Y59                                                       f  O_OBUF[11]_inst_i_2/I4
    SLICE_X2Y59          LUT5 (Prop_lut5_I4_O)        0.043     2.174 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.591     2.766    O_OBUF[11]_inst_i_2_n_0
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.809 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.282     4.091    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.921 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     5.921    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.522ns  (logic 2.724ns (49.332%)  route 2.798ns (50.668%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT4=1 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          0.885     1.473    A_IBUF[5]
    SLICE_X1Y59                                                       r  O_OBUF[11]_inst_i_11/I0
    SLICE_X1Y59          LUT4 (Prop_lut4_I0_O)        0.048     1.521 r  O_OBUF[11]_inst_i_11/O
                         net (fo=3, routed)           0.292     1.813    N_1187
    SLICE_X1Y60                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X1Y60          LUT5 (Prop_lut5_I0_O)        0.137     1.950 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.441     2.391    N_1425
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X1Y62          LUT2 (Prop_lut2_I0_O)        0.129     2.520 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.180     3.700    O_OBUF[7]
    AK26                                                              r  O_OBUF[7]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     5.522 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.522    O[7]
    AK26                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.339ns  (logic 2.499ns (46.808%)  route 2.840ns (53.192%))
  Logic Levels:           4  (IBUF=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.068     1.662    A_IBUF[3]
    SLICE_X1Y59                                                       r  O_OBUF[9]_inst_i_3/I5
    SLICE_X1Y59          LUT6 (Prop_lut6_I5_O)        0.043     1.705 r  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.423     2.128    N_1408
    SLICE_X0Y59                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X0Y59          LUT6 (Prop_lut6_I1_O)        0.043     2.171 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.349     3.520    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.339 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.339    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.946ns  (logic 2.502ns (50.589%)  route 2.444ns (49.411%))
  Logic Levels:           4  (IBUF=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.122     1.716    A_IBUF[3]
    SLICE_X1Y60                                                       r  O_OBUF[6]_inst_i_3/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.043     1.759 r  O_OBUF[6]_inst_i_3/O
                         net (fo=1, routed)           0.138     1.896    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y60                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.043     1.939 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           1.184     3.124    O_OBUF[1]
    AK27                                                              r  O_OBUF[6]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     4.946 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     4.946    O[6]
    AK27                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[1]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.862ns  (logic 2.477ns (50.951%)  route 2.385ns (49.049%))
  Logic Levels:           4  (IBUF=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.122     1.716    A_IBUF[3]
    SLICE_X1Y60                                                       r  O_OBUF[6]_inst_i_3/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.043     1.759 r  O_OBUF[6]_inst_i_3/O
                         net (fo=1, routed)           0.138     1.896    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y60                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.043     1.939 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           1.126     3.065    O_OBUF[1]
    AH24                                                              r  O_OBUF[1]_inst/I
    AH24                 OBUF (Prop_obuf_I_O)         1.797     4.862 r  O_OBUF[1]_inst/O
                         net (fo=0)                   0.000     4.862    O[1]
    AH24                                                              r  O[1] (OUT)
  -------------------------------------------------------------------    -------------------




