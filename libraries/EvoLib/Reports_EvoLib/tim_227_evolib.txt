Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 00:49:04 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_227_evolib.txt -name O
| Design       : mul8_227
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.984ns  (logic 2.832ns (35.478%)  route 5.151ns (64.522%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              f  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[3]_inst/O
                         net (fo=10, routed)          1.391     1.987    A_IBUF[3]
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_11/I1
    SLICE_X2Y62          LUT2 (Prop_lut2_I1_O)        0.048     2.035 r  O_OBUF[9]_inst_i_11/O
                         net (fo=1, routed)           0.224     2.259    O_OBUF[9]_inst_i_11_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.132     2.391 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.513     2.903    N_1665
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_2/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[2]_inst_i_2/O
                         net (fo=2, routed)           0.228     3.174    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y62          LUT4 (Prop_lut4_I2_O)        0.043     3.217 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.682     3.899    O_OBUF[2]
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     3.942 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.356     4.298    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.341 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.331     4.671    N_1983
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.043     4.714 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.428     6.142    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.984 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.984    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.957ns  (logic 2.824ns (35.487%)  route 5.133ns (64.513%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              f  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[3]_inst/O
                         net (fo=10, routed)          1.391     1.987    A_IBUF[3]
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_11/I1
    SLICE_X2Y62          LUT2 (Prop_lut2_I1_O)        0.048     2.035 r  O_OBUF[9]_inst_i_11/O
                         net (fo=1, routed)           0.224     2.259    O_OBUF[9]_inst_i_11_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.132     2.391 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.513     2.903    N_1665
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_2/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[2]_inst_i_2/O
                         net (fo=2, routed)           0.228     3.174    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y62          LUT4 (Prop_lut4_I2_O)        0.043     3.217 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.682     3.899    O_OBUF[2]
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     3.942 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.356     4.298    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.341 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.418     4.758    N_1983
    SLICE_X1Y62                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     4.801 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.323     6.124    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.957 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.957    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.955ns  (logic 2.826ns (35.531%)  route 5.128ns (64.469%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              f  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[3]_inst/O
                         net (fo=10, routed)          1.391     1.987    A_IBUF[3]
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_11/I1
    SLICE_X2Y62          LUT2 (Prop_lut2_I1_O)        0.048     2.035 r  O_OBUF[9]_inst_i_11/O
                         net (fo=1, routed)           0.224     2.259    O_OBUF[9]_inst_i_11_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.132     2.391 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.513     2.903    N_1665
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_2/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[2]_inst_i_2/O
                         net (fo=2, routed)           0.228     3.174    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y62          LUT4 (Prop_lut4_I2_O)        0.043     3.217 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.682     3.899    O_OBUF[2]
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     3.942 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.356     4.298    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.341 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.331     4.671    N_1983
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.714 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.405     6.119    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.955 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.955    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.558ns  (logic 2.967ns (39.257%)  route 4.591ns (60.744%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              f  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[3]_inst/O
                         net (fo=10, routed)          1.391     1.987    A_IBUF[3]
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_11/I1
    SLICE_X2Y62          LUT2 (Prop_lut2_I1_O)        0.048     2.035 r  O_OBUF[9]_inst_i_11/O
                         net (fo=1, routed)           0.224     2.259    O_OBUF[9]_inst_i_11_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.132     2.391 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.520     2.910    N_1665
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.953 r  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.410     3.364    N_1682
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_2/I0
    SLICE_X1Y62          LUT5 (Prop_lut5_I0_O)        0.048     3.412 r  O_OBUF[10]_inst_i_2/O
                         net (fo=2, routed)           0.349     3.761    N_1933
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.129     3.890 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.421     4.311    N_1949
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.053     4.364 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.276     5.640    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.918     7.558 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.558    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.525ns  (logic 2.789ns (37.065%)  route 4.736ns (62.935%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              f  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[3]_inst/O
                         net (fo=10, routed)          1.391     1.987    A_IBUF[3]
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_11/I1
    SLICE_X2Y62          LUT2 (Prop_lut2_I1_O)        0.048     2.035 r  O_OBUF[9]_inst_i_11/O
                         net (fo=1, routed)           0.224     2.259    O_OBUF[9]_inst_i_11_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.132     2.391 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.513     2.903    N_1665
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_2/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[2]_inst_i_2/O
                         net (fo=2, routed)           0.228     3.174    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y62          LUT4 (Prop_lut4_I2_O)        0.043     3.217 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.682     3.899    O_OBUF[2]
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     3.942 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.355     4.296    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_1/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.339 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.344     5.683    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.525 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.525    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.114ns  (logic 2.924ns (41.104%)  route 4.190ns (58.896%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              f  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[3]_inst/O
                         net (fo=10, routed)          1.391     1.987    A_IBUF[3]
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_11/I1
    SLICE_X2Y62          LUT2 (Prop_lut2_I1_O)        0.048     2.035 r  O_OBUF[9]_inst_i_11/O
                         net (fo=1, routed)           0.224     2.259    O_OBUF[9]_inst_i_11_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.132     2.391 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.520     2.910    N_1665
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.953 r  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.410     3.364    N_1682
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_2/I0
    SLICE_X1Y62          LUT5 (Prop_lut5_I0_O)        0.048     3.412 r  O_OBUF[10]_inst_i_2/O
                         net (fo=2, routed)           0.349     3.761    N_1933
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.138     3.899 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.296     5.195    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.919     7.114 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.114    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.505ns  (logic 2.681ns (41.209%)  route 3.824ns (58.791%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              f  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[3]_inst/O
                         net (fo=10, routed)          1.391     1.987    A_IBUF[3]
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_11/I1
    SLICE_X2Y62          LUT2 (Prop_lut2_I1_O)        0.048     2.035 r  O_OBUF[9]_inst_i_11/O
                         net (fo=1, routed)           0.224     2.259    O_OBUF[9]_inst_i_11_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.132     2.391 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.520     2.910    N_1665
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.953 r  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.410     3.364    N_1682
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_1/I3
    SLICE_X1Y62          LUT5 (Prop_lut5_I3_O)        0.043     3.407 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.279     4.686    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.505 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.505    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[2]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.209ns  (logic 2.651ns (42.697%)  route 3.558ns (57.303%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              f  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[3]_inst/O
                         net (fo=10, routed)          1.391     1.987    A_IBUF[3]
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_11/I1
    SLICE_X2Y62          LUT2 (Prop_lut2_I1_O)        0.048     2.035 r  O_OBUF[9]_inst_i_11/O
                         net (fo=1, routed)           0.224     2.259    O_OBUF[9]_inst_i_11_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.132     2.391 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.513     2.903    N_1665
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_2/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[2]_inst_i_2/O
                         net (fo=2, routed)           0.228     3.174    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y62          LUT4 (Prop_lut4_I2_O)        0.043     3.217 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           1.203     4.420    O_OBUF[2]
    AH25                                                              r  O_OBUF[2]_inst/I
    AH25                 OBUF (Prop_obuf_I_O)         1.789     6.209 r  O_OBUF[2]_inst/O
                         net (fo=0)                   0.000     6.209    O[2]
    AH25                                                              r  O[2] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.723ns  (logic 2.533ns (44.262%)  route 3.190ns (55.738%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.176     1.764    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_4/I2
    SLICE_X1Y60          LUT6 (Prop_lut6_I2_O)        0.043     1.807 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.428     2.235    N_1164
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_2/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.278 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.406     2.683    N_1664
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y63          LUT3 (Prop_lut3_I0_O)        0.043     2.726 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.180     3.906    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.723 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.723    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[4]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.175ns  (logic 2.531ns (48.913%)  route 2.644ns (51.087%))
  Logic Levels:           3  (IBUF=1 LUT2=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              r  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[3]_inst/O
                         net (fo=10, routed)          1.447     2.043    A_IBUF[3]
    SLICE_X0Y61                                                       r  O_OBUF[4]_inst_i_1/I0
    SLICE_X0Y61          LUT2 (Prop_lut2_I0_O)        0.048     2.091 r  O_OBUF[4]_inst_i_1/O
                         net (fo=1, routed)           1.197     3.288    O_OBUF[4]
    AJ27                                                              r  O_OBUF[4]_inst/I
    AJ27                 OBUF (Prop_obuf_I_O)         1.888     5.175 r  O_OBUF[4]_inst/O
                         net (fo=0)                   0.000     5.175    O[4]
    AJ27                                                              r  O[4] (OUT)
  -------------------------------------------------------------------    -------------------




