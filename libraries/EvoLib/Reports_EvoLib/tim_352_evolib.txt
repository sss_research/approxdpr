Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 06:57:24 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_352_evolib.txt -name O
| Design       : mul8_352
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.853ns  (logic 3.011ns (38.343%)  route 4.842ns (61.657%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=21, routed)          1.134     1.722    A_IBUF[6]
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_8/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.775 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.347     2.122    N_914
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.136     2.258 r  O_OBUF[9]_inst_i_4/O
                         net (fo=6, routed)           0.426     2.684    N_1164
    SLICE_X3Y61                                                       r  O_OBUF[2]_inst_i_2/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.813 r  O_OBUF[2]_inst_i_2/O
                         net (fo=4, routed)           0.508     3.322    N_1432
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.053     3.375 r  O_OBUF[12]_inst_i_7/O
                         net (fo=4, routed)           0.289     3.664    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.131     3.795 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.292     4.087    N_1715
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.130 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.517     4.647    N_1748
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_1/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     4.690 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.328     6.018    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.853 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.853    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.827ns  (logic 3.115ns (39.804%)  route 4.711ns (60.196%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=21, routed)          1.134     1.722    A_IBUF[6]
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_8/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.775 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.347     2.122    N_914
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.136     2.258 r  O_OBUF[9]_inst_i_4/O
                         net (fo=6, routed)           0.426     2.684    N_1164
    SLICE_X3Y61                                                       r  O_OBUF[2]_inst_i_2/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.813 r  O_OBUF[2]_inst_i_2/O
                         net (fo=4, routed)           0.508     3.322    N_1432
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.053     3.375 r  O_OBUF[12]_inst_i_7/O
                         net (fo=4, routed)           0.161     3.536    N_1683
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_3/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.131     3.667 r  O_OBUF[11]_inst_i_3/O
                         net (fo=3, routed)           0.382     4.050    N_1949
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_3/I2
    SLICE_X0Y62          LUT6 (Prop_lut6_I2_O)        0.043     4.093 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.416     4.509    N_1983
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_1/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.053     4.562 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.335     5.897    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.930     7.827 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.827    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.743ns  (logic 3.008ns (38.850%)  route 4.735ns (61.150%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=21, routed)          1.134     1.722    A_IBUF[6]
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_8/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.775 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.347     2.122    N_914
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.136     2.258 r  O_OBUF[9]_inst_i_4/O
                         net (fo=6, routed)           0.426     2.684    N_1164
    SLICE_X3Y61                                                       r  O_OBUF[2]_inst_i_2/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.813 r  O_OBUF[2]_inst_i_2/O
                         net (fo=4, routed)           0.508     3.322    N_1432
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.053     3.375 r  O_OBUF[12]_inst_i_7/O
                         net (fo=4, routed)           0.289     3.664    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.131     3.795 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.292     4.087    N_1715
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.130 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.324     4.454    N_1748
    SLICE_X0Y63                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.497 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.414     5.911    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.743 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.743    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.595ns  (logic 3.111ns (40.957%)  route 4.485ns (59.043%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=21, routed)          1.134     1.722    A_IBUF[6]
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_8/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.775 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.347     2.122    N_914
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.136     2.258 r  O_OBUF[9]_inst_i_4/O
                         net (fo=6, routed)           0.426     2.684    N_1164
    SLICE_X3Y61                                                       r  O_OBUF[2]_inst_i_2/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.813 r  O_OBUF[2]_inst_i_2/O
                         net (fo=4, routed)           0.508     3.322    N_1432
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.053     3.375 r  O_OBUF[12]_inst_i_7/O
                         net (fo=4, routed)           0.283     3.658    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.131     3.789 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.299     4.089    N_1714
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_5/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.051     4.140 r  O_OBUF[12]_inst_i_5/O
                         net (fo=1, routed)           0.171     4.311    N_1965
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.129     4.440 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.314     5.754    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.595 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.595    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.344ns  (logic 2.964ns (40.363%)  route 4.380ns (59.637%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=4 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=21, routed)          1.134     1.722    A_IBUF[6]
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_8/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.775 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.347     2.122    N_914
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.136     2.258 r  O_OBUF[9]_inst_i_4/O
                         net (fo=6, routed)           0.426     2.684    N_1164
    SLICE_X3Y61                                                       r  O_OBUF[2]_inst_i_2/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.813 r  O_OBUF[2]_inst_i_2/O
                         net (fo=4, routed)           0.508     3.322    N_1432
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.053     3.375 r  O_OBUF[12]_inst_i_7/O
                         net (fo=4, routed)           0.463     3.838    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_2/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.131     3.969 r  O_OBUF[10]_inst_i_2/O
                         net (fo=1, routed)           0.229     4.199    N_1698
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     4.242 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.271     5.512    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.344 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.344    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.239ns  (logic 2.963ns (40.931%)  route 4.276ns (59.069%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=21, routed)          1.134     1.722    A_IBUF[6]
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_8/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.775 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.347     2.122    N_914
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.136     2.258 r  O_OBUF[9]_inst_i_4/O
                         net (fo=6, routed)           0.426     2.684    N_1164
    SLICE_X3Y61                                                       r  O_OBUF[2]_inst_i_2/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.813 r  O_OBUF[2]_inst_i_2/O
                         net (fo=4, routed)           0.508     3.322    N_1432
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.053     3.375 r  O_OBUF[12]_inst_i_7/O
                         net (fo=4, routed)           0.283     3.658    N_1683
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.131     3.789 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.299     4.089    N_1714
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.043     4.132 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.277     5.408    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.239 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.239    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.700ns  (logic 2.810ns (41.947%)  route 3.889ns (58.053%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=21, routed)          1.134     1.722    A_IBUF[6]
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_8/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.775 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.347     2.122    N_914
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.136     2.258 r  O_OBUF[9]_inst_i_4/O
                         net (fo=6, routed)           0.426     2.684    N_1164
    SLICE_X3Y61                                                       r  O_OBUF[2]_inst_i_2/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.813 r  O_OBUF[2]_inst_i_2/O
                         net (fo=4, routed)           0.508     3.322    N_1432
    SLICE_X3Y62                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X3Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.365 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.217     3.582    N_1682
    SLICE_X3Y62                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X3Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.625 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.256     4.881    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.700 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.700    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.238ns  (logic 2.855ns (45.769%)  route 3.383ns (54.231%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=21, routed)          1.134     1.722    A_IBUF[6]
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_8/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.775 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.347     2.122    N_914
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.136     2.258 r  O_OBUF[9]_inst_i_4/O
                         net (fo=6, routed)           0.227     2.486    N_1164
    SLICE_X0Y61                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.129     2.615 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.413     3.028    N_1664
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X0Y62          LUT3 (Prop_lut3_I0_O)        0.050     3.078 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.261     4.338    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.899     6.238 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.238    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[2]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.229ns  (logic 2.647ns (42.494%)  route 3.582ns (57.506%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AN28                                                              r  A_IBUF[6]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[6]_inst/O
                         net (fo=21, routed)          1.169     1.757    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[2]_inst_i_13/I0
    SLICE_X1Y60          LUT2 (Prop_lut2_I0_O)        0.053     1.810 r  O_OBUF[2]_inst_i_13/O
                         net (fo=2, routed)           0.422     2.232    N_798
    SLICE_X0Y60                                                       r  O_OBUF[2]_inst_i_10/I5
    SLICE_X0Y60          LUT6 (Prop_lut6_I5_O)        0.131     2.363 r  O_OBUF[2]_inst_i_10/O
                         net (fo=3, routed)           0.334     2.697    N_1183
    SLICE_X1Y60                                                       r  O_OBUF[2]_inst_i_6/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     2.740 r  O_OBUF[2]_inst_i_6/O
                         net (fo=5, routed)           0.519     3.259    N_1198
    SLICE_X2Y61                                                       r  O_OBUF[2]_inst_i_1/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     3.302 r  O_OBUF[2]_inst_i_1/O
                         net (fo=2, routed)           1.137     4.440    O_OBUF[2]
    AH25                                                              r  O_OBUF[2]_inst/I
    AH25                 OBUF (Prop_obuf_I_O)         1.789     6.229 r  O_OBUF[2]_inst/O
                         net (fo=0)                   0.000     6.229    O[2]
    AH25                                                              r  O[2] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.787ns  (logic 2.441ns (50.984%)  route 2.346ns (49.016%))
  Logic Levels:           3  (IBUF=1 LUT2=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AN27                                                              r  A_IBUF[3]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.219     1.815    A_IBUF[3]
    SLICE_X0Y63                                                       r  O_OBUF[5]_inst_i_1/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.858 r  O_OBUF[5]_inst_i_1/O
                         net (fo=1, routed)           1.127     2.985    O_OBUF[5]
    AJ26                                                              r  O_OBUF[5]_inst/I
    AJ26                 OBUF (Prop_obuf_I_O)         1.802     4.787 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     4.787    O[5]
    AJ26                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------




