Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 18:41:39 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_102_evolib.txt -name O
| Design       : mul8_102
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.316ns  (logic 2.738ns (37.423%)  route 4.578ns (62.577%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.232     1.820    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_6/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.863 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.517     2.380    N_1203
    SLICE_X2Y65                                                       r  O_OBUF[7]_inst_i_2/I5
    SLICE_X2Y65          LUT6 (Prop_lut6_I5_O)        0.043     2.423 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.427     2.850    N_1632
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X2Y64          LUT3 (Prop_lut3_I2_O)        0.043     2.893 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.487     3.380    N_1781
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_5/I0
    SLICE_X1Y65          LUT6 (Prop_lut6_I0_O)        0.127     3.507 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.518     4.025    N_1957
    SLICE_X2Y66                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X2Y66          LUT6 (Prop_lut6_I3_O)        0.043     4.068 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.397     5.465    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.316 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.316    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.229ns  (logic 2.722ns (37.656%)  route 4.507ns (62.344%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.232     1.820    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_6/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.863 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.517     2.380    N_1203
    SLICE_X2Y65                                                       r  O_OBUF[7]_inst_i_2/I5
    SLICE_X2Y65          LUT6 (Prop_lut6_I5_O)        0.043     2.423 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.427     2.850    N_1632
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X2Y64          LUT3 (Prop_lut3_I2_O)        0.043     2.893 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.487     3.380    N_1781
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_5/I0
    SLICE_X1Y65          LUT6 (Prop_lut6_I0_O)        0.127     3.507 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.432     3.940    N_1957
    SLICE_X2Y65                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X2Y65          LUT6 (Prop_lut6_I5_O)        0.043     3.983 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.411     5.394    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.229 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.229    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.172ns  (logic 2.719ns (37.916%)  route 4.453ns (62.084%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.232     1.820    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_6/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.863 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.517     2.380    N_1203
    SLICE_X2Y65                                                       r  O_OBUF[7]_inst_i_2/I5
    SLICE_X2Y65          LUT6 (Prop_lut6_I5_O)        0.043     2.423 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.427     2.850    N_1632
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X2Y64          LUT3 (Prop_lut3_I2_O)        0.043     2.893 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.487     3.380    N_1781
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_5/I0
    SLICE_X1Y65          LUT6 (Prop_lut6_I0_O)        0.127     3.507 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.514     4.021    N_1957
    SLICE_X2Y66                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X2Y66          LUT3 (Prop_lut3_I2_O)        0.043     4.064 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.275     5.339    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.172 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.172    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.937ns  (logic 2.644ns (38.117%)  route 4.293ns (61.883%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.232     1.820    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_6/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.863 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.517     2.380    N_1203
    SLICE_X2Y65                                                       r  O_OBUF[7]_inst_i_2/I5
    SLICE_X2Y65          LUT6 (Prop_lut6_I5_O)        0.043     2.423 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.427     2.850    N_1632
    SLICE_X2Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.893 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           0.421     3.315    O_OBUF[7]
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.043     3.358 r  O_OBUF[12]_inst_i_4/O
                         net (fo=1, routed)           0.421     3.778    N_1943
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X1Y65          LUT3 (Prop_lut3_I2_O)        0.043     3.821 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.274     5.096    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.937 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.937    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.618ns  (logic 2.695ns (40.719%)  route 3.923ns (59.281%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.234     1.822    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     1.865 r  O_OBUF[9]_inst_i_10/O
                         net (fo=2, routed)           0.409     2.274    N_1173
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_7/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.317 r  O_OBUF[12]_inst_i_7/O
                         net (fo=3, routed)           0.484     2.801    N_1409
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X1Y64          LUT5 (Prop_lut5_I4_O)        0.051     2.852 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.503     3.355    N_1764
    SLICE_X0Y65                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y65          LUT4 (Prop_lut4_I0_O)        0.129     3.484 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.293     4.777    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.618 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.618    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.607ns  (logic 2.778ns (42.041%)  route 3.830ns (57.959%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.234     1.822    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     1.865 r  O_OBUF[9]_inst_i_10/O
                         net (fo=2, routed)           0.409     2.274    N_1173
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_7/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.317 r  O_OBUF[12]_inst_i_7/O
                         net (fo=3, routed)           0.484     2.801    N_1409
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X1Y64          LUT5 (Prop_lut5_I4_O)        0.051     2.852 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.503     3.355    N_1764
    SLICE_X0Y65                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X0Y65          LUT2 (Prop_lut2_I0_O)        0.137     3.492 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.200     4.691    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.916     6.607 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.607    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.967ns  (logic 2.533ns (42.447%)  route 3.434ns (57.553%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.232     1.820    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_6/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.863 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.517     2.380    N_1203
    SLICE_X2Y65                                                       r  O_OBUF[7]_inst_i_2/I5
    SLICE_X2Y65          LUT6 (Prop_lut6_I5_O)        0.043     2.423 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.427     2.850    N_1632
    SLICE_X2Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.893 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           1.258     4.151    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.967 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.967    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.895ns  (logic 2.554ns (43.322%)  route 3.341ns (56.678%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=3, routed)           1.197     1.790    A_IBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_2/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     1.833 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.522     2.355    N_1394
    SLICE_X0Y64                                                       r  O_OBUF[9]_inst_i_5/I2
    SLICE_X0Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.398 r  O_OBUF[9]_inst_i_5/O
                         net (fo=2, routed)           0.343     2.741    N_1573
    SLICE_X0Y64                                                       r  O_OBUF[9]_inst_i_1/I3
    SLICE_X0Y64          LUT5 (Prop_lut5_I3_O)        0.043     2.784 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.279     4.063    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.895 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.895    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.665ns  (logic 2.527ns (44.608%)  route 3.138ns (55.392%))
  Logic Levels:           5  (IBUF=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=23, routed)          1.216     1.795    A_IBUF[7]
    SLICE_X3Y64                                                       r  O_OBUF[9]_inst_i_8/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     1.838 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.317     2.155    N_1069
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_4/I4
    SLICE_X1Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.198 r  O_OBUF[8]_inst_i_4/O
                         net (fo=1, routed)           0.337     2.535    N_1334
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_1/I2
    SLICE_X1Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.578 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.269     3.847    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.665 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.665    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[2]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.271ns  (logic 2.461ns (46.684%)  route 2.810ns (53.316%))
  Logic Levels:           4  (IBUF=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=23, routed)          1.291     1.871    A_IBUF[7]
    SLICE_X0Y62                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     1.914 r  O_OBUF[1]_inst_i_1/O
                         net (fo=3, routed)           0.395     2.308    O_OBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[2]_inst_i_1/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.351 r  O_OBUF[2]_inst_i_1/O
                         net (fo=1, routed)           1.124     3.476    O_OBUF[2]
    AG25                                                              r  O_OBUF[2]_inst/I
    AG25                 OBUF (Prop_obuf_I_O)         1.795     5.271 r  O_OBUF[2]_inst/O
                         net (fo=0)                   0.000     5.271    O[2]
    AG25                                                              r  O[2] (OUT)
  -------------------------------------------------------------------    -------------------




