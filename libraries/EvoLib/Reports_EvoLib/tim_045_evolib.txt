Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 15:50:08 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_045_evolib.txt -name O
| Design       : mul8_045
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.346ns  (logic 2.656ns (36.153%)  route 4.690ns (63.847%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.338     1.928    A_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[7]_inst_i_7/I0
    SLICE_X3Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.971 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.422     2.392    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_11/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.435 r  O_OBUF[13]_inst_i_11/O
                         net (fo=3, routed)           0.420     2.855    O_OBUF[13]_inst_i_11_n_0
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.898 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.500     3.398    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y64                                                       f  O_OBUF[15]_inst_i_4/I2
    SLICE_X0Y64          LUT6 (Prop_lut6_I2_O)        0.043     3.441 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.596     4.037    O_OBUF[15]_inst_i_4_n_0
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X1Y65          LUT6 (Prop_lut6_I2_O)        0.043     4.080 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.415     5.495    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.346 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.346    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.086ns  (logic 2.637ns (37.218%)  route 4.449ns (62.782%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.338     1.928    A_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[7]_inst_i_7/I0
    SLICE_X3Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.971 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.429     2.399    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y63                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.442 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.427     2.870    O_OBUF[7]_inst_i_2_n_0
    SLICE_X2Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.913 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           0.422     3.335    O_OBUF[7]
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_5/I0
    SLICE_X0Y64          LUT5 (Prop_lut5_I0_O)        0.043     3.378 r  O_OBUF[13]_inst_i_5/O
                         net (fo=2, routed)           0.511     3.889    O_OBUF[13]_inst_i_5_n_0
    SLICE_X2Y64                                                       r  O_OBUF[13]_inst_i_1/I4
    SLICE_X2Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.932 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.321     5.253    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.086 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.086    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.002ns  (logic 2.640ns (37.705%)  route 4.362ns (62.295%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.338     1.928    A_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[7]_inst_i_7/I0
    SLICE_X3Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.971 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.422     2.392    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_11/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.435 r  O_OBUF[13]_inst_i_11/O
                         net (fo=3, routed)           0.420     2.855    O_OBUF[13]_inst_i_11_n_0
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.898 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.500     3.398    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y64                                                       f  O_OBUF[15]_inst_i_4/I2
    SLICE_X0Y64          LUT6 (Prop_lut6_I2_O)        0.043     3.441 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.325     3.766    O_OBUF[15]_inst_i_4_n_0
    SLICE_X1Y65                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X1Y65          LUT6 (Prop_lut6_I4_O)        0.043     3.809 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.358     5.167    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.002 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.002    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.830ns  (logic 2.646ns (38.743%)  route 4.184ns (61.257%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=2 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.338     1.928    A_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[7]_inst_i_7/I0
    SLICE_X3Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.971 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.429     2.399    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y63                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.442 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.427     2.870    O_OBUF[7]_inst_i_2_n_0
    SLICE_X2Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.913 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           0.422     3.335    O_OBUF[7]
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_5/I0
    SLICE_X0Y64          LUT5 (Prop_lut5_I0_O)        0.043     3.378 r  O_OBUF[13]_inst_i_5/O
                         net (fo=2, routed)           0.221     3.599    O_OBUF[13]_inst_i_5_n_0
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y64          LUT3 (Prop_lut3_I2_O)        0.043     3.642 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.347     4.989    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.830 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.830    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.483ns  (logic 2.603ns (40.147%)  route 3.880ns (59.853%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT3=1 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.338     1.928    A_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[7]_inst_i_7/I0
    SLICE_X3Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.971 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.429     2.399    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y63                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.442 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.427     2.870    O_OBUF[7]_inst_i_2_n_0
    SLICE_X2Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.913 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           0.415     3.328    O_OBUF[7]
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X0Y64          LUT4 (Prop_lut4_I2_O)        0.043     3.371 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.271     4.642    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.483 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.483    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.353ns  (logic 2.684ns (42.244%)  route 3.669ns (57.756%))
  Logic Levels:           6  (IBUF=1 LUT2=2 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.338     1.928    A_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[7]_inst_i_7/I0
    SLICE_X3Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.971 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.422     2.392    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_11/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.435 r  O_OBUF[13]_inst_i_11/O
                         net (fo=3, routed)           0.420     2.855    O_OBUF[13]_inst_i_11_n_0
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.898 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.221     3.119    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y64          LUT2 (Prop_lut2_I1_O)        0.049     3.168 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.437    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.916     6.353 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.353    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.151ns  (logic 2.668ns (43.374%)  route 3.483ns (56.626%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL26                                              0.000     0.000 r  B[5] (IN)
                         net (fo=0)                   0.000     0.000    B[5]
    AL26                                                              r  B_IBUF[5]_inst/I
    AL26                 IBUF (Prop_ibuf_I_O)         0.570     0.570 r  B_IBUF[5]_inst/O
                         net (fo=13, routed)          1.089     1.659    B_IBUF[5]
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_7/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.702 f  O_OBUF[9]_inst_i_7/O
                         net (fo=3, routed)           0.415     2.117    O_OBUF[9]_inst_i_7_n_0
    SLICE_X0Y62                                                       f  O_OBUF[11]_inst_i_6/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.160 r  O_OBUF[11]_inst_i_6/O
                         net (fo=5, routed)           0.495     2.654    O_OBUF[11]_inst_i_6_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_2/I1
    SLICE_X1Y63          LUT5 (Prop_lut5_I1_O)        0.051     2.705 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.220     2.925    O_OBUF[9]_inst_i_2_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X1Y63          LUT3 (Prop_lut3_I0_O)        0.129     3.054 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.265     4.319    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.151 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.151    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.900ns  (logic 2.535ns (42.969%)  route 3.365ns (57.031%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT3=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.338     1.928    A_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[7]_inst_i_7/I0
    SLICE_X3Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.971 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.429     2.399    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y63                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.442 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.427     2.870    O_OBUF[7]_inst_i_2_n_0
    SLICE_X2Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.913 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           1.170     4.083    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.900 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.900    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.447ns  (logic 2.633ns (48.338%)  route 2.814ns (51.662%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT3=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=6, routed)           0.995     1.588    A_IBUF[1]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_11/I0
    SLICE_X1Y62          LUT2 (Prop_lut2_I0_O)        0.049     1.637 r  O_OBUF[9]_inst_i_11/O
                         net (fo=2, routed)           0.345     1.982    O_OBUF[9]_inst_i_11_n_0
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_3/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.129     2.111 r  O_OBUF[8]_inst_i_3/O
                         net (fo=1, routed)           0.219     2.330    O_OBUF[8]_inst_i_3_n_0
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X0Y62          LUT3 (Prop_lut3_I1_O)        0.043     2.373 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.255     3.628    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.447 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.447    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.972ns  (logic 2.523ns (50.738%)  route 2.450ns (49.262%))
  Logic Levels:           4  (IBUF=1 LUT3=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=2, routed)           1.086     1.607    A_IBUF[0]
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_2/I1
    SLICE_X0Y61          LUT3 (Prop_lut3_I1_O)        0.051     1.658 r  O_OBUF[5]_inst_i_2/O
                         net (fo=1, routed)           0.095     1.753    O_OBUF[5]_inst_i_2_n_0
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_1/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.129     1.882 r  O_OBUF[5]_inst_i_1/O
                         net (fo=1, routed)           1.269     3.151    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     4.972 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     4.972    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------




