Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 08:08:45 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_376_evolib.txt -name O
| Design       : mul8_376
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.254ns  (logic 2.921ns (40.267%)  route 4.333ns (59.733%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=15, routed)          1.126     1.706    A_IBUF[7]
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_3/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.053     1.759 r  O_OBUF[11]_inst_i_3/O
                         net (fo=6, routed)           0.470     2.229    O_OBUF[11]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[2]_inst_i_1/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.131     2.360 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.412     2.772    O_OBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[13]_inst_i_2/I1
    SLICE_X1Y63          LUT3 (Prop_lut3_I1_O)        0.051     2.823 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.693     3.516    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.129     3.645 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.222     3.867    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I0
    SLICE_X0Y65          LUT5 (Prop_lut5_I0_O)        0.054     3.921 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.409     5.331    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.924     7.254 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.254    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.176ns  (logic 2.838ns (39.549%)  route 4.338ns (60.451%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=15, routed)          1.126     1.706    A_IBUF[7]
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_3/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.053     1.759 r  O_OBUF[11]_inst_i_3/O
                         net (fo=6, routed)           0.470     2.229    O_OBUF[11]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[2]_inst_i_1/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.131     2.360 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.412     2.772    O_OBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[13]_inst_i_2/I1
    SLICE_X1Y63          LUT3 (Prop_lut3_I1_O)        0.051     2.823 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.693     3.516    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.129     3.645 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.222     3.867    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.043     3.910 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.414     5.324    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.176 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.176    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.664ns  (logic 2.779ns (41.703%)  route 3.885ns (58.297%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=13, routed)          1.145     1.732    A_IBUF[5]
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     1.775 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.541     2.317    O_OBUF[2]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_8/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.127     2.444 r  O_OBUF[13]_inst_i_8/O
                         net (fo=1, routed)           0.407     2.851    O_OBUF[13]_inst_i_8_n_0
    SLICE_X1Y63                                                       r  O_OBUF[13]_inst_i_3/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.894 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.522     3.416    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y65          LUT3 (Prop_lut3_I2_O)        0.051     3.467 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.270     4.737    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     6.664 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.664    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.628ns  (logic 2.676ns (40.381%)  route 3.951ns (59.619%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=13, routed)          1.145     1.732    A_IBUF[5]
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     1.775 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.541     2.317    O_OBUF[2]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_8/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.127     2.444 r  O_OBUF[13]_inst_i_8/O
                         net (fo=1, routed)           0.407     2.851    O_OBUF[13]_inst_i_8_n_0
    SLICE_X1Y63                                                       r  O_OBUF[13]_inst_i_3/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.894 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.522     3.416    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y65                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X0Y65          LUT5 (Prop_lut5_I1_O)        0.043     3.459 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.336     4.795    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.628 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.628    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.319ns  (logic 2.691ns (42.578%)  route 3.629ns (57.422%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=15, routed)          1.126     1.706    A_IBUF[7]
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_3/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.053     1.759 r  O_OBUF[11]_inst_i_3/O
                         net (fo=6, routed)           0.470     2.229    O_OBUF[11]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[2]_inst_i_1/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.131     2.360 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.412     2.772    O_OBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_7/I1
    SLICE_X1Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.815 r  O_OBUF[11]_inst_i_7/O
                         net (fo=1, routed)           0.304     3.119    O_OBUF[11]_inst_i_7_n_0
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_1/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     3.162 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.316     4.478    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.319 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.319    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.992ns  (logic 2.725ns (45.471%)  route 3.268ns (54.529%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=13, routed)          1.145     1.732    A_IBUF[5]
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     1.775 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.442     2.217    O_OBUF[2]_inst_i_3_n_0
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X2Y62          LUT5 (Prop_lut5_I0_O)        0.132     2.349 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.411     2.760    O_OBUF[11]_inst_i_2_n_0
    SLICE_X2Y64                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X2Y64          LUT5 (Prop_lut5_I2_O)        0.132     2.892 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.270     4.162    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.992 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.992    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.832ns  (logic 2.548ns (43.690%)  route 3.284ns (56.310%))
  Logic Levels:           5  (IBUF=1 LUT4=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=13, routed)          1.145     1.732    A_IBUF[5]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_9/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     1.775 r  O_OBUF[11]_inst_i_9/O
                         net (fo=3, routed)           0.438     2.213    O_OBUF[11]_inst_i_9_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_2/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.256 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.417     2.673    O_OBUF[9]_inst_i_2_n_0
    SLICE_X3Y63                                                       r  O_OBUF[9]_inst_i_1/I2
    SLICE_X3Y63          LUT4 (Prop_lut4_I2_O)        0.043     2.716 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.284     4.001    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.832 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.832    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.805ns  (logic 2.547ns (43.866%)  route 3.259ns (56.134%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=7, routed)           1.355     1.951    A_IBUF[2]
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.043     1.994 f  O_OBUF[6]_inst_i_3/O
                         net (fo=2, routed)           0.302     2.295    O_OBUF[6]_inst_i_3_n_0
    SLICE_X0Y61                                                       f  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.338 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.326     2.664    O_OBUF[1]
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.707 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.277     3.984    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.805 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.805    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.703ns  (logic 2.541ns (44.561%)  route 3.161ns (55.438%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=7, routed)           1.355     1.951    A_IBUF[2]
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.043     1.994 f  O_OBUF[6]_inst_i_3/O
                         net (fo=2, routed)           0.302     2.295    O_OBUF[6]_inst_i_3_n_0
    SLICE_X0Y61                                                       f  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.338 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.326     2.664    O_OBUF[1]
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.707 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.179     3.886    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.703 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.703    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[4]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.531ns  (logic 2.484ns (44.903%)  route 3.048ns (55.097%))
  Logic Levels:           4  (IBUF=1 LUT3=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=7, routed)           1.449     2.044    A_IBUF[2]
    SLICE_X0Y63                                                       r  O_OBUF[0]_inst_i_1/I2
    SLICE_X0Y63          LUT3 (Prop_lut3_I2_O)        0.043     2.087 r  O_OBUF[0]_inst_i_1/O
                         net (fo=2, routed)           0.422     2.509    O_OBUF[0]
    SLICE_X0Y61                                                       r  O_OBUF[4]_inst_i_1/I2
    SLICE_X0Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.552 r  O_OBUF[4]_inst_i_1/O
                         net (fo=1, routed)           1.177     3.729    O_OBUF[4]
    AJ26                                                              r  O_OBUF[4]_inst/I
    AJ26                 OBUF (Prop_obuf_I_O)         1.802     5.531 r  O_OBUF[4]_inst/O
                         net (fo=0)                   0.000     5.531    O[4]
    AJ26                                                              r  O[4] (OUT)
  -------------------------------------------------------------------    -------------------




