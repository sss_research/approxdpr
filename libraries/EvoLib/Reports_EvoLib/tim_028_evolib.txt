Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 14:59:05 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_028_evolib.txt -name O
| Design       : mul8_028
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.545ns  (logic 3.029ns (31.737%)  route 6.516ns (68.263%))
  Logic Levels:           12  (IBUF=1 LUT2=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=8, routed)           1.083     1.664    B_IBUF[0]
    SLICE_X3Y60                                                       r  O_OBUF[7]_inst_i_5/I0
    SLICE_X3Y60          LUT2 (Prop_lut2_I0_O)        0.048     1.712 f  O_OBUF[7]_inst_i_5/O
                         net (fo=3, routed)           0.450     2.162    O_OBUF[7]_inst_i_5_n_0
    SLICE_X2Y61                                                       f  O_OBUF[9]_inst_i_15/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.291 r  O_OBUF[9]_inst_i_15/O
                         net (fo=3, routed)           0.577     2.868    N_899
    SLICE_X3Y59                                                       r  O_OBUF[9]_inst_i_22/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.043     2.911 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.417     3.329    N_932
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_10/I5
    SLICE_X3Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.372 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.784    N_1183
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_7/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.049     3.833 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.447     4.280    N_1198
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_9/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.129     4.409 r  O_OBUF[11]_inst_i_9/O
                         net (fo=2, routed)           0.334     4.743    N_1699
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_5/I2
    SLICE_X2Y63          LUT4 (Prop_lut4_I2_O)        0.043     4.786 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.508     5.294    N_1715
    SLICE_X1Y62                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     5.337 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.448     5.785    N_1732
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_4/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     5.828 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.418     6.247    N_1999
    SLICE_X1Y63                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     6.290 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.420     7.710    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     9.545 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     9.545    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.465ns  (logic 3.045ns (32.175%)  route 6.419ns (67.825%))
  Logic Levels:           12  (IBUF=1 LUT2=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=8, routed)           1.083     1.664    B_IBUF[0]
    SLICE_X3Y60                                                       r  O_OBUF[7]_inst_i_5/I0
    SLICE_X3Y60          LUT2 (Prop_lut2_I0_O)        0.048     1.712 f  O_OBUF[7]_inst_i_5/O
                         net (fo=3, routed)           0.450     2.162    O_OBUF[7]_inst_i_5_n_0
    SLICE_X2Y61                                                       f  O_OBUF[9]_inst_i_15/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.291 r  O_OBUF[9]_inst_i_15/O
                         net (fo=3, routed)           0.577     2.868    N_899
    SLICE_X3Y59                                                       r  O_OBUF[9]_inst_i_22/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.043     2.911 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.417     3.329    N_932
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_10/I5
    SLICE_X3Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.372 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.784    N_1183
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_7/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.049     3.833 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.447     4.280    N_1198
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_9/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.129     4.409 r  O_OBUF[11]_inst_i_9/O
                         net (fo=2, routed)           0.334     4.743    N_1699
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_5/I2
    SLICE_X2Y63          LUT4 (Prop_lut4_I2_O)        0.043     4.786 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.508     5.294    N_1715
    SLICE_X1Y62                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     5.337 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.448     5.785    N_1732
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_4/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     5.828 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.417     6.246    N_1999
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_1/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     6.289 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.324     7.613    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     9.465 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     9.465    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.993ns  (logic 3.177ns (35.320%)  route 5.817ns (64.680%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=8, routed)           1.083     1.664    B_IBUF[0]
    SLICE_X3Y60                                                       r  O_OBUF[7]_inst_i_5/I0
    SLICE_X3Y60          LUT2 (Prop_lut2_I0_O)        0.048     1.712 f  O_OBUF[7]_inst_i_5/O
                         net (fo=3, routed)           0.450     2.162    O_OBUF[7]_inst_i_5_n_0
    SLICE_X2Y61                                                       f  O_OBUF[9]_inst_i_15/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.291 r  O_OBUF[9]_inst_i_15/O
                         net (fo=3, routed)           0.577     2.868    N_899
    SLICE_X3Y59                                                       r  O_OBUF[9]_inst_i_22/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.043     2.911 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.417     3.329    N_932
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_10/I5
    SLICE_X3Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.372 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.784    N_1183
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_7/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.049     3.833 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.447     4.280    N_1198
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_9/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.129     4.409 r  O_OBUF[11]_inst_i_9/O
                         net (fo=2, routed)           0.334     4.743    N_1699
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.049     4.792 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.337     5.129    N_1714
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.127     5.256 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.420     5.676    N_1965
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.051     5.727 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.339     7.066    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     8.993 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.993    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.916ns  (logic 2.892ns (32.431%)  route 6.024ns (67.569%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT6=8 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=8, routed)           1.083     1.664    B_IBUF[0]
    SLICE_X3Y60                                                       r  O_OBUF[7]_inst_i_5/I0
    SLICE_X3Y60          LUT2 (Prop_lut2_I0_O)        0.048     1.712 f  O_OBUF[7]_inst_i_5/O
                         net (fo=3, routed)           0.450     2.162    O_OBUF[7]_inst_i_5_n_0
    SLICE_X2Y61                                                       f  O_OBUF[9]_inst_i_15/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.291 r  O_OBUF[9]_inst_i_15/O
                         net (fo=3, routed)           0.577     2.868    N_899
    SLICE_X3Y59                                                       r  O_OBUF[9]_inst_i_22/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.043     2.911 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.417     3.329    N_932
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_10/I5
    SLICE_X3Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.372 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.480     3.852    N_1183
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_13/I1
    SLICE_X3Y63          LUT6 (Prop_lut6_I1_O)        0.043     3.895 r  O_OBUF[15]_inst_i_13/O
                         net (fo=2, routed)           0.347     4.242    N_1214
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_9/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     4.285 r  O_OBUF[15]_inst_i_9/O
                         net (fo=2, routed)           0.359     4.644    N_1465
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_3/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     4.687 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.413     5.100    N_1483
    SLICE_X1Y62                                                       r  O_OBUF[13]_inst_i_4/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     5.143 r  O_OBUF[13]_inst_i_4/O
                         net (fo=2, routed)           0.575     5.718    N_1748
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     5.761 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.323     7.083    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.916 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.916    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.513ns  (logic 3.039ns (35.698%)  route 5.474ns (64.302%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=8, routed)           1.083     1.664    B_IBUF[0]
    SLICE_X3Y60                                                       r  O_OBUF[7]_inst_i_5/I0
    SLICE_X3Y60          LUT2 (Prop_lut2_I0_O)        0.048     1.712 f  O_OBUF[7]_inst_i_5/O
                         net (fo=3, routed)           0.450     2.162    O_OBUF[7]_inst_i_5_n_0
    SLICE_X2Y61                                                       f  O_OBUF[9]_inst_i_15/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.291 r  O_OBUF[9]_inst_i_15/O
                         net (fo=3, routed)           0.577     2.868    N_899
    SLICE_X3Y59                                                       r  O_OBUF[9]_inst_i_22/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.043     2.911 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.417     3.329    N_932
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_10/I5
    SLICE_X3Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.372 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.784    N_1183
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_7/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.049     3.833 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.447     4.280    N_1198
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_9/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.129     4.409 r  O_OBUF[11]_inst_i_9/O
                         net (fo=2, routed)           0.334     4.743    N_1699
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.049     4.792 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.430     5.222    N_1714
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.127     5.349 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.323     6.672    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.513 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.513    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[0]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.867ns  (logic 2.895ns (36.801%)  route 4.972ns (63.199%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM25                                              0.000     0.000 r  B[0] (IN)
                         net (fo=0)                   0.000     0.000    B[0]
    AM25                                                              r  B_IBUF[0]_inst/I
    AM25                 IBUF (Prop_ibuf_I_O)         0.581     0.581 r  B_IBUF[0]_inst/O
                         net (fo=8, routed)           1.083     1.664    B_IBUF[0]
    SLICE_X3Y60                                                       r  O_OBUF[7]_inst_i_5/I0
    SLICE_X3Y60          LUT2 (Prop_lut2_I0_O)        0.048     1.712 f  O_OBUF[7]_inst_i_5/O
                         net (fo=3, routed)           0.450     2.162    O_OBUF[7]_inst_i_5_n_0
    SLICE_X2Y61                                                       f  O_OBUF[9]_inst_i_15/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.291 r  O_OBUF[9]_inst_i_15/O
                         net (fo=3, routed)           0.577     2.868    N_899
    SLICE_X3Y59                                                       r  O_OBUF[9]_inst_i_22/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.043     2.911 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.417     3.329    N_932
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_10/I5
    SLICE_X3Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.372 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.784    N_1183
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_7/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.049     3.833 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.342     4.175    N_1198
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_3/I2
    SLICE_X3Y62          LUT6 (Prop_lut6_I2_O)        0.129     4.304 r  O_OBUF[11]_inst_i_3/O
                         net (fo=3, routed)           0.413     4.717    N_1698
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.043     4.760 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.277     6.037    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.867 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.867    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.180ns  (logic 2.776ns (38.667%)  route 4.403ns (61.333%))
  Logic Levels:           8  (IBUF=1 LUT5=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.050     1.639    A_IBUF[4]
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_2/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.682 r  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.411     2.094    N_864
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_12/I0
    SLICE_X2Y61          LUT5 (Prop_lut5_I0_O)        0.051     2.145 r  O_OBUF[7]_inst_i_12/O
                         net (fo=4, routed)           0.382     2.526    N_883
    SLICE_X0Y60                                                       r  O_OBUF[9]_inst_i_20/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.132     2.658 r  O_OBUF[9]_inst_i_20/O
                         net (fo=3, routed)           0.422     3.080    N_1149
    SLICE_X4Y60                                                       r  O_OBUF[9]_inst_i_10/I3
    SLICE_X4Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.123 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.501     3.624    N_1415
    SLICE_X3Y61                                                       r  O_OBUF[9]_inst_i_4/I1
    SLICE_X3Y61          LUT6 (Prop_lut6_I1_O)        0.043     3.667 r  O_OBUF[9]_inst_i_4/O
                         net (fo=2, routed)           0.295     3.962    N_1682
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_1/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.043     4.005 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.343     5.348    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.180 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.180    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.758ns  (logic 2.630ns (38.909%)  route 4.129ns (61.091%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.063     1.659    A_IBUF[2]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.702 r  O_OBUF[7]_inst_i_4/O
                         net (fo=2, routed)           0.465     2.167    O_OBUF[7]_inst_i_4_n_0
    SLICE_X2Y59                                                       r  O_OBUF[7]_inst_i_8/I2
    SLICE_X2Y59          LUT6 (Prop_lut6_I2_O)        0.043     2.210 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.506     2.717    N_1383
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_3/I0
    SLICE_X2Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.760 r  O_OBUF[7]_inst_i_3/O
                         net (fo=2, routed)           0.421     3.181    N_1398
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_2/I2
    SLICE_X1Y60          LUT6 (Prop_lut6_I2_O)        0.043     3.224 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.416     3.640    N_1899
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.683 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.257     4.939    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.758 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.758    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.180ns  (logic 2.584ns (41.816%)  route 3.596ns (58.184%))
  Logic Levels:           6  (IBUF=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.063     1.659    A_IBUF[2]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.702 r  O_OBUF[7]_inst_i_4/O
                         net (fo=2, routed)           0.465     2.167    O_OBUF[7]_inst_i_4_n_0
    SLICE_X2Y59                                                       r  O_OBUF[7]_inst_i_8/I2
    SLICE_X2Y59          LUT6 (Prop_lut6_I2_O)        0.043     2.210 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.506     2.717    N_1383
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_3/I0
    SLICE_X2Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.760 r  O_OBUF[7]_inst_i_3/O
                         net (fo=2, routed)           0.304     3.064    N_1398
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_1/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     3.107 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.257     4.363    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.180 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.180    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.733ns  (logic 2.730ns (47.614%)  route 3.003ns (52.386%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              f  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 f  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.052     1.646    A_IBUF[3]
    SLICE_X1Y62                                                       f  O_OBUF[6]_inst_i_3/I1
    SLICE_X1Y62          LUT2 (Prop_lut2_I1_O)        0.049     1.695 r  O_OBUF[6]_inst_i_3/O
                         net (fo=3, routed)           0.291     1.986    O_OBUF[6]_inst_i_3_n_0
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_2/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.115 r  O_OBUF[6]_inst_i_2/O
                         net (fo=1, routed)           0.405     2.521    N_1132
    SLICE_X1Y60                                                       r  O_OBUF[6]_inst_i_1/I2
    SLICE_X1Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.570 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.255     3.824    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.909     5.733 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.733    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




