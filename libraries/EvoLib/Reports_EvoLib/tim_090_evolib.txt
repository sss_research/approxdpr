Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 18:05:51 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_090_evolib.txt -name O
| Design       : mul8_090
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.259ns  (logic 2.816ns (38.789%)  route 4.443ns (61.211%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=13, routed)          1.230     1.816    B_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_11/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     1.859 r  O_OBUF[11]_inst_i_11/O
                         net (fo=2, routed)           0.513     2.372    N_1232
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_8/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.415 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.501     2.917    N_1616
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_6/I1
    SLICE_X1Y64          LUT3 (Prop_lut3_I1_O)        0.048     2.965 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.369     3.334    N_1795
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.133     3.467 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.415     3.882    N_1973
    SLICE_X0Y64                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.127     4.009 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.414     5.423    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.259 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.259    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.102ns  (logic 2.831ns (39.866%)  route 4.271ns (60.134%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=13, routed)          1.230     1.816    B_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_11/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     1.859 r  O_OBUF[11]_inst_i_11/O
                         net (fo=2, routed)           0.513     2.372    N_1232
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_8/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.415 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.501     2.917    N_1616
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_6/I1
    SLICE_X1Y64          LUT3 (Prop_lut3_I1_O)        0.048     2.965 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.369     3.334    N_1795
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.133     3.467 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.330     3.796    N_1973
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y64          LUT6 (Prop_lut6_I1_O)        0.127     3.923 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.328     5.251    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.102 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.102    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.692ns  (logic 2.682ns (40.071%)  route 4.011ns (59.929%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=13, routed)          1.230     1.816    B_IBUF[4]
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_11/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     1.859 r  O_OBUF[11]_inst_i_11/O
                         net (fo=2, routed)           0.513     2.372    N_1232
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_8/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.415 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.501     2.917    N_1616
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_6/I1
    SLICE_X1Y64          LUT3 (Prop_lut3_I1_O)        0.048     2.965 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.369     3.334    N_1795
    SLICE_X2Y64                                                       r  O_OBUF[13]_inst_i_1/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.129     3.463 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.397     4.860    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.692 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.692    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.576ns  (logic 2.692ns (40.934%)  route 3.884ns (59.066%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT3=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              f  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 f  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.175     1.769    A_IBUF[3]
    SLICE_X2Y63                                                       f  O_OBUF[2]_inst_i_5/I0
    SLICE_X2Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.812 r  O_OBUF[2]_inst_i_5/O
                         net (fo=2, routed)           0.505     2.317    O_OBUF[2]_inst_i_5_n_0
    SLICE_X3Y64                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X3Y64          LUT6 (Prop_lut6_I3_O)        0.127     2.444 f  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.416     2.860    N_1602
    SLICE_X1Y64                                                       f  O_OBUF[13]_inst_i_2/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.903 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.439     3.341    O_OBUF[13]_inst_i_2_n_0
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X2Y64          LUT3 (Prop_lut3_I2_O)        0.043     3.384 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.350     4.734    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.576 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.576    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.003ns  (logic 2.637ns (43.937%)  route 3.365ns (56.063%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT3=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              f  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 f  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.175     1.769    A_IBUF[3]
    SLICE_X2Y63                                                       f  O_OBUF[2]_inst_i_5/I0
    SLICE_X2Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.812 r  O_OBUF[2]_inst_i_5/O
                         net (fo=2, routed)           0.505     2.317    O_OBUF[2]_inst_i_5_n_0
    SLICE_X3Y64                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X3Y64          LUT6 (Prop_lut6_I3_O)        0.127     2.444 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.421     2.865    N_1602
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X1Y64          LUT3 (Prop_lut3_I1_O)        0.043     2.908 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.265     4.172    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.003 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.003    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.955ns  (logic 2.639ns (44.318%)  route 3.316ns (55.682%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=13, routed)          1.340     1.926    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_7/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.043     1.969 r  O_OBUF[11]_inst_i_7/O
                         net (fo=3, routed)           0.291     2.260    N_1186
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X1Y63          LUT5 (Prop_lut5_I2_O)        0.126     2.386 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.416     2.801    O_OBUF[11]_inst_i_2_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.844 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.270     4.114    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     5.955 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     5.955    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.738ns  (logic 2.531ns (44.113%)  route 3.207ns (55.887%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=13, routed)          1.340     1.926    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[2]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.043     1.969 f  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.225     2.194    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y63                                                       f  O_OBUF[11]_inst_i_5/I2
    SLICE_X2Y63          LUT5 (Prop_lut5_I2_O)        0.043     2.237 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.422     2.658    N_1425
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.701 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.221     3.922    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.738 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.738    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.652ns  (logic 2.537ns (44.886%)  route 3.115ns (55.114%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=13, routed)          1.340     1.926    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[2]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.043     1.969 f  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.225     2.194    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y63                                                       f  O_OBUF[11]_inst_i_5/I2
    SLICE_X2Y63          LUT5 (Prop_lut5_I2_O)        0.043     2.237 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.422     2.658    N_1425
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.701 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.128     3.830    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.652 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.652    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.650ns  (logic 2.629ns (46.538%)  route 3.021ns (53.462%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=13, routed)          1.340     1.926    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_7/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.043     1.969 r  O_OBUF[11]_inst_i_7/O
                         net (fo=3, routed)           0.221     2.190    N_1186
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.126     2.316 r  O_OBUF[9]_inst_i_4/O
                         net (fo=1, routed)           0.165     2.481    N_1350
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_1/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.524 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.294     3.819    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.650 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.650    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.441ns  (logic 2.488ns (45.730%)  route 2.953ns (54.270%))
  Logic Levels:           4  (IBUF=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=12, routed)          1.303     1.883    A_IBUF[7]
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     1.926 f  O_OBUF[6]_inst_i_2/O
                         net (fo=1, routed)           0.341     2.266    N_90
    SLICE_X0Y62                                                       f  O_OBUF[6]_inst_i_1/I4
    SLICE_X0Y62          LUT5 (Prop_lut5_I4_O)        0.043     2.309 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           1.309     3.618    O_OBUF[1]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     5.441 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.441    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




