Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 09:14:24 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_398_evolib.txt -name O
| Design       : mul8_398
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.194ns  (logic 2.910ns (35.512%)  route 5.284ns (64.488%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=18, routed)          1.220     1.814    A_IBUF[3]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.857 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.410     2.268    N_459
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_15/I2
    SLICE_X1Y62          LUT3 (Prop_lut3_I2_O)        0.048     2.316 r  O_OBUF[9]_inst_i_15/O
                         net (fo=2, routed)           0.515     2.831    N_571
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.129     2.960 r  O_OBUF[10]_inst_i_9/O
                         net (fo=5, routed)           0.586     3.546    N_676
    SLICE_X5Y64                                                       r  O_OBUF[10]_inst_i_4/I5
    SLICE_X5Y64          LUT6 (Prop_lut6_I5_O)        0.043     3.589 r  O_OBUF[10]_inst_i_4/O
                         net (fo=4, routed)           0.494     4.083    N_820
    SLICE_X4Y65                                                       r  O_OBUF[11]_inst_i_4/I2
    SLICE_X4Y65          LUT5 (Prop_lut5_I2_O)        0.043     4.126 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.304     4.430    N_1146
    SLICE_X6Y65                                                       r  O_OBUF[14]_inst_i_6/I0
    SLICE_X6Y65          LUT5 (Prop_lut5_I0_O)        0.050     4.480 r  O_OBUF[14]_inst_i_6/O
                         net (fo=2, routed)           0.441     4.922    O_OBUF[14]_inst_i_6_n_0
    SLICE_X6Y65                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X6Y65          LUT6 (Prop_lut6_I2_O)        0.127     5.049 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.313     6.361    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.194 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.194    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.169ns  (logic 2.837ns (34.736%)  route 5.331ns (65.264%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=18, routed)          1.220     1.814    A_IBUF[3]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.857 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.410     2.268    N_459
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_15/I2
    SLICE_X1Y62          LUT3 (Prop_lut3_I2_O)        0.048     2.316 r  O_OBUF[9]_inst_i_15/O
                         net (fo=2, routed)           0.515     2.831    N_571
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.129     2.960 r  O_OBUF[10]_inst_i_9/O
                         net (fo=5, routed)           0.586     3.546    N_676
    SLICE_X5Y64                                                       r  O_OBUF[10]_inst_i_4/I5
    SLICE_X5Y64          LUT6 (Prop_lut6_I5_O)        0.043     3.589 r  O_OBUF[10]_inst_i_4/O
                         net (fo=4, routed)           0.494     4.083    N_820
    SLICE_X4Y65                                                       r  O_OBUF[11]_inst_i_4/I2
    SLICE_X4Y65          LUT5 (Prop_lut5_I2_O)        0.043     4.126 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.304     4.430    N_1146
    SLICE_X6Y65                                                       r  O_OBUF[15]_inst_i_6/I3
    SLICE_X6Y65          LUT5 (Prop_lut5_I3_O)        0.043     4.473 r  O_OBUF[15]_inst_i_6/O
                         net (fo=1, routed)           0.492     4.965    O_OBUF[15]_inst_i_6_n_0
    SLICE_X4Y66                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X4Y66          LUT6 (Prop_lut6_I4_O)        0.043     5.008 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.310     6.317    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.169 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.169    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.168ns  (logic 2.918ns (35.729%)  route 5.250ns (64.271%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=18, routed)          1.220     1.814    A_IBUF[3]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.857 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.410     2.268    N_459
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_15/I2
    SLICE_X1Y62          LUT3 (Prop_lut3_I2_O)        0.048     2.316 r  O_OBUF[9]_inst_i_15/O
                         net (fo=2, routed)           0.515     2.831    N_571
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.129     2.960 r  O_OBUF[10]_inst_i_9/O
                         net (fo=5, routed)           0.437     3.397    N_676
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_12/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     3.440 f  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.616     4.056    O_OBUF[15]_inst_i_12_n_0
    SLICE_X2Y65                                                       f  O_OBUF[10]_inst_i_6/I0
    SLICE_X2Y65          LUT6 (Prop_lut6_I0_O)        0.043     4.099 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.445     4.544    O_OBUF[10]_inst_i_6_n_0
    SLICE_X4Y66                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X4Y66          LUT4 (Prop_lut4_I1_O)        0.043     4.587 r  O_OBUF[11]_inst_i_5/O
                         net (fo=1, routed)           0.343     4.930    O_OBUF[11]_inst_i_5_n_0
    SLICE_X4Y66                                                       r  O_OBUF[11]_inst_i_1/I4
    SLICE_X4Y66          LUT5 (Prop_lut5_I4_O)        0.048     4.978 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.263     6.241    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.927     8.168 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.168    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.030ns  (logic 2.822ns (35.139%)  route 5.208ns (64.861%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT3=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=18, routed)          1.220     1.814    A_IBUF[3]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.857 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.410     2.268    N_459
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_15/I2
    SLICE_X1Y62          LUT3 (Prop_lut3_I2_O)        0.048     2.316 r  O_OBUF[9]_inst_i_15/O
                         net (fo=2, routed)           0.515     2.831    N_571
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.129     2.960 r  O_OBUF[10]_inst_i_9/O
                         net (fo=5, routed)           0.437     3.397    N_676
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_12/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     3.440 f  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.519     3.959    O_OBUF[15]_inst_i_12_n_0
    SLICE_X2Y65                                                       f  O_OBUF[15]_inst_i_3/I1
    SLICE_X2Y65          LUT2 (Prop_lut2_I1_O)        0.043     4.002 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.505     4.507    N_1072
    SLICE_X4Y65                                                       r  O_OBUF[14]_inst_i_7/I2
    SLICE_X4Y65          LUT4 (Prop_lut4_I2_O)        0.043     4.550 r  O_OBUF[14]_inst_i_7/O
                         net (fo=1, routed)           0.292     4.842    O_OBUF[14]_inst_i_7_n_0
    SLICE_X5Y65                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X5Y65          LUT6 (Prop_lut6_I5_O)        0.043     4.885 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.310     6.195    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.030 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.030    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.995ns  (logic 2.828ns (35.371%)  route 5.167ns (64.629%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=18, routed)          1.220     1.814    A_IBUF[3]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.857 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.410     2.268    N_459
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_15/I2
    SLICE_X1Y62          LUT3 (Prop_lut3_I2_O)        0.048     2.316 r  O_OBUF[9]_inst_i_15/O
                         net (fo=2, routed)           0.515     2.831    N_571
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.129     2.960 r  O_OBUF[10]_inst_i_9/O
                         net (fo=5, routed)           0.586     3.546    N_676
    SLICE_X5Y64                                                       r  O_OBUF[10]_inst_i_4/I5
    SLICE_X5Y64          LUT6 (Prop_lut6_I5_O)        0.043     3.589 r  O_OBUF[10]_inst_i_4/O
                         net (fo=4, routed)           0.494     4.083    N_820
    SLICE_X4Y65                                                       r  O_OBUF[11]_inst_i_4/I2
    SLICE_X4Y65          LUT5 (Prop_lut5_I2_O)        0.043     4.126 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.408     4.534    N_1146
    SLICE_X4Y66                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     4.577 r  O_OBUF[12]_inst_i_3/O
                         net (fo=1, routed)           0.251     4.828    O_OBUF[12]_inst_i_3_n_0
    SLICE_X5Y66                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X5Y66          LUT6 (Prop_lut6_I2_O)        0.043     4.871 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.282     6.153    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.995 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.995    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.644ns  (logic 2.773ns (36.283%)  route 4.870ns (63.717%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=18, routed)          1.220     1.814    A_IBUF[3]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.857 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.410     2.268    N_459
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_15/I2
    SLICE_X1Y62          LUT3 (Prop_lut3_I2_O)        0.048     2.316 r  O_OBUF[9]_inst_i_15/O
                         net (fo=2, routed)           0.515     2.831    N_571
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.129     2.960 r  O_OBUF[10]_inst_i_9/O
                         net (fo=5, routed)           0.437     3.397    N_676
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_12/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     3.440 f  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.616     4.056    O_OBUF[15]_inst_i_12_n_0
    SLICE_X2Y65                                                       f  O_OBUF[10]_inst_i_6/I0
    SLICE_X2Y65          LUT6 (Prop_lut6_I0_O)        0.043     4.099 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.400     4.499    O_OBUF[10]_inst_i_6_n_0
    SLICE_X5Y65                                                       r  O_OBUF[10]_inst_i_1/I5
    SLICE_X5Y65          LUT6 (Prop_lut6_I5_O)        0.043     4.542 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.272     5.813    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.644 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.644    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.586ns  (logic 2.775ns (36.577%)  route 4.811ns (63.423%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=19, routed)          1.139     1.732    A_IBUF[1]
    SLICE_X2Y63                                                       r  O_OBUF[5]_inst_i_5/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.043     1.775 r  O_OBUF[5]_inst_i_5/O
                         net (fo=5, routed)           0.433     2.208    N_444
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_8/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.049     2.257 r  O_OBUF[7]_inst_i_8/O
                         net (fo=1, routed)           0.681     2.938    N_553
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X1Y64          LUT6 (Prop_lut6_I2_O)        0.129     3.067 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.407     3.474    N_735
    SLICE_X1Y65                                                       r  O_OBUF[7]_inst_i_4/I1
    SLICE_X1Y65          LUT2 (Prop_lut2_I1_O)        0.043     3.517 f  O_OBUF[7]_inst_i_4/O
                         net (fo=4, routed)           0.422     3.939    O_OBUF[7]_inst_i_4_n_0
    SLICE_X3Y65                                                       f  O_OBUF[9]_inst_i_6/I2
    SLICE_X3Y65          LUT5 (Prop_lut5_I2_O)        0.043     3.982 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.424     4.407    N_966
    SLICE_X2Y65                                                       r  O_OBUF[9]_inst_i_1/I4
    SLICE_X2Y65          LUT6 (Prop_lut6_I4_O)        0.043     4.450 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.305     5.754    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.586 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.586    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.464ns  (logic 2.762ns (37.005%)  route 4.702ns (62.995%))
  Logic Levels:           8  (IBUF=1 LUT2=2 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=19, routed)          1.139     1.732    A_IBUF[1]
    SLICE_X2Y63                                                       r  O_OBUF[5]_inst_i_5/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.043     1.775 r  O_OBUF[5]_inst_i_5/O
                         net (fo=5, routed)           0.433     2.208    N_444
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_8/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.049     2.257 r  O_OBUF[7]_inst_i_8/O
                         net (fo=1, routed)           0.681     2.938    N_553
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X1Y64          LUT6 (Prop_lut6_I2_O)        0.129     3.067 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.407     3.474    N_735
    SLICE_X1Y65                                                       r  O_OBUF[7]_inst_i_4/I1
    SLICE_X1Y65          LUT2 (Prop_lut2_I1_O)        0.043     3.517 f  O_OBUF[7]_inst_i_4/O
                         net (fo=4, routed)           0.422     3.939    O_OBUF[7]_inst_i_4_n_0
    SLICE_X3Y65                                                       f  O_OBUF[9]_inst_i_6/I2
    SLICE_X3Y65          LUT5 (Prop_lut5_I2_O)        0.043     3.982 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.344     4.326    N_966
    SLICE_X3Y65                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X3Y65          LUT2 (Prop_lut2_I1_O)        0.043     4.369 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.276     5.645    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.464 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.464    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.209ns  (logic 2.895ns (40.152%)  route 4.315ns (59.848%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT4=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=19, routed)          1.139     1.732    A_IBUF[1]
    SLICE_X2Y63                                                       r  O_OBUF[5]_inst_i_5/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.043     1.775 r  O_OBUF[5]_inst_i_5/O
                         net (fo=5, routed)           0.433     2.208    N_444
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_8/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.049     2.257 r  O_OBUF[7]_inst_i_8/O
                         net (fo=1, routed)           0.681     2.938    N_553
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X1Y64          LUT6 (Prop_lut6_I2_O)        0.129     3.067 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.407     3.474    N_735
    SLICE_X1Y65                                                       r  O_OBUF[7]_inst_i_2/I1
    SLICE_X1Y65          LUT2 (Prop_lut2_I1_O)        0.051     3.525 r  O_OBUF[7]_inst_i_2/O
                         net (fo=6, routed)           0.436     3.961    N_808
    SLICE_X2Y65                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y65          LUT4 (Prop_lut4_I0_O)        0.129     4.090 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.218     5.309    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.900     7.209 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     7.209    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.043ns  (logic 2.817ns (39.997%)  route 4.226ns (60.003%))
  Logic Levels:           7  (IBUF=1 LUT2=2 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=19, routed)          1.139     1.732    A_IBUF[1]
    SLICE_X2Y63                                                       r  O_OBUF[5]_inst_i_5/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.043     1.775 r  O_OBUF[5]_inst_i_5/O
                         net (fo=5, routed)           0.433     2.208    N_444
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_8/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.049     2.257 r  O_OBUF[7]_inst_i_8/O
                         net (fo=1, routed)           0.681     2.938    N_553
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X1Y64          LUT6 (Prop_lut6_I2_O)        0.129     3.067 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.407     3.474    N_735
    SLICE_X1Y65                                                       r  O_OBUF[7]_inst_i_2/I1
    SLICE_X1Y65          LUT2 (Prop_lut2_I1_O)        0.051     3.525 r  O_OBUF[7]_inst_i_2/O
                         net (fo=6, routed)           0.436     3.961    N_808
    SLICE_X2Y65                                                       r  O_OBUF[6]_inst_i_1/I1
    SLICE_X2Y65          LUT2 (Prop_lut2_I1_O)        0.129     4.090 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.130     5.220    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     7.043 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     7.043    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




