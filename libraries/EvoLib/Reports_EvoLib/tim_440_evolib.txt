Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 11:23:54 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_440_evolib.txt -name O
| Design       : mul8_440
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.758ns  (logic 2.771ns (35.722%)  route 4.987ns (64.278%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=26, routed)          1.321     1.908    A_IBUF[6]
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_20/I1
    SLICE_X2Y64          LUT6 (Prop_lut6_I1_O)        0.043     1.951 f  O_OBUF[11]_inst_i_20/O
                         net (fo=4, routed)           0.509     2.460    O_OBUF[11]_inst_i_20_n_0
    SLICE_X3Y64                                                       f  O_OBUF[13]_inst_i_11/I2
    SLICE_X3Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.503 r  O_OBUF[13]_inst_i_11/O
                         net (fo=2, routed)           0.500     3.003    O_OBUF[13]_inst_i_11_n_0
    SLICE_X4Y66                                                       r  O_OBUF[11]_inst_i_6/I1
    SLICE_X4Y66          LUT3 (Prop_lut3_I1_O)        0.043     3.046 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.446     3.492    O_OBUF[11]_inst_i_6_n_0
    SLICE_X2Y66                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.535 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.447     3.982    O_OBUF[13]_inst_i_3_n_0
    SLICE_X2Y66                                                       r  O_OBUF[15]_inst_i_2/I1
    SLICE_X2Y66          LUT6 (Prop_lut6_I1_O)        0.043     4.025 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.493     4.518    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y67                                                       r  O_OBUF[14]_inst_i_1/I0
    SLICE_X0Y67          LUT5 (Prop_lut5_I0_O)        0.048     4.566 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.270     5.836    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.922     7.758 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.758    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.731ns  (logic 2.696ns (34.874%)  route 5.035ns (65.127%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=26, routed)          1.321     1.908    A_IBUF[6]
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_20/I1
    SLICE_X2Y64          LUT6 (Prop_lut6_I1_O)        0.043     1.951 f  O_OBUF[11]_inst_i_20/O
                         net (fo=4, routed)           0.509     2.460    O_OBUF[11]_inst_i_20_n_0
    SLICE_X3Y64                                                       f  O_OBUF[13]_inst_i_11/I2
    SLICE_X3Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.503 r  O_OBUF[13]_inst_i_11/O
                         net (fo=2, routed)           0.500     3.003    O_OBUF[13]_inst_i_11_n_0
    SLICE_X4Y66                                                       r  O_OBUF[11]_inst_i_6/I1
    SLICE_X4Y66          LUT3 (Prop_lut3_I1_O)        0.043     3.046 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.446     3.492    O_OBUF[11]_inst_i_6_n_0
    SLICE_X2Y66                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.535 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.447     3.982    O_OBUF[13]_inst_i_3_n_0
    SLICE_X2Y66                                                       r  O_OBUF[15]_inst_i_2/I1
    SLICE_X2Y66          LUT6 (Prop_lut6_I1_O)        0.043     4.025 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.493     4.518    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y67                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y67          LUT5 (Prop_lut5_I4_O)        0.043     4.561 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.318     5.879    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.731 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.731    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.033ns  (logic 2.731ns (38.833%)  route 4.302ns (61.167%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=26, routed)          1.321     1.908    A_IBUF[6]
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_20/I1
    SLICE_X2Y64          LUT6 (Prop_lut6_I1_O)        0.043     1.951 f  O_OBUF[11]_inst_i_20/O
                         net (fo=4, routed)           0.509     2.460    O_OBUF[11]_inst_i_20_n_0
    SLICE_X3Y64                                                       f  O_OBUF[13]_inst_i_11/I2
    SLICE_X3Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.503 r  O_OBUF[13]_inst_i_11/O
                         net (fo=2, routed)           0.500     3.003    O_OBUF[13]_inst_i_11_n_0
    SLICE_X4Y66                                                       r  O_OBUF[11]_inst_i_6/I1
    SLICE_X4Y66          LUT3 (Prop_lut3_I1_O)        0.043     3.046 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.446     3.492    O_OBUF[11]_inst_i_6_n_0
    SLICE_X2Y66                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.535 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.235     3.770    O_OBUF[13]_inst_i_3_n_0
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X2Y66          LUT3 (Prop_lut3_I2_O)        0.047     3.817 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.291     5.108    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.926     7.033 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.033    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.921ns  (logic 2.634ns (38.065%)  route 4.286ns (61.935%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=26, routed)          1.321     1.908    A_IBUF[6]
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_20/I1
    SLICE_X2Y64          LUT6 (Prop_lut6_I1_O)        0.043     1.951 f  O_OBUF[11]_inst_i_20/O
                         net (fo=4, routed)           0.509     2.460    O_OBUF[11]_inst_i_20_n_0
    SLICE_X3Y64                                                       f  O_OBUF[13]_inst_i_11/I2
    SLICE_X3Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.503 r  O_OBUF[13]_inst_i_11/O
                         net (fo=2, routed)           0.500     3.003    O_OBUF[13]_inst_i_11_n_0
    SLICE_X4Y66                                                       r  O_OBUF[11]_inst_i_6/I1
    SLICE_X4Y66          LUT3 (Prop_lut3_I1_O)        0.043     3.046 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.446     3.492    O_OBUF[11]_inst_i_6_n_0
    SLICE_X2Y66                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.043     3.535 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.235     3.770    O_OBUF[13]_inst_i_3_n_0
    SLICE_X2Y66                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X2Y66          LUT5 (Prop_lut5_I1_O)        0.043     3.813 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.275     5.088    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.921 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.921    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.892ns  (logic 2.627ns (38.116%)  route 4.265ns (61.884%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=13, routed)          1.289     1.882    A_IBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[5]_inst_i_5/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.043     1.925 r  O_OBUF[5]_inst_i_5/O
                         net (fo=5, routed)           0.373     2.299    O_OBUF[5]_inst_i_5_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.342 r  O_OBUF[9]_inst_i_10/O
                         net (fo=1, routed)           0.520     2.861    O_OBUF[9]_inst_i_10_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y63          LUT3 (Prop_lut3_I0_O)        0.043     2.904 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.446     3.350    O_OBUF[9]_inst_i_6_n_0
    SLICE_X2Y65                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X2Y65          LUT6 (Prop_lut6_I0_O)        0.043     3.393 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.361     3.754    O_OBUF[8]_inst_i_2_n_0
    SLICE_X2Y65                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X2Y65          LUT5 (Prop_lut5_I0_O)        0.043     3.797 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.276     5.073    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.892 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.892    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.802ns  (logic 2.717ns (39.941%)  route 4.085ns (60.059%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=13, routed)          1.289     1.882    A_IBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[5]_inst_i_5/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.043     1.925 r  O_OBUF[5]_inst_i_5/O
                         net (fo=5, routed)           0.373     2.299    O_OBUF[5]_inst_i_5_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.342 r  O_OBUF[9]_inst_i_10/O
                         net (fo=1, routed)           0.520     2.861    O_OBUF[9]_inst_i_10_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y63          LUT3 (Prop_lut3_I0_O)        0.043     2.904 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.446     3.350    O_OBUF[9]_inst_i_6_n_0
    SLICE_X2Y65                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X2Y65          LUT6 (Prop_lut6_I0_O)        0.043     3.393 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.334     3.727    O_OBUF[8]_inst_i_2_n_0
    SLICE_X1Y66                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X1Y66          LUT5 (Prop_lut5_I3_O)        0.049     3.776 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.123     4.899    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.902     6.802 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.802    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.587ns  (logic 2.695ns (40.912%)  route 3.892ns (59.088%))
  Logic Levels:           6  (IBUF=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.279     1.866    A_IBUF[5]
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_14/I1
    SLICE_X0Y64          LUT6 (Prop_lut6_I1_O)        0.043     1.909 r  O_OBUF[11]_inst_i_14/O
                         net (fo=3, routed)           0.442     2.351    O_OBUF[11]_inst_i_14_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_10/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.043     2.394 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.522     2.916    O_OBUF[11]_inst_i_10_n_0
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X2Y64          LUT5 (Prop_lut5_I2_O)        0.048     2.964 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.443     3.407    O_OBUF[11]_inst_i_2_n_0
    SLICE_X2Y66                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X2Y66          LUT6 (Prop_lut6_I0_O)        0.132     3.539 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.207     4.746    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.587 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.587    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.549ns  (logic 2.640ns (40.308%)  route 3.909ns (59.692%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=13, routed)          1.289     1.882    A_IBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[5]_inst_i_5/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.043     1.925 r  O_OBUF[5]_inst_i_5/O
                         net (fo=5, routed)           0.373     2.299    O_OBUF[5]_inst_i_5_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.342 r  O_OBUF[9]_inst_i_10/O
                         net (fo=1, routed)           0.520     2.861    O_OBUF[9]_inst_i_10_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_6/I0
    SLICE_X2Y63          LUT3 (Prop_lut3_I0_O)        0.043     2.904 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.435     3.339    O_OBUF[9]_inst_i_6_n_0
    SLICE_X2Y65                                                       r  O_OBUF[9]_inst_i_3/I3
    SLICE_X2Y65          LUT6 (Prop_lut6_I3_O)        0.043     3.382 r  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.102     3.484    O_OBUF[9]_inst_i_3_n_0
    SLICE_X2Y65                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X2Y65          LUT6 (Prop_lut6_I1_O)        0.043     3.527 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.190     4.717    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.549 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.549    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.543ns  (logic 2.684ns (41.016%)  route 3.860ns (58.984%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.279     1.866    A_IBUF[5]
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_14/I1
    SLICE_X0Y64          LUT6 (Prop_lut6_I1_O)        0.043     1.909 r  O_OBUF[11]_inst_i_14/O
                         net (fo=3, routed)           0.442     2.351    O_OBUF[11]_inst_i_14_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_10/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.043     2.394 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.522     2.916    O_OBUF[11]_inst_i_10_n_0
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X2Y64          LUT5 (Prop_lut5_I2_O)        0.048     2.964 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.338     3.302    O_OBUF[11]_inst_i_2_n_0
    SLICE_X1Y66                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X1Y66          LUT4 (Prop_lut4_I3_O)        0.132     3.434 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.280     4.713    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.543 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.543    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.426ns  (logic 2.588ns (40.272%)  route 3.838ns (59.728%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=13, routed)          1.289     1.882    A_IBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[5]_inst_i_5/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.043     1.925 r  O_OBUF[5]_inst_i_5/O
                         net (fo=5, routed)           0.759     2.684    O_OBUF[5]_inst_i_5_n_0
    SLICE_X3Y65                                                       r  O_OBUF[5]_inst_i_3/I0
    SLICE_X3Y65          LUT6 (Prop_lut6_I0_O)        0.043     2.727 r  O_OBUF[5]_inst_i_3/O
                         net (fo=3, routed)           0.308     3.035    O_OBUF[5]_inst_i_3_n_0
    SLICE_X0Y66                                                       r  O_OBUF[7]_inst_i_4/I3
    SLICE_X0Y66          LUT5 (Prop_lut5_I3_O)        0.043     3.078 r  O_OBUF[7]_inst_i_4/O
                         net (fo=2, routed)           0.334     3.412    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y66                                                       r  O_OBUF[6]_inst_i_1/I2
    SLICE_X1Y66          LUT3 (Prop_lut3_I2_O)        0.043     3.455 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.148     4.603    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.426 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.426    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




