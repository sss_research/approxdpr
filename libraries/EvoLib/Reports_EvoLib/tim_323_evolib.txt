Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 05:31:50 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_323_evolib.txt -name O
| Design       : mul8_323
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.228ns  (logic 2.955ns (35.916%)  route 5.273ns (64.084%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=4 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.489     2.079    A_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_17/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.122 r  O_OBUF[12]_inst_i_17/O
                         net (fo=2, routed)           0.404     2.526    N_1186
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_8/I0
    SLICE_X1Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.569 r  O_OBUF[12]_inst_i_8/O
                         net (fo=3, routed)           0.441     3.010    N_1351
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_15/I1
    SLICE_X0Y64          LUT3 (Prop_lut3_I1_O)        0.043     3.053 r  O_OBUF[12]_inst_i_15/O
                         net (fo=2, routed)           0.513     3.566    N_1602
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y64          LUT5 (Prop_lut5_I3_O)        0.043     3.609 r  O_OBUF[12]_inst_i_4/O
                         net (fo=3, routed)           0.679     4.288    N_1765
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X0Y65          LUT5 (Prop_lut5_I3_O)        0.135     4.423 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.403     4.825    N_1957
    SLICE_X0Y66                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X0Y66          LUT5 (Prop_lut5_I2_O)        0.137     4.962 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.344     6.307    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.922     8.228 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.228    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.159ns  (logic 2.877ns (35.261%)  route 5.282ns (64.739%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.489     2.079    A_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_17/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.122 r  O_OBUF[12]_inst_i_17/O
                         net (fo=2, routed)           0.404     2.526    N_1186
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_8/I0
    SLICE_X1Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.569 r  O_OBUF[12]_inst_i_8/O
                         net (fo=3, routed)           0.441     3.010    N_1351
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_15/I1
    SLICE_X0Y64          LUT3 (Prop_lut3_I1_O)        0.043     3.053 r  O_OBUF[12]_inst_i_15/O
                         net (fo=2, routed)           0.513     3.566    N_1602
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y64          LUT5 (Prop_lut5_I3_O)        0.043     3.609 r  O_OBUF[12]_inst_i_4/O
                         net (fo=3, routed)           0.679     4.288    N_1765
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X0Y65          LUT5 (Prop_lut5_I3_O)        0.135     4.423 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.418     4.840    N_1957
    SLICE_X0Y66                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X0Y66          LUT6 (Prop_lut6_I2_O)        0.129     4.969 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.338     6.308    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.159 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.159    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.058ns  (logic 2.858ns (35.474%)  route 5.199ns (64.526%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.489     2.079    A_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_17/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.122 r  O_OBUF[12]_inst_i_17/O
                         net (fo=2, routed)           0.404     2.526    N_1186
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_8/I0
    SLICE_X1Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.569 r  O_OBUF[12]_inst_i_8/O
                         net (fo=3, routed)           0.441     3.010    N_1351
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_15/I1
    SLICE_X0Y64          LUT3 (Prop_lut3_I1_O)        0.043     3.053 r  O_OBUF[12]_inst_i_15/O
                         net (fo=2, routed)           0.513     3.566    N_1602
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y64          LUT5 (Prop_lut5_I3_O)        0.043     3.609 r  O_OBUF[12]_inst_i_4/O
                         net (fo=3, routed)           0.679     4.288    N_1765
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X0Y65          LUT5 (Prop_lut5_I3_O)        0.135     4.423 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.403     4.825    N_1957
    SLICE_X0Y66                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X0Y66          LUT3 (Prop_lut3_I1_O)        0.129     4.954 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.271     6.225    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.058 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.058    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.527ns  (logic 2.730ns (36.273%)  route 4.797ns (63.727%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.489     2.079    A_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_17/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.122 r  O_OBUF[12]_inst_i_17/O
                         net (fo=2, routed)           0.404     2.526    N_1186
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_8/I0
    SLICE_X1Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.569 r  O_OBUF[12]_inst_i_8/O
                         net (fo=3, routed)           0.441     3.010    N_1351
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_15/I1
    SLICE_X0Y64          LUT3 (Prop_lut3_I1_O)        0.043     3.053 r  O_OBUF[12]_inst_i_15/O
                         net (fo=2, routed)           0.513     3.566    N_1602
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y64          LUT5 (Prop_lut5_I3_O)        0.043     3.609 r  O_OBUF[12]_inst_i_4/O
                         net (fo=3, routed)           0.679     4.288    N_1765
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.127     4.415 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.271     5.685    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.527 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.527    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.419ns  (logic 2.875ns (38.747%)  route 4.545ns (61.253%))
  Logic Levels:           8  (IBUF=1 LUT3=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.249     1.838    A_IBUF[4]
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_20/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     1.881 r  O_OBUF[12]_inst_i_20/O
                         net (fo=2, routed)           0.424     2.306    N_1172
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_6/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.349 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.404     2.753    N_1334
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X1Y62          LUT3 (Prop_lut3_I0_O)        0.051     2.804 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.437     3.241    N_1572
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.135     3.376 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.516     3.892    N_1735
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X1Y65          LUT5 (Prop_lut5_I2_O)        0.129     4.021 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.238     4.259    N_1929
    SLICE_X0Y66                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X0Y66          LUT3 (Prop_lut3_I1_O)        0.043     4.302 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.277     5.578    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.419 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.419    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.142ns  (logic 2.912ns (40.772%)  route 4.230ns (59.228%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.249     1.838    A_IBUF[4]
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_20/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     1.881 r  O_OBUF[12]_inst_i_20/O
                         net (fo=2, routed)           0.424     2.306    N_1172
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_6/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.349 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.404     2.753    N_1334
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X1Y62          LUT3 (Prop_lut3_I0_O)        0.051     2.804 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.437     3.241    N_1572
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.135     3.376 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.516     3.892    N_1735
    SLICE_X1Y65                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X1Y65          LUT5 (Prop_lut5_I1_O)        0.134     4.026 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.200     5.225    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.916     7.142 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.142    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.953ns  (logic 2.822ns (40.586%)  route 4.131ns (59.414%))
  Logic Levels:           7  (IBUF=1 LUT3=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.249     1.838    A_IBUF[4]
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_20/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     1.881 r  O_OBUF[12]_inst_i_20/O
                         net (fo=2, routed)           0.424     2.306    N_1172
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_6/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.349 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.404     2.753    N_1334
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X1Y62          LUT3 (Prop_lut3_I0_O)        0.051     2.804 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.437     3.241    N_1572
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.135     3.376 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.417     3.793    N_1735
    SLICE_X1Y65                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X1Y65          LUT3 (Prop_lut3_I0_O)        0.129     3.922 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.200     5.122    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.953 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.953    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.460ns  (logic 2.724ns (42.168%)  route 3.736ns (57.832%))
  Logic Levels:           7  (IBUF=1 LUT4=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=12, routed)          1.450     2.043    A_IBUF[1]
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_5/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.086 r  O_OBUF[6]_inst_i_5/O
                         net (fo=2, routed)           0.219     2.305    N_1142
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X0Y62          LUT5 (Prop_lut5_I2_O)        0.043     2.348 r  O_OBUF[6]_inst_i_3/O
                         net (fo=2, routed)           0.326     2.674    N_1306
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.052     2.726 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.336     3.063    N_1543
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_5/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.131     3.194 r  O_OBUF[8]_inst_i_5/O
                         net (fo=1, routed)           0.224     3.418    N_1721
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_1/I3
    SLICE_X1Y64          LUT4 (Prop_lut4_I3_O)        0.043     3.461 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.180     4.641    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.460 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.460    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.157ns  (logic 2.679ns (43.502%)  route 3.479ns (56.498%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=12, routed)          1.450     2.043    A_IBUF[1]
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_5/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.086 r  O_OBUF[6]_inst_i_5/O
                         net (fo=2, routed)           0.219     2.305    N_1142
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X0Y62          LUT5 (Prop_lut5_I2_O)        0.043     2.348 r  O_OBUF[6]_inst_i_3/O
                         net (fo=2, routed)           0.326     2.674    N_1306
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.052     2.726 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.223     2.950    N_1543
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.131     3.081 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.260     4.341    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.157 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.157    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.753ns  (logic 2.545ns (44.237%)  route 3.208ns (55.763%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=12, routed)          1.450     2.043    A_IBUF[1]
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_5/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.086 r  O_OBUF[6]_inst_i_5/O
                         net (fo=2, routed)           0.219     2.305    N_1142
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X0Y62          LUT5 (Prop_lut5_I2_O)        0.043     2.348 r  O_OBUF[6]_inst_i_3/O
                         net (fo=2, routed)           0.326     2.674    N_1306
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.043     2.717 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.213     3.930    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     5.753 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.753    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




