Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 20:22:17 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_135_evolib.txt -name O
| Design       : mul8_135
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.806ns  (logic 2.849ns (36.498%)  route 4.957ns (63.502%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT5=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=11, routed)          1.397     2.002    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_12/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.045 r  O_OBUF[11]_inst_i_12/O
                         net (fo=2, routed)           0.359     2.404    N_1069
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_8/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.447 r  O_OBUF[11]_inst_i_8/O
                         net (fo=3, routed)           0.430     2.877    N_1335
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_2/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.920 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.524     3.444    N_1586
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.487 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.339     3.826    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y65                                                       f  O_OBUF[13]_inst_i_3/I1
    SLICE_X0Y65          LUT5 (Prop_lut5_I1_O)        0.043     3.869 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.253     4.122    N_1943
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_3/I2
    SLICE_X1Y65          LUT5 (Prop_lut5_I2_O)        0.049     4.171 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.228     4.398    N_1973
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X1Y65          LUT6 (Prop_lut6_I1_O)        0.129     4.527 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.428     5.955    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.806 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.806    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.717ns  (logic 2.833ns (36.716%)  route 4.884ns (63.284%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT5=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=11, routed)          1.397     2.002    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_12/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.045 r  O_OBUF[11]_inst_i_12/O
                         net (fo=2, routed)           0.359     2.404    N_1069
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_8/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.447 r  O_OBUF[11]_inst_i_8/O
                         net (fo=3, routed)           0.430     2.877    N_1335
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_2/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.920 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.524     3.444    N_1586
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.487 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.339     3.826    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y65                                                       f  O_OBUF[13]_inst_i_3/I1
    SLICE_X0Y65          LUT5 (Prop_lut5_I1_O)        0.043     3.869 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.253     4.122    N_1943
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_3/I2
    SLICE_X1Y65          LUT5 (Prop_lut5_I2_O)        0.049     4.171 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.226     4.396    N_1973
    SLICE_X1Y65                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X1Y65          LUT6 (Prop_lut6_I4_O)        0.129     4.525 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.356     5.881    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.717 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.717    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.377ns  (logic 2.704ns (36.663%)  route 4.672ns (63.337%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=11, routed)          1.397     2.002    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_12/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.045 r  O_OBUF[11]_inst_i_12/O
                         net (fo=2, routed)           0.359     2.404    N_1069
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_8/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.447 r  O_OBUF[11]_inst_i_8/O
                         net (fo=3, routed)           0.430     2.877    N_1335
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_2/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.920 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.524     3.444    N_1586
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.487 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.339     3.826    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y65                                                       f  O_OBUF[13]_inst_i_3/I1
    SLICE_X0Y65          LUT5 (Prop_lut5_I1_O)        0.043     3.869 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.349     4.218    N_1943
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X0Y65          LUT3 (Prop_lut3_I1_O)        0.043     4.261 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.274     5.535    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.377 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.377    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.334ns  (logic 2.696ns (36.756%)  route 4.638ns (63.244%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=11, routed)          1.397     2.002    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_12/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.045 r  O_OBUF[11]_inst_i_12/O
                         net (fo=2, routed)           0.359     2.404    N_1069
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_8/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.447 r  O_OBUF[11]_inst_i_8/O
                         net (fo=3, routed)           0.430     2.877    N_1335
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_2/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.920 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.524     3.444    N_1586
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.487 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.339     3.826    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y65                                                       f  O_OBUF[13]_inst_i_3/I1
    SLICE_X0Y65          LUT5 (Prop_lut5_I1_O)        0.043     3.869 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.253     4.122    N_1943
    SLICE_X1Y65                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X1Y65          LUT5 (Prop_lut5_I1_O)        0.043     4.165 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.336     5.501    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.334 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.334    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.075ns  (logic 2.753ns (38.913%)  route 4.322ns (61.087%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=11, routed)          1.397     2.002    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_12/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.045 r  O_OBUF[11]_inst_i_12/O
                         net (fo=2, routed)           0.359     2.404    N_1069
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_8/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.447 r  O_OBUF[11]_inst_i_8/O
                         net (fo=3, routed)           0.430     2.877    N_1335
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_2/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.920 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.524     3.444    N_1586
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.487 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.339     3.826    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y65          LUT5 (Prop_lut5_I0_O)        0.049     3.875 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.273     5.147    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.927     7.075 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.075    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.992ns  (logic 2.650ns (37.903%)  route 4.342ns (62.097%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=11, routed)          1.397     2.002    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_12/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.045 r  O_OBUF[11]_inst_i_12/O
                         net (fo=2, routed)           0.359     2.404    N_1069
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_8/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.447 r  O_OBUF[11]_inst_i_8/O
                         net (fo=3, routed)           0.430     2.877    N_1335
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_2/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.920 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.524     3.444    N_1586
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.487 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.432     3.919    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X0Y65          LUT3 (Prop_lut3_I2_O)        0.043     3.962 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.199     5.161    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.992 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.992    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.505ns  (logic 2.608ns (40.095%)  route 3.897ns (59.905%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=11, routed)          1.397     2.002    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_12/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.045 r  O_OBUF[11]_inst_i_12/O
                         net (fo=2, routed)           0.359     2.404    N_1069
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_8/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.447 r  O_OBUF[11]_inst_i_8/O
                         net (fo=3, routed)           0.430     2.877    N_1335
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_2/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.920 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.518     3.438    N_1586
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     3.481 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.193     4.674    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.505 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.505    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.374ns  (logic 2.596ns (40.720%)  route 3.779ns (59.280%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=11, routed)          1.395     2.000    B_IBUF[1]
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_6/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.043 r  O_OBUF[7]_inst_i_6/O
                         net (fo=2, routed)           0.369     2.412    N_1068
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_9/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.455 r  O_OBUF[9]_inst_i_9/O
                         net (fo=2, routed)           0.336     2.791    N_1321
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_5/I1
    SLICE_X0Y62          LUT3 (Prop_lut3_I1_O)        0.043     2.834 r  O_OBUF[9]_inst_i_5/O
                         net (fo=3, routed)           0.421     3.255    N_1572
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X0Y63          LUT3 (Prop_lut3_I1_O)        0.043     3.298 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.258     4.555    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.374 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.374    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[3]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.755ns  (logic 2.609ns (45.341%)  route 3.146ns (54.659%))
  Logic Levels:           5  (IBUF=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=14, routed)          1.403     1.991    A_IBUF[5]
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_14/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.034 r  O_OBUF[13]_inst_i_14/O
                         net (fo=2, routed)           0.440     2.474    N_1203
    SLICE_X0Y64                                                       r  O_OBUF[3]_inst_i_3/I4
    SLICE_X0Y64          LUT5 (Prop_lut5_I4_O)        0.048     2.522 r  O_OBUF[3]_inst_i_3/O
                         net (fo=2, routed)           0.245     2.767    N_1438
    SLICE_X0Y64                                                       r  O_OBUF[3]_inst_i_2/I4
    SLICE_X0Y64          LUT5 (Prop_lut5_I4_O)        0.129     2.896 r  O_OBUF[3]_inst_i_2/O
                         net (fo=3, routed)           1.057     3.953    O_OBUF[3]
    AJ27                                                              r  O_OBUF[3]_inst/I
    AJ27                 OBUF (Prop_obuf_I_O)         1.802     5.755 r  O_OBUF[3]_inst/O
                         net (fo=0)                   0.000     5.755    O[3]
    AJ27                                                              r  O[3] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.664ns  (logic 2.541ns (44.866%)  route 3.123ns (55.134%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=7, routed)           1.018     1.614    A_IBUF[2]
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_12/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.043     1.657 r  O_OBUF[9]_inst_i_12/O
                         net (fo=3, routed)           0.413     2.070    N_1142
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X0Y62          LUT5 (Prop_lut5_I2_O)        0.043     2.113 r  O_OBUF[7]_inst_i_2/O
                         net (fo=1, routed)           0.433     2.547    N_1380
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X1Y62          LUT3 (Prop_lut3_I0_O)        0.043     2.590 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.258     3.848    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.664 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.664    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




