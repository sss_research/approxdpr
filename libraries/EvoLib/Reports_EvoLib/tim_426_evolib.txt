Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 10:41:06 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_426_evolib.txt -name O
| Design       : mul8_426
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.545ns  (logic 2.861ns (33.481%)  route 5.684ns (66.519%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.025     1.619    A_IBUF[3]
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_8/I0
    SLICE_X0Y62          LUT2 (Prop_lut2_I0_O)        0.050     1.669 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.592     2.261    N_1014
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_5/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.126     2.387 r  O_OBUF[6]_inst_i_5/O
                         net (fo=5, routed)           0.516     2.903    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[0]_inst_i_6/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[0]_inst_i_6/O
                         net (fo=3, routed)           0.416     3.363    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[0]_inst_i_2/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.406 r  O_OBUF[0]_inst_i_2/O
                         net (fo=2, routed)           0.623     4.029    N_1198
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_5/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.072 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.413     4.485    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.528 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.448     4.976    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_3/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     5.019 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.328     5.346    N_1983
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_1/I3
    SLICE_X0Y64          LUT4 (Prop_lut4_I3_O)        0.043     5.389 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.323     6.712    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.545 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.545    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.522ns  (logic 2.864ns (33.603%)  route 5.658ns (66.397%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.025     1.619    A_IBUF[3]
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_8/I0
    SLICE_X0Y62          LUT2 (Prop_lut2_I0_O)        0.050     1.669 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.592     2.261    N_1014
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_5/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.126     2.387 r  O_OBUF[6]_inst_i_5/O
                         net (fo=5, routed)           0.516     2.903    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[0]_inst_i_6/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[0]_inst_i_6/O
                         net (fo=3, routed)           0.416     3.363    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[0]_inst_i_2/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.406 r  O_OBUF[0]_inst_i_2/O
                         net (fo=2, routed)           0.623     4.029    N_1198
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_5/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.072 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.413     4.485    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.528 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.448     4.976    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_3/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     5.019 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.218     5.236    N_1983
    SLICE_X1Y64                                                       r  O_OBUF[14]_inst_i_1/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     5.279 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.407     6.687    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.522 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.522    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.453ns  (logic 2.879ns (34.064%)  route 5.574ns (65.936%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.025     1.619    A_IBUF[3]
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_8/I0
    SLICE_X0Y62          LUT2 (Prop_lut2_I0_O)        0.050     1.669 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.592     2.261    N_1014
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_5/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.126     2.387 r  O_OBUF[6]_inst_i_5/O
                         net (fo=5, routed)           0.516     2.903    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[0]_inst_i_6/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[0]_inst_i_6/O
                         net (fo=3, routed)           0.416     3.363    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[0]_inst_i_2/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.406 r  O_OBUF[0]_inst_i_2/O
                         net (fo=2, routed)           0.623     4.029    N_1198
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_5/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.072 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.413     4.485    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.528 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.448     4.976    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_3/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     5.019 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.218     5.236    N_1983
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X1Y64          LUT6 (Prop_lut6_I2_O)        0.043     5.279 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.323     6.602    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.453 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.453    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.109ns  (logic 2.827ns (34.859%)  route 5.282ns (65.141%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.025     1.619    A_IBUF[3]
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_8/I0
    SLICE_X0Y62          LUT2 (Prop_lut2_I0_O)        0.050     1.669 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.592     2.261    N_1014
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_5/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.126     2.387 r  O_OBUF[6]_inst_i_5/O
                         net (fo=5, routed)           0.516     2.903    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[0]_inst_i_6/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[0]_inst_i_6/O
                         net (fo=3, routed)           0.416     3.363    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[0]_inst_i_2/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.406 r  O_OBUF[0]_inst_i_2/O
                         net (fo=2, routed)           0.623     4.029    N_1198
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_5/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.072 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.413     4.485    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_4/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.528 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.339     4.867    N_1732
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.910 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.358     6.268    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.109 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.109    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.062ns  (logic 2.826ns (35.058%)  route 5.236ns (64.942%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.025     1.619    A_IBUF[3]
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_8/I0
    SLICE_X0Y62          LUT2 (Prop_lut2_I0_O)        0.050     1.669 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.592     2.261    N_1014
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_5/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.126     2.387 r  O_OBUF[6]_inst_i_5/O
                         net (fo=5, routed)           0.516     2.903    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[0]_inst_i_6/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[0]_inst_i_6/O
                         net (fo=3, routed)           0.416     3.363    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[0]_inst_i_2/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.406 r  O_OBUF[0]_inst_i_2/O
                         net (fo=2, routed)           0.623     4.029    N_1198
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_5/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.072 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.413     4.485    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.528 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.336     4.864    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.907 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.314     6.221    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.062 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.062    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.623ns  (logic 2.954ns (38.749%)  route 4.669ns (61.251%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=17, routed)          1.388     1.978    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_14/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.051     2.029 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.303     2.331    N_764
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_9/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.129     2.460 r  O_OBUF[6]_inst_i_9/O
                         net (fo=2, routed)           0.329     2.790    N_1148
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_3/I4
    SLICE_X3Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.833 r  O_OBUF[6]_inst_i_3/O
                         net (fo=4, routed)           0.516     3.349    N_1414
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_5/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.043     3.392 r  O_OBUF[10]_inst_i_5/O
                         net (fo=4, routed)           0.430     3.821    N_1665
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_3/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.127     3.948 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.337     4.285    N_1933
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.053     4.338 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.367     5.705    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.918     7.623 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.623    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.529ns  (logic 2.951ns (39.194%)  route 4.578ns (60.806%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=17, routed)          1.388     1.978    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_14/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.051     2.029 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.303     2.331    N_764
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_9/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.129     2.460 r  O_OBUF[6]_inst_i_9/O
                         net (fo=2, routed)           0.329     2.790    N_1148
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_3/I4
    SLICE_X3Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.833 r  O_OBUF[6]_inst_i_3/O
                         net (fo=4, routed)           0.516     3.349    N_1414
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_5/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.043     3.392 r  O_OBUF[10]_inst_i_5/O
                         net (fo=4, routed)           0.345     3.737    N_1665
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_2/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.127     3.864 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.431     4.295    N_1682
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.051     4.346 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.266     5.612    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.917     7.529 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.529    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.932ns  (logic 2.717ns (39.200%)  route 4.215ns (60.800%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=17, routed)          1.388     1.978    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_14/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.051     2.029 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.303     2.331    N_764
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_9/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.129     2.460 r  O_OBUF[6]_inst_i_9/O
                         net (fo=2, routed)           0.329     2.790    N_1148
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_3/I4
    SLICE_X3Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.833 r  O_OBUF[6]_inst_i_3/O
                         net (fo=4, routed)           0.516     3.349    N_1414
    SLICE_X2Y63                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.043     3.392 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.411     3.802    N_1664
    SLICE_X0Y64                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X0Y64          LUT6 (Prop_lut6_I0_O)        0.043     3.845 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.268     5.113    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.932 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.932    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[0]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.892ns  (logic 2.696ns (39.120%)  route 4.196ns (60.880%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.025     1.619    A_IBUF[3]
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_8/I0
    SLICE_X0Y62          LUT2 (Prop_lut2_I0_O)        0.050     1.669 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.592     2.261    N_1014
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_5/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.126     2.387 r  O_OBUF[6]_inst_i_5/O
                         net (fo=5, routed)           0.516     2.903    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[0]_inst_i_6/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.946 r  O_OBUF[0]_inst_i_6/O
                         net (fo=3, routed)           0.416     3.363    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[0]_inst_i_2/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.406 r  O_OBUF[0]_inst_i_2/O
                         net (fo=2, routed)           0.524     3.930    N_1198
    SLICE_X2Y63                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     3.973 r  O_OBUF[0]_inst_i_1/O
                         net (fo=3, routed)           1.122     5.095    O_OBUF[0]
    AH24                                                              r  O_OBUF[0]_inst/I
    AH24                 OBUF (Prop_obuf_I_O)         1.797     6.892 r  O_OBUF[0]_inst/O
                         net (fo=0)                   0.000     6.892    O[0]
    AH24                                                              r  O[0] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.301ns  (logic 2.678ns (42.501%)  route 3.623ns (57.499%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=17, routed)          1.388     1.978    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_14/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.051     2.029 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.303     2.331    N_764
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_9/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.129     2.460 r  O_OBUF[6]_inst_i_9/O
                         net (fo=2, routed)           0.329     2.790    N_1148
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_3/I4
    SLICE_X3Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.833 r  O_OBUF[6]_inst_i_3/O
                         net (fo=4, routed)           0.305     3.138    N_1414
    SLICE_X1Y62                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.181 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.298     4.479    O_OBUF[3]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.301 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.301    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




