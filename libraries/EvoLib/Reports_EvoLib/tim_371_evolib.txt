Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 07:53:48 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_371_evolib.txt -name O
| Design       : mul8_371
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.796ns  (logic 2.919ns (33.192%)  route 5.876ns (66.808%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT5=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.175     1.780    B_IBUF[2]
    SLICE_X4Y60                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X4Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.823 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.244     2.067    N_664
    SLICE_X4Y60                                                       r  O_OBUF[8]_inst_i_8/I2
    SLICE_X4Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.116 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.591     2.707    N_914
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y58          LUT4 (Prop_lut4_I1_O)        0.129     2.836 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.506     3.342    N_1164
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.385 r  O_OBUF[8]_inst_i_2/O
                         net (fo=4, routed)           0.230     3.615    N_1414
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_4/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     3.658 r  O_OBUF[10]_inst_i_4/O
                         net (fo=4, routed)           0.444     4.103    N_1665
    SLICE_X2Y59                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y59          LUT6 (Prop_lut6_I1_O)        0.043     4.146 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.414     4.560    N_1698
    SLICE_X0Y59                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X0Y59          LUT4 (Prop_lut4_I3_O)        0.043     4.603 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.435     5.038    N_1949
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     5.081 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.422     5.504    N_1983
    SLICE_X0Y60                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     5.547 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.413     6.960    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.796 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.796    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.776ns  (logic 2.917ns (33.233%)  route 5.860ns (66.767%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT5=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.175     1.780    B_IBUF[2]
    SLICE_X4Y60                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X4Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.823 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.244     2.067    N_664
    SLICE_X4Y60                                                       r  O_OBUF[8]_inst_i_8/I2
    SLICE_X4Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.116 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.591     2.707    N_914
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y58          LUT4 (Prop_lut4_I1_O)        0.129     2.836 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.506     3.342    N_1164
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.385 r  O_OBUF[8]_inst_i_2/O
                         net (fo=4, routed)           0.230     3.615    N_1414
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_4/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     3.658 r  O_OBUF[10]_inst_i_4/O
                         net (fo=4, routed)           0.444     4.103    N_1665
    SLICE_X2Y59                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y59          LUT6 (Prop_lut6_I1_O)        0.043     4.146 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.414     4.560    N_1698
    SLICE_X0Y59                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X0Y59          LUT4 (Prop_lut4_I3_O)        0.043     4.603 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.435     5.038    N_1949
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     5.081 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.417     5.498    N_1983
    SLICE_X0Y60                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     5.541 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.402     6.943    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.776 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.776    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.693ns  (logic 3.119ns (35.883%)  route 5.574ns (64.117%))
  Logic Levels:           11  (IBUF=1 LUT4=4 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.051     1.656    B_IBUF[2]
    SLICE_X1Y58                                                       r  O_OBUF[8]_inst_i_11/I2
    SLICE_X1Y58          LUT4 (Prop_lut4_I2_O)        0.051     1.707 r  O_OBUF[8]_inst_i_11/O
                         net (fo=2, routed)           0.432     2.139    N_898
    SLICE_X1Y58                                                       r  O_OBUF[8]_inst_i_7/I3
    SLICE_X1Y58          LUT4 (Prop_lut4_I3_O)        0.139     2.278 r  O_OBUF[8]_inst_i_7/O
                         net (fo=3, routed)           0.260     2.539    N_1149
    SLICE_X2Y60                                                       r  O_OBUF[8]_inst_i_4/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.131     2.670 r  O_OBUF[8]_inst_i_4/O
                         net (fo=6, routed)           0.426     3.096    N_1183
    SLICE_X2Y59                                                       r  O_OBUF[10]_inst_i_8/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.139 r  O_OBUF[10]_inst_i_8/O
                         net (fo=3, routed)           0.442     3.581    N_1415
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_8/I3
    SLICE_X3Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.624 r  O_OBUF[15]_inst_i_8/O
                         net (fo=3, routed)           0.311     3.935    N_1449
    SLICE_X3Y59                                                       r  O_OBUF[12]_inst_i_6/I0
    SLICE_X3Y59          LUT4 (Prop_lut4_I0_O)        0.051     3.986 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.430     4.416    N_1464
    SLICE_X4Y59                                                       r  O_OBUF[15]_inst_i_7/I4
    SLICE_X4Y59          LUT6 (Prop_lut6_I4_O)        0.129     4.545 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.314     4.859    N_1733
    SLICE_X0Y59                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X0Y59          LUT6 (Prop_lut6_I4_O)        0.043     4.902 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.497     5.399    N_1748
    SLICE_X0Y61                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.043     5.442 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.410     6.852    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.693 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.693    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.353ns  (logic 2.871ns (34.370%)  route 5.482ns (65.630%))
  Logic Levels:           10  (IBUF=1 LUT4=4 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.175     1.780    B_IBUF[2]
    SLICE_X4Y60                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X4Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.823 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.244     2.067    N_664
    SLICE_X4Y60                                                       r  O_OBUF[8]_inst_i_8/I2
    SLICE_X4Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.116 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.591     2.707    N_914
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y58          LUT4 (Prop_lut4_I1_O)        0.129     2.836 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.506     3.342    N_1164
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.385 r  O_OBUF[8]_inst_i_2/O
                         net (fo=4, routed)           0.230     3.615    N_1414
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_4/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     3.658 r  O_OBUF[10]_inst_i_4/O
                         net (fo=4, routed)           0.444     4.103    N_1665
    SLICE_X2Y59                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y59          LUT6 (Prop_lut6_I1_O)        0.043     4.146 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.414     4.560    N_1698
    SLICE_X0Y59                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X0Y59          LUT4 (Prop_lut4_I3_O)        0.043     4.603 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.534     5.137    N_1949
    SLICE_X0Y61                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     5.180 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.343     6.523    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     8.353 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.353    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.243ns  (logic 2.882ns (34.965%)  route 5.361ns (65.035%))
  Logic Levels:           10  (IBUF=1 LUT4=3 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.175     1.780    B_IBUF[2]
    SLICE_X4Y60                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X4Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.823 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.244     2.067    N_664
    SLICE_X4Y60                                                       r  O_OBUF[8]_inst_i_8/I2
    SLICE_X4Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.116 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.591     2.707    N_914
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y58          LUT4 (Prop_lut4_I1_O)        0.129     2.836 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.506     3.342    N_1164
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.385 r  O_OBUF[8]_inst_i_2/O
                         net (fo=4, routed)           0.230     3.615    N_1414
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_4/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     3.658 r  O_OBUF[10]_inst_i_4/O
                         net (fo=4, routed)           0.444     4.103    N_1665
    SLICE_X2Y59                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y59          LUT6 (Prop_lut6_I1_O)        0.043     4.146 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.414     4.560    N_1698
    SLICE_X0Y59                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X0Y59          LUT4 (Prop_lut4_I3_O)        0.043     4.603 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.430     5.033    N_1949
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y61          LUT6 (Prop_lut6_I2_O)        0.043     5.076 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.325     6.401    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.243 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.243    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.867ns  (logic 2.927ns (37.208%)  route 4.940ns (62.792%))
  Logic Levels:           9  (IBUF=1 LUT4=3 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.175     1.780    B_IBUF[2]
    SLICE_X4Y60                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X4Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.823 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.244     2.067    N_664
    SLICE_X4Y60                                                       r  O_OBUF[8]_inst_i_8/I2
    SLICE_X4Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.116 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.591     2.707    N_914
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y58          LUT4 (Prop_lut4_I1_O)        0.129     2.836 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.506     3.342    N_1164
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.385 r  O_OBUF[8]_inst_i_2/O
                         net (fo=4, routed)           0.230     3.615    N_1414
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_4/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     3.658 r  O_OBUF[10]_inst_i_4/O
                         net (fo=4, routed)           0.444     4.103    N_1665
    SLICE_X2Y59                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y59          LUT6 (Prop_lut6_I1_O)        0.043     4.146 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.414     4.560    N_1698
    SLICE_X0Y59                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y59          LUT4 (Prop_lut4_I1_O)        0.053     4.613 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.335     5.948    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.919     7.867 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.867    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.454ns  (logic 2.817ns (37.786%)  route 4.637ns (62.214%))
  Logic Levels:           9  (IBUF=1 LUT4=4 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.175     1.780    B_IBUF[2]
    SLICE_X4Y60                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X4Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.823 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.244     2.067    N_664
    SLICE_X4Y60                                                       r  O_OBUF[8]_inst_i_8/I2
    SLICE_X4Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.116 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.591     2.707    N_914
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y58          LUT4 (Prop_lut4_I1_O)        0.129     2.836 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.506     3.342    N_1164
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.385 r  O_OBUF[8]_inst_i_2/O
                         net (fo=4, routed)           0.230     3.615    N_1414
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_4/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     3.658 r  O_OBUF[10]_inst_i_4/O
                         net (fo=4, routed)           0.417     4.076    N_1665
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X1Y61          LUT4 (Prop_lut4_I0_O)        0.043     4.119 r  O_OBUF[9]_inst_i_3/O
                         net (fo=1, routed)           0.217     4.335    N_1682
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.043     4.378 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.257     5.635    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.454 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.454    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.047ns  (logic 2.734ns (38.788%)  route 4.314ns (61.212%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.175     1.780    B_IBUF[2]
    SLICE_X4Y60                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X4Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.823 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.244     2.067    N_664
    SLICE_X4Y60                                                       r  O_OBUF[8]_inst_i_8/I2
    SLICE_X4Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.116 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.591     2.707    N_914
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y58          LUT4 (Prop_lut4_I1_O)        0.129     2.836 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.506     3.342    N_1164
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.385 r  O_OBUF[8]_inst_i_2/O
                         net (fo=4, routed)           0.414     3.799    N_1414
    SLICE_X1Y59                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X1Y59          LUT6 (Prop_lut6_I0_O)        0.043     3.842 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.383     5.226    O_OBUF[3]
    AK27                                                              r  O_OBUF[6]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     7.047 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     7.047    O[6]
    AK27                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[3]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.851ns  (logic 2.707ns (39.511%)  route 4.144ns (60.489%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.175     1.780    B_IBUF[2]
    SLICE_X4Y60                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X4Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.823 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.244     2.067    N_664
    SLICE_X4Y60                                                       r  O_OBUF[8]_inst_i_8/I2
    SLICE_X4Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.116 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.591     2.707    N_914
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y58          LUT4 (Prop_lut4_I1_O)        0.129     2.836 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.506     3.342    N_1164
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.385 r  O_OBUF[8]_inst_i_2/O
                         net (fo=4, routed)           0.414     3.799    N_1414
    SLICE_X1Y59                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X1Y59          LUT6 (Prop_lut6_I0_O)        0.043     3.842 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.214     5.056    O_OBUF[3]
    AG25                                                              r  O_OBUF[3]_inst/I
    AG25                 OBUF (Prop_obuf_I_O)         1.795     6.851 r  O_OBUF[3]_inst/O
                         net (fo=0)                   0.000     6.851    O[3]
    AG25                                                              r  O[3] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.718ns  (logic 2.728ns (40.608%)  route 3.990ns (59.392%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AP29                                                              r  B_IBUF[2]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[2]_inst/O
                         net (fo=9, routed)           1.175     1.780    B_IBUF[2]
    SLICE_X4Y60                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X4Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.823 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.244     2.067    N_664
    SLICE_X4Y60                                                       r  O_OBUF[8]_inst_i_8/I2
    SLICE_X4Y60          LUT5 (Prop_lut5_I2_O)        0.049     2.116 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.591     2.707    N_914
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y58          LUT4 (Prop_lut4_I1_O)        0.129     2.836 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.506     3.342    N_1164
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     3.385 r  O_OBUF[8]_inst_i_2/O
                         net (fo=4, routed)           0.217     3.602    N_1414
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_1/I4
    SLICE_X1Y60          LUT6 (Prop_lut6_I4_O)        0.043     3.645 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.257     4.902    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.718 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.718    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------




