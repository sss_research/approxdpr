Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 10:32:02 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_423_evolib.txt -name O
| Design       : mul8_423
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.446ns  (logic 2.726ns (32.278%)  route 5.720ns (67.722%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.404     1.994    A_IBUF[4]
    SLICE_X2Y60                                                       r  O_OBUF[9]_inst_i_9/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     2.037 r  O_OBUF[9]_inst_i_9/O
                         net (fo=3, routed)           0.692     2.729    O_OBUF[9]_inst_i_9_n_0
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.772 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.273     3.045    N_1415
    SLICE_X3Y60                                                       r  O_OBUF[12]_inst_i_9/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.043     3.088 r  O_OBUF[12]_inst_i_9/O
                         net (fo=1, routed)           0.411     3.499    N_1449
    SLICE_X0Y60                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.542 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.490     4.032    N_1482
    SLICE_X0Y58                                                       r  O_OBUF[12]_inst_i_2/I0
    SLICE_X0Y58          LUT6 (Prop_lut6_I0_O)        0.043     4.075 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.224     4.300    N_1732
    SLICE_X0Y58                                                       r  O_OBUF[15]_inst_i_3/I3
    SLICE_X0Y58          LUT4 (Prop_lut4_I3_O)        0.043     4.343 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.809     5.152    N_1983
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X0Y61          LUT6 (Prop_lut6_I2_O)        0.043     5.195 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.416     6.610    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.446 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.446    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.359ns  (logic 2.826ns (33.812%)  route 5.533ns (66.188%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.404     1.994    A_IBUF[4]
    SLICE_X2Y60                                                       r  O_OBUF[9]_inst_i_9/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     2.037 r  O_OBUF[9]_inst_i_9/O
                         net (fo=3, routed)           0.692     2.729    O_OBUF[9]_inst_i_9_n_0
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.772 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.273     3.045    N_1415
    SLICE_X3Y60                                                       r  O_OBUF[12]_inst_i_9/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.043     3.088 r  O_OBUF[12]_inst_i_9/O
                         net (fo=1, routed)           0.411     3.499    N_1449
    SLICE_X0Y60                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.542 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.490     4.032    N_1482
    SLICE_X0Y58                                                       r  O_OBUF[12]_inst_i_2/I0
    SLICE_X0Y58          LUT6 (Prop_lut6_I0_O)        0.043     4.075 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.224     4.300    N_1732
    SLICE_X0Y58                                                       r  O_OBUF[15]_inst_i_3/I3
    SLICE_X0Y58          LUT4 (Prop_lut4_I3_O)        0.043     4.343 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.710     5.052    N_1983
    SLICE_X0Y61                                                       r  O_OBUF[13]_inst_i_1/I3
    SLICE_X0Y61          LUT4 (Prop_lut4_I3_O)        0.051     5.103 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.328     6.431    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     8.359 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.359    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.268ns  (logic 2.723ns (32.940%)  route 5.544ns (67.060%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.404     1.994    A_IBUF[4]
    SLICE_X2Y60                                                       r  O_OBUF[9]_inst_i_9/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     2.037 r  O_OBUF[9]_inst_i_9/O
                         net (fo=3, routed)           0.692     2.729    O_OBUF[9]_inst_i_9_n_0
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.772 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.273     3.045    N_1415
    SLICE_X3Y60                                                       r  O_OBUF[12]_inst_i_9/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.043     3.088 r  O_OBUF[12]_inst_i_9/O
                         net (fo=1, routed)           0.411     3.499    N_1449
    SLICE_X0Y60                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.542 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.490     4.032    N_1482
    SLICE_X0Y58                                                       r  O_OBUF[12]_inst_i_2/I0
    SLICE_X0Y58          LUT6 (Prop_lut6_I0_O)        0.043     4.075 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.224     4.300    N_1732
    SLICE_X0Y58                                                       r  O_OBUF[15]_inst_i_3/I3
    SLICE_X0Y58          LUT4 (Prop_lut4_I3_O)        0.043     4.343 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.715     5.057    N_1983
    SLICE_X0Y61                                                       r  O_OBUF[14]_inst_i_1/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     5.100 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.335     6.435    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.268 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.268    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.681ns  (logic 2.783ns (36.229%)  route 4.898ns (63.771%))
  Logic Levels:           8  (IBUF=1 LUT4=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.404     1.994    A_IBUF[4]
    SLICE_X2Y60                                                       r  O_OBUF[9]_inst_i_9/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     2.037 r  O_OBUF[9]_inst_i_9/O
                         net (fo=3, routed)           0.692     2.729    O_OBUF[9]_inst_i_9_n_0
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.772 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.273     3.045    N_1415
    SLICE_X3Y60                                                       r  O_OBUF[12]_inst_i_9/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.043     3.088 r  O_OBUF[12]_inst_i_9/O
                         net (fo=1, routed)           0.411     3.499    N_1449
    SLICE_X0Y60                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.542 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.490     4.032    N_1482
    SLICE_X0Y58                                                       r  O_OBUF[12]_inst_i_2/I0
    SLICE_X0Y58          LUT6 (Prop_lut6_I0_O)        0.043     4.075 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.224     4.300    N_1732
    SLICE_X0Y58                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X0Y58          LUT4 (Prop_lut4_I0_O)        0.051     4.351 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.403     5.754    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.927     7.681 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.681    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.484ns  (logic 2.678ns (35.783%)  route 4.806ns (64.217%))
  Logic Levels:           8  (IBUF=1 LUT4=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.404     1.994    A_IBUF[4]
    SLICE_X2Y60                                                       r  O_OBUF[9]_inst_i_9/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     2.037 r  O_OBUF[9]_inst_i_9/O
                         net (fo=3, routed)           0.692     2.729    O_OBUF[9]_inst_i_9_n_0
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.772 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.339     3.111    N_1415
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_8/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.154 r  O_OBUF[11]_inst_i_8/O
                         net (fo=2, routed)           0.324     3.478    N_1683
    SLICE_X0Y59                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X0Y59          LUT6 (Prop_lut6_I0_O)        0.043     3.521 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.343     3.863    N_1699
    SLICE_X1Y59                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y59          LUT4 (Prop_lut4_I3_O)        0.043     3.906 r  O_OBUF[11]_inst_i_2/O
                         net (fo=1, routed)           0.338     4.245    N_1714
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     4.288 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.366     5.653    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.484 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.484    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.397ns  (logic 2.727ns (36.868%)  route 4.670ns (63.132%))
  Logic Levels:           7  (IBUF=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.404     1.994    A_IBUF[4]
    SLICE_X2Y60                                                       r  O_OBUF[9]_inst_i_9/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     2.037 r  O_OBUF[9]_inst_i_9/O
                         net (fo=3, routed)           0.692     2.729    O_OBUF[9]_inst_i_9_n_0
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.772 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.339     3.111    N_1415
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.154 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.414     3.568    N_1682
    SLICE_X1Y60                                                       r  O_OBUF[11]_inst_i_3/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.048     3.616 r  O_OBUF[11]_inst_i_3/O
                         net (fo=2, routed)           0.506     4.122    N_1933
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X1Y61          LUT4 (Prop_lut4_I3_O)        0.129     4.251 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.314     5.565    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.397 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.397    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.729ns  (logic 2.580ns (38.345%)  route 4.149ns (61.655%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.404     1.994    A_IBUF[4]
    SLICE_X2Y60                                                       r  O_OBUF[9]_inst_i_9/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     2.037 r  O_OBUF[9]_inst_i_9/O
                         net (fo=3, routed)           0.692     2.729    O_OBUF[9]_inst_i_9_n_0
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_4/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.772 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.339     3.111    N_1415
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.154 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.414     3.568    N_1682
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X1Y60          LUT4 (Prop_lut4_I0_O)        0.043     3.611 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.299     4.910    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.729 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.729    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.402ns  (logic 2.672ns (41.740%)  route 3.730ns (58.260%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.234     1.824    A_IBUF[4]
    SLICE_X3Y59                                                       r  O_OBUF[8]_inst_i_5/I1
    SLICE_X3Y59          LUT4 (Prop_lut4_I1_O)        0.043     1.867 r  O_OBUF[8]_inst_i_5/O
                         net (fo=1, routed)           0.342     2.209    N_898
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I4
    SLICE_X2Y59          LUT5 (Prop_lut5_I4_O)        0.048     2.257 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.436     2.692    N_1398
    SLICE_X2Y58                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X2Y58          LUT6 (Prop_lut6_I0_O)        0.132     2.824 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.409     3.233    N_1664
    SLICE_X1Y58                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y58          LUT5 (Prop_lut5_I0_O)        0.043     3.276 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.309     4.585    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.402 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.402    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[7]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.918ns  (logic 2.534ns (51.518%)  route 2.384ns (48.482%))
  Logic Levels:           3  (IBUF=1 LUT4=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL25                                              0.000     0.000 r  B[7] (IN)
                         net (fo=0)                   0.000     0.000    B[7]
    AL25                                                              r  B_IBUF[7]_inst/I
    AL25                 IBUF (Prop_ibuf_I_O)         0.574     0.574 r  B_IBUF[7]_inst/O
                         net (fo=15, routed)          1.054     1.628    B_IBUF[7]
    SLICE_X1Y58                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X1Y58          LUT4 (Prop_lut4_I2_O)        0.051     1.679 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.330     3.009    O_OBUF[7]
    AK26                                                              r  O_OBUF[7]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.909     4.918 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     4.918    O[7]
    AK26                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.912ns  (logic 2.533ns (51.574%)  route 2.378ns (48.426%))
  Logic Levels:           3  (IBUF=1 LUT2=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=4, routed)           1.056     1.649    A_IBUF[1]
    SLICE_X0Y58                                                       r  O_OBUF[5]_inst_i_1/I0
    SLICE_X0Y58          LUT2 (Prop_lut2_I0_O)        0.052     1.701 r  O_OBUF[5]_inst_i_1/O
                         net (fo=2, routed)           1.322     3.024    O_OBUF[1]
    AJ26                                                              r  O_OBUF[5]_inst/I
    AJ26                 OBUF (Prop_obuf_I_O)         1.888     4.912 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     4.912    O[5]
    AJ26                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------




