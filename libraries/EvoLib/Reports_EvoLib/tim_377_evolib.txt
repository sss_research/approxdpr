Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 08:11:41 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_377_evolib.txt -name O
| Design       : mul8_377
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.652ns  (logic 2.764ns (31.952%)  route 5.887ns (68.048%))
  Logic Levels:           10  (IBUF=1 LUT4=2 LUT5=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.148     1.735    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.043     1.778 r  O_OBUF[1]_inst_i_1/O
                         net (fo=5, routed)           0.554     2.332    O_OBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X2Y59          LUT5 (Prop_lut5_I0_O)        0.043     2.375 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.515     2.890    N_1149
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.933 r  O_OBUF[6]_inst_i_2/O
                         net (fo=6, routed)           0.516     3.449    N_1415
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.492 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.344     3.837    N_1683
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X1Y61          LUT4 (Prop_lut4_I2_O)        0.043     3.880 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.501     4.381    N_1699
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.424 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.409     4.833    N_1965
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.043     4.876 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.500     5.376    N_1983
    SLICE_X2Y62                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X2Y62          LUT6 (Prop_lut6_I1_O)        0.043     5.419 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.400     6.819    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.652 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.652    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.585ns  (logic 2.871ns (33.444%)  route 5.714ns (66.556%))
  Logic Levels:           10  (IBUF=1 LUT4=3 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.148     1.735    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.043     1.778 r  O_OBUF[1]_inst_i_1/O
                         net (fo=5, routed)           0.554     2.332    O_OBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X2Y59          LUT5 (Prop_lut5_I0_O)        0.043     2.375 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.515     2.890    N_1149
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.933 r  O_OBUF[6]_inst_i_2/O
                         net (fo=6, routed)           0.516     3.449    N_1415
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.492 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.344     3.837    N_1683
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X1Y61          LUT4 (Prop_lut4_I2_O)        0.043     3.880 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.501     4.381    N_1699
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.424 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.409     4.833    N_1965
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.043     4.876 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.297     5.173    N_1983
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.053     5.226 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.430     6.656    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.930     8.585 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.585    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.547ns  (logic 2.767ns (32.378%)  route 5.779ns (67.622%))
  Logic Levels:           10  (IBUF=1 LUT4=2 LUT5=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.148     1.735    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.043     1.778 r  O_OBUF[1]_inst_i_1/O
                         net (fo=5, routed)           0.554     2.332    O_OBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X2Y59          LUT5 (Prop_lut5_I0_O)        0.043     2.375 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.515     2.890    N_1149
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.933 r  O_OBUF[6]_inst_i_2/O
                         net (fo=6, routed)           0.516     3.449    N_1415
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.492 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.344     3.837    N_1683
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X1Y61          LUT4 (Prop_lut4_I2_O)        0.043     3.880 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.501     4.381    N_1699
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.424 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.409     4.833    N_1965
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.043     4.876 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.386     5.262    N_1983
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     5.305 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.406     6.711    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.547 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.547    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.156ns  (logic 2.824ns (34.625%)  route 5.332ns (65.375%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.148     1.735    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.043     1.778 r  O_OBUF[1]_inst_i_1/O
                         net (fo=5, routed)           0.554     2.332    O_OBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X2Y59          LUT5 (Prop_lut5_I0_O)        0.043     2.375 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.515     2.890    N_1149
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.933 r  O_OBUF[6]_inst_i_2/O
                         net (fo=6, routed)           0.516     3.449    N_1415
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.492 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.344     3.837    N_1683
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X1Y61          LUT4 (Prop_lut4_I2_O)        0.043     3.880 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.501     4.381    N_1699
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.424 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.409     4.833    N_1965
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.051     4.884 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.344     6.228    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.927     8.156 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.156    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.860ns  (logic 2.719ns (34.593%)  route 5.141ns (65.407%))
  Logic Levels:           9  (IBUF=1 LUT4=3 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.148     1.735    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.043     1.778 r  O_OBUF[1]_inst_i_1/O
                         net (fo=5, routed)           0.554     2.332    O_OBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X2Y59          LUT5 (Prop_lut5_I0_O)        0.043     2.375 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.515     2.890    N_1149
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.933 r  O_OBUF[6]_inst_i_2/O
                         net (fo=6, routed)           0.516     3.449    N_1415
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.492 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.344     3.837    N_1683
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X1Y61          LUT4 (Prop_lut4_I2_O)        0.043     3.880 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.502     4.382    N_1699
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     4.425 r  O_OBUF[11]_inst_i_3/O
                         net (fo=1, routed)           0.247     4.672    N_1714
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X0Y62          LUT4 (Prop_lut4_I1_O)        0.043     4.715 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.314     6.029    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.860 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.860    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.378ns  (logic 2.768ns (37.517%)  route 4.610ns (62.483%))
  Logic Levels:           8  (IBUF=1 LUT4=2 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.148     1.735    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.043     1.778 r  O_OBUF[1]_inst_i_1/O
                         net (fo=5, routed)           0.554     2.332    O_OBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X2Y59          LUT5 (Prop_lut5_I0_O)        0.043     2.375 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.515     2.890    N_1149
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.933 r  O_OBUF[6]_inst_i_2/O
                         net (fo=6, routed)           0.516     3.449    N_1415
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.492 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.344     3.837    N_1683
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X1Y61          LUT4 (Prop_lut4_I0_O)        0.048     3.885 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.219     4.104    N_1698
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.129     4.233 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.314     5.547    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.378 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.378    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.781ns  (logic 2.621ns (38.657%)  route 4.160ns (61.343%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.148     1.735    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.043     1.778 r  O_OBUF[1]_inst_i_1/O
                         net (fo=5, routed)           0.554     2.332    O_OBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X2Y59          LUT5 (Prop_lut5_I0_O)        0.043     2.375 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.515     2.890    N_1149
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.933 r  O_OBUF[6]_inst_i_2/O
                         net (fo=6, routed)           0.513     3.446    N_1415
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.489 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.096     3.586    N_1432
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_1/I4
    SLICE_X0Y61          LUT6 (Prop_lut6_I4_O)        0.043     3.629 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.334     4.962    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.781 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.781    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.472ns  (logic 2.670ns (41.257%)  route 3.802ns (58.743%))
  Logic Levels:           6  (IBUF=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.148     1.735    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.043     1.778 r  O_OBUF[1]_inst_i_1/O
                         net (fo=5, routed)           0.554     2.332    O_OBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X2Y59          LUT5 (Prop_lut5_I0_O)        0.043     2.375 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.448     2.822    N_1149
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_2/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.043     2.865 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.385     3.250    N_1414
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_1/I2
    SLICE_X0Y63          LUT5 (Prop_lut5_I2_O)        0.051     3.301 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.268     4.569    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.902     6.472 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.472    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.300ns  (logic 2.581ns (40.976%)  route 3.718ns (59.024%))
  Logic Levels:           6  (IBUF=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.148     1.735    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[1]_inst_i_1/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.043     1.778 r  O_OBUF[1]_inst_i_1/O
                         net (fo=5, routed)           0.554     2.332    O_OBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X2Y59          LUT5 (Prop_lut5_I0_O)        0.043     2.375 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.515     2.890    N_1149
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.933 r  O_OBUF[6]_inst_i_2/O
                         net (fo=6, routed)           0.311     3.244    N_1415
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_1/I2
    SLICE_X0Y62          LUT6 (Prop_lut6_I2_O)        0.043     3.287 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.191     4.478    O_OBUF[6]
    AK27                                                              r  O_OBUF[6]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     6.300 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.300    O[6]
    AK27                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.939ns  (logic 2.478ns (50.164%)  route 2.462ns (49.836%))
  Logic Levels:           3  (IBUF=1 LUT2=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=1, routed)           1.204     1.725    A_IBUF[0]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y60          LUT2 (Prop_lut2_I0_O)        0.048     1.773 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.258     3.031    O_OBUF[7]
    AK26                                                              r  O_OBUF[7]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.909     4.939 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     4.939    O[7]
    AK26                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




