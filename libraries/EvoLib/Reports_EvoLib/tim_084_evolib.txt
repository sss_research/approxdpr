Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 17:47:45 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_084_evolib.txt -name O
| Design       : mul8_084
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.046ns  (logic 2.859ns (35.538%)  route 5.187ns (64.462%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.457     2.043    A_IBUF[6]
    SLICE_X3Y59                                                       f  O_OBUF[8]_inst_i_9/I3
    SLICE_X3Y59          LUT4 (Prop_lut4_I3_O)        0.043     2.086 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.344     2.431    O_OBUF[8]_inst_i_9_n_0
    SLICE_X3Y59                                                       r  O_OBUF[8]_inst_i_2/I1
    SLICE_X3Y59          LUT5 (Prop_lut5_I1_O)        0.048     2.479 f  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.420     2.899    O_OBUF[8]_inst_i_2_n_0
    SLICE_X3Y60                                                       f  O_OBUF[10]_inst_i_2/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.129     3.028 f  O_OBUF[10]_inst_i_2/O
                         net (fo=4, routed)           0.442     3.470    O_OBUF[10]_inst_i_2_n_0
    SLICE_X4Y61                                                       f  O_OBUF[6]_inst_i_1/I2
    SLICE_X4Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.513 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           0.704     4.217    O_OBUF[6]
    SLICE_X1Y65                                                       r  O_OBUF[14]_inst_i_5/I1
    SLICE_X1Y65          LUT3 (Prop_lut3_I1_O)        0.048     4.265 r  O_OBUF[14]_inst_i_5/O
                         net (fo=2, routed)           0.537     4.802    O_OBUF[14]_inst_i_5_n_0
    SLICE_X1Y65                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X1Y65          LUT3 (Prop_lut3_I2_O)        0.129     4.931 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.282     6.213    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.046 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.046    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.031ns  (logic 2.862ns (35.641%)  route 5.169ns (64.359%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.457     2.043    A_IBUF[6]
    SLICE_X3Y59                                                       f  O_OBUF[8]_inst_i_9/I3
    SLICE_X3Y59          LUT4 (Prop_lut4_I3_O)        0.043     2.086 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.344     2.431    O_OBUF[8]_inst_i_9_n_0
    SLICE_X3Y59                                                       r  O_OBUF[8]_inst_i_2/I1
    SLICE_X3Y59          LUT5 (Prop_lut5_I1_O)        0.048     2.479 f  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.420     2.899    O_OBUF[8]_inst_i_2_n_0
    SLICE_X3Y60                                                       f  O_OBUF[10]_inst_i_2/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.129     3.028 f  O_OBUF[10]_inst_i_2/O
                         net (fo=4, routed)           0.442     3.470    O_OBUF[10]_inst_i_2_n_0
    SLICE_X4Y61                                                       f  O_OBUF[6]_inst_i_1/I2
    SLICE_X4Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.513 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           0.704     4.217    O_OBUF[6]
    SLICE_X1Y65                                                       r  O_OBUF[14]_inst_i_5/I1
    SLICE_X1Y65          LUT3 (Prop_lut3_I1_O)        0.048     4.265 r  O_OBUF[14]_inst_i_5/O
                         net (fo=2, routed)           0.443     4.708    O_OBUF[14]_inst_i_5_n_0
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.129     4.837 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.358     6.195    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.031 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.031    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.541ns  (logic 2.879ns (38.176%)  route 4.662ns (61.824%))
  Logic Levels:           8  (IBUF=1 LUT4=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.457     2.043    A_IBUF[6]
    SLICE_X3Y59                                                       f  O_OBUF[8]_inst_i_9/I3
    SLICE_X3Y59          LUT4 (Prop_lut4_I3_O)        0.043     2.086 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.344     2.431    O_OBUF[8]_inst_i_9_n_0
    SLICE_X3Y59                                                       r  O_OBUF[8]_inst_i_2/I1
    SLICE_X3Y59          LUT5 (Prop_lut5_I1_O)        0.048     2.479 f  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.420     2.899    O_OBUF[8]_inst_i_2_n_0
    SLICE_X3Y60                                                       f  O_OBUF[10]_inst_i_2/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.129     3.028 f  O_OBUF[10]_inst_i_2/O
                         net (fo=4, routed)           0.442     3.470    O_OBUF[10]_inst_i_2_n_0
    SLICE_X4Y61                                                       f  O_OBUF[6]_inst_i_1/I2
    SLICE_X4Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.513 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           0.363     3.876    O_OBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X0Y65          LUT6 (Prop_lut6_I2_O)        0.043     3.919 r  O_OBUF[15]_inst_i_2/O
                         net (fo=1, routed)           0.231     4.149    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.049     4.198 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.406     5.604    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.937     7.541 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.541    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.373ns  (logic 2.734ns (37.087%)  route 4.638ns (62.913%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.457     2.043    A_IBUF[6]
    SLICE_X3Y59                                                       f  O_OBUF[8]_inst_i_9/I3
    SLICE_X3Y59          LUT4 (Prop_lut4_I3_O)        0.043     2.086 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.344     2.431    O_OBUF[8]_inst_i_9_n_0
    SLICE_X3Y59                                                       r  O_OBUF[8]_inst_i_2/I1
    SLICE_X3Y59          LUT5 (Prop_lut5_I1_O)        0.048     2.479 f  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.420     2.899    O_OBUF[8]_inst_i_2_n_0
    SLICE_X3Y60                                                       f  O_OBUF[10]_inst_i_2/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.129     3.028 f  O_OBUF[10]_inst_i_2/O
                         net (fo=4, routed)           0.442     3.470    O_OBUF[10]_inst_i_2_n_0
    SLICE_X4Y61                                                       f  O_OBUF[6]_inst_i_1/I2
    SLICE_X4Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.513 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           0.704     4.217    O_OBUF[6]
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X1Y65          LUT3 (Prop_lut3_I2_O)        0.043     4.260 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.271     5.531    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.373 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.373    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.070ns  (logic 2.734ns (38.670%)  route 4.336ns (61.330%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.393     1.980    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[10]_inst_i_10/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.023 r  O_OBUF[10]_inst_i_10/O
                         net (fo=4, routed)           0.461     2.484    O_OBUF[10]_inst_i_10_n_0
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.527 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.304     2.831    O_OBUF[11]_inst_i_5_n_0
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.050     2.881 r  O_OBUF[10]_inst_i_6/O
                         net (fo=3, routed)           0.438     3.320    O_OBUF[10]_inst_i_6_n_0
    SLICE_X4Y61                                                       r  O_OBUF[11]_inst_i_4/I0
    SLICE_X4Y61          LUT5 (Prop_lut5_I0_O)        0.127     3.447 r  O_OBUF[11]_inst_i_4/O
                         net (fo=1, routed)           0.342     3.789    O_OBUF[11]_inst_i_4_n_0
    SLICE_X4Y61                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X4Y61          LUT3 (Prop_lut3_I2_O)        0.043     3.832 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.397     5.228    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.070 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.070    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.733ns  (logic 2.684ns (39.863%)  route 4.049ns (60.137%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.332     1.918    A_IBUF[6]
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_10/I4
    SLICE_X2Y59          LUT6 (Prop_lut6_I4_O)        0.043     1.961 f  O_OBUF[8]_inst_i_10/O
                         net (fo=4, routed)           0.350     2.311    O_OBUF[8]_inst_i_10_n_0
    SLICE_X3Y59                                                       f  O_OBUF[8]_inst_i_5/I0
    SLICE_X3Y59          LUT6 (Prop_lut6_I0_O)        0.043     2.354 r  O_OBUF[8]_inst_i_5/O
                         net (fo=4, routed)           0.488     2.842    O_OBUF[8]_inst_i_5_n_0
    SLICE_X3Y61                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X3Y61          LUT3 (Prop_lut3_I1_O)        0.043     2.885 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.515     3.400    O_OBUF[10]_inst_i_5_n_0
    SLICE_X4Y61                                                       r  O_OBUF[9]_inst_i_1/I3
    SLICE_X4Y61          LUT4 (Prop_lut4_I3_O)        0.051     3.451 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.365     4.816    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.917     6.733 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.733    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.732ns  (logic 2.672ns (39.691%)  route 4.060ns (60.309%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.457     2.043    A_IBUF[6]
    SLICE_X3Y59                                                       f  O_OBUF[8]_inst_i_9/I3
    SLICE_X3Y59          LUT4 (Prop_lut4_I3_O)        0.043     2.086 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.344     2.431    O_OBUF[8]_inst_i_9_n_0
    SLICE_X3Y59                                                       r  O_OBUF[8]_inst_i_2/I1
    SLICE_X3Y59          LUT5 (Prop_lut5_I1_O)        0.048     2.479 f  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.420     2.899    O_OBUF[8]_inst_i_2_n_0
    SLICE_X3Y60                                                       f  O_OBUF[10]_inst_i_2/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.129     3.028 f  O_OBUF[10]_inst_i_2/O
                         net (fo=4, routed)           0.442     3.470    O_OBUF[10]_inst_i_2_n_0
    SLICE_X4Y61                                                       f  O_OBUF[6]_inst_i_1/I2
    SLICE_X4Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.513 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           1.397     4.910    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.732 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.732    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.580ns  (logic 2.680ns (40.725%)  route 3.900ns (59.275%))
  Logic Levels:           6  (IBUF=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.393     1.980    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[10]_inst_i_10/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.023 r  O_OBUF[10]_inst_i_10/O
                         net (fo=4, routed)           0.461     2.484    O_OBUF[10]_inst_i_10_n_0
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.527 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.304     2.831    O_OBUF[11]_inst_i_5_n_0
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.050     2.881 r  O_OBUF[10]_inst_i_6/O
                         net (fo=3, routed)           0.432     3.313    O_OBUF[10]_inst_i_6_n_0
    SLICE_X4Y61                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X4Y61          LUT6 (Prop_lut6_I4_O)        0.127     3.440 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.310     4.750    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.580 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.580    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.515ns  (logic 2.577ns (39.558%)  route 3.938ns (60.442%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.457     2.043    A_IBUF[6]
    SLICE_X3Y59                                                       f  O_OBUF[8]_inst_i_9/I3
    SLICE_X3Y59          LUT4 (Prop_lut4_I3_O)        0.043     2.086 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.344     2.431    O_OBUF[8]_inst_i_9_n_0
    SLICE_X3Y59                                                       r  O_OBUF[7]_inst_i_3/I3
    SLICE_X3Y59          LUT5 (Prop_lut5_I3_O)        0.043     2.474 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.419     2.892    O_OBUF[7]_inst_i_3_n_0
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_7/I5
    SLICE_X2Y59          LUT6 (Prop_lut6_I5_O)        0.043     2.935 r  O_OBUF[8]_inst_i_7/O
                         net (fo=1, routed)           0.383     3.318    O_OBUF[8]_inst_i_7_n_0
    SLICE_X3Y60                                                       r  O_OBUF[8]_inst_i_1/I5
    SLICE_X3Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.361 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.336     4.697    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.515 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.515    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.073ns  (logic 2.532ns (41.691%)  route 3.541ns (58.309%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=33, routed)          1.457     2.043    A_IBUF[6]
    SLICE_X3Y59                                                       f  O_OBUF[8]_inst_i_9/I3
    SLICE_X3Y59          LUT4 (Prop_lut4_I3_O)        0.043     2.086 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.344     2.431    O_OBUF[8]_inst_i_9_n_0
    SLICE_X3Y59                                                       r  O_OBUF[7]_inst_i_3/I3
    SLICE_X3Y59          LUT5 (Prop_lut5_I3_O)        0.043     2.474 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.485     2.958    O_OBUF[7]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_1/I5
    SLICE_X0Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.001 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.256     4.257    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.073 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.073    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




