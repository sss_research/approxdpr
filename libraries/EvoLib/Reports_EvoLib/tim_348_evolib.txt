Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 06:45:24 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_348_evolib.txt -name O
| Design       : mul8_348
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.162ns  (logic 2.837ns (34.765%)  route 5.324ns (65.235%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.493     2.088    A_IBUF[3]
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_12/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.131 r  O_OBUF[8]_inst_i_12/O
                         net (fo=4, routed)           0.510     2.641    N_1156
    SLICE_X4Y66                                                       r  O_OBUF[8]_inst_i_10/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     2.684 r  O_OBUF[8]_inst_i_10/O
                         net (fo=3, routed)           0.423     3.106    N_1320
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X4Y65          LUT3 (Prop_lut3_I0_O)        0.048     3.154 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.409     3.563    N_1556
    SLICE_X4Y66                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X4Y66          LUT6 (Prop_lut6_I2_O)        0.129     3.692 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.248     3.940    N_1899
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_3/I3
    SLICE_X2Y66          LUT6 (Prop_lut6_I3_O)        0.043     3.983 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.423     4.406    N_1929
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_5/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.449 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.404     4.853    N_1957
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     4.896 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.414     6.311    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.162 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.162    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.100ns  (logic 2.819ns (34.802%)  route 5.281ns (65.198%))
  Logic Levels:           9  (IBUF=1 LUT3=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.493     2.088    A_IBUF[3]
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_12/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.131 r  O_OBUF[8]_inst_i_12/O
                         net (fo=4, routed)           0.510     2.641    N_1156
    SLICE_X4Y66                                                       r  O_OBUF[8]_inst_i_10/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     2.684 r  O_OBUF[8]_inst_i_10/O
                         net (fo=3, routed)           0.423     3.106    N_1320
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X4Y65          LUT3 (Prop_lut3_I0_O)        0.048     3.154 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.409     3.563    N_1556
    SLICE_X4Y66                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X4Y66          LUT6 (Prop_lut6_I2_O)        0.129     3.692 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.248     3.940    N_1899
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_3/I3
    SLICE_X2Y66          LUT6 (Prop_lut6_I3_O)        0.043     3.983 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.423     4.406    N_1929
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_5/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.449 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.504     4.953    N_1957
    SLICE_X1Y66                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X1Y66          LUT3 (Prop_lut3_I2_O)        0.043     4.996 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.271     6.267    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.100 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.100    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.935ns  (logic 2.822ns (35.561%)  route 5.113ns (64.439%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.493     2.088    A_IBUF[3]
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_12/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.131 r  O_OBUF[8]_inst_i_12/O
                         net (fo=4, routed)           0.510     2.641    N_1156
    SLICE_X4Y66                                                       r  O_OBUF[8]_inst_i_10/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     2.684 r  O_OBUF[8]_inst_i_10/O
                         net (fo=3, routed)           0.423     3.106    N_1320
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X4Y65          LUT3 (Prop_lut3_I0_O)        0.048     3.154 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.409     3.563    N_1556
    SLICE_X4Y66                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X4Y66          LUT6 (Prop_lut6_I2_O)        0.129     3.692 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.248     3.940    N_1899
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_3/I3
    SLICE_X2Y66          LUT6 (Prop_lut6_I3_O)        0.043     3.983 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.423     4.406    N_1929
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_5/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.449 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.263     4.713    N_1957
    SLICE_X0Y66                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X0Y66          LUT6 (Prop_lut6_I5_O)        0.043     4.756 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.344     6.099    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.935 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.935    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.819ns  (logic 2.875ns (36.775%)  route 4.943ns (63.225%))
  Logic Levels:           8  (IBUF=1 LUT3=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.493     2.088    A_IBUF[3]
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_12/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.131 r  O_OBUF[8]_inst_i_12/O
                         net (fo=4, routed)           0.510     2.641    N_1156
    SLICE_X4Y66                                                       r  O_OBUF[8]_inst_i_10/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     2.684 r  O_OBUF[8]_inst_i_10/O
                         net (fo=3, routed)           0.423     3.106    N_1320
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X4Y65          LUT3 (Prop_lut3_I0_O)        0.048     3.154 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.409     3.563    N_1556
    SLICE_X4Y66                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X4Y66          LUT6 (Prop_lut6_I2_O)        0.129     3.692 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.248     3.940    N_1899
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_3/I3
    SLICE_X2Y66          LUT6 (Prop_lut6_I3_O)        0.043     3.983 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.516     4.499    N_1929
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X0Y63          LUT3 (Prop_lut3_I2_O)        0.048     4.547 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.344     5.891    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.927     7.819 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.819    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.721ns  (logic 2.785ns (36.070%)  route 4.936ns (63.930%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.493     2.088    A_IBUF[3]
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_12/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.131 r  O_OBUF[8]_inst_i_12/O
                         net (fo=4, routed)           0.510     2.641    N_1156
    SLICE_X4Y66                                                       r  O_OBUF[8]_inst_i_10/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     2.684 r  O_OBUF[8]_inst_i_10/O
                         net (fo=3, routed)           0.423     3.106    N_1320
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X4Y65          LUT3 (Prop_lut3_I0_O)        0.048     3.154 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.409     3.563    N_1556
    SLICE_X4Y66                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X4Y66          LUT6 (Prop_lut6_I2_O)        0.129     3.692 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.248     3.940    N_1899
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_3/I3
    SLICE_X2Y66          LUT6 (Prop_lut6_I3_O)        0.043     3.983 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.516     4.499    N_1929
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X0Y63          LUT5 (Prop_lut5_I1_O)        0.043     4.542 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.337     5.879    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.721 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.721    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.327ns  (logic 2.908ns (39.685%)  route 4.420ns (60.315%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.493     2.088    A_IBUF[3]
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_12/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.131 r  O_OBUF[8]_inst_i_12/O
                         net (fo=4, routed)           0.510     2.641    N_1156
    SLICE_X4Y66                                                       r  O_OBUF[8]_inst_i_10/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     2.684 r  O_OBUF[8]_inst_i_10/O
                         net (fo=3, routed)           0.423     3.106    N_1320
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X4Y65          LUT3 (Prop_lut3_I0_O)        0.048     3.154 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.436     3.590    N_1556
    SLICE_X3Y66                                                       r  O_OBUF[8]_inst_i_6/I2
    SLICE_X3Y66          LUT5 (Prop_lut5_I2_O)        0.139     3.729 r  O_OBUF[8]_inst_i_6/O
                         net (fo=1, routed)           0.339     4.068    N_1883
    SLICE_X3Y66                                                       r  O_OBUF[8]_inst_i_1/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.136     4.204 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.219     5.423    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.905     7.327 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.327    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.107ns  (logic 2.722ns (38.305%)  route 4.385ns (61.695%))
  Logic Levels:           7  (IBUF=1 LUT3=3 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.493     2.088    A_IBUF[3]
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_12/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.131 r  O_OBUF[8]_inst_i_12/O
                         net (fo=4, routed)           0.510     2.641    N_1156
    SLICE_X4Y66                                                       r  O_OBUF[8]_inst_i_10/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     2.684 r  O_OBUF[8]_inst_i_10/O
                         net (fo=3, routed)           0.423     3.106    N_1320
    SLICE_X4Y65                                                       r  O_OBUF[8]_inst_i_3/I2
    SLICE_X4Y65          LUT3 (Prop_lut3_I2_O)        0.043     3.149 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.337     3.486    N_1557
    SLICE_X3Y66                                                       r  O_OBUF[10]_inst_i_2/I0
    SLICE_X3Y66          LUT3 (Prop_lut3_I0_O)        0.043     3.529 r  O_OBUF[10]_inst_i_2/O
                         net (fo=3, routed)           0.422     3.951    N_1735
    SLICE_X2Y66                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X2Y66          LUT5 (Prop_lut5_I0_O)        0.043     3.994 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.200     5.194    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.913     7.107 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.107    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.037ns  (logic 2.641ns (37.526%)  route 4.396ns (62.474%))
  Logic Levels:           7  (IBUF=1 LUT3=4 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.493     2.088    A_IBUF[3]
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_12/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.131 r  O_OBUF[8]_inst_i_12/O
                         net (fo=4, routed)           0.510     2.641    N_1156
    SLICE_X4Y66                                                       r  O_OBUF[8]_inst_i_10/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     2.684 r  O_OBUF[8]_inst_i_10/O
                         net (fo=3, routed)           0.423     3.106    N_1320
    SLICE_X4Y65                                                       r  O_OBUF[8]_inst_i_3/I2
    SLICE_X4Y65          LUT3 (Prop_lut3_I2_O)        0.043     3.149 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.337     3.486    N_1557
    SLICE_X3Y66                                                       r  O_OBUF[10]_inst_i_2/I0
    SLICE_X3Y66          LUT3 (Prop_lut3_I0_O)        0.043     3.529 r  O_OBUF[10]_inst_i_2/O
                         net (fo=3, routed)           0.422     3.951    N_1735
    SLICE_X2Y66                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X2Y66          LUT3 (Prop_lut3_I1_O)        0.043     3.994 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.212     5.205    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.037 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.037    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.665ns  (logic 2.674ns (40.112%)  route 3.992ns (59.888%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.493     2.088    A_IBUF[3]
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_12/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.131 r  O_OBUF[8]_inst_i_12/O
                         net (fo=4, routed)           0.510     2.641    N_1156
    SLICE_X4Y66                                                       r  O_OBUF[8]_inst_i_10/I2
    SLICE_X4Y66          LUT3 (Prop_lut3_I2_O)        0.043     2.684 r  O_OBUF[8]_inst_i_10/O
                         net (fo=3, routed)           0.423     3.106    N_1320
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X4Y65          LUT3 (Prop_lut3_I0_O)        0.048     3.154 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.436     3.590    N_1556
    SLICE_X3Y66                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X3Y66          LUT5 (Prop_lut5_I0_O)        0.129     3.719 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.130     4.849    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.665 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.665    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.629ns  (logic 2.683ns (40.470%)  route 3.946ns (59.530%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.479     2.074    A_IBUF[3]
    SLICE_X3Y65                                                       r  O_OBUF[6]_inst_i_6/I4
    SLICE_X3Y65          LUT6 (Prop_lut6_I4_O)        0.043     2.117 r  O_OBUF[6]_inst_i_6/O
                         net (fo=4, routed)           0.230     2.346    N_1009
    SLICE_X3Y65                                                       r  O_OBUF[7]_inst_i_13/I0
    SLICE_X3Y65          LUT6 (Prop_lut6_I0_O)        0.043     2.389 r  O_OBUF[7]_inst_i_13/O
                         net (fo=4, routed)           0.676     3.065    N_1277
    SLICE_X6Y65                                                       r  O_OBUF[6]_inst_i_2/I5
    SLICE_X6Y65          LUT6 (Prop_lut6_I5_O)        0.043     3.108 r  O_OBUF[6]_inst_i_2/O
                         net (fo=1, routed)           0.417     3.525    N_1706
    SLICE_X5Y65                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X5Y65          LUT2 (Prop_lut2_I0_O)        0.051     3.576 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.144     4.720    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.909     6.629 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.629    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




