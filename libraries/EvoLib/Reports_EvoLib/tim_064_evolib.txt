Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 16:46:49 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_064_evolib.txt -name O
| Design       : mul8_064
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.774ns  (logic 2.831ns (36.414%)  route 4.943ns (63.586%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT5=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP26                                                              r  A_IBUF[4]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.032     1.626    A_IBUF[4]
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_10/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.669 r  O_OBUF[10]_inst_i_10/O
                         net (fo=4, routed)           0.438     2.108    N_1164
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.051     2.159 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     2.688    N_1415
    SLICE_X2Y60                                                       r  O_OBUF[2]_inst_i_3/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.129     2.817 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.417     3.234    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.277 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.321     3.598    O_OBUF[2]
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_7/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.641 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.359     4.001    N_1733
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     4.044 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.488     4.531    N_1748
    SLICE_X0Y63                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     4.574 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.358     5.932    O_OBUF[14]
    AL31                                                              r  O_OBUF[14]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.774 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.774    O[14]
    AL31                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.696ns  (logic 2.822ns (36.668%)  route 4.874ns (63.332%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT5=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP26                                                              r  A_IBUF[4]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.032     1.626    A_IBUF[4]
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_10/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.669 r  O_OBUF[10]_inst_i_10/O
                         net (fo=4, routed)           0.438     2.108    N_1164
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.051     2.159 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     2.688    N_1415
    SLICE_X2Y60                                                       r  O_OBUF[2]_inst_i_3/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.129     2.817 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.417     3.234    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.277 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.321     3.598    O_OBUF[2]
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_7/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.641 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.359     4.001    N_1733
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     4.044 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.361     4.405    N_1748
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.448 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.415     5.863    O_OBUF[15]
    AK29                                                              r  O_OBUF[15]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.696 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.696    O[15]
    AK29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.688ns  (logic 2.830ns (36.813%)  route 4.858ns (63.187%))
  Logic Levels:           9  (IBUF=1 LUT4=3 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP26                                                              r  A_IBUF[4]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.032     1.626    A_IBUF[4]
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_10/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.669 r  O_OBUF[10]_inst_i_10/O
                         net (fo=4, routed)           0.438     2.108    N_1164
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.051     2.159 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     2.688    N_1415
    SLICE_X2Y60                                                       r  O_OBUF[2]_inst_i_3/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.129     2.817 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.417     3.234    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.277 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.332     3.609    O_OBUF[2]
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.652 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.351     4.003    N_1714
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X0Y61          LUT6 (Prop_lut6_I4_O)        0.043     4.046 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.339     4.385    N_1983
    SLICE_X0Y61                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     4.428 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.419     5.847    O_OBUF[13]
    AM31                                                              r  O_OBUF[13]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.688 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.688    O[13]
    AM31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.289ns  (logic 2.872ns (39.398%)  route 4.417ns (60.602%))
  Logic Levels:           8  (IBUF=1 LUT4=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP26                                                              r  A_IBUF[4]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.032     1.626    A_IBUF[4]
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_10/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.669 r  O_OBUF[10]_inst_i_10/O
                         net (fo=4, routed)           0.438     2.108    N_1164
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.051     2.159 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     2.688    N_1415
    SLICE_X2Y60                                                       r  O_OBUF[2]_inst_i_3/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.129     2.817 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.417     3.234    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.277 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.332     3.609    O_OBUF[2]
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.652 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.390     4.042    N_1714
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.051     4.093 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.278     5.371    O_OBUF[11]
    AL30                                                              r  O_OBUF[11]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.917     7.289 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.289    O[11]
    AL30                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.243ns  (logic 2.776ns (38.331%)  route 4.467ns (61.669%))
  Logic Levels:           8  (IBUF=1 LUT4=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP26                                                              r  A_IBUF[4]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.032     1.626    A_IBUF[4]
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_10/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.669 r  O_OBUF[10]_inst_i_10/O
                         net (fo=4, routed)           0.438     2.108    N_1164
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.051     2.159 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     2.688    N_1415
    SLICE_X2Y60                                                       r  O_OBUF[2]_inst_i_3/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.129     2.817 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.417     3.234    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.277 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.332     3.609    O_OBUF[2]
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.652 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.350     4.002    N_1714
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_1/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     4.045 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.368     5.413    O_OBUF[12]
    AL29                                                              r  O_OBUF[12]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.243 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.243    O[12]
    AL29                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.808ns  (logic 2.813ns (41.314%)  route 3.996ns (58.686%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP26                                                              r  A_IBUF[4]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.032     1.626    A_IBUF[4]
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_10/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.669 r  O_OBUF[10]_inst_i_10/O
                         net (fo=4, routed)           0.438     2.108    N_1164
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.051     2.159 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     2.688    N_1415
    SLICE_X2Y60                                                       r  O_OBUF[2]_inst_i_3/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.129     2.817 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.417     3.234    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_4/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.050     3.284 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.299     3.583    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.127     3.710 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.279     4.990    O_OBUF[10]
    AK28                                                              r  O_OBUF[10]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.808 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.808    O[10]
    AK28                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.172ns  (logic 2.672ns (43.293%)  route 3.500ns (56.707%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AM28                                                              r  A_IBUF[7]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[7]_inst/O
                         net (fo=15, routed)          1.123     1.710    A_IBUF[7]
    SLICE_X0Y60                                                       r  O_OBUF[10]_inst_i_14/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     1.753 r  O_OBUF[10]_inst_i_14/O
                         net (fo=3, routed)           0.258     2.011    O_OBUF[10]_inst_i_14_n_0
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X1Y60          LUT5 (Prop_lut5_I0_O)        0.052     2.063 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.419     2.482    N_1398
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.131     2.613 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.520     3.132    O_OBUF[10]_inst_i_3_n_0
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_1/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.175 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.180     4.356    O_OBUF[9]
    AL28                                                              r  O_OBUF[9]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.172 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.172    O[9]
    AL28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[2]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.124ns  (logic 2.657ns (43.389%)  route 3.467ns (56.611%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP26                                                              r  A_IBUF[4]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.032     1.626    A_IBUF[4]
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_10/I0
    SLICE_X1Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.669 r  O_OBUF[10]_inst_i_10/O
                         net (fo=4, routed)           0.438     2.108    N_1164
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X1Y60          LUT5 (Prop_lut5_I4_O)        0.051     2.159 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     2.688    N_1415
    SLICE_X2Y60                                                       r  O_OBUF[2]_inst_i_3/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.129     2.817 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.417     3.234    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[2]_inst_i_1/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.277 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           1.050     4.327    O_OBUF[2]
    AH24                                                              r  O_OBUF[2]_inst/I
    AH24                 OBUF (Prop_obuf_I_O)         1.797     6.124 r  O_OBUF[2]_inst/O
                         net (fo=0)                   0.000     6.124    O[2]
    AH24                                                              r  O[2] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.866ns  (logic 2.678ns (45.652%)  route 3.188ns (54.348%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AM28                                                              r  A_IBUF[7]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[7]_inst/O
                         net (fo=15, routed)          1.123     1.710    A_IBUF[7]
    SLICE_X0Y60                                                       r  O_OBUF[10]_inst_i_14/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     1.753 r  O_OBUF[10]_inst_i_14/O
                         net (fo=3, routed)           0.258     2.011    O_OBUF[10]_inst_i_14_n_0
    SLICE_X1Y60                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X1Y60          LUT5 (Prop_lut5_I0_O)        0.052     2.063 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.331     2.394    N_1398
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_2/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.131     2.525 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.295     2.820    N_1664
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X0Y62          LUT3 (Prop_lut3_I0_O)        0.043     2.863 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.181     4.044    O_OBUF[8]
    AK26                                                              r  O_OBUF[8]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     5.866 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.866    O[8]
    AK26                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.998ns  (logic 2.540ns (50.830%)  route 2.457ns (49.170%))
  Logic Levels:           3  (IBUF=1 LUT2=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN29                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AN29                                                              r  B_IBUF[4]_inst/I
    AN29                 IBUF (Prop_ibuf_I_O)         0.604     0.604 r  B_IBUF[4]_inst/O
                         net (fo=9, routed)           1.168     1.773    B_IBUF[4]
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_1/I1
    SLICE_X0Y61          LUT2 (Prop_lut2_I1_O)        0.048     1.821 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.289     3.110    O_OBUF[3]
    AJ26                                                              r  O_OBUF[6]_inst/I
    AJ26                 OBUF (Prop_obuf_I_O)         1.888     4.998 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     4.998    O[6]
    AJ26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




