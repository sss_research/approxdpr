Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 23:38:58 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_203_evolib.txt -name O
| Design       : mul8_203
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.170ns  (logic 3.003ns (32.747%)  route 6.167ns (67.253%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=5, routed)           1.213     1.806    A_IBUF[1]
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_8/I1
    SLICE_X1Y61          LUT2 (Prop_lut2_I1_O)        0.053     1.859 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.619     2.478    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_7/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.131     2.609 r  O_OBUF[6]_inst_i_7/O
                         net (fo=3, routed)           0.421     3.030    N_682
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.049     3.079 r  O_OBUF[6]_inst_i_4/O
                         net (fo=4, routed)           0.352     3.431    N_932
    SLICE_X1Y60                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X1Y60          LUT6 (Prop_lut6_I4_O)        0.129     3.560 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.349     3.909    N_1199
    SLICE_X0Y60                                                       r  O_OBUF[15]_inst_i_6/I5
    SLICE_X0Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.952 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.692     4.643    N_1215
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.686 r  O_OBUF[12]_inst_i_7/O
                         net (fo=2, routed)           0.343     5.029    N_1482
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_7/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     5.072 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.417     5.489    N_1733
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X1Y64          LUT6 (Prop_lut6_I4_O)        0.043     5.532 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.354     5.887    N_1748
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X0Y64          LUT4 (Prop_lut4_I1_O)        0.043     5.930 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.408     7.337    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     9.170 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     9.170    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.164ns  (logic 3.006ns (32.801%)  route 6.158ns (67.199%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=1 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=5, routed)           1.213     1.806    A_IBUF[1]
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_8/I1
    SLICE_X1Y61          LUT2 (Prop_lut2_I1_O)        0.053     1.859 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.619     2.478    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_7/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.131     2.609 r  O_OBUF[6]_inst_i_7/O
                         net (fo=3, routed)           0.421     3.030    N_682
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.049     3.079 r  O_OBUF[6]_inst_i_4/O
                         net (fo=4, routed)           0.352     3.431    N_932
    SLICE_X1Y60                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X1Y60          LUT6 (Prop_lut6_I4_O)        0.129     3.560 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.349     3.909    N_1199
    SLICE_X0Y60                                                       r  O_OBUF[15]_inst_i_6/I5
    SLICE_X0Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.952 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.692     4.643    N_1215
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.686 r  O_OBUF[12]_inst_i_7/O
                         net (fo=2, routed)           0.343     5.029    N_1482
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_7/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     5.072 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.417     5.489    N_1733
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X1Y64          LUT6 (Prop_lut6_I4_O)        0.043     5.532 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.339     5.871    N_1748
    SLICE_X0Y64                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X0Y64          LUT6 (Prop_lut6_I2_O)        0.043     5.914 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.414     7.328    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     9.164 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     9.164    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.110ns  (logic 3.022ns (33.169%)  route 6.088ns (66.831%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=1 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=5, routed)           1.213     1.806    A_IBUF[1]
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_8/I1
    SLICE_X1Y61          LUT2 (Prop_lut2_I1_O)        0.053     1.859 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.619     2.478    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_7/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.131     2.609 r  O_OBUF[6]_inst_i_7/O
                         net (fo=3, routed)           0.421     3.030    N_682
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.049     3.079 r  O_OBUF[6]_inst_i_4/O
                         net (fo=4, routed)           0.352     3.431    N_932
    SLICE_X1Y60                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X1Y60          LUT6 (Prop_lut6_I4_O)        0.129     3.560 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.349     3.909    N_1199
    SLICE_X0Y60                                                       r  O_OBUF[15]_inst_i_6/I5
    SLICE_X0Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.952 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.692     4.643    N_1215
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.686 r  O_OBUF[12]_inst_i_7/O
                         net (fo=2, routed)           0.343     5.029    N_1482
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_7/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     5.072 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.417     5.489    N_1733
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X1Y64          LUT6 (Prop_lut6_I4_O)        0.043     5.532 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.356     5.888    N_1748
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     5.931 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.328     7.258    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     9.110 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     9.110    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.724ns  (logic 3.060ns (35.076%)  route 5.664ns (64.924%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=5, routed)           1.213     1.806    A_IBUF[1]
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_8/I1
    SLICE_X1Y61          LUT2 (Prop_lut2_I1_O)        0.053     1.859 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.619     2.478    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_7/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.131     2.609 r  O_OBUF[6]_inst_i_7/O
                         net (fo=3, routed)           0.421     3.030    N_682
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.049     3.079 r  O_OBUF[6]_inst_i_4/O
                         net (fo=4, routed)           0.521     3.600    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_5/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.129     3.729 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.415     4.144    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.043     4.187 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.355     4.542    N_1683
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X3Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.585 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.353     4.938    N_1949
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     4.981 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.423     5.404    N_1965
    SLICE_X1Y64                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X1Y64          LUT4 (Prop_lut4_I0_O)        0.048     5.452 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.344     6.796    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     8.724 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.724    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.459ns  (logic 3.159ns (37.352%)  route 5.299ns (62.648%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=4 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=5, routed)           1.213     1.806    A_IBUF[1]
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_8/I1
    SLICE_X1Y61          LUT2 (Prop_lut2_I1_O)        0.053     1.859 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.619     2.478    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_7/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.131     2.609 r  O_OBUF[6]_inst_i_7/O
                         net (fo=3, routed)           0.421     3.030    N_682
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.049     3.079 r  O_OBUF[6]_inst_i_4/O
                         net (fo=4, routed)           0.352     3.431    N_932
    SLICE_X1Y60                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X1Y60          LUT6 (Prop_lut6_I4_O)        0.129     3.560 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.347     3.907    N_1199
    SLICE_X0Y60                                                       r  O_OBUF[15]_inst_i_9/I4
    SLICE_X0Y60          LUT6 (Prop_lut6_I4_O)        0.043     3.950 r  O_OBUF[15]_inst_i_9/O
                         net (fo=3, routed)           0.406     4.357    N_1214
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_5/I1
    SLICE_X0Y63          LUT4 (Prop_lut4_I1_O)        0.049     4.406 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.452     4.858    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_3/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.129     4.987 r  O_OBUF[11]_inst_i_3/O
                         net (fo=1, routed)           0.137     5.124    N_1714
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.054     5.178 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.351     6.529    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.929     8.459 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.459    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.411ns  (logic 3.012ns (35.814%)  route 5.399ns (64.186%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=4 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=5, routed)           1.213     1.806    A_IBUF[1]
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_8/I1
    SLICE_X1Y61          LUT2 (Prop_lut2_I1_O)        0.053     1.859 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.619     2.478    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_7/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.131     2.609 r  O_OBUF[6]_inst_i_7/O
                         net (fo=3, routed)           0.421     3.030    N_682
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.049     3.079 r  O_OBUF[6]_inst_i_4/O
                         net (fo=4, routed)           0.521     3.600    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_5/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.129     3.729 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.415     4.144    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.043     4.187 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.342     4.529    N_1683
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.053     4.582 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.593     5.175    N_1698
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.131     5.306 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.275     6.581    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     8.411 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     8.411    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.931ns  (logic 2.970ns (37.446%)  route 4.961ns (62.554%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=5, routed)           1.213     1.806    A_IBUF[1]
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_8/I1
    SLICE_X1Y61          LUT2 (Prop_lut2_I1_O)        0.053     1.859 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.619     2.478    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_7/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.131     2.609 r  O_OBUF[6]_inst_i_7/O
                         net (fo=3, routed)           0.421     3.030    N_682
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.049     3.079 r  O_OBUF[6]_inst_i_4/O
                         net (fo=4, routed)           0.521     3.600    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_5/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.129     3.729 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.415     4.144    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_3/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.051     4.195 r  O_OBUF[9]_inst_i_3/O
                         net (fo=1, routed)           0.493     4.688    N_1682
    SLICE_X0Y64                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X0Y64          LUT4 (Prop_lut4_I1_O)        0.132     4.820 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.279     6.099    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.931 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.931    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.619ns  (logic 2.778ns (41.966%)  route 3.841ns (58.034%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=5, routed)           1.213     1.806    A_IBUF[1]
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_8/I1
    SLICE_X1Y61          LUT2 (Prop_lut2_I1_O)        0.053     1.859 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.619     2.478    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_7/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.131     2.609 r  O_OBUF[6]_inst_i_7/O
                         net (fo=3, routed)           0.421     3.030    N_682
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.049     3.079 r  O_OBUF[6]_inst_i_4/O
                         net (fo=4, routed)           0.406     3.485    N_932
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.129     3.614 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           1.183     4.797    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.619 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.619    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.106ns  (logic 2.668ns (43.701%)  route 3.438ns (56.299%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=19, routed)          0.975     1.561    A_IBUF[6]
    SLICE_X0Y61                                                       f  O_OBUF[8]_inst_i_12/I1
    SLICE_X0Y61          LUT2 (Prop_lut2_I1_O)        0.048     1.609 r  O_OBUF[8]_inst_i_12/O
                         net (fo=2, routed)           0.345     1.954    O_OBUF[8]_inst_i_12_n_0
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_7/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.083 r  O_OBUF[8]_inst_i_7/O
                         net (fo=4, routed)           0.418     2.501    N_914
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_3/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.544 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.421     2.965    N_1664
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_1/I3
    SLICE_X0Y62          LUT5 (Prop_lut5_I3_O)        0.043     3.008 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.279     4.287    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.106 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.106    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.190ns  (logic 2.497ns (48.101%)  route 2.694ns (51.899%))
  Logic Levels:           4  (IBUF=1 LUT3=1 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=11, routed)          0.927     1.521    A_IBUF[3]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_2/I2
    SLICE_X1Y60          LUT5 (Prop_lut5_I2_O)        0.043     1.564 r  O_OBUF[8]_inst_i_2/O
                         net (fo=7, routed)           0.586     2.150    N_36
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X0Y63          LUT3 (Prop_lut3_I2_O)        0.043     2.193 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.181     3.374    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.190 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.190    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




