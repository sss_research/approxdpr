Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 01:36:07 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_243_evolib.txt -name O
| Design       : mul8_243
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.199ns  (logic 2.765ns (33.727%)  route 5.434ns (66.273%))
  Logic Levels:           10  (IBUF=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.123     1.703    A_IBUF[7]
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_16/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.746 r  O_OBUF[10]_inst_i_16/O
                         net (fo=1, routed)           0.341     2.087    N_282
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.130 r  O_OBUF[10]_inst_i_12/O
                         net (fo=5, routed)           0.427     2.557    N_915
    SLICE_X1Y59                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X1Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.600 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.012    N_1183
    SLICE_X1Y58                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X1Y58          LUT4 (Prop_lut4_I1_O)        0.043     3.055 r  O_OBUF[10]_inst_i_5/O
                         net (fo=4, routed)           0.523     3.577    N_1198
    SLICE_X2Y58                                                       r  O_OBUF[12]_inst_i_6/I4
    SLICE_X2Y58          LUT6 (Prop_lut6_I4_O)        0.043     3.620 r  O_OBUF[12]_inst_i_6/O
                         net (fo=4, routed)           0.536     4.157    N_1464
    SLICE_X2Y61                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     4.200 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.352     4.551    N_1965
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_3/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     4.594 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.397     4.992    N_1983
    SLICE_X1Y61                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.043     5.035 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.323     6.357    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.199 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.199    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.180ns  (logic 2.756ns (33.694%)  route 5.424ns (66.306%))
  Logic Levels:           10  (IBUF=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.123     1.703    A_IBUF[7]
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_16/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.746 r  O_OBUF[10]_inst_i_16/O
                         net (fo=1, routed)           0.341     2.087    N_282
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.130 r  O_OBUF[10]_inst_i_12/O
                         net (fo=5, routed)           0.427     2.557    N_915
    SLICE_X1Y59                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X1Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.600 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.012    N_1183
    SLICE_X1Y58                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X1Y58          LUT4 (Prop_lut4_I1_O)        0.043     3.055 r  O_OBUF[10]_inst_i_5/O
                         net (fo=4, routed)           0.523     3.577    N_1198
    SLICE_X2Y58                                                       r  O_OBUF[12]_inst_i_6/I4
    SLICE_X2Y58          LUT6 (Prop_lut6_I4_O)        0.043     3.620 r  O_OBUF[12]_inst_i_6/O
                         net (fo=4, routed)           0.536     4.157    N_1464
    SLICE_X2Y61                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     4.200 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.352     4.551    N_1965
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_3/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     4.594 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.305     4.900    N_1983
    SLICE_X0Y61                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.043     4.943 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.405     6.347    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.180 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.180    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.154ns  (logic 2.759ns (33.837%)  route 5.395ns (66.163%))
  Logic Levels:           10  (IBUF=1 LUT4=1 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.123     1.703    A_IBUF[7]
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_16/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.746 r  O_OBUF[10]_inst_i_16/O
                         net (fo=1, routed)           0.341     2.087    N_282
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.130 r  O_OBUF[10]_inst_i_12/O
                         net (fo=5, routed)           0.427     2.557    N_915
    SLICE_X1Y59                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X1Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.600 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.012    N_1183
    SLICE_X1Y58                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X1Y58          LUT4 (Prop_lut4_I1_O)        0.043     3.055 r  O_OBUF[10]_inst_i_5/O
                         net (fo=4, routed)           0.523     3.577    N_1198
    SLICE_X2Y58                                                       r  O_OBUF[12]_inst_i_6/I4
    SLICE_X2Y58          LUT6 (Prop_lut6_I4_O)        0.043     3.620 r  O_OBUF[12]_inst_i_6/O
                         net (fo=4, routed)           0.435     4.056    N_1464
    SLICE_X2Y59                                                       r  O_OBUF[15]_inst_i_7/I0
    SLICE_X2Y59          LUT6 (Prop_lut6_I0_O)        0.043     4.099 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.220     4.319    N_1733
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X3Y59          LUT6 (Prop_lut6_I4_O)        0.043     4.362 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.506     4.868    N_1748
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_1/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     4.911 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.408     6.319    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.154 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.154    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.865ns  (logic 2.816ns (35.802%)  route 5.049ns (64.198%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.123     1.703    A_IBUF[7]
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_16/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.746 r  O_OBUF[10]_inst_i_16/O
                         net (fo=1, routed)           0.341     2.087    N_282
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.130 r  O_OBUF[10]_inst_i_12/O
                         net (fo=5, routed)           0.427     2.557    N_915
    SLICE_X1Y59                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X1Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.600 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.012    N_1183
    SLICE_X1Y58                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X1Y58          LUT4 (Prop_lut4_I1_O)        0.043     3.055 r  O_OBUF[10]_inst_i_5/O
                         net (fo=4, routed)           0.523     3.577    N_1198
    SLICE_X2Y58                                                       r  O_OBUF[12]_inst_i_6/I4
    SLICE_X2Y58          LUT6 (Prop_lut6_I4_O)        0.043     3.620 r  O_OBUF[12]_inst_i_6/O
                         net (fo=4, routed)           0.536     4.157    N_1464
    SLICE_X2Y61                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     4.200 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.352     4.551    N_1965
    SLICE_X2Y61                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X2Y61          LUT4 (Prop_lut4_I1_O)        0.048     4.599 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.335     5.934    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.930     7.865 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.865    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.481ns  (logic 2.711ns (36.236%)  route 4.770ns (63.764%))
  Logic Levels:           9  (IBUF=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.123     1.703    A_IBUF[7]
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_16/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.746 r  O_OBUF[10]_inst_i_16/O
                         net (fo=1, routed)           0.341     2.087    N_282
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.130 r  O_OBUF[10]_inst_i_12/O
                         net (fo=5, routed)           0.427     2.557    N_915
    SLICE_X1Y59                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X1Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.600 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.012    N_1183
    SLICE_X1Y58                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X1Y58          LUT4 (Prop_lut4_I1_O)        0.043     3.055 r  O_OBUF[10]_inst_i_5/O
                         net (fo=4, routed)           0.523     3.577    N_1198
    SLICE_X2Y58                                                       r  O_OBUF[12]_inst_i_6/I4
    SLICE_X2Y58          LUT6 (Prop_lut6_I4_O)        0.043     3.620 r  O_OBUF[12]_inst_i_6/O
                         net (fo=4, routed)           0.324     3.945    N_1464
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.988 r  O_OBUF[11]_inst_i_2/O
                         net (fo=1, routed)           0.292     4.280    N_1714
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X1Y61          LUT4 (Prop_lut4_I0_O)        0.043     4.323 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.328     5.651    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.481 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.481    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.208ns  (logic 2.669ns (37.030%)  route 4.539ns (62.970%))
  Logic Levels:           8  (IBUF=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.123     1.703    A_IBUF[7]
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_16/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.746 r  O_OBUF[10]_inst_i_16/O
                         net (fo=1, routed)           0.341     2.087    N_282
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.130 r  O_OBUF[10]_inst_i_12/O
                         net (fo=5, routed)           0.427     2.557    N_915
    SLICE_X1Y59                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X1Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.600 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.412     3.012    N_1183
    SLICE_X1Y58                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X1Y58          LUT4 (Prop_lut4_I1_O)        0.043     3.055 r  O_OBUF[10]_inst_i_5/O
                         net (fo=4, routed)           0.605     3.660    N_1198
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_2/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.703 r  O_OBUF[10]_inst_i_2/O
                         net (fo=1, routed)           0.229     3.932    N_1698
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X2Y60          LUT4 (Prop_lut4_I0_O)        0.043     3.975 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.401     5.376    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.208 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.208    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[7]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.760ns  (logic 2.699ns (39.919%)  route 4.062ns (60.081%))
  Logic Levels:           7  (IBUF=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL25                                              0.000     0.000 f  B[7] (IN)
                         net (fo=0)                   0.000     0.000    B[7]
    AL25                                                              f  B_IBUF[7]_inst/I
    AL25                 IBUF (Prop_ibuf_I_O)         0.574     0.574 f  B_IBUF[7]_inst/O
                         net (fo=18, routed)          1.157     1.731    B_IBUF[7]
    SLICE_X0Y58                                                       f  O_OBUF[9]_inst_i_10/I1
    SLICE_X0Y58          LUT6 (Prop_lut6_I1_O)        0.043     1.774 r  O_OBUF[9]_inst_i_10/O
                         net (fo=2, routed)           0.516     2.290    N_414
    SLICE_X0Y59                                                       r  O_OBUF[9]_inst_i_6/I2
    SLICE_X0Y59          LUT6 (Prop_lut6_I2_O)        0.043     2.333 r  O_OBUF[9]_inst_i_6/O
                         net (fo=5, routed)           0.298     2.631    N_1164
    SLICE_X3Y60                                                       r  O_OBUF[10]_inst_i_9/I3
    SLICE_X3Y60          LUT4 (Prop_lut4_I3_O)        0.043     2.674 r  O_OBUF[10]_inst_i_9/O
                         net (fo=6, routed)           0.348     3.022    N_1415
    SLICE_X3Y60                                                       r  O_OBUF[9]_inst_i_2/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.043     3.065 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.412     3.477    N_1682
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X1Y60          LUT4 (Prop_lut4_I0_O)        0.048     3.525 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.330     4.856    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.905     6.760 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.760    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[7]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.190ns  (logic 2.656ns (42.917%)  route 3.533ns (57.083%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL25                                              0.000     0.000 f  B[7] (IN)
                         net (fo=0)                   0.000     0.000    B[7]
    AL25                                                              f  B_IBUF[7]_inst/I
    AL25                 IBUF (Prop_ibuf_I_O)         0.574     0.574 f  B_IBUF[7]_inst/O
                         net (fo=18, routed)          1.157     1.731    B_IBUF[7]
    SLICE_X0Y58                                                       f  O_OBUF[9]_inst_i_10/I1
    SLICE_X0Y58          LUT6 (Prop_lut6_I1_O)        0.043     1.774 r  O_OBUF[9]_inst_i_10/O
                         net (fo=2, routed)           0.516     2.290    N_414
    SLICE_X0Y59                                                       r  O_OBUF[9]_inst_i_6/I2
    SLICE_X0Y59          LUT6 (Prop_lut6_I2_O)        0.043     2.333 r  O_OBUF[9]_inst_i_6/O
                         net (fo=5, routed)           0.306     2.638    N_1164
    SLICE_X0Y60                                                       r  O_OBUF[8]_inst_i_2/I5
    SLICE_X0Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.681 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.374     3.056    N_1664
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_1/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.051     3.107 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.181     4.287    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.902     6.190 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.190    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[4]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.994ns  (logic 2.537ns (50.797%)  route 2.457ns (49.203%))
  Logic Levels:           3  (IBUF=1 LUT2=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=12, routed)          1.196     1.790    A_IBUF[3]
    SLICE_X1Y60                                                       r  O_OBUF[4]_inst_i_1/I0
    SLICE_X1Y60          LUT2 (Prop_lut2_I0_O)        0.053     1.843 r  O_OBUF[4]_inst_i_1/O
                         net (fo=1, routed)           1.261     3.104    O_OBUF[4]
    AJ27                                                              r  O_OBUF[4]_inst/I
    AJ27                 OBUF (Prop_obuf_I_O)         1.890     4.994 r  O_OBUF[4]_inst/O
                         net (fo=0)                   0.000     4.994    O[4]
    AJ27                                                              r  O[4] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.828ns  (logic 2.518ns (52.164%)  route 2.309ns (47.836%))
  Logic Levels:           3  (IBUF=1 LUT2=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.118     1.698    A_IBUF[7]
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_1/I1
    SLICE_X0Y61          LUT2 (Prop_lut2_I1_O)        0.051     1.749 r  O_OBUF[5]_inst_i_1/O
                         net (fo=1, routed)           1.191     2.940    O_OBUF[5]
    AJ26                                                              r  O_OBUF[5]_inst/I
    AJ26                 OBUF (Prop_obuf_I_O)         1.888     4.828 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     4.828    O[5]
    AJ26                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------




