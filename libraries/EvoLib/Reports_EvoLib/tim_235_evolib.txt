Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 01:12:26 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_235_evolib.txt -name O
| Design       : mul8_235
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.041ns  (logic 2.774ns (34.501%)  route 5.267ns (65.499%))
  Logic Levels:           8  (IBUF=1 LUT4=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=18, routed)          1.369     1.958    A_IBUF[4]
    SLICE_X7Y61                                                       r  O_OBUF[11]_inst_i_12/I4
    SLICE_X7Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.001 f  O_OBUF[11]_inst_i_12/O
                         net (fo=3, routed)           0.524     2.526    O_OBUF[11]_inst_i_12_n_0
    SLICE_X6Y60                                                       f  O_OBUF[11]_inst_i_9/I1
    SLICE_X6Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.569 r  O_OBUF[11]_inst_i_9/O
                         net (fo=4, routed)           0.433     3.002    O_OBUF[11]_inst_i_9_n_0
    SLICE_X6Y61                                                       r  O_OBUF[13]_inst_i_12/I0
    SLICE_X6Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.045 r  O_OBUF[13]_inst_i_12/O
                         net (fo=3, routed)           0.417     3.462    O_OBUF[13]_inst_i_12_n_0
    SLICE_X5Y61                                                       r  O_OBUF[13]_inst_i_3/I2
    SLICE_X5Y61          LUT5 (Prop_lut5_I2_O)        0.043     3.505 f  O_OBUF[13]_inst_i_3/O
                         net (fo=4, routed)           0.691     4.196    O_OBUF[13]_inst_i_3_n_0
    SLICE_X3Y63                                                       f  O_OBUF[14]_inst_i_3/I3
    SLICE_X3Y63          LUT4 (Prop_lut4_I3_O)        0.048     4.244 r  O_OBUF[14]_inst_i_3/O
                         net (fo=1, routed)           0.413     4.656    O_OBUF[14]_inst_i_3_n_0
    SLICE_X1Y63                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.129     4.785 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.420     6.205    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.041 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.041    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.875ns  (logic 2.866ns (36.391%)  route 5.009ns (63.609%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=18, routed)          1.286     1.876    A_IBUF[4]
    SLICE_X5Y59                                                       r  O_OBUF[7]_inst_i_8/I2
    SLICE_X5Y59          LUT6 (Prop_lut6_I2_O)        0.043     1.919 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.420     2.339    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y59                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X3Y59          LUT3 (Prop_lut3_I0_O)        0.051     2.390 r  O_OBUF[7]_inst_i_4/O
                         net (fo=5, routed)           0.482     2.872    O_OBUF[7]_inst_i_4_n_0
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_6/I0
    SLICE_X3Y61          LUT5 (Prop_lut5_I0_O)        0.137     3.009 r  O_OBUF[6]_inst_i_6/O
                         net (fo=3, routed)           0.417     3.426    O_OBUF[6]_inst_i_6_n_0
    SLICE_X3Y60                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.129     3.555 r  O_OBUF[8]_inst_i_3/O
                         net (fo=5, routed)           0.668     4.223    O_OBUF[8]_inst_i_3_n_0
    SLICE_X4Y62                                                       r  O_OBUF[13]_inst_i_7/I0
    SLICE_X4Y62          LUT6 (Prop_lut6_I0_O)        0.043     4.266 r  O_OBUF[13]_inst_i_7/O
                         net (fo=2, routed)           0.442     4.707    O_OBUF[13]_inst_i_7_n_0
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X2Y63          LUT2 (Prop_lut2_I1_O)        0.043     4.750 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.294     6.045    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.875 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.875    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.852ns  (logic 2.760ns (35.152%)  route 5.092ns (64.848%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL26                                              0.000     0.000 r  B[5] (IN)
                         net (fo=0)                   0.000     0.000    B[5]
    AL26                                                              r  B_IBUF[5]_inst/I
    AL26                 IBUF (Prop_ibuf_I_O)         0.570     0.570 r  B_IBUF[5]_inst/O
                         net (fo=14, routed)          1.249     1.819    B_IBUF[5]
    SLICE_X3Y60                                                       r  O_OBUF[13]_inst_i_43/I2
    SLICE_X3Y60          LUT6 (Prop_lut6_I2_O)        0.043     1.862 f  O_OBUF[13]_inst_i_43/O
                         net (fo=3, routed)           0.490     2.353    O_OBUF[13]_inst_i_43_n_0
    SLICE_X4Y60                                                       f  O_OBUF[13]_inst_i_32/I5
    SLICE_X4Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.396 r  O_OBUF[13]_inst_i_32/O
                         net (fo=3, routed)           0.495     2.890    O_OBUF[13]_inst_i_32_n_0
    SLICE_X4Y62                                                       r  O_OBUF[12]_inst_i_9/I1
    SLICE_X4Y62          LUT3 (Prop_lut3_I1_O)        0.050     2.940 r  O_OBUF[12]_inst_i_9/O
                         net (fo=6, routed)           0.503     3.443    O_OBUF[12]_inst_i_9_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_5/I2
    SLICE_X4Y63          LUT4 (Prop_lut4_I2_O)        0.126     3.569 r  O_OBUF[11]_inst_i_5/O
                         net (fo=7, routed)           0.514     4.083    O_OBUF[11]_inst_i_5_n_0
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_4/I0
    SLICE_X3Y62          LUT5 (Prop_lut5_I0_O)        0.043     4.126 r  O_OBUF[12]_inst_i_4/O
                         net (fo=1, routed)           0.417     4.544    O_OBUF[12]_inst_i_4_n_0
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.043     4.587 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.424     6.010    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.852 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.852    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.805ns  (logic 2.868ns (36.753%)  route 4.936ns (63.247%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=18, routed)          1.286     1.876    A_IBUF[4]
    SLICE_X5Y59                                                       r  O_OBUF[7]_inst_i_8/I2
    SLICE_X5Y59          LUT6 (Prop_lut6_I2_O)        0.043     1.919 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.420     2.339    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y59                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X3Y59          LUT3 (Prop_lut3_I0_O)        0.051     2.390 r  O_OBUF[7]_inst_i_4/O
                         net (fo=5, routed)           0.482     2.872    O_OBUF[7]_inst_i_4_n_0
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_6/I0
    SLICE_X3Y61          LUT5 (Prop_lut5_I0_O)        0.137     3.009 r  O_OBUF[6]_inst_i_6/O
                         net (fo=3, routed)           0.417     3.426    O_OBUF[6]_inst_i_6_n_0
    SLICE_X3Y60                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.129     3.555 r  O_OBUF[8]_inst_i_3/O
                         net (fo=5, routed)           0.668     4.223    O_OBUF[8]_inst_i_3_n_0
    SLICE_X4Y62                                                       r  O_OBUF[13]_inst_i_7/I0
    SLICE_X4Y62          LUT6 (Prop_lut6_I0_O)        0.043     4.266 r  O_OBUF[13]_inst_i_7/O
                         net (fo=2, routed)           0.241     4.506    O_OBUF[13]_inst_i_7_n_0
    SLICE_X2Y62                                                       r  O_OBUF[13]_inst_i_1/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     4.549 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.422     5.972    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.805 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.805    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.802ns  (logic 2.780ns (35.632%)  route 5.022ns (64.368%))
  Logic Levels:           8  (IBUF=1 LUT2=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=19, routed)          1.305     1.885    A_IBUF[7]
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X1Y62          LUT2 (Prop_lut2_I0_O)        0.048     1.933 r  O_OBUF[15]_inst_i_14/O
                         net (fo=4, routed)           0.512     2.445    O_OBUF[15]_inst_i_14_n_0
    SLICE_X1Y61                                                       r  O_OBUF[14]_inst_i_8/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.129     2.574 r  O_OBUF[14]_inst_i_8/O
                         net (fo=4, routed)           0.512     3.085    O_OBUF[14]_inst_i_8_n_0
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_2/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.128 f  O_OBUF[12]_inst_i_2/O
                         net (fo=4, routed)           0.436     3.564    O_OBUF[12]_inst_i_2_n_0
    SLICE_X2Y63                                                       f  O_OBUF[14]_inst_i_4/I1
    SLICE_X2Y63          LUT2 (Prop_lut2_I1_O)        0.043     3.607 r  O_OBUF[14]_inst_i_4/O
                         net (fo=4, routed)           0.510     4.117    O_OBUF[14]_inst_i_4_n_0
    SLICE_X4Y63                                                       r  O_OBUF[15]_inst_i_2/I0
    SLICE_X4Y63          LUT6 (Prop_lut6_I0_O)        0.043     4.160 f  O_OBUF[15]_inst_i_2/O
                         net (fo=1, routed)           0.423     4.583    O_OBUF[15]_inst_i_2_n_0
    SLICE_X1Y63                                                       f  O_OBUF[15]_inst_i_1/I0
    SLICE_X1Y63          LUT6 (Prop_lut6_I0_O)        0.043     4.626 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.324     5.950    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.802 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.802    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.651ns  (logic 2.968ns (38.789%)  route 4.683ns (61.211%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=18, routed)          1.286     1.876    A_IBUF[4]
    SLICE_X5Y59                                                       r  O_OBUF[7]_inst_i_8/I2
    SLICE_X5Y59          LUT6 (Prop_lut6_I2_O)        0.043     1.919 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.420     2.339    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y59                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X3Y59          LUT3 (Prop_lut3_I0_O)        0.051     2.390 r  O_OBUF[7]_inst_i_4/O
                         net (fo=5, routed)           0.482     2.872    O_OBUF[7]_inst_i_4_n_0
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_6/I0
    SLICE_X3Y61          LUT5 (Prop_lut5_I0_O)        0.137     3.009 r  O_OBUF[6]_inst_i_6/O
                         net (fo=3, routed)           0.417     3.426    O_OBUF[6]_inst_i_6_n_0
    SLICE_X3Y60                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.129     3.555 r  O_OBUF[8]_inst_i_3/O
                         net (fo=5, routed)           0.425     3.980    O_OBUF[8]_inst_i_3_n_0
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_6/I2
    SLICE_X3Y62          LUT3 (Prop_lut3_I2_O)        0.048     4.028 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.294     4.322    O_OBUF[11]_inst_i_6_n_0
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_1/I5
    SLICE_X3Y63          LUT6 (Prop_lut6_I5_O)        0.129     4.451 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.359     5.810    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.651 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.651    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.510ns  (logic 2.958ns (39.390%)  route 4.552ns (60.610%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT3=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=18, routed)          1.286     1.876    A_IBUF[4]
    SLICE_X5Y59                                                       r  O_OBUF[7]_inst_i_8/I2
    SLICE_X5Y59          LUT6 (Prop_lut6_I2_O)        0.043     1.919 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.420     2.339    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y59                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X3Y59          LUT3 (Prop_lut3_I0_O)        0.051     2.390 r  O_OBUF[7]_inst_i_4/O
                         net (fo=5, routed)           0.482     2.872    O_OBUF[7]_inst_i_4_n_0
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_6/I0
    SLICE_X3Y61          LUT5 (Prop_lut5_I0_O)        0.137     3.009 r  O_OBUF[6]_inst_i_6/O
                         net (fo=3, routed)           0.417     3.426    O_OBUF[6]_inst_i_6_n_0
    SLICE_X3Y60                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.129     3.555 r  O_OBUF[8]_inst_i_3/O
                         net (fo=5, routed)           0.425     3.980    O_OBUF[8]_inst_i_3_n_0
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_6/I2
    SLICE_X3Y62          LUT3 (Prop_lut3_I2_O)        0.048     4.028 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.214     4.242    O_OBUF[11]_inst_i_6_n_0
    SLICE_X3Y63                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X3Y63          LUT2 (Prop_lut2_I1_O)        0.129     4.371 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.307     5.678    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.510 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.510    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.171ns  (logic 2.894ns (40.355%)  route 4.277ns (59.645%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=18, routed)          1.286     1.876    A_IBUF[4]
    SLICE_X5Y59                                                       r  O_OBUF[7]_inst_i_8/I2
    SLICE_X5Y59          LUT6 (Prop_lut6_I2_O)        0.043     1.919 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.420     2.339    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y59                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X3Y59          LUT3 (Prop_lut3_I0_O)        0.051     2.390 r  O_OBUF[7]_inst_i_4/O
                         net (fo=5, routed)           0.482     2.872    O_OBUF[7]_inst_i_4_n_0
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_6/I0
    SLICE_X3Y61          LUT5 (Prop_lut5_I0_O)        0.137     3.009 r  O_OBUF[6]_inst_i_6/O
                         net (fo=3, routed)           0.425     3.434    O_OBUF[6]_inst_i_6_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_2/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.129     3.563 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.487     4.050    O_OBUF[7]_inst_i_2_n_0
    SLICE_X2Y63                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y63          LUT2 (Prop_lut2_I0_O)        0.045     4.095 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.177     5.272    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.899     7.171 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     7.171    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.014ns  (logic 2.899ns (41.335%)  route 4.115ns (58.665%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=18, routed)          1.286     1.876    A_IBUF[4]
    SLICE_X5Y59                                                       r  O_OBUF[7]_inst_i_8/I2
    SLICE_X5Y59          LUT6 (Prop_lut6_I2_O)        0.043     1.919 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.420     2.339    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y59                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X3Y59          LUT3 (Prop_lut3_I0_O)        0.051     2.390 r  O_OBUF[7]_inst_i_4/O
                         net (fo=5, routed)           0.482     2.872    O_OBUF[7]_inst_i_4_n_0
    SLICE_X3Y61                                                       r  O_OBUF[6]_inst_i_6/I0
    SLICE_X3Y61          LUT5 (Prop_lut5_I0_O)        0.137     3.009 r  O_OBUF[6]_inst_i_6/O
                         net (fo=3, routed)           0.417     3.426    O_OBUF[6]_inst_i_6_n_0
    SLICE_X3Y60                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.129     3.555 r  O_OBUF[8]_inst_i_3/O
                         net (fo=5, routed)           0.326     3.881    O_OBUF[8]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X2Y63          LUT2 (Prop_lut2_I1_O)        0.046     3.927 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.184     5.110    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.904     7.014 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.014    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.709ns  (logic 2.715ns (40.465%)  route 3.994ns (59.535%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=22, routed)          1.277     1.871    A_IBUF[1]
    SLICE_X2Y59                                                       r  O_OBUF[6]_inst_i_9/I4
    SLICE_X2Y59          LUT6 (Prop_lut6_I4_O)        0.043     1.914 r  O_OBUF[6]_inst_i_9/O
                         net (fo=2, routed)           0.356     2.269    O_OBUF[6]_inst_i_9_n_0
    SLICE_X2Y59                                                       r  O_OBUF[6]_inst_i_3/I1
    SLICE_X2Y59          LUT3 (Prop_lut3_I1_O)        0.043     2.312 r  O_OBUF[6]_inst_i_3/O
                         net (fo=5, routed)           0.462     2.774    O_OBUF[6]_inst_i_3_n_0
    SLICE_X3Y59                                                       r  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y59          LUT6 (Prop_lut6_I0_O)        0.127     2.901 r  O_OBUF[5]_inst_i_2/O
                         net (fo=3, routed)           0.296     3.198    O_OBUF[5]_inst_i_2_n_0
    SLICE_X3Y60                                                       r  O_OBUF[6]_inst_i_7/I1
    SLICE_X3Y60          LUT3 (Prop_lut3_I1_O)        0.043     3.241 r  O_OBUF[6]_inst_i_7/O
                         net (fo=1, routed)           0.342     3.583    O_OBUF[6]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     3.626 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.261     4.886    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.709 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.709    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




