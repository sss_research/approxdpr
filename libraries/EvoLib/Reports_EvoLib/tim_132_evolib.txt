Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 20:13:37 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_132_evolib.txt -name O
| Design       : mul8_132
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.489ns  (logic 3.020ns (35.580%)  route 5.469ns (64.420%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.410     2.004    A_IBUF[3]
    SLICE_X4Y62                                                       r  O_OBUF[11]_inst_i_22/I3
    SLICE_X4Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.047 r  O_OBUF[11]_inst_i_22/O
                         net (fo=2, routed)           0.343     2.391    N_514
    SLICE_X4Y62                                                       r  O_OBUF[11]_inst_i_18/I5
    SLICE_X4Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.434 r  O_OBUF[11]_inst_i_18/O
                         net (fo=2, routed)           0.508     2.942    N_582
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_10/I3
    SLICE_X3Y62          LUT5 (Prop_lut5_I3_O)        0.053     2.995 r  O_OBUF[11]_inst_i_10/O
                         net (fo=5, routed)           0.444     3.439    N_684
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X2Y63          LUT3 (Prop_lut3_I0_O)        0.131     3.570 r  O_OBUF[11]_inst_i_3/O
                         net (fo=4, routed)           0.436     4.006    N_753
    SLICE_X3Y65                                                       r  O_OBUF[15]_inst_i_17/I1
    SLICE_X3Y65          LUT3 (Prop_lut3_I1_O)        0.133     4.139 r  O_OBUF[15]_inst_i_17/O
                         net (fo=2, routed)           0.511     4.650    N_1160
    SLICE_X3Y64                                                       r  O_OBUF[15]_inst_i_4/I5
    SLICE_X3Y64          LUT6 (Prop_lut6_I5_O)        0.129     4.779 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.416     5.195    O_OBUF[15]_inst_i_4_n_0
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     5.238 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.400     6.638    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.489 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.489    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.472ns  (logic 3.095ns (36.531%)  route 5.377ns (63.469%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT3=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.410     2.004    A_IBUF[3]
    SLICE_X4Y62                                                       r  O_OBUF[11]_inst_i_22/I3
    SLICE_X4Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.047 r  O_OBUF[11]_inst_i_22/O
                         net (fo=2, routed)           0.343     2.391    N_514
    SLICE_X4Y62                                                       r  O_OBUF[11]_inst_i_18/I5
    SLICE_X4Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.434 r  O_OBUF[11]_inst_i_18/O
                         net (fo=2, routed)           0.508     2.942    N_582
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_10/I3
    SLICE_X3Y62          LUT5 (Prop_lut5_I3_O)        0.053     2.995 r  O_OBUF[11]_inst_i_10/O
                         net (fo=5, routed)           0.444     3.439    N_684
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X2Y63          LUT3 (Prop_lut3_I0_O)        0.131     3.570 r  O_OBUF[11]_inst_i_3/O
                         net (fo=4, routed)           0.436     4.006    N_753
    SLICE_X3Y65                                                       r  O_OBUF[15]_inst_i_17/I1
    SLICE_X3Y65          LUT3 (Prop_lut3_I1_O)        0.133     4.139 r  O_OBUF[15]_inst_i_17/O
                         net (fo=2, routed)           0.431     4.570    N_1160
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_2/I1
    SLICE_X2Y64          LUT6 (Prop_lut6_I1_O)        0.129     4.699 r  O_OBUF[12]_inst_i_2/O
                         net (fo=1, routed)           0.443     5.142    N_1304
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X2Y64          LUT2 (Prop_lut2_I1_O)        0.043     5.185 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.361     6.546    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.926     8.472 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.472    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.260ns  (logic 3.002ns (36.340%)  route 5.259ns (63.660%))
  Logic Levels:           9  (IBUF=1 LUT3=3 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.410     2.004    A_IBUF[3]
    SLICE_X4Y62                                                       r  O_OBUF[11]_inst_i_22/I3
    SLICE_X4Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.047 r  O_OBUF[11]_inst_i_22/O
                         net (fo=2, routed)           0.343     2.391    N_514
    SLICE_X4Y62                                                       r  O_OBUF[11]_inst_i_18/I5
    SLICE_X4Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.434 r  O_OBUF[11]_inst_i_18/O
                         net (fo=2, routed)           0.508     2.942    N_582
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_10/I3
    SLICE_X3Y62          LUT5 (Prop_lut5_I3_O)        0.053     2.995 r  O_OBUF[11]_inst_i_10/O
                         net (fo=5, routed)           0.444     3.439    N_684
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X2Y63          LUT3 (Prop_lut3_I0_O)        0.131     3.570 r  O_OBUF[11]_inst_i_3/O
                         net (fo=4, routed)           0.436     4.006    N_753
    SLICE_X3Y65                                                       r  O_OBUF[15]_inst_i_17/I1
    SLICE_X3Y65          LUT3 (Prop_lut3_I1_O)        0.133     4.139 r  O_OBUF[15]_inst_i_17/O
                         net (fo=2, routed)           0.511     4.650    N_1160
    SLICE_X3Y64                                                       r  O_OBUF[15]_inst_i_4/I5
    SLICE_X3Y64          LUT6 (Prop_lut6_I5_O)        0.129     4.779 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.263     5.042    O_OBUF[15]_inst_i_4_n_0
    SLICE_X2Y65                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X2Y65          LUT3 (Prop_lut3_I2_O)        0.043     5.085 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.343     6.428    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.260 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.260    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.093ns  (logic 2.731ns (33.742%)  route 5.362ns (66.258%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT5=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.410     2.004    A_IBUF[3]
    SLICE_X4Y62                                                       r  O_OBUF[11]_inst_i_22/I3
    SLICE_X4Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.047 r  O_OBUF[11]_inst_i_22/O
                         net (fo=2, routed)           0.343     2.391    N_514
    SLICE_X4Y62                                                       r  O_OBUF[11]_inst_i_18/I5
    SLICE_X4Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.434 r  O_OBUF[11]_inst_i_18/O
                         net (fo=2, routed)           0.508     2.942    N_582
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_9/I3
    SLICE_X3Y62          LUT5 (Prop_lut5_I3_O)        0.043     2.985 r  O_OBUF[11]_inst_i_9/O
                         net (fo=5, routed)           0.522     3.507    N_685
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X3Y63          LUT3 (Prop_lut3_I2_O)        0.043     3.550 r  O_OBUF[11]_inst_i_2/O
                         net (fo=4, routed)           0.429     3.980    N_758
    SLICE_X3Y65                                                       r  O_OBUF[14]_inst_i_6/I2
    SLICE_X3Y65          LUT6 (Prop_lut6_I2_O)        0.043     4.023 r  O_OBUF[14]_inst_i_6/O
                         net (fo=1, routed)           0.406     4.429    O_OBUF[14]_inst_i_6_n_0
    SLICE_X3Y64                                                       r  O_OBUF[14]_inst_i_2/I3
    SLICE_X3Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.472 r  O_OBUF[14]_inst_i_2/O
                         net (fo=1, routed)           0.332     4.804    O_OBUF[14]_inst_i_2_n_0
    SLICE_X2Y64                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X2Y64          LUT6 (Prop_lut6_I1_O)        0.043     4.847 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.410     6.257    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.093 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.093    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.434ns  (logic 2.910ns (39.144%)  route 4.524ns (60.856%))
  Logic Levels:           9  (IBUF=1 LUT2=2 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=19, routed)          1.357     1.947    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.051     1.998 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.245     2.243    N_408
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_2/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.372 f  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.517     2.889    O_OBUF[5]_inst_i_2_n_0
    SLICE_X1Y60                                                       f  O_OBUF[7]_inst_i_18/I1
    SLICE_X1Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.932 r  O_OBUF[7]_inst_i_18/O
                         net (fo=1, routed)           0.177     3.109    O_OBUF[7]_inst_i_18_n_0
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_6/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.152 r  O_OBUF[7]_inst_i_6/O
                         net (fo=2, routed)           0.379     3.532    N_922
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_3/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.051     3.583 r  O_OBUF[8]_inst_i_3/O
                         net (fo=4, routed)           0.305     3.887    N_966
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_6/I3
    SLICE_X2Y63          LUT5 (Prop_lut5_I3_O)        0.129     4.016 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.354     4.370    N_1022
    SLICE_X2Y65                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X2Y65          LUT2 (Prop_lut2_I1_O)        0.043     4.413 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.190     5.603    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.434 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.434    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.403ns  (logic 2.909ns (39.294%)  route 4.494ns (60.706%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=19, routed)          1.357     1.947    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.051     1.998 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.245     2.243    N_408
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_2/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.372 f  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.517     2.889    O_OBUF[5]_inst_i_2_n_0
    SLICE_X1Y60                                                       f  O_OBUF[7]_inst_i_18/I1
    SLICE_X1Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.932 r  O_OBUF[7]_inst_i_18/O
                         net (fo=1, routed)           0.177     3.109    O_OBUF[7]_inst_i_18_n_0
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_6/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.152 r  O_OBUF[7]_inst_i_6/O
                         net (fo=2, routed)           0.379     3.532    N_922
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_3/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.051     3.583 r  O_OBUF[8]_inst_i_3/O
                         net (fo=4, routed)           0.305     3.887    N_966
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_6/I3
    SLICE_X2Y63          LUT5 (Prop_lut5_I3_O)        0.129     4.016 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.231     4.247    N_1022
    SLICE_X3Y65                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X3Y65          LUT4 (Prop_lut4_I3_O)        0.043     4.290 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.283     5.573    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.403 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.403    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.382ns  (logic 2.920ns (39.556%)  route 4.462ns (60.444%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT5=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=19, routed)          1.357     1.947    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.051     1.998 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.245     2.243    N_408
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_2/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.372 f  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.517     2.889    O_OBUF[5]_inst_i_2_n_0
    SLICE_X1Y60                                                       f  O_OBUF[7]_inst_i_18/I1
    SLICE_X1Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.932 r  O_OBUF[7]_inst_i_18/O
                         net (fo=1, routed)           0.177     3.109    O_OBUF[7]_inst_i_18_n_0
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_6/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.152 r  O_OBUF[7]_inst_i_6/O
                         net (fo=2, routed)           0.379     3.532    N_922
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_3/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.051     3.583 r  O_OBUF[8]_inst_i_3/O
                         net (fo=4, routed)           0.305     3.887    N_966
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_6/I3
    SLICE_X2Y63          LUT5 (Prop_lut5_I3_O)        0.129     4.016 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.180     4.196    N_1022
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_1/I5
    SLICE_X4Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.239 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.301     5.540    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.382 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.382    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.186ns  (logic 2.948ns (41.031%)  route 4.237ns (58.969%))
  Logic Levels:           8  (IBUF=1 LUT2=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=19, routed)          1.357     1.947    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.051     1.998 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.245     2.243    N_408
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_2/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.372 f  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.517     2.889    O_OBUF[5]_inst_i_2_n_0
    SLICE_X1Y60                                                       f  O_OBUF[7]_inst_i_18/I1
    SLICE_X1Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.932 r  O_OBUF[7]_inst_i_18/O
                         net (fo=1, routed)           0.177     3.109    O_OBUF[7]_inst_i_18_n_0
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_6/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.152 r  O_OBUF[7]_inst_i_6/O
                         net (fo=2, routed)           0.379     3.532    N_922
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_3/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.051     3.583 r  O_OBUF[8]_inst_i_3/O
                         net (fo=4, routed)           0.293     3.876    N_966
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.137     4.013 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.268     5.281    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.905     7.186 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.186    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.787ns  (logic 2.633ns (38.798%)  route 4.154ns (61.202%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=17, routed)          1.301     1.896    A_IBUF[2]
    SLICE_X2Y62                                                       r  O_OBUF[7]_inst_i_15/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.939 r  O_OBUF[7]_inst_i_15/O
                         net (fo=3, routed)           0.329     2.268    N_453
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_3/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.043     2.311 r  O_OBUF[6]_inst_i_3/O
                         net (fo=4, routed)           0.504     2.815    N_564
    SLICE_X1Y61                                                       r  O_OBUF[5]_inst_i_3/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.858 r  O_OBUF[5]_inst_i_3/O
                         net (fo=2, routed)           0.414     3.272    N_664
    SLICE_X1Y60                                                       r  O_OBUF[6]_inst_i_7/I1
    SLICE_X1Y60          LUT6 (Prop_lut6_I1_O)        0.043     3.315 r  O_OBUF[6]_inst_i_7/O
                         net (fo=1, routed)           0.338     3.653    O_OBUF[6]_inst_i_7_n_0
    SLICE_X1Y62                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.696 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.965    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.787 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.787    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.571ns  (logic 2.715ns (41.318%)  route 3.856ns (58.682%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=19, routed)          1.357     1.947    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.051     1.998 r  O_OBUF[2]_inst_i_3/O
                         net (fo=2, routed)           0.245     2.243    N_408
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_2/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.372 f  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.517     2.889    O_OBUF[5]_inst_i_2_n_0
    SLICE_X1Y60                                                       f  O_OBUF[7]_inst_i_18/I1
    SLICE_X1Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.932 r  O_OBUF[7]_inst_i_18/O
                         net (fo=1, routed)           0.177     3.109    O_OBUF[7]_inst_i_18_n_0
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_6/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.152 r  O_OBUF[7]_inst_i_6/O
                         net (fo=2, routed)           0.379     3.532    N_922
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.043     3.575 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.180     4.755    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.571 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.571    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




