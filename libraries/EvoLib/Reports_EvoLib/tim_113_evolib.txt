Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 19:14:45 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_113_evolib.txt -name O
| Design       : mul8_113
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.974ns  (logic 2.747ns (39.387%)  route 4.227ns (60.613%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.108     1.695    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.051     1.746 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.453     2.199    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y60          LUT5 (Prop_lut5_I0_O)        0.129     2.328 r  O_OBUF[12]_inst_i_3/O
                         net (fo=5, routed)           0.415     2.743    O_OBUF[12]_inst_i_3_n_0
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_3/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.786 r  O_OBUF[15]_inst_i_3/O
                         net (fo=1, routed)           0.412     3.198    O_OBUF[15]_inst_i_3_n_0
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_2/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.241 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.407     3.648    O_OBUF[15]_inst_i_2_n_0
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.043     3.691 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.432     5.123    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     6.974 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     6.974    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.949ns  (logic 2.825ns (40.659%)  route 4.123ns (59.341%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.108     1.695    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.051     1.746 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.453     2.199    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y60          LUT5 (Prop_lut5_I0_O)        0.129     2.328 r  O_OBUF[12]_inst_i_3/O
                         net (fo=5, routed)           0.415     2.743    O_OBUF[12]_inst_i_3_n_0
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_3/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.786 r  O_OBUF[15]_inst_i_3/O
                         net (fo=1, routed)           0.412     3.198    O_OBUF[15]_inst_i_3_n_0
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_2/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.241 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.407     3.648    O_OBUF[15]_inst_i_2_n_0
    SLICE_X1Y63                                                       r  O_OBUF[14]_inst_i_1/I0
    SLICE_X1Y63          LUT5 (Prop_lut5_I0_O)        0.051     3.699 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.328     5.027    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.922     6.949 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     6.949    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.856ns  (logic 3.012ns (43.935%)  route 3.844ns (56.065%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.108     1.695    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.051     1.746 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.453     2.199    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X0Y60          LUT5 (Prop_lut5_I0_O)        0.139     2.338 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.300     2.637    O_OBUF[10]_inst_i_4_n_0
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X0Y61          LUT5 (Prop_lut5_I0_O)        0.139     2.776 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.324     3.100    O_OBUF[12]_inst_i_5_n_0
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_4/I3
    SLICE_X0Y62          LUT5 (Prop_lut5_I3_O)        0.135     3.235 r  O_OBUF[13]_inst_i_4/O
                         net (fo=1, routed)           0.320     3.555    O_OBUF[13]_inst_i_4_n_0
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X0Y63          LUT3 (Prop_lut3_I2_O)        0.129     3.684 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.339     5.024    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.856 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.856    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.430ns  (logic 2.886ns (44.884%)  route 3.544ns (55.116%))
  Logic Levels:           6  (IBUF=1 LUT4=2 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.108     1.695    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.051     1.746 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.453     2.199    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X0Y60          LUT5 (Prop_lut5_I0_O)        0.139     2.338 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.300     2.637    O_OBUF[10]_inst_i_4_n_0
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X0Y61          LUT5 (Prop_lut5_I0_O)        0.139     2.776 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.270     3.047    O_OBUF[12]_inst_i_5_n_0
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_1/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.129     3.176 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.413     4.588    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.430 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.430    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.402ns  (logic 2.886ns (45.088%)  route 3.515ns (54.912%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.108     1.695    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.051     1.746 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.453     2.199    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X0Y60          LUT5 (Prop_lut5_I0_O)        0.139     2.338 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.300     2.637    O_OBUF[10]_inst_i_4_n_0
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X0Y61          LUT5 (Prop_lut5_I0_O)        0.139     2.776 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.324     3.100    O_OBUF[12]_inst_i_5_n_0
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X0Y62          LUT5 (Prop_lut5_I4_O)        0.129     3.229 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.330     4.560    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.402 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.402    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.997ns  (logic 2.626ns (43.791%)  route 3.371ns (56.209%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.108     1.695    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.051     1.746 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.453     2.199    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y60          LUT5 (Prop_lut5_I0_O)        0.129     2.328 r  O_OBUF[12]_inst_i_3/O
                         net (fo=5, routed)           0.450     2.778    O_OBUF[12]_inst_i_3_n_0
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.821 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.359     4.180    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.997 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.997    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.926ns  (logic 2.738ns (46.200%)  route 3.188ns (53.800%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.108     1.695    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.051     1.746 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.453     2.199    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X0Y60          LUT5 (Prop_lut5_I0_O)        0.139     2.338 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.300     2.637    O_OBUF[10]_inst_i_4_n_0
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X0Y61          LUT5 (Prop_lut5_I2_O)        0.131     2.768 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.328     4.096    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.926 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.926    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.910ns  (logic 2.631ns (44.526%)  route 3.278ns (55.474%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.108     1.695    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.051     1.746 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.453     2.199    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X0Y60          LUT5 (Prop_lut5_I0_O)        0.129     2.328 r  O_OBUF[12]_inst_i_3/O
                         net (fo=5, routed)           0.450     2.778    O_OBUF[12]_inst_i_3_n_0
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.821 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.267     4.088    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.910 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.910    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.585ns  (logic 2.547ns (45.609%)  route 3.037ns (54.391%))
  Logic Levels:           5  (IBUF=1 LUT4=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.108     1.695    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[10]_inst_i_7/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.738 r  O_OBUF[10]_inst_i_7/O
                         net (fo=3, routed)           0.236     1.973    O_OBUF[10]_inst_i_7_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_2/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.016 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.334     2.351    O_OBUF[9]_inst_i_2_n_0
    SLICE_X3Y61                                                       r  O_OBUF[9]_inst_i_1/I2
    SLICE_X3Y61          LUT4 (Prop_lut4_I2_O)        0.043     2.394 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.360     3.753    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.585 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.585    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[4]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.504ns  (logic 2.477ns (45.016%)  route 3.026ns (54.984%))
  Logic Levels:           4  (IBUF=1 LUT2=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.424     2.013    A_IBUF[4]
    SLICE_X1Y61                                                       r  O_OBUF[4]_inst_i_2/I0
    SLICE_X1Y61          LUT2 (Prop_lut2_I0_O)        0.043     2.056 r  O_OBUF[4]_inst_i_2/O
                         net (fo=1, routed)           0.342     2.398    O_OBUF[4]_inst_i_2_n_0
    SLICE_X1Y61                                                       r  O_OBUF[4]_inst_i_1/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.043     2.441 r  O_OBUF[4]_inst_i_1/O
                         net (fo=1, routed)           1.260     3.702    O_OBUF[4]
    AJ26                                                              r  O_OBUF[4]_inst/I
    AJ26                 OBUF (Prop_obuf_I_O)         1.802     5.504 r  O_OBUF[4]_inst/O
                         net (fo=0)                   0.000     5.504    O[4]
    AJ26                                                              r  O[4] (OUT)
  -------------------------------------------------------------------    -------------------




