Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 02:43:21 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_266_evolib.txt -name O
| Design       : mul8_266
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.629ns  (logic 2.780ns (36.443%)  route 4.849ns (63.557%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.304     1.892    A_IBUF[5]
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_15/I3
    SLICE_X3Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.935 f  O_OBUF[11]_inst_i_15/O
                         net (fo=4, routed)           0.533     2.468    O_OBUF[11]_inst_i_15_n_0
    SLICE_X2Y64                                                       f  O_OBUF[13]_inst_i_12/I3
    SLICE_X2Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.511 r  O_OBUF[13]_inst_i_12/O
                         net (fo=2, routed)           0.533     3.044    O_OBUF[13]_inst_i_12_n_0
    SLICE_X2Y65                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X2Y65          LUT3 (Prop_lut3_I0_O)        0.043     3.087 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.449     3.535    O_OBUF[11]_inst_i_6_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     3.578 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.386     3.964    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_2/I1
    SLICE_X0Y65          LUT6 (Prop_lut6_I1_O)        0.043     4.007 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.235     4.242    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I0
    SLICE_X0Y65          LUT5 (Prop_lut5_I0_O)        0.054     4.296 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.409     5.705    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.924     7.629 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.629    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.551ns  (logic 2.697ns (35.719%)  route 4.854ns (64.281%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.304     1.892    A_IBUF[5]
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_15/I3
    SLICE_X3Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.935 f  O_OBUF[11]_inst_i_15/O
                         net (fo=4, routed)           0.533     2.468    O_OBUF[11]_inst_i_15_n_0
    SLICE_X2Y64                                                       f  O_OBUF[13]_inst_i_12/I3
    SLICE_X2Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.511 r  O_OBUF[13]_inst_i_12/O
                         net (fo=2, routed)           0.533     3.044    O_OBUF[13]_inst_i_12_n_0
    SLICE_X2Y65                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X2Y65          LUT3 (Prop_lut3_I0_O)        0.043     3.087 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.449     3.535    O_OBUF[11]_inst_i_6_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     3.578 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.386     3.964    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_2/I1
    SLICE_X0Y65          LUT6 (Prop_lut6_I1_O)        0.043     4.007 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.235     4.242    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.043     4.285 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.414     5.699    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.551 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.551    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.216ns  (logic 2.738ns (37.950%)  route 4.477ns (62.050%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.304     1.892    A_IBUF[5]
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_15/I3
    SLICE_X3Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.935 f  O_OBUF[11]_inst_i_15/O
                         net (fo=4, routed)           0.533     2.468    O_OBUF[11]_inst_i_15_n_0
    SLICE_X2Y64                                                       f  O_OBUF[13]_inst_i_12/I3
    SLICE_X2Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.511 r  O_OBUF[13]_inst_i_12/O
                         net (fo=2, routed)           0.533     3.044    O_OBUF[13]_inst_i_12_n_0
    SLICE_X2Y65                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X2Y65          LUT3 (Prop_lut3_I0_O)        0.043     3.087 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.449     3.535    O_OBUF[11]_inst_i_6_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     3.578 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.389     3.967    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y65          LUT3 (Prop_lut3_I2_O)        0.051     4.018 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.270     5.288    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     7.216 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.216    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.179ns  (logic 2.635ns (36.710%)  route 4.544ns (63.290%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=16, routed)          1.304     1.892    A_IBUF[5]
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_15/I3
    SLICE_X3Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.935 f  O_OBUF[11]_inst_i_15/O
                         net (fo=4, routed)           0.533     2.468    O_OBUF[11]_inst_i_15_n_0
    SLICE_X2Y64                                                       f  O_OBUF[13]_inst_i_12/I3
    SLICE_X2Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.511 r  O_OBUF[13]_inst_i_12/O
                         net (fo=2, routed)           0.533     3.044    O_OBUF[13]_inst_i_12_n_0
    SLICE_X2Y65                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X2Y65          LUT3 (Prop_lut3_I0_O)        0.043     3.087 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.449     3.535    O_OBUF[11]_inst_i_6_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     3.578 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.389     3.967    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y65                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X0Y65          LUT5 (Prop_lut5_I1_O)        0.043     4.010 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.336     5.346    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.179 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.179    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.825ns  (logic 2.694ns (39.467%)  route 4.132ns (60.533%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.043     1.630    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[0]_inst_i_3/I1
    SLICE_X3Y62          LUT2 (Prop_lut2_I1_O)        0.051     1.681 r  O_OBUF[0]_inst_i_3/O
                         net (fo=6, routed)           0.502     2.183    O_OBUF[0]_inst_i_3_n_0
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_12/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.129     2.312 r  O_OBUF[11]_inst_i_12/O
                         net (fo=3, routed)           0.428     2.741    O_OBUF[11]_inst_i_12_n_0
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.784 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.798     3.582    O_OBUF[11]_inst_i_4_n_0
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.625 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.359     4.984    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.825 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.825    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.498ns  (logic 2.810ns (43.241%)  route 3.688ns (56.759%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.043     1.630    A_IBUF[6]
    SLICE_X3Y62                                                       f  O_OBUF[0]_inst_i_3/I1
    SLICE_X3Y62          LUT2 (Prop_lut2_I1_O)        0.051     1.681 f  O_OBUF[0]_inst_i_3/O
                         net (fo=6, routed)           0.402     2.083    O_OBUF[0]_inst_i_3_n_0
    SLICE_X2Y61                                                       f  O_OBUF[8]_inst_i_4/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.212 f  O_OBUF[8]_inst_i_4/O
                         net (fo=5, routed)           0.422     2.634    O_OBUF[8]_inst_i_4_n_0
    SLICE_X0Y61                                                       f  O_OBUF[9]_inst_i_6/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.053     2.687 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.229     2.916    O_OBUF[9]_inst_i_6_n_0
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_2/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.131     3.047 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.411     3.458    O_OBUF[8]_inst_i_2_n_0
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     3.501 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.181     4.682    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.498 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.498    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.440ns  (logic 2.825ns (43.865%)  route 3.615ns (56.135%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.043     1.630    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[0]_inst_i_3/I1
    SLICE_X3Y62          LUT2 (Prop_lut2_I1_O)        0.051     1.681 r  O_OBUF[0]_inst_i_3/O
                         net (fo=6, routed)           0.402     2.083    O_OBUF[0]_inst_i_3_n_0
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.212 r  O_OBUF[8]_inst_i_4/O
                         net (fo=5, routed)           0.422     2.634    O_OBUF[8]_inst_i_4_n_0
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_6/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.053     2.687 f  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.225     2.913    O_OBUF[9]_inst_i_6_n_0
    SLICE_X0Y62                                                       f  O_OBUF[9]_inst_i_3/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.131     3.044 r  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.256     3.300    O_OBUF[9]_inst_i_3_n_0
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.343 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.266     4.609    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.440 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.440    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.435ns  (logic 2.680ns (41.653%)  route 3.755ns (58.347%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=15, routed)          1.241     1.835    A_IBUF[3]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_23/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     1.878 r  O_OBUF[11]_inst_i_23/O
                         net (fo=2, routed)           0.356     2.234    O_OBUF[11]_inst_i_23_n_0
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_13/I4
    SLICE_X2Y62          LUT5 (Prop_lut5_I4_O)        0.043     2.277 r  O_OBUF[11]_inst_i_13/O
                         net (fo=3, routed)           0.522     2.798    O_OBUF[11]_inst_i_13_n_0
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_5/I2
    SLICE_X2Y63          LUT5 (Prop_lut5_I2_O)        0.127     2.925 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.293     3.219    O_OBUF[11]_inst_i_5_n_0
    SLICE_X1Y63                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X1Y63          LUT4 (Prop_lut4_I3_O)        0.043     3.262 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.343     4.605    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.435 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.435    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.428ns  (logic 2.812ns (43.751%)  route 3.616ns (56.249%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.043     1.630    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[0]_inst_i_3/I1
    SLICE_X3Y62          LUT2 (Prop_lut2_I1_O)        0.051     1.681 r  O_OBUF[0]_inst_i_3/O
                         net (fo=6, routed)           0.402     2.083    O_OBUF[0]_inst_i_3_n_0
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.129     2.212 r  O_OBUF[8]_inst_i_4/O
                         net (fo=5, routed)           0.422     2.634    O_OBUF[8]_inst_i_4_n_0
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_6/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.053     2.687 f  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.225     2.913    O_OBUF[9]_inst_i_6_n_0
    SLICE_X0Y62                                                       f  O_OBUF[9]_inst_i_3/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.131     3.044 r  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.343     3.387    O_OBUF[9]_inst_i_3_n_0
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     3.430 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.179     4.609    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.428 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.428    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.004ns  (logic 2.563ns (42.690%)  route 3.441ns (57.310%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=14, routed)          1.379     1.901    A_IBUF[0]
    SLICE_X0Y61                                                       r  O_OBUF[5]_inst_i_2/I2
    SLICE_X0Y61          LUT3 (Prop_lut3_I2_O)        0.048     1.949 r  O_OBUF[5]_inst_i_2/O
                         net (fo=3, routed)           0.379     2.327    O_OBUF[5]_inst_i_2_n_0
    SLICE_X1Y63                                                       r  O_OBUF[6]_inst_i_2/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.129     2.456 r  O_OBUF[6]_inst_i_2/O
                         net (fo=4, routed)           0.546     3.002    O_OBUF[6]_inst_i_2_n_0
    SLICE_X0Y63                                                       r  O_OBUF[5]_inst_i_1/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.045 r  O_OBUF[5]_inst_i_1/O
                         net (fo=1, routed)           1.137     4.182    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     6.004 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     6.004    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------




