Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 13:51:46 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_487_evolib.txt -name O
| Design       : mul8_487
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.166ns  (logic 2.830ns (39.498%)  route 4.335ns (60.502%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=12, routed)          0.959     1.547    A_IBUF[5]
    SLICE_X1Y61                                                       f  O_OBUF[2]_inst_i_4/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.600 r  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.632     2.232    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y61                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.131     2.363 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.503     2.866    O_OBUF[2]
    SLICE_X0Y60                                                       r  O_OBUF[13]_inst_i_2/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.909 f  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.500     3.409    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y63                                                       f  O_OBUF[15]_inst_i_3/I1
    SLICE_X0Y63          LUT5 (Prop_lut5_I1_O)        0.051     3.460 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.417     3.877    N_1973
    SLICE_X0Y62                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.129     4.006 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.324     5.330    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.166 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.166    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.070ns  (logic 2.846ns (40.255%)  route 4.224ns (59.745%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=12, routed)          0.959     1.547    A_IBUF[5]
    SLICE_X1Y61                                                       f  O_OBUF[2]_inst_i_4/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.600 r  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.632     2.232    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y61                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.131     2.363 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.503     2.866    O_OBUF[2]
    SLICE_X0Y60                                                       r  O_OBUF[13]_inst_i_2/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.909 f  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.500     3.409    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y63                                                       f  O_OBUF[15]_inst_i_3/I1
    SLICE_X0Y63          LUT5 (Prop_lut5_I1_O)        0.051     3.460 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.227     3.686    N_1973
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.129     3.815 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.403     5.219    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.070 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.070    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.688ns  (logic 2.699ns (40.361%)  route 3.989ns (59.639%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=12, routed)          0.959     1.547    A_IBUF[5]
    SLICE_X1Y61                                                       f  O_OBUF[2]_inst_i_4/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.600 r  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.632     2.232    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y61                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.131     2.363 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.503     2.866    O_OBUF[2]
    SLICE_X0Y60                                                       r  O_OBUF[13]_inst_i_2/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.909 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.580     3.489    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y64          LUT3 (Prop_lut3_I2_O)        0.043     3.532 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.314     4.846    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.688 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.688    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.610ns  (logic 2.690ns (40.702%)  route 3.920ns (59.298%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=12, routed)          0.959     1.547    A_IBUF[5]
    SLICE_X1Y61                                                       f  O_OBUF[2]_inst_i_4/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.600 r  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.632     2.232    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y61                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.131     2.363 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.503     2.866    O_OBUF[2]
    SLICE_X0Y60                                                       r  O_OBUF[13]_inst_i_2/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.909 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.500     3.409    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     3.452 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.325     4.777    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.610 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.610    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.151ns  (logic 2.656ns (43.176%)  route 3.495ns (56.824%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=12, routed)          0.959     1.547    A_IBUF[5]
    SLICE_X1Y61                                                       f  O_OBUF[2]_inst_i_4/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.600 r  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.632     2.232    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y61                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.131     2.363 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           0.498     2.861    O_OBUF[2]
    SLICE_X0Y60                                                       r  O_OBUF[11]_inst_i_1/I4
    SLICE_X0Y60          LUT6 (Prop_lut6_I4_O)        0.043     2.904 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.406     4.310    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.151 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.151    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.004ns  (logic 2.631ns (43.818%)  route 3.373ns (56.182%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=12, routed)          0.959     1.547    A_IBUF[5]
    SLICE_X1Y61                                                       r  O_OBUF[2]_inst_i_4/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.600 f  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.436     2.036    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y61                                                       f  O_OBUF[11]_inst_i_5/I2
    SLICE_X2Y61          LUT5 (Prop_lut5_I2_O)        0.131     2.167 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.715     2.882    N_1425
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y62          LUT5 (Prop_lut5_I3_O)        0.043     2.925 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.263     4.188    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.004 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.004    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.927ns  (logic 2.636ns (44.485%)  route 3.290ns (55.515%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=12, routed)          0.959     1.547    A_IBUF[5]
    SLICE_X1Y61                                                       r  O_OBUF[2]_inst_i_4/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.600 f  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.436     2.036    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y61                                                       f  O_OBUF[11]_inst_i_5/I2
    SLICE_X2Y61          LUT5 (Prop_lut5_I2_O)        0.131     2.167 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.715     2.882    N_1425
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y62          LUT5 (Prop_lut5_I3_O)        0.043     2.925 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.180     4.105    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.927 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.927    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.900ns  (logic 2.645ns (44.825%)  route 3.256ns (55.175%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=12, routed)          0.959     1.547    A_IBUF[5]
    SLICE_X1Y61                                                       f  O_OBUF[2]_inst_i_4/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.600 r  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.625     2.225    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.131     2.356 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.328     2.684    N_1602
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.043     2.727 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.343     4.070    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.900 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.900    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[2]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.452ns  (logic 2.567ns (47.084%)  route 2.885ns (52.916%))
  Logic Levels:           4  (IBUF=1 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=12, routed)          0.959     1.547    A_IBUF[5]
    SLICE_X1Y61                                                       f  O_OBUF[2]_inst_i_4/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.053     1.600 r  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.632     2.232    O_OBUF[2]_inst_i_4_n_0
    SLICE_X2Y61                                                       r  O_OBUF[2]_inst_i_1/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.131     2.363 r  O_OBUF[2]_inst_i_1/O
                         net (fo=4, routed)           1.294     3.656    O_OBUF[2]
    AG25                                                              r  O_OBUF[2]_inst/I
    AG25                 OBUF (Prop_obuf_I_O)         1.795     5.452 r  O_OBUF[2]_inst/O
                         net (fo=0)                   0.000     5.452    O[2]
    AG25                                                              r  O[2] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.418ns  (logic 2.512ns (46.355%)  route 2.907ns (53.645%))
  Logic Levels:           4  (IBUF=1 LUT3=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=6, routed)           1.155     1.749    A_IBUF[3]
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X2Y61          LUT3 (Prop_lut3_I0_O)        0.043     1.792 r  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.412     2.204    N_420
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_1/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.247 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.340     3.587    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.418 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.418    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------




