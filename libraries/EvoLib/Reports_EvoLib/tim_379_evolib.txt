Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 08:17:36 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_379_evolib.txt -name O
| Design       : mul8_379
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.963ns  (logic 2.912ns (41.827%)  route 4.050ns (58.173%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          1.077     1.665    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.051     1.716 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.325     2.041    N_1186
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X1Y64          LUT5 (Prop_lut5_I2_O)        0.135     2.176 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.427     2.603    O_OBUF[11]_inst_i_2_n_0
    SLICE_X1Y63                                                       f  O_OBUF[13]_inst_i_2/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.129     2.732 f  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.324     3.056    O_OBUF[13]_inst_i_2_n_0
    SLICE_X2Y63                                                       f  O_OBUF[15]_inst_i_3/I1
    SLICE_X2Y63          LUT5 (Prop_lut5_I1_O)        0.047     3.103 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.497     3.600    N_1973
    SLICE_X1Y61                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.127     3.727 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.400     5.127    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     6.963 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     6.963    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.941ns  (logic 2.928ns (42.186%)  route 4.013ns (57.814%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          1.077     1.665    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.051     1.716 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.325     2.041    N_1186
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X1Y64          LUT5 (Prop_lut5_I2_O)        0.135     2.176 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.427     2.603    O_OBUF[11]_inst_i_2_n_0
    SLICE_X1Y63                                                       f  O_OBUF[13]_inst_i_2/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.129     2.732 f  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.324     3.056    O_OBUF[13]_inst_i_2_n_0
    SLICE_X2Y63                                                       f  O_OBUF[15]_inst_i_3/I1
    SLICE_X2Y63          LUT5 (Prop_lut5_I1_O)        0.047     3.103 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.382     3.485    N_1973
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.127     3.612 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.477     5.089    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     6.941 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     6.941    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.467ns  (logic 2.787ns (43.100%)  route 3.680ns (56.900%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          1.077     1.665    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.051     1.716 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.325     2.041    N_1186
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X1Y64          LUT5 (Prop_lut5_I2_O)        0.135     2.176 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.427     2.603    O_OBUF[11]_inst_i_2_n_0
    SLICE_X1Y63                                                       r  O_OBUF[13]_inst_i_2/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.129     2.732 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.529     3.261    O_OBUF[13]_inst_i_2_n_0
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.043     3.304 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.321     4.625    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.467 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.467    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.269ns  (logic 2.778ns (44.321%)  route 3.490ns (55.679%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          1.077     1.665    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.051     1.716 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.325     2.041    N_1186
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I2
    SLICE_X1Y64          LUT5 (Prop_lut5_I2_O)        0.135     2.176 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.427     2.603    O_OBUF[11]_inst_i_2_n_0
    SLICE_X1Y63                                                       r  O_OBUF[13]_inst_i_2/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.129     2.732 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.324     3.056    O_OBUF[13]_inst_i_2_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X2Y63          LUT5 (Prop_lut5_I0_O)        0.043     3.099 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.337     4.436    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.269 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.269    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.824ns  (logic 2.558ns (43.921%)  route 3.266ns (56.079%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          1.077     1.665    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_10/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     1.708 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.256     1.964    N_1187
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X0Y62          LUT5 (Prop_lut5_I0_O)        0.043     2.007 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.664     2.671    N_1425
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_1/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.714 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.269     3.983    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     5.824 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     5.824    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.808ns  (logic 2.641ns (45.470%)  route 3.167ns (54.530%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          1.077     1.665    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.051     1.716 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.308     2.024    N_1186
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_3/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.129     2.153 r  O_OBUF[11]_inst_i_3/O
                         net (fo=3, routed)           0.517     2.670    N_1351
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.713 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.265     3.978    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.808 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.808    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.750ns  (logic 2.533ns (44.054%)  route 3.217ns (55.946%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          1.077     1.665    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_10/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     1.708 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.256     1.964    N_1187
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X0Y62          LUT5 (Prop_lut5_I0_O)        0.043     2.007 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.663     2.670    N_1425
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y64          LUT5 (Prop_lut5_I3_O)        0.043     2.713 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.221     3.933    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.750 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.750    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.663ns  (logic 2.538ns (44.825%)  route 3.124ns (55.175%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          1.077     1.665    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_10/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     1.708 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.256     1.964    N_1187
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X0Y62          LUT5 (Prop_lut5_I0_O)        0.043     2.007 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.663     2.670    N_1425
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y64          LUT5 (Prop_lut5_I3_O)        0.043     2.713 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.128     3.841    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.663 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.663    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.392ns  (logic 2.618ns (48.559%)  route 2.774ns (51.441%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=13, routed)          0.993     1.573    A_IBUF[7]
    SLICE_X0Y61                                                       r  O_OBUF[11]_inst_i_9/I3
    SLICE_X0Y61          LUT4 (Prop_lut4_I3_O)        0.048     1.621 r  O_OBUF[11]_inst_i_9/O
                         net (fo=3, routed)           0.418     2.039    N_1216
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_6/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.129     2.168 r  O_OBUF[11]_inst_i_6/O
                         net (fo=4, routed)           0.177     2.344    N_1603
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.387 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.186     3.573    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.392 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.392    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.281ns  (logic 2.599ns (49.220%)  route 2.681ns (50.780%))
  Logic Levels:           4  (IBUF=1 LUT4=1 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=11, routed)          1.077     1.665    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.051     1.716 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.325     2.041    N_1186
    SLICE_X1Y64                                                       r  O_OBUF[9]_inst_i_1/I2
    SLICE_X1Y64          LUT5 (Prop_lut5_I2_O)        0.129     2.170 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.279     3.449    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.281 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.281    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------




