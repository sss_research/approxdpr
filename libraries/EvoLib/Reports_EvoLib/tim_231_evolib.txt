Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 01:00:46 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_231_evolib.txt -name O
| Design       : mul8_231
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.738ns  (logic 3.115ns (31.990%)  route 6.623ns (68.010%))
  Logic Levels:           12  (IBUF=1 LUT4=5 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.238     1.826    A_IBUF[5]
    SLICE_X3Y64                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X3Y64          LUT6 (Prop_lut6_I0_O)        0.043     1.869 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.420     2.289    O_OBUF[0]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_8/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.340 r  O_OBUF[7]_inst_i_8/O
                         net (fo=4, routed)           0.595     2.935    N_648
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.129     3.064 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.532     3.596    N_915
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_13/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.639 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.542     4.181    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.127     4.308 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.296     4.604    N_1182
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_8/I4
    SLICE_X3Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.647 r  O_OBUF[15]_inst_i_8/O
                         net (fo=3, routed)           0.351     4.997    N_1449
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_6/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.043     5.040 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.526     5.567    N_1464
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y64          LUT4 (Prop_lut4_I1_O)        0.127     5.694 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.414     6.107    N_1714
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X1Y65          LUT6 (Prop_lut6_I4_O)        0.043     6.150 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.340     6.490    N_1983
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X0Y65          LUT6 (Prop_lut6_I1_O)        0.043     6.533 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.370     7.903    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     9.738 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     9.738    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.719ns  (logic 3.131ns (32.214%)  route 6.588ns (67.786%))
  Logic Levels:           12  (IBUF=1 LUT4=5 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.238     1.826    A_IBUF[5]
    SLICE_X3Y64                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X3Y64          LUT6 (Prop_lut6_I0_O)        0.043     1.869 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.420     2.289    O_OBUF[0]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_8/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.340 r  O_OBUF[7]_inst_i_8/O
                         net (fo=4, routed)           0.595     2.935    N_648
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.129     3.064 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.532     3.596    N_915
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_13/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.639 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.542     4.181    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.127     4.308 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.296     4.604    N_1182
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_8/I4
    SLICE_X3Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.647 r  O_OBUF[15]_inst_i_8/O
                         net (fo=3, routed)           0.351     4.997    N_1449
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_6/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.043     5.040 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.526     5.567    N_1464
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y64          LUT4 (Prop_lut4_I1_O)        0.127     5.694 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.414     6.107    N_1714
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X1Y65          LUT6 (Prop_lut6_I4_O)        0.043     6.150 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.260     6.410    N_1983
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     6.453 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.415     7.868    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     9.719 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     9.719    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.631ns  (logic 3.112ns (32.316%)  route 6.519ns (67.684%))
  Logic Levels:           12  (IBUF=1 LUT4=6 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.238     1.826    A_IBUF[5]
    SLICE_X3Y64                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X3Y64          LUT6 (Prop_lut6_I0_O)        0.043     1.869 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.420     2.289    O_OBUF[0]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_8/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.340 r  O_OBUF[7]_inst_i_8/O
                         net (fo=4, routed)           0.595     2.935    N_648
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.129     3.064 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.532     3.596    N_915
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_13/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.639 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.542     4.181    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.127     4.308 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.296     4.604    N_1182
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_8/I4
    SLICE_X3Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.647 r  O_OBUF[15]_inst_i_8/O
                         net (fo=3, routed)           0.351     4.997    N_1449
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_6/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.043     5.040 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.526     5.567    N_1464
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y64          LUT4 (Prop_lut4_I1_O)        0.127     5.694 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.414     6.107    N_1714
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X1Y65          LUT6 (Prop_lut6_I4_O)        0.043     6.150 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.258     6.409    N_1983
    SLICE_X0Y65                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y65          LUT4 (Prop_lut4_I0_O)        0.043     6.452 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.347     7.798    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     9.631 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     9.631    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.372ns  (logic 3.172ns (33.843%)  route 6.200ns (66.157%))
  Logic Levels:           11  (IBUF=1 LUT4=6 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.238     1.826    A_IBUF[5]
    SLICE_X3Y64                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X3Y64          LUT6 (Prop_lut6_I0_O)        0.043     1.869 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.420     2.289    O_OBUF[0]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_8/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.340 r  O_OBUF[7]_inst_i_8/O
                         net (fo=4, routed)           0.595     2.935    N_648
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.129     3.064 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.532     3.596    N_915
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_13/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.639 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.542     4.181    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.127     4.308 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.296     4.604    N_1182
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_8/I4
    SLICE_X3Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.647 r  O_OBUF[15]_inst_i_8/O
                         net (fo=3, routed)           0.351     4.997    N_1449
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_6/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.043     5.040 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.526     5.567    N_1464
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y64          LUT4 (Prop_lut4_I1_O)        0.127     5.694 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.413     6.106    N_1714
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X1Y64          LUT4 (Prop_lut4_I1_O)        0.051     6.157 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.288     7.445    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.927     9.372 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     9.372    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.255ns  (logic 3.078ns (33.261%)  route 6.177ns (66.739%))
  Logic Levels:           11  (IBUF=1 LUT4=5 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.238     1.826    A_IBUF[5]
    SLICE_X3Y64                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X3Y64          LUT6 (Prop_lut6_I0_O)        0.043     1.869 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.420     2.289    O_OBUF[0]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_8/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.340 r  O_OBUF[7]_inst_i_8/O
                         net (fo=4, routed)           0.595     2.935    N_648
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.129     3.064 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.532     3.596    N_915
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_13/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.639 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.542     4.181    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.127     4.308 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.296     4.604    N_1182
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_8/I4
    SLICE_X3Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.647 r  O_OBUF[15]_inst_i_8/O
                         net (fo=3, routed)           0.351     4.997    N_1449
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_6/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.043     5.040 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.526     5.567    N_1464
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y64          LUT4 (Prop_lut4_I1_O)        0.127     5.694 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.407     6.100    N_1714
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_1/I3
    SLICE_X1Y65          LUT6 (Prop_lut6_I3_O)        0.043     6.143 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.270     7.413    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     9.255 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     9.255    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.578ns  (logic 2.940ns (34.272%)  route 5.638ns (65.728%))
  Logic Levels:           10  (IBUF=1 LUT4=4 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.238     1.826    A_IBUF[5]
    SLICE_X3Y64                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X3Y64          LUT6 (Prop_lut6_I0_O)        0.043     1.869 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.420     2.289    O_OBUF[0]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_8/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.340 r  O_OBUF[7]_inst_i_8/O
                         net (fo=4, routed)           0.595     2.935    N_648
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.129     3.064 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.532     3.596    N_915
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_13/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.639 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.542     4.181    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.127     4.308 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.436     4.744    N_1182
    SLICE_X2Y64                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X2Y64          LUT6 (Prop_lut6_I4_O)        0.043     4.787 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.334     5.121    N_1683
    SLICE_X3Y64                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X3Y64          LUT4 (Prop_lut4_I0_O)        0.043     5.164 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.252     5.416    N_1698
    SLICE_X1Y65                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X1Y65          LUT6 (Prop_lut6_I4_O)        0.043     5.459 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.289     6.748    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     8.578 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     8.578    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.260ns  (logic 2.898ns (35.085%)  route 5.362ns (64.915%))
  Logic Levels:           9  (IBUF=1 LUT4=4 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.238     1.826    A_IBUF[5]
    SLICE_X3Y64                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X3Y64          LUT6 (Prop_lut6_I0_O)        0.043     1.869 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.420     2.289    O_OBUF[0]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_8/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.051     2.340 r  O_OBUF[7]_inst_i_8/O
                         net (fo=4, routed)           0.595     2.935    N_648
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.129     3.064 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.532     3.596    N_915
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_13/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.639 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.542     4.181    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.127     4.308 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.400     4.708    N_1182
    SLICE_X2Y65                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X2Y65          LUT6 (Prop_lut6_I2_O)        0.043     4.751 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.418     5.169    N_1682
    SLICE_X0Y65                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X0Y65          LUT4 (Prop_lut4_I1_O)        0.043     5.212 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.217     6.429    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     8.260 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     8.260    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.611ns  (logic 2.756ns (36.216%)  route 4.855ns (63.784%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.316     1.904    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[6]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.947 r  O_OBUF[6]_inst_i_4/O
                         net (fo=3, routed)           0.507     2.453    N_865
    SLICE_X1Y62                                                       r  O_OBUF[6]_inst_i_2/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.496 r  O_OBUF[6]_inst_i_2/O
                         net (fo=3, routed)           0.339     2.835    N_1132
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X0Y64          LUT3 (Prop_lut3_I0_O)        0.049     2.884 r  O_OBUF[7]_inst_i_4/O
                         net (fo=3, routed)           0.497     3.381    N_1383
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_5/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.129     3.510 r  O_OBUF[8]_inst_i_5/O
                         net (fo=3, routed)           0.546     4.056    N_1399
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X2Y62          LUT6 (Prop_lut6_I1_O)        0.043     4.099 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.382     4.481    N_1664
    SLICE_X0Y64                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X0Y64          LUT4 (Prop_lut4_I1_O)        0.043     4.524 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.268     5.792    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.611 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.611    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.904ns  (logic 2.802ns (40.585%)  route 4.102ns (59.415%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.316     1.904    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[6]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.947 r  O_OBUF[6]_inst_i_4/O
                         net (fo=3, routed)           0.507     2.453    N_865
    SLICE_X1Y62                                                       r  O_OBUF[6]_inst_i_2/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.496 r  O_OBUF[6]_inst_i_2/O
                         net (fo=3, routed)           0.339     2.835    N_1132
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X0Y64          LUT3 (Prop_lut3_I0_O)        0.049     2.884 r  O_OBUF[7]_inst_i_4/O
                         net (fo=3, routed)           0.497     3.381    N_1383
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.134     3.515 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.175     3.690    N_1398
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.129     3.819 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.269     5.088    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.904 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.904    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.009ns  (logic 2.636ns (43.873%)  route 3.373ns (56.127%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.316     1.904    A_IBUF[5]
    SLICE_X1Y63                                                       r  O_OBUF[6]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.947 r  O_OBUF[6]_inst_i_4/O
                         net (fo=3, routed)           0.507     2.453    N_865
    SLICE_X1Y62                                                       r  O_OBUF[6]_inst_i_2/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.496 r  O_OBUF[6]_inst_i_2/O
                         net (fo=3, routed)           0.343     2.839    N_1132
    SLICE_X0Y64                                                       r  O_OBUF[6]_inst_i_1/I2
    SLICE_X0Y64          LUT5 (Prop_lut5_I2_O)        0.052     2.891 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.207     4.098    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.911     6.009 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.009    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




