Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 22:26:46 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_178_evolib.txt -name O
| Design       : mul8_178
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.945ns  (logic 2.835ns (40.821%)  route 4.110ns (59.179%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.137     1.724    A_IBUF[6]
    SLICE_X3Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.048     1.772 r  O_OBUF[2]_inst_i_3/O
                         net (fo=3, routed)           0.367     2.139    N_1216
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.129     2.268 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.502     2.770    N_1602
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_2/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.813 f  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.545     3.358    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y64                                                       f  O_OBUF[15]_inst_i_3/I1
    SLICE_X0Y64          LUT5 (Prop_lut5_I1_O)        0.048     3.406 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.231     3.637    N_1973
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y64          LUT6 (Prop_lut6_I1_O)        0.129     3.766 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.328     5.093    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     6.945 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     6.945    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.888ns  (logic 2.819ns (40.932%)  route 4.068ns (59.068%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.137     1.724    A_IBUF[6]
    SLICE_X3Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.048     1.772 r  O_OBUF[2]_inst_i_3/O
                         net (fo=3, routed)           0.367     2.139    N_1216
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.129     2.268 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.502     2.770    N_1602
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_2/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.813 f  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.545     3.358    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y64                                                       f  O_OBUF[15]_inst_i_3/I1
    SLICE_X0Y64          LUT5 (Prop_lut5_I1_O)        0.048     3.406 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.103     3.509    N_1973
    SLICE_X0Y64                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.129     3.638 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.414     5.052    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     6.888 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     6.888    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.557ns  (logic 2.682ns (40.911%)  route 3.874ns (59.089%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.137     1.724    A_IBUF[6]
    SLICE_X3Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.048     1.772 r  O_OBUF[2]_inst_i_3/O
                         net (fo=3, routed)           0.367     2.139    N_1216
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.129     2.268 f  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.502     2.770    N_1602
    SLICE_X1Y64                                                       f  O_OBUF[13]_inst_i_2/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.813 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.545     3.358    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y64          LUT5 (Prop_lut5_I0_O)        0.043     3.401 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.323     4.724    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.557 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.557    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.369ns  (logic 2.691ns (42.259%)  route 3.677ns (57.741%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.137     1.724    A_IBUF[6]
    SLICE_X3Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.048     1.772 r  O_OBUF[2]_inst_i_3/O
                         net (fo=3, routed)           0.367     2.139    N_1216
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.129     2.268 f  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.502     2.770    N_1602
    SLICE_X1Y64                                                       f  O_OBUF[13]_inst_i_2/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     2.813 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.324     3.137    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y64          LUT3 (Prop_lut3_I2_O)        0.043     3.180 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.347     4.527    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.369 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.369    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.926ns  (logic 2.648ns (44.680%)  route 3.278ns (55.320%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.137     1.724    A_IBUF[6]
    SLICE_X3Y63                                                       r  O_OBUF[2]_inst_i_3/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.048     1.772 r  O_OBUF[2]_inst_i_3/O
                         net (fo=3, routed)           0.367     2.139    N_1216
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.129     2.268 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.503     2.771    N_1602
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X1Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.814 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.271     4.085    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     5.926 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     5.926    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.773ns  (logic 2.553ns (44.227%)  route 3.220ns (55.773%))
  Logic Levels:           5  (IBUF=1 LUT3=2 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.210     1.805    A_IBUF[3]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_8/I0
    SLICE_X2Y62          LUT3 (Prop_lut3_I0_O)        0.043     1.848 r  O_OBUF[11]_inst_i_8/O
                         net (fo=1, routed)           0.431     2.279    N_420
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X2Y63          LUT5 (Prop_lut5_I3_O)        0.043     2.322 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.310     2.632    O_OBUF[11]_inst_i_2_n_0
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X1Y64          LUT3 (Prop_lut3_I2_O)        0.043     2.675 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.268     3.943    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.773 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.773    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.716ns  (logic 2.628ns (45.986%)  route 3.087ns (54.014%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.317     1.907    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_2/I3
    SLICE_X0Y63          LUT5 (Prop_lut5_I3_O)        0.048     1.955 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.289     2.244    N_1172
    SLICE_X0Y61                                                       r  O_OBUF[8]_inst_i_2/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.129     2.373 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.224     2.597    O_OBUF[8]_inst_i_2_n_0
    SLICE_X0Y61                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.640 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.257     3.897    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.716 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.716    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.639ns  (logic 2.679ns (47.511%)  route 2.960ns (52.489%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=4, routed)           0.960     1.553    A_IBUF[1]
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_4/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.049     1.602 f  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.318     1.919    N_52
    SLICE_X1Y62                                                       f  O_OBUF[6]_inst_i_1/I3
    SLICE_X1Y62          LUT5 (Prop_lut5_I3_O)        0.129     2.048 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.342     2.391    O_OBUF[1]
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.043     2.434 r  O_OBUF[7]_inst_i_2/O
                         net (fo=1, routed)           0.147     2.580    N_1306
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X0Y62          LUT5 (Prop_lut5_I4_O)        0.043     2.623 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.194     3.817    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.639 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.639    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.623ns  (logic 2.674ns (47.550%)  route 2.949ns (52.450%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=4, routed)           0.960     1.553    A_IBUF[1]
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_4/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.049     1.602 f  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.318     1.919    N_52
    SLICE_X1Y62                                                       f  O_OBUF[6]_inst_i_1/I3
    SLICE_X1Y62          LUT5 (Prop_lut5_I3_O)        0.129     2.048 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.342     2.391    O_OBUF[1]
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.043     2.434 r  O_OBUF[7]_inst_i_2/O
                         net (fo=1, routed)           0.147     2.580    N_1306
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X0Y62          LUT5 (Prop_lut5_I4_O)        0.043     2.623 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.183     3.806    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.623 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.623    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.535ns  (logic 2.631ns (47.535%)  route 2.904ns (52.465%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.060     1.647    A_IBUF[5]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_7/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     1.690 r  O_OBUF[11]_inst_i_7/O
                         net (fo=3, routed)           0.430     2.120    N_1186
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.126     2.246 r  O_OBUF[9]_inst_i_4/O
                         net (fo=1, routed)           0.147     2.392    N_1350
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.435 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.268     3.704    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.535 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.535    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------




