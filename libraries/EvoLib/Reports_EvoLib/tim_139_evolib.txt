Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 20:33:56 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_139_evolib.txt -name O
| Design       : mul8_139
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.904ns  (logic 2.919ns (32.781%)  route 5.985ns (67.219%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=25, routed)          1.228     1.814    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_23/I0
    SLICE_X3Y62          LUT2 (Prop_lut2_I0_O)        0.051     1.865 r  O_OBUF[15]_inst_i_23/O
                         net (fo=1, routed)           0.494     2.359    N_532
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_19/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.129     2.488 r  O_OBUF[15]_inst_i_19/O
                         net (fo=1, routed)           0.224     2.713    O_OBUF[15]_inst_i_19_n_0
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_17/I0
    SLICE_X3Y59          LUT6 (Prop_lut6_I0_O)        0.043     2.756 r  O_OBUF[15]_inst_i_17/O
                         net (fo=3, routed)           0.502     3.257    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.300 r  O_OBUF[10]_inst_i_12/O
                         net (fo=4, routed)           0.425     3.725    N_1182
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.768 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.499     4.267    N_1432
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     4.310 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.343     4.653    N_1933
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_2/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.696 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.435     5.130    N_1949
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_3/I2
    SLICE_X0Y61          LUT6 (Prop_lut6_I2_O)        0.043     5.173 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.421     5.594    N_1983
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X0Y62          LUT6 (Prop_lut6_I2_O)        0.043     5.637 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.416     7.053    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.904 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.904    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.880ns  (logic 2.900ns (32.663%)  route 5.979ns (67.337%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=25, routed)          1.228     1.814    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_23/I0
    SLICE_X3Y62          LUT2 (Prop_lut2_I0_O)        0.051     1.865 r  O_OBUF[15]_inst_i_23/O
                         net (fo=1, routed)           0.494     2.359    N_532
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_19/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.129     2.488 r  O_OBUF[15]_inst_i_19/O
                         net (fo=1, routed)           0.224     2.713    O_OBUF[15]_inst_i_19_n_0
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_17/I0
    SLICE_X3Y59          LUT6 (Prop_lut6_I0_O)        0.043     2.756 r  O_OBUF[15]_inst_i_17/O
                         net (fo=3, routed)           0.502     3.257    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.300 r  O_OBUF[10]_inst_i_12/O
                         net (fo=4, routed)           0.425     3.725    N_1182
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.768 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.499     4.267    N_1432
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     4.310 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.343     4.653    N_1933
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_2/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.696 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.435     5.130    N_1949
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_3/I2
    SLICE_X0Y61          LUT6 (Prop_lut6_I2_O)        0.043     5.173 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.421     5.594    N_1983
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_1/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.043     5.637 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.410     7.047    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.880 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.880    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.596ns  (logic 2.964ns (34.479%)  route 5.632ns (65.521%))
  Logic Levels:           10  (IBUF=1 LUT4=4 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=25, routed)          1.214     1.801    A_IBUF[6]
    SLICE_X1Y59                                                       r  O_OBUF[7]_inst_i_8/I3
    SLICE_X1Y59          LUT6 (Prop_lut6_I3_O)        0.043     1.844 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.350     2.193    O_OBUF[7]_inst_i_8_n_0
    SLICE_X2Y59                                                       r  O_OBUF[9]_inst_i_13/I5
    SLICE_X2Y59          LUT6 (Prop_lut6_I5_O)        0.043     2.236 r  O_OBUF[9]_inst_i_13/O
                         net (fo=3, routed)           0.409     2.645    N_899
    SLICE_X3Y60                                                       r  O_OBUF[9]_inst_i_8/I3
    SLICE_X3Y60          LUT4 (Prop_lut4_I3_O)        0.043     2.688 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.557     3.246    N_914
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_5/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     3.289 r  O_OBUF[9]_inst_i_5/O
                         net (fo=3, routed)           0.414     3.702    N_1414
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_5/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.053     3.755 r  O_OBUF[10]_inst_i_5/O
                         net (fo=4, routed)           0.412     4.167    N_1665
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_6/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.131     4.298 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.342     4.640    N_1699
    SLICE_X2Y61                                                       r  O_OBUF[12]_inst_i_3/I3
    SLICE_X2Y61          LUT4 (Prop_lut4_I3_O)        0.043     4.683 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.514     5.197    N_1714
    SLICE_X0Y61                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.051     5.248 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.421     6.669    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.927     8.596 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.596    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.555ns  (logic 2.903ns (33.936%)  route 5.652ns (66.064%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=25, routed)          1.228     1.814    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_23/I0
    SLICE_X3Y62          LUT2 (Prop_lut2_I0_O)        0.051     1.865 r  O_OBUF[15]_inst_i_23/O
                         net (fo=1, routed)           0.494     2.359    N_532
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_19/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.129     2.488 r  O_OBUF[15]_inst_i_19/O
                         net (fo=1, routed)           0.224     2.713    O_OBUF[15]_inst_i_19_n_0
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_17/I0
    SLICE_X3Y59          LUT6 (Prop_lut6_I0_O)        0.043     2.756 r  O_OBUF[15]_inst_i_17/O
                         net (fo=3, routed)           0.502     3.257    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.300 r  O_OBUF[10]_inst_i_12/O
                         net (fo=4, routed)           0.425     3.725    N_1182
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.768 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.499     4.267    N_1432
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     4.310 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.343     4.653    N_1933
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_2/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.696 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.435     5.130    N_1949
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_3/I2
    SLICE_X0Y61          LUT6 (Prop_lut6_I2_O)        0.043     5.173 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.179     5.352    N_1983
    SLICE_X0Y62                                                       r  O_OBUF[14]_inst_i_1/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     5.395 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.324     6.719    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.555 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.555    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.202ns  (logic 2.950ns (35.970%)  route 5.252ns (64.030%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=25, routed)          1.228     1.814    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_23/I0
    SLICE_X3Y62          LUT2 (Prop_lut2_I0_O)        0.051     1.865 r  O_OBUF[15]_inst_i_23/O
                         net (fo=1, routed)           0.494     2.359    N_532
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_19/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.129     2.488 r  O_OBUF[15]_inst_i_19/O
                         net (fo=1, routed)           0.224     2.713    O_OBUF[15]_inst_i_19_n_0
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_17/I0
    SLICE_X3Y59          LUT6 (Prop_lut6_I0_O)        0.043     2.756 r  O_OBUF[15]_inst_i_17/O
                         net (fo=3, routed)           0.502     3.257    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.300 r  O_OBUF[10]_inst_i_12/O
                         net (fo=4, routed)           0.425     3.725    N_1182
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_9/I3
    SLICE_X2Y61          LUT4 (Prop_lut4_I3_O)        0.043     3.768 r  O_OBUF[15]_inst_i_9/O
                         net (fo=2, routed)           0.237     4.005    N_1433
    SLICE_X2Y61                                                       r  O_OBUF[12]_inst_i_5/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.127     4.132 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.404     4.536    N_1464
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     4.579 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.416     4.995    N_1732
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X0Y61          LUT6 (Prop_lut6_I4_O)        0.043     5.038 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.323     6.361    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.202 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.202    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.887ns  (logic 2.903ns (36.806%)  route 4.984ns (63.194%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=25, routed)          1.228     1.814    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_23/I0
    SLICE_X3Y62          LUT2 (Prop_lut2_I0_O)        0.051     1.865 r  O_OBUF[15]_inst_i_23/O
                         net (fo=1, routed)           0.494     2.359    N_532
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_19/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.129     2.488 r  O_OBUF[15]_inst_i_19/O
                         net (fo=1, routed)           0.224     2.713    O_OBUF[15]_inst_i_19_n_0
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_17/I0
    SLICE_X3Y59          LUT6 (Prop_lut6_I0_O)        0.043     2.756 r  O_OBUF[15]_inst_i_17/O
                         net (fo=3, routed)           0.502     3.257    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.300 r  O_OBUF[10]_inst_i_12/O
                         net (fo=4, routed)           0.425     3.725    N_1182
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.768 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.499     4.267    N_1432
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     4.310 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.343     4.653    N_1933
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.048     4.701 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.270     5.971    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.916     7.887 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.887    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.747ns  (logic 2.813ns (36.311%)  route 4.934ns (63.689%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=25, routed)          1.228     1.814    A_IBUF[6]
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_23/I0
    SLICE_X3Y62          LUT2 (Prop_lut2_I0_O)        0.051     1.865 r  O_OBUF[15]_inst_i_23/O
                         net (fo=1, routed)           0.494     2.359    N_532
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_19/I1
    SLICE_X3Y59          LUT6 (Prop_lut6_I1_O)        0.129     2.488 r  O_OBUF[15]_inst_i_19/O
                         net (fo=1, routed)           0.224     2.713    O_OBUF[15]_inst_i_19_n_0
    SLICE_X3Y59                                                       r  O_OBUF[15]_inst_i_17/I0
    SLICE_X3Y59          LUT6 (Prop_lut6_I0_O)        0.043     2.756 r  O_OBUF[15]_inst_i_17/O
                         net (fo=3, routed)           0.502     3.257    N_932
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.300 r  O_OBUF[10]_inst_i_12/O
                         net (fo=4, routed)           0.425     3.725    N_1182
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.768 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.618     4.386    N_1432
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.429 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.251     4.680    N_1682
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.723 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.193     5.916    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.747 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.747    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.103ns  (logic 2.663ns (37.495%)  route 4.440ns (62.505%))
  Logic Levels:           8  (IBUF=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=25, routed)          1.214     1.801    A_IBUF[6]
    SLICE_X1Y59                                                       r  O_OBUF[7]_inst_i_8/I3
    SLICE_X1Y59          LUT6 (Prop_lut6_I3_O)        0.043     1.844 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.350     2.193    O_OBUF[7]_inst_i_8_n_0
    SLICE_X2Y59                                                       r  O_OBUF[9]_inst_i_13/I5
    SLICE_X2Y59          LUT6 (Prop_lut6_I5_O)        0.043     2.236 r  O_OBUF[9]_inst_i_13/O
                         net (fo=3, routed)           0.409     2.645    N_899
    SLICE_X3Y60                                                       r  O_OBUF[9]_inst_i_8/I3
    SLICE_X3Y60          LUT4 (Prop_lut4_I3_O)        0.043     2.688 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.557     3.246    N_914
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_5/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     3.289 r  O_OBUF[9]_inst_i_5/O
                         net (fo=3, routed)           0.414     3.702    N_1414
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.745 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.217     3.962    N_1664
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     4.005 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.279     5.285    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.103 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.103    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.998ns  (logic 2.575ns (42.933%)  route 3.423ns (57.067%))
  Logic Levels:           6  (IBUF=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=25, routed)          1.214     1.801    A_IBUF[6]
    SLICE_X1Y59                                                       r  O_OBUF[7]_inst_i_8/I3
    SLICE_X1Y59          LUT6 (Prop_lut6_I3_O)        0.043     1.844 r  O_OBUF[7]_inst_i_8/O
                         net (fo=3, routed)           0.236     2.079    O_OBUF[7]_inst_i_8_n_0
    SLICE_X2Y59                                                       r  O_OBUF[7]_inst_i_5/I0
    SLICE_X2Y59          LUT6 (Prop_lut6_I0_O)        0.043     2.122 r  O_OBUF[7]_inst_i_5/O
                         net (fo=4, routed)           0.414     2.536    N_898
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_2/I1
    SLICE_X1Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.579 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.304     2.883    N_1398
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.926 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.255     4.181    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.998 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.998    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.443ns  (logic 2.597ns (47.710%)  route 2.846ns (52.290%))
  Logic Levels:           4  (IBUF=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.333     1.927    A_IBUF[3]
    SLICE_X1Y59                                                       r  O_OBUF[6]_inst_i_2/I0
    SLICE_X1Y59          LUT5 (Prop_lut5_I0_O)        0.051     1.978 r  O_OBUF[6]_inst_i_2/O
                         net (fo=4, routed)           0.334     2.312    N_1132
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.129     2.441 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.179     3.620    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     5.443 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.443    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




