Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 10:25:55 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_421_evolib.txt -name O
| Design       : mul8_421
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.969ns  (logic 2.831ns (40.623%)  route 4.138ns (59.377%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.164     1.751    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_10/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.794 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.298     2.092    N_1203
    SLICE_X3Y64                                                       r  O_OBUF[13]_inst_i_8/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.135 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.344     2.479    N_1616
    SLICE_X2Y64                                                       r  O_OBUF[13]_inst_i_6/I1
    SLICE_X2Y64          LUT3 (Prop_lut3_I1_O)        0.043     2.522 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.502     3.023    N_1795
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X0Y64          LUT5 (Prop_lut5_I4_O)        0.135     3.158 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.425     3.584    N_1973
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y64          LUT6 (Prop_lut6_I1_O)        0.129     3.713 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.405     5.118    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     6.969 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     6.969    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.763ns  (logic 2.815ns (41.626%)  route 3.948ns (58.374%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.164     1.751    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_10/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.794 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.298     2.092    N_1203
    SLICE_X3Y64                                                       r  O_OBUF[13]_inst_i_8/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.135 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.344     2.479    N_1616
    SLICE_X2Y64                                                       r  O_OBUF[13]_inst_i_6/I1
    SLICE_X2Y64          LUT3 (Prop_lut3_I1_O)        0.043     2.522 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.502     3.023    N_1795
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X0Y64          LUT5 (Prop_lut5_I4_O)        0.135     3.158 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.225     3.383    N_1973
    SLICE_X0Y64                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.129     3.512 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.415     4.928    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     6.763 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     6.763    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.306ns  (logic 2.675ns (42.429%)  route 3.630ns (57.571%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.164     1.751    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_10/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.794 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.298     2.092    N_1203
    SLICE_X3Y64                                                       r  O_OBUF[13]_inst_i_8/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.135 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.344     2.479    N_1616
    SLICE_X2Y64                                                       r  O_OBUF[13]_inst_i_6/I1
    SLICE_X2Y64          LUT3 (Prop_lut3_I1_O)        0.043     2.522 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.502     3.023    N_1795
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_1/I4
    SLICE_X0Y64          LUT5 (Prop_lut5_I4_O)        0.127     3.150 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.323     4.473    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.306 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.306    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.124ns  (logic 2.600ns (42.462%)  route 3.524ns (57.538%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.164     1.751    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_10/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.794 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.496     2.290    N_1203
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_6/I4
    SLICE_X1Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.333 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.418     2.751    N_1632
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X0Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.794 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.175     2.969    O_OBUF[13]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y65          LUT3 (Prop_lut3_I2_O)        0.043     3.012 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.270     4.282    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.124 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.124    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.934ns  (logic 2.557ns (43.090%)  route 3.377ns (56.910%))
  Logic Levels:           5  (IBUF=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.164     1.751    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_10/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.043     1.794 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.496     2.290    N_1203
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_6/I4
    SLICE_X1Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.333 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.423     2.756    N_1632
    SLICE_X0Y65                                                       r  O_OBUF[11]_inst_i_1/I5
    SLICE_X0Y65          LUT6 (Prop_lut6_I5_O)        0.043     2.799 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.293     4.093    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     5.934 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     5.934    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.817ns  (logic 2.547ns (43.787%)  route 3.270ns (56.213%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.065     1.652    A_IBUF[5]
    SLICE_X3Y63                                                       f  O_OBUF[11]_inst_i_9/I1
    SLICE_X3Y63          LUT6 (Prop_lut6_I1_O)        0.043     1.695 f  O_OBUF[11]_inst_i_9/O
                         net (fo=1, routed)           0.424     2.120    N_1335
    SLICE_X2Y63                                                       f  O_OBUF[11]_inst_i_2/I4
    SLICE_X2Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.163 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.590     2.752    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X0Y65          LUT3 (Prop_lut3_I2_O)        0.043     2.795 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.191     3.986    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.817 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.817    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.475ns  (logic 2.561ns (46.769%)  route 2.914ns (53.231%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=2, routed)           1.175     1.696    A_IBUF[0]
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_4/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.051     1.747 f  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.225     1.972    N_52
    SLICE_X0Y63                                                       f  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y63          LUT5 (Prop_lut5_I3_O)        0.129     2.101 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.221     2.323    O_OBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.366 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.293     3.658    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.475 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.475    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.428ns  (logic 2.650ns (48.821%)  route 2.778ns (51.179%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AN29                                                              r  B_IBUF[2]_inst/I
    AN29                 IBUF (Prop_ibuf_I_O)         0.604     0.604 r  B_IBUF[2]_inst/O
                         net (fo=5, routed)           0.998     1.603    B_IBUF[2]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_2/I2
    SLICE_X0Y63          LUT5 (Prop_lut5_I2_O)        0.053     1.656 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.302     1.958    N_1172
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_2/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.131     2.089 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.217     2.306    O_OBUF[8]_inst_i_2_n_0
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.349 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.261     3.610    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.428 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.428    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.388ns  (logic 2.566ns (47.624%)  route 2.822ns (52.376%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=2, routed)           1.175     1.696    A_IBUF[0]
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_4/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.051     1.747 f  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.225     1.972    N_52
    SLICE_X0Y63                                                       f  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y63          LUT5 (Prop_lut5_I3_O)        0.129     2.101 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.221     2.323    O_OBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.366 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.201     3.566    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.388 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.388    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.311ns  (logic 2.620ns (49.330%)  route 2.691ns (50.670%))
  Logic Levels:           4  (IBUF=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN29                                              0.000     0.000 r  B[2] (IN)
                         net (fo=0)                   0.000     0.000    B[2]
    AN29                                                              r  B_IBUF[2]_inst/I
    AN29                 IBUF (Prop_ibuf_I_O)         0.604     0.604 r  B_IBUF[2]_inst/O
                         net (fo=5, routed)           0.998     1.603    B_IBUF[2]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_2/I2
    SLICE_X0Y63          LUT5 (Prop_lut5_I2_O)        0.053     1.656 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.427     2.083    N_1172
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_1/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.131     2.214 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.265     3.480    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.311 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.311    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------




