Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 20:28:05 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_137_evolib.txt -name O
| Design       : mul8_137
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.606ns  (logic 2.961ns (30.825%)  route 6.645ns (69.175%))
  Logic Levels:           12  (IBUF=1 LUT4=4 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.138     1.734    A_IBUF[2]
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_5/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     1.777 r  O_OBUF[7]_inst_i_5/O
                         net (fo=4, routed)           0.423     2.200    N_864
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.243 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.496     2.739    N_898
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_8/I3
    SLICE_X1Y61          LUT4 (Prop_lut4_I3_O)        0.043     2.782 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.448     3.230    N_1149
    SLICE_X2Y60                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.273 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.532     3.805    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.848 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.426     4.273    N_1198
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.127     4.400 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.350     4.751    N_1448
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_5/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.794 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.695     5.488    N_1715
    SLICE_X0Y61                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     5.531 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.423     5.954    N_1732
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_4/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     5.997 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.299     6.296    N_1999
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_1/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     6.339 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.416     7.755    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     9.606 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     9.606    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.426ns  (logic 2.945ns (31.248%)  route 6.481ns (68.752%))
  Logic Levels:           12  (IBUF=1 LUT4=4 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.138     1.734    A_IBUF[2]
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_5/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     1.777 r  O_OBUF[7]_inst_i_5/O
                         net (fo=4, routed)           0.423     2.200    N_864
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.243 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.496     2.739    N_898
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_8/I3
    SLICE_X1Y61          LUT4 (Prop_lut4_I3_O)        0.043     2.782 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.448     3.230    N_1149
    SLICE_X2Y60                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.273 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.532     3.805    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.848 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.426     4.273    N_1198
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.127     4.400 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.350     4.751    N_1448
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_5/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.794 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.695     5.488    N_1715
    SLICE_X0Y61                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     5.531 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.423     5.954    N_1732
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_4/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     5.997 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.149     6.146    N_1999
    SLICE_X0Y62                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     6.189 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.402     7.590    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     9.426 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     9.426    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.273ns  (logic 3.000ns (32.358%)  route 6.272ns (67.642%))
  Logic Levels:           11  (IBUF=1 LUT4=5 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.138     1.734    A_IBUF[2]
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_5/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     1.777 r  O_OBUF[7]_inst_i_5/O
                         net (fo=4, routed)           0.423     2.200    N_864
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.243 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.496     2.739    N_898
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_8/I3
    SLICE_X1Y61          LUT4 (Prop_lut4_I3_O)        0.043     2.782 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.448     3.230    N_1149
    SLICE_X2Y60                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.273 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.532     3.805    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.848 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.426     4.273    N_1198
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.127     4.400 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.350     4.751    N_1448
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_5/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.794 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.695     5.488    N_1715
    SLICE_X0Y61                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     5.531 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.420     5.952    N_1732
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X0Y64          LUT4 (Prop_lut4_I1_O)        0.049     6.001 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.344     7.345    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     9.273 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     9.273    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.057ns  (logic 3.010ns (33.229%)  route 6.048ns (66.771%))
  Logic Levels:           11  (IBUF=1 LUT4=4 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=9, routed)           1.218     1.823    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X2Y62          LUT6 (Prop_lut6_I1_O)        0.043     1.866 r  O_OBUF[7]_inst_i_8/O
                         net (fo=2, routed)           0.494     2.361    N_648
    SLICE_X3Y61                                                       r  O_OBUF[15]_inst_i_17/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.404 r  O_OBUF[15]_inst_i_17/O
                         net (fo=3, routed)           0.435     2.839    N_899
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_9/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     2.882 r  O_OBUF[9]_inst_i_9/O
                         net (fo=4, routed)           0.504     3.387    N_914
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_8/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     3.430 r  O_OBUF[10]_inst_i_8/O
                         net (fo=2, routed)           0.229     3.659    N_1182
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.050     3.709 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.395     4.104    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.127     4.231 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.513     4.744    N_1683
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.787 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.421     5.208    N_1949
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X0Y63          LUT4 (Prop_lut4_I2_O)        0.051     5.259 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.425     5.684    N_1965
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X0Y62          LUT6 (Prop_lut6_I2_O)        0.129     5.813 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.411     7.224    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     9.057 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     9.057    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.406ns  (logic 2.881ns (34.271%)  route 5.525ns (65.729%))
  Logic Levels:           10  (IBUF=1 LUT4=4 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=9, routed)           1.218     1.823    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X2Y62          LUT6 (Prop_lut6_I1_O)        0.043     1.866 r  O_OBUF[7]_inst_i_8/O
                         net (fo=2, routed)           0.494     2.361    N_648
    SLICE_X3Y61                                                       r  O_OBUF[15]_inst_i_17/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.404 r  O_OBUF[15]_inst_i_17/O
                         net (fo=3, routed)           0.435     2.839    N_899
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_9/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     2.882 r  O_OBUF[9]_inst_i_9/O
                         net (fo=4, routed)           0.504     3.387    N_914
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_8/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     3.430 r  O_OBUF[10]_inst_i_8/O
                         net (fo=2, routed)           0.229     3.659    N_1182
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.050     3.709 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.395     4.104    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.127     4.231 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.513     4.744    N_1683
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.787 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.421     5.208    N_1949
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     5.251 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.314     6.565    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.406 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.406    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.270ns  (logic 2.854ns (34.512%)  route 5.416ns (65.488%))
  Logic Levels:           10  (IBUF=1 LUT4=5 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.138     1.734    A_IBUF[2]
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_5/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     1.777 r  O_OBUF[7]_inst_i_5/O
                         net (fo=4, routed)           0.423     2.200    N_864
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.243 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.496     2.739    N_898
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_8/I3
    SLICE_X1Y61          LUT4 (Prop_lut4_I3_O)        0.043     2.782 r  O_OBUF[9]_inst_i_8/O
                         net (fo=4, routed)           0.448     3.230    N_1149
    SLICE_X2Y60                                                       r  O_OBUF[15]_inst_i_10/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.043     3.273 r  O_OBUF[15]_inst_i_10/O
                         net (fo=3, routed)           0.532     3.805    N_1183
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_14/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.848 r  O_OBUF[15]_inst_i_14/O
                         net (fo=3, routed)           0.426     4.273    N_1198
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.127     4.400 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.452     4.852    N_1448
    SLICE_X1Y63                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.043     4.895 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.217     5.112    N_1698
    SLICE_X1Y63                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.043     5.155 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.285     6.440    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     8.270 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     8.270    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.906ns  (logic 2.919ns (36.922%)  route 4.987ns (63.078%))
  Logic Levels:           9  (IBUF=1 LUT4=4 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=9, routed)           1.218     1.823    B_IBUF[1]
    SLICE_X2Y62                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X2Y62          LUT6 (Prop_lut6_I1_O)        0.043     1.866 r  O_OBUF[7]_inst_i_8/O
                         net (fo=2, routed)           0.494     2.361    N_648
    SLICE_X3Y61                                                       r  O_OBUF[15]_inst_i_17/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.404 r  O_OBUF[15]_inst_i_17/O
                         net (fo=3, routed)           0.435     2.839    N_899
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_9/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     2.882 r  O_OBUF[9]_inst_i_9/O
                         net (fo=4, routed)           0.504     3.387    N_914
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_8/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     3.430 r  O_OBUF[10]_inst_i_8/O
                         net (fo=2, routed)           0.229     3.659    N_1182
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.050     3.709 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.395     4.104    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_3/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.134     4.238 r  O_OBUF[9]_inst_i_3/O
                         net (fo=1, routed)           0.439     4.677    N_1682
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.127     4.804 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.271     6.075    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.906 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.906    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.462ns  (logic 2.767ns (37.073%)  route 4.696ns (62.927%))
  Logic Levels:           8  (IBUF=1 LUT4=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.138     1.734    A_IBUF[2]
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_5/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     1.777 r  O_OBUF[7]_inst_i_5/O
                         net (fo=4, routed)           0.423     2.200    N_864
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.243 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.495     2.738    N_898
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_3/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.781 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.686     3.467    N_1398
    SLICE_X0Y64                                                       r  O_OBUF[9]_inst_i_4/I4
    SLICE_X0Y64          LUT5 (Prop_lut5_I4_O)        0.043     3.510 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.336     3.846    N_1649
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X1Y64          LUT4 (Prop_lut4_I0_O)        0.051     3.897 r  O_OBUF[8]_inst_i_3/O
                         net (fo=1, routed)           0.337     4.234    N_1664
    SLICE_X0Y64                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X0Y64          LUT4 (Prop_lut4_I1_O)        0.129     4.363 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.281     5.644    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.462 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.462    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.328ns  (logic 2.584ns (40.840%)  route 3.743ns (59.160%))
  Logic Levels:           6  (IBUF=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.138     1.734    A_IBUF[2]
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_5/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     1.777 r  O_OBUF[7]_inst_i_5/O
                         net (fo=4, routed)           0.423     2.200    N_864
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.243 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.495     2.738    N_898
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_3/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.781 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.496     3.277    N_1398
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_1/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     3.320 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.191     4.511    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.328 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.328    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.910ns  (logic 2.654ns (44.912%)  route 3.256ns (55.088%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=9, routed)           1.201     1.806    B_IBUF[1]
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_4/I0
    SLICE_X3Y62          LUT4 (Prop_lut4_I0_O)        0.043     1.849 r  O_OBUF[7]_inst_i_4/O
                         net (fo=4, routed)           0.345     2.195    N_632
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_2/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.238 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.508     2.745    N_1132
    SLICE_X0Y64                                                       r  O_OBUF[6]_inst_i_1/I2
    SLICE_X0Y64          LUT3 (Prop_lut3_I2_O)        0.053     2.798 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.201     4.000    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.911     5.910 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.910    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




