Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 08:20:36 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_380_evolib.txt -name O
| Design       : mul8_380
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.937ns  (logic 3.003ns (33.604%)  route 5.934ns (66.396%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 r  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       r  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 r  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.314     2.691    N_915
    SLICE_X2Y59                                                       r  O_OBUF[15]_inst_i_23/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.734 r  O_OBUF[15]_inst_i_23/O
                         net (fo=2, routed)           0.510     3.244    N_948
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.287 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.344     3.631    N_1199
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_26/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.674 r  O_OBUF[15]_inst_i_26/O
                         net (fo=2, routed)           0.342     4.016    N_1214
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     4.059 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.521     4.579    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_12/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.622 r  O_OBUF[15]_inst_i_12/O
                         net (fo=3, routed)           0.447     5.069    N_1715
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_4/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.048     5.117 r  O_OBUF[13]_inst_i_4/O
                         net (fo=2, routed)           0.411     5.528    N_1732
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X0Y64          LUT4 (Prop_lut4_I0_O)        0.137     5.665 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.344     7.010    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     8.937 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.937    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.928ns  (logic 2.900ns (32.486%)  route 6.028ns (67.514%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 r  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       r  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 r  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.314     2.691    N_915
    SLICE_X2Y59                                                       r  O_OBUF[15]_inst_i_23/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.734 r  O_OBUF[15]_inst_i_23/O
                         net (fo=2, routed)           0.510     3.244    N_948
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.287 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.344     3.631    N_1199
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_26/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.674 r  O_OBUF[15]_inst_i_26/O
                         net (fo=2, routed)           0.342     4.016    N_1214
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     4.059 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.521     4.579    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_12/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.622 r  O_OBUF[15]_inst_i_12/O
                         net (fo=3, routed)           0.447     5.069    N_1715
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_4/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.048     5.117 r  O_OBUF[13]_inst_i_4/O
                         net (fo=2, routed)           0.511     5.628    N_1732
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_1/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.129     5.757 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.338     7.095    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.928 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.928    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.854ns  (logic 2.828ns (31.939%)  route 6.026ns (68.061%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 r  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       r  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 r  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.314     2.691    N_915
    SLICE_X2Y59                                                       r  O_OBUF[15]_inst_i_23/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.734 r  O_OBUF[15]_inst_i_23/O
                         net (fo=2, routed)           0.510     3.244    N_948
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.287 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.344     3.631    N_1199
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_26/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.674 r  O_OBUF[15]_inst_i_26/O
                         net (fo=2, routed)           0.342     4.016    N_1214
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     4.059 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.521     4.579    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_12/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.622 r  O_OBUF[15]_inst_i_12/O
                         net (fo=3, routed)           0.447     5.069    N_1715
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_5/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     5.112 r  O_OBUF[15]_inst_i_5/O
                         net (fo=2, routed)           0.432     5.545    N_1733
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     5.588 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.415     7.003    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.854 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.854    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.719ns  (logic 2.812ns (32.255%)  route 5.906ns (67.745%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=1 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 r  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       r  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 r  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.314     2.691    N_915
    SLICE_X2Y59                                                       r  O_OBUF[15]_inst_i_23/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.734 r  O_OBUF[15]_inst_i_23/O
                         net (fo=2, routed)           0.510     3.244    N_948
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.287 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.344     3.631    N_1199
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_26/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.674 r  O_OBUF[15]_inst_i_26/O
                         net (fo=2, routed)           0.342     4.016    N_1214
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     4.059 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.444     4.503    N_1464
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_11/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     4.546 r  O_OBUF[15]_inst_i_11/O
                         net (fo=1, routed)           0.486     5.032    O_OBUF[15]_inst_i_11_n_0
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_3/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     5.075 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.407     5.482    N_1983
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X0Y65          LUT6 (Prop_lut6_I1_O)        0.043     5.525 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.358     6.883    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.719 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.719    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.178ns  (logic 2.862ns (34.994%)  route 5.316ns (65.006%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=2 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 f  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              f  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 f  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       f  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 f  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       f  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 f  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.321     2.698    N_915
    SLICE_X2Y59                                                       f  O_OBUF[7]_inst_i_7/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.741 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.350     3.091    N_1115
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.134 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.292     3.426    N_1398
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_5/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     3.469 r  O_OBUF[9]_inst_i_5/O
                         net (fo=5, routed)           0.429     3.898    N_1649
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_3/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.941 r  O_OBUF[9]_inst_i_3/O
                         net (fo=3, routed)           0.417     4.359    N_1915
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.053     4.412 r  O_OBUF[11]_inst_i_3/O
                         net (fo=3, routed)           0.508     4.919    N_1933
    SLICE_X0Y65                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.131     5.050 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.298     6.348    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     8.178 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     8.178    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.076ns  (logic 2.775ns (34.358%)  route 5.301ns (65.642%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 r  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       r  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 r  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.314     2.691    N_915
    SLICE_X2Y59                                                       r  O_OBUF[15]_inst_i_23/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.734 r  O_OBUF[15]_inst_i_23/O
                         net (fo=2, routed)           0.510     3.244    N_948
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_13/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.287 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.344     3.631    N_1199
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_26/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.674 r  O_OBUF[15]_inst_i_26/O
                         net (fo=2, routed)           0.342     4.016    N_1214
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     4.059 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.521     4.579    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X1Y63          LUT6 (Prop_lut6_I0_O)        0.043     4.622 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.301     4.923    N_1714
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y64          LUT6 (Prop_lut6_I0_O)        0.043     4.966 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.269     6.235    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.076 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.076    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.426ns  (logic 2.722ns (36.656%)  route 4.704ns (63.344%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 f  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              f  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 f  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       f  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 f  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       f  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 f  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.321     2.698    N_915
    SLICE_X2Y59                                                       f  O_OBUF[7]_inst_i_7/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.741 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.350     3.091    N_1115
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.134 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.292     3.426    N_1398
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_5/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     3.469 r  O_OBUF[9]_inst_i_5/O
                         net (fo=5, routed)           0.429     3.898    N_1649
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_3/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.941 r  O_OBUF[9]_inst_i_3/O
                         net (fo=3, routed)           0.417     4.359    N_1915
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_1/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.043     4.402 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.193     5.594    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.426 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.426    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.147ns  (logic 2.709ns (37.907%)  route 4.438ns (62.093%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 r  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       r  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 r  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.314     2.691    N_915
    SLICE_X2Y59                                                       r  O_OBUF[5]_inst_i_2/I2
    SLICE_X2Y59          LUT6 (Prop_lut6_I2_O)        0.043     2.734 r  O_OBUF[5]_inst_i_2/O
                         net (fo=6, routed)           0.382     3.116    N_949
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_11/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.043     3.159 r  O_OBUF[9]_inst_i_11/O
                         net (fo=1, routed)           0.227     3.386    N_1383
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_6/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.429 r  O_OBUF[9]_inst_i_6/O
                         net (fo=5, routed)           0.310     3.739    N_1414
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.782 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.224     4.006    N_1664
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.043     4.049 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.279     5.329    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.147 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.147    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.640ns  (logic 2.724ns (41.028%)  route 3.916ns (58.972%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 r  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       r  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 r  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.314     2.691    N_915
    SLICE_X2Y59                                                       r  O_OBUF[5]_inst_i_2/I2
    SLICE_X2Y59          LUT6 (Prop_lut6_I2_O)        0.043     2.734 r  O_OBUF[5]_inst_i_2/O
                         net (fo=6, routed)           0.382     3.116    N_949
    SLICE_X1Y61                                                       r  O_OBUF[7]_inst_i_3/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.043     3.159 r  O_OBUF[7]_inst_i_3/O
                         net (fo=4, routed)           0.258     3.418    N_1382
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X0Y61          LUT3 (Prop_lut3_I0_O)        0.052     3.470 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.259     4.729    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.911     6.640 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.640    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.583ns  (logic 2.621ns (39.814%)  route 3.962ns (60.186%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 f  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              f  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 f  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.125     1.715    A_IBUF[4]
    SLICE_X2Y62                                                       f  O_OBUF[5]_inst_i_6/I0
    SLICE_X2Y62          LUT2 (Prop_lut2_I0_O)        0.043     1.758 f  O_OBUF[5]_inst_i_6/O
                         net (fo=4, routed)           0.577     2.334    N_764
    SLICE_X3Y60                                                       f  O_OBUF[5]_inst_i_4/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.377 f  O_OBUF[5]_inst_i_4/O
                         net (fo=3, routed)           0.321     2.698    N_915
    SLICE_X2Y59                                                       f  O_OBUF[7]_inst_i_7/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.741 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.350     3.091    N_1115
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_2/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     3.134 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.332     3.466    N_1398
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.509 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.258     4.767    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.583 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.583    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




