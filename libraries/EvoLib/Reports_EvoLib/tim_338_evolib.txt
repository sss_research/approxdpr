Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 06:15:45 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_338_evolib.txt -name O
| Design       : mul8_338
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.506ns  (logic 3.013ns (31.696%)  route 6.493ns (68.304%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=1 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.142     1.731    A_IBUF[4]
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_9/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.048     1.779 f  O_OBUF[7]_inst_i_9/O
                         net (fo=5, routed)           0.611     2.390    O_OBUF[7]_inst_i_9_n_0
    SLICE_X2Y61                                                       f  O_OBUF[15]_inst_i_13/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.129     2.519 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.414     2.933    N_915
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.051     2.984 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.597     3.581    N_932
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.129     3.710 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.613     4.323    N_1199
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_6/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.366 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.428     4.794    N_1215
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.837 r  O_OBUF[12]_inst_i_7/O
                         net (fo=2, routed)           0.427     5.264    N_1482
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_7/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.043     5.307 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.336     5.643    N_1733
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_4/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     5.686 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.510     6.196    N_1749
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I5
    SLICE_X0Y65          LUT6 (Prop_lut6_I5_O)        0.043     6.239 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.415     7.655    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     9.506 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     9.506    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.354ns  (logic 2.997ns (32.041%)  route 6.357ns (67.959%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=1 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.142     1.731    A_IBUF[4]
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_9/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.048     1.779 f  O_OBUF[7]_inst_i_9/O
                         net (fo=5, routed)           0.611     2.390    O_OBUF[7]_inst_i_9_n_0
    SLICE_X2Y61                                                       f  O_OBUF[15]_inst_i_13/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.129     2.519 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.414     2.933    N_915
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.051     2.984 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.597     3.581    N_932
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.129     3.710 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.613     4.323    N_1199
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_6/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.366 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.428     4.794    N_1215
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.837 r  O_OBUF[12]_inst_i_7/O
                         net (fo=2, routed)           0.427     5.264    N_1482
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_7/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.043     5.307 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.345     5.652    N_1733
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     5.695 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.423     6.118    N_1748
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X0Y65          LUT6 (Prop_lut6_I2_O)        0.043     6.161 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.358     7.519    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     9.354 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     9.354    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.317ns  (logic 3.088ns (33.146%)  route 6.229ns (66.854%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.142     1.731    A_IBUF[4]
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_9/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.048     1.779 f  O_OBUF[7]_inst_i_9/O
                         net (fo=5, routed)           0.611     2.390    O_OBUF[7]_inst_i_9_n_0
    SLICE_X2Y61                                                       f  O_OBUF[15]_inst_i_13/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.129     2.519 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.414     2.933    N_915
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.051     2.984 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.597     3.581    N_932
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.129     3.710 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.613     4.323    N_1199
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_6/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.366 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.428     4.794    N_1215
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.837 r  O_OBUF[12]_inst_i_7/O
                         net (fo=2, routed)           0.427     5.264    N_1482
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_7/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.043     5.307 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.345     5.652    N_1733
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     5.695 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.308     6.003    N_1748
    SLICE_X0Y65                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X0Y65          LUT4 (Prop_lut4_I1_O)        0.051     6.054 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.344     7.399    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.919     9.317 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     9.317    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.887ns  (logic 3.054ns (34.370%)  route 5.832ns (65.630%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.142     1.731    A_IBUF[4]
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_9/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.048     1.779 f  O_OBUF[7]_inst_i_9/O
                         net (fo=5, routed)           0.611     2.390    O_OBUF[7]_inst_i_9_n_0
    SLICE_X2Y61                                                       f  O_OBUF[15]_inst_i_13/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.129     2.519 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.414     2.933    N_915
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.051     2.984 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.597     3.581    N_932
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_12/I4
    SLICE_X2Y63          LUT6 (Prop_lut6_I4_O)        0.129     3.710 r  O_OBUF[15]_inst_i_12/O
                         net (fo=2, routed)           0.613     4.323    N_1199
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_6/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.366 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.428     4.794    N_1215
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_7/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.837 r  O_OBUF[12]_inst_i_7/O
                         net (fo=2, routed)           0.326     5.163    N_1482
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_3/I3
    SLICE_X2Y64          LUT6 (Prop_lut6_I3_O)        0.043     5.206 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.352     5.558    N_1732
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X2Y64          LUT4 (Prop_lut4_I1_O)        0.048     5.606 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.350     6.956    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.931     8.887 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.887    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.438ns  (logic 3.048ns (36.122%)  route 5.390ns (63.878%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=5 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.142     1.731    A_IBUF[4]
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_9/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.048     1.779 f  O_OBUF[7]_inst_i_9/O
                         net (fo=5, routed)           0.611     2.390    O_OBUF[7]_inst_i_9_n_0
    SLICE_X2Y61                                                       f  O_OBUF[15]_inst_i_13/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.129     2.519 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.414     2.933    N_915
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.051     2.984 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.590     3.574    N_932
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_8/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.129     3.703 r  O_OBUF[11]_inst_i_8/O
                         net (fo=2, routed)           0.228     3.931    N_1198
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.047     3.978 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.294     4.272    N_1448
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.127     4.399 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.432     4.831    N_1699
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X2Y64          LUT4 (Prop_lut4_I0_O)        0.043     4.874 r  O_OBUF[11]_inst_i_3/O
                         net (fo=1, routed)           0.409     5.283    N_1714
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X1Y64          LUT4 (Prop_lut4_I1_O)        0.043     5.326 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.271     6.596    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.438 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.438    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.000ns  (logic 3.092ns (38.649%)  route 4.908ns (61.351%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=4 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          1.142     1.731    A_IBUF[4]
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_9/I1
    SLICE_X1Y64          LUT2 (Prop_lut2_I1_O)        0.048     1.779 f  O_OBUF[7]_inst_i_9/O
                         net (fo=5, routed)           0.611     2.390    O_OBUF[7]_inst_i_9_n_0
    SLICE_X2Y61                                                       f  O_OBUF[15]_inst_i_13/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.129     2.519 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.414     2.933    N_915
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.051     2.984 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.590     3.574    N_932
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_8/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.129     3.703 r  O_OBUF[11]_inst_i_8/O
                         net (fo=2, routed)           0.228     3.931    N_1198
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.047     3.978 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.294     4.272    N_1448
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X0Y63          LUT4 (Prop_lut4_I1_O)        0.137     4.409 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.345     4.754    N_1698
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y63          LUT4 (Prop_lut4_I1_O)        0.131     4.885 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.285     6.170    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     8.000 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     8.000    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.252ns  (logic 2.872ns (39.606%)  route 4.380ns (60.394%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=9, routed)           1.398     1.994    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X1Y63          LUT2 (Prop_lut2_I1_O)        0.050     2.044 f  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.440     2.484    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_7/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.126     2.610 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.342     2.952    N_1399
    SLICE_X3Y62                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X3Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.995 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.459     3.454    N_1415
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.497 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.334     3.831    N_1432
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_3/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.052     3.883 r  O_OBUF[9]_inst_i_3/O
                         net (fo=1, routed)           0.139     4.021    N_1682
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.131     4.152 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.268     5.421    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.252 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.252    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.018ns  (logic 2.811ns (40.045%)  route 4.208ns (59.955%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT4=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=9, routed)           1.398     1.994    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X1Y63          LUT2 (Prop_lut2_I1_O)        0.050     2.044 f  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.440     2.484    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y62                                                       f  O_OBUF[9]_inst_i_7/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.126     2.610 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.344     2.954    N_1399
    SLICE_X3Y62                                                       r  O_OBUF[9]_inst_i_5/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.997 r  O_OBUF[9]_inst_i_5/O
                         net (fo=3, routed)           0.414     3.411    N_1414
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.048     3.459 r  O_OBUF[8]_inst_i_3/O
                         net (fo=1, routed)           0.432     3.892    N_1664
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.129     4.021 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.179     5.200    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.018 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.018    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.811ns  (logic 2.631ns (45.279%)  route 3.180ns (54.721%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              f  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[2]_inst/O
                         net (fo=9, routed)           1.398     1.994    A_IBUF[2]
    SLICE_X1Y63                                                       f  O_OBUF[7]_inst_i_7/I1
    SLICE_X1Y63          LUT2 (Prop_lut2_I1_O)        0.050     2.044 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.348     2.392    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y62                                                       r  O_OBUF[7]_inst_i_3/I0
    SLICE_X2Y62          LUT6 (Prop_lut6_I0_O)        0.126     2.518 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.176     2.694    N_1398
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_1/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.737 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.258     3.995    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.811 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.811    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.286ns  (logic 2.640ns (49.943%)  route 2.646ns (50.057%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=13, routed)          0.953     1.543    A_IBUF[4]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_5/I4
    SLICE_X0Y60          LUT6 (Prop_lut6_I4_O)        0.043     1.586 r  O_OBUF[7]_inst_i_5/O
                         net (fo=7, routed)           0.222     1.808    O_OBUF[7]_inst_i_5_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_3/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.054     1.862 r  O_OBUF[6]_inst_i_3/O
                         net (fo=1, routed)           0.294     2.155    N_882
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.131     2.286 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.177     3.464    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     5.286 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.286    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




