Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 23:44:47 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_205_evolib.txt -name O
| Design       : mul8_205
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 B[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.113ns  (logic 2.730ns (38.385%)  route 4.383ns (61.615%))
  Logic Levels:           7  (IBUF=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL25                                              0.000     0.000 r  B[6] (IN)
                         net (fo=0)                   0.000     0.000    B[6]
    AL25                                                              r  B_IBUF[6]_inst/I
    AL25                 IBUF (Prop_ibuf_I_O)         0.574     0.574 r  B_IBUF[6]_inst/O
                         net (fo=19, routed)          1.081     1.655    B_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[11]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.698 r  O_OBUF[11]_inst_i_9/O
                         net (fo=1, routed)           0.688     2.386    N_1335
    SLICE_X0Y61                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     2.429 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.441     2.870    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y62                                                       f  O_OBUF[13]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.913 f  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.338     3.252    O_OBUF[13]_inst_i_2_n_0
    SLICE_X2Y63                                                       f  O_OBUF[15]_inst_i_3/I1
    SLICE_X2Y63          LUT5 (Prop_lut5_I1_O)        0.049     3.301 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.384     3.685    N_1973
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.127     3.812 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.450     5.262    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.113 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.113    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.878ns  (logic 2.715ns (39.469%)  route 4.163ns (60.531%))
  Logic Levels:           7  (IBUF=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL25                                              0.000     0.000 r  B[6] (IN)
                         net (fo=0)                   0.000     0.000    B[6]
    AL25                                                              r  B_IBUF[6]_inst/I
    AL25                 IBUF (Prop_ibuf_I_O)         0.574     0.574 r  B_IBUF[6]_inst/O
                         net (fo=19, routed)          1.081     1.655    B_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[11]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.698 r  O_OBUF[11]_inst_i_9/O
                         net (fo=1, routed)           0.688     2.386    N_1335
    SLICE_X0Y61                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     2.429 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.441     2.870    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y62                                                       f  O_OBUF[13]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.913 f  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.338     3.252    O_OBUF[13]_inst_i_2_n_0
    SLICE_X2Y63                                                       f  O_OBUF[15]_inst_i_3/I1
    SLICE_X2Y63          LUT5 (Prop_lut5_I1_O)        0.049     3.301 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.292     3.593    N_1973
    SLICE_X0Y63                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.127     3.720 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.323     5.042    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     6.878 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     6.878    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.464ns  (logic 2.579ns (39.893%)  route 3.885ns (60.107%))
  Logic Levels:           6  (IBUF=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL25                                              0.000     0.000 f  B[6] (IN)
                         net (fo=0)                   0.000     0.000    B[6]
    AL25                                                              f  B_IBUF[6]_inst/I
    AL25                 IBUF (Prop_ibuf_I_O)         0.574     0.574 f  B_IBUF[6]_inst/O
                         net (fo=19, routed)          1.081     1.655    B_IBUF[6]
    SLICE_X0Y60                                                       f  O_OBUF[11]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.698 f  O_OBUF[11]_inst_i_9/O
                         net (fo=1, routed)           0.688     2.386    N_1335
    SLICE_X0Y61                                                       f  O_OBUF[11]_inst_i_2/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     2.429 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.441     2.870    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.913 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.338     3.252    O_OBUF[13]_inst_i_2_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X2Y63          LUT5 (Prop_lut5_I0_O)        0.043     3.295 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.337     4.631    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.464 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.464    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.451ns  (logic 2.588ns (40.110%)  route 3.864ns (59.890%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL25                                              0.000     0.000 f  B[6] (IN)
                         net (fo=0)                   0.000     0.000    B[6]
    AL25                                                              f  B_IBUF[6]_inst/I
    AL25                 IBUF (Prop_ibuf_I_O)         0.574     0.574 f  B_IBUF[6]_inst/O
                         net (fo=19, routed)          1.081     1.655    B_IBUF[6]
    SLICE_X0Y60                                                       f  O_OBUF[11]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.698 f  O_OBUF[11]_inst_i_9/O
                         net (fo=1, routed)           0.688     2.386    N_1335
    SLICE_X0Y61                                                       f  O_OBUF[11]_inst_i_2/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     2.429 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.441     2.870    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     2.913 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.339     3.253    O_OBUF[13]_inst_i_2_n_0
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.043     3.296 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.314     4.610    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.451 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.451    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.070ns  (logic 2.544ns (41.915%)  route 3.526ns (58.085%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL25                                              0.000     0.000 f  B[6] (IN)
                         net (fo=0)                   0.000     0.000    B[6]
    AL25                                                              f  B_IBUF[6]_inst/I
    AL25                 IBUF (Prop_ibuf_I_O)         0.574     0.574 f  B_IBUF[6]_inst/O
                         net (fo=19, routed)          1.081     1.655    B_IBUF[6]
    SLICE_X0Y60                                                       f  O_OBUF[11]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     1.698 f  O_OBUF[11]_inst_i_9/O
                         net (fo=1, routed)           0.688     2.386    N_1335
    SLICE_X0Y61                                                       f  O_OBUF[11]_inst_i_2/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     2.429 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.442     2.871    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.914 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.314     4.229    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.070 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.070    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.809ns  (logic 2.637ns (45.395%)  route 3.172ns (54.605%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.140     1.727    A_IBUF[6]
    SLICE_X0Y62                                                       f  O_OBUF[2]_inst_i_4/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.048     1.775 r  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.330     2.105    O_OBUF[2]_inst_i_4_n_0
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_4/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.129     2.234 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.427     2.661    N_1602
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.704 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.274     3.978    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.809 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.809    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.765ns  (logic 2.623ns (45.500%)  route 3.142ns (54.500%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.140     1.727    A_IBUF[6]
    SLICE_X0Y62                                                       r  O_OBUF[2]_inst_i_4/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.048     1.775 f  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.330     2.105    O_OBUF[2]_inst_i_4_n_0
    SLICE_X1Y62                                                       f  O_OBUF[11]_inst_i_5/I2
    SLICE_X1Y62          LUT5 (Prop_lut5_I2_O)        0.129     2.234 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.381     2.615    N_1425
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y63          LUT5 (Prop_lut5_I3_O)        0.043     2.658 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.290     3.948    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.765 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.765    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.763ns  (logic 2.629ns (45.627%)  route 3.133ns (54.373%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 f  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              f  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 f  A_IBUF[5]_inst/O
                         net (fo=15, routed)          1.300     1.888    A_IBUF[5]
    SLICE_X1Y60                                                       f  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y60          LUT5 (Prop_lut5_I0_O)        0.051     1.939 f  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.230     2.168    N_1172
    SLICE_X1Y60                                                       f  O_OBUF[8]_inst_i_3/I3
    SLICE_X1Y60          LUT6 (Prop_lut6_I3_O)        0.129     2.297 r  O_OBUF[8]_inst_i_3/O
                         net (fo=1, routed)           0.337     2.634    O_OBUF[8]_inst_i_3_n_0
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X1Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.677 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.267     3.944    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.763 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.763    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.678ns  (logic 2.628ns (46.292%)  route 3.050ns (53.708%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.140     1.727    A_IBUF[6]
    SLICE_X0Y62                                                       r  O_OBUF[2]_inst_i_4/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.048     1.775 f  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.330     2.105    O_OBUF[2]_inst_i_4_n_0
    SLICE_X1Y62                                                       f  O_OBUF[11]_inst_i_5/I2
    SLICE_X1Y62          LUT5 (Prop_lut5_I2_O)        0.129     2.234 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.381     2.615    N_1425
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y63          LUT5 (Prop_lut5_I3_O)        0.043     2.658 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.198     3.856    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.678 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.678    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.602ns  (logic 2.547ns (45.471%)  route 3.055ns (54.529%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.140     1.727    A_IBUF[6]
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.043     1.770 r  O_OBUF[11]_inst_i_7/O
                         net (fo=3, routed)           0.231     2.000    N_1186
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.043 r  O_OBUF[9]_inst_i_4/O
                         net (fo=1, routed)           0.341     2.384    N_1350
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_1/I5
    SLICE_X1Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.427 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.343     3.770    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.602 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.602    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------




