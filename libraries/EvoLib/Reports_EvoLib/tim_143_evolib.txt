Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 20:45:34 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_143_evolib.txt -name O
| Design       : mul8_143
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.689ns  (logic 2.946ns (30.408%)  route 6.743ns (69.592%))
  Logic Levels:           12  (IBUF=1 LUT4=3 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.637     2.970    N_883
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_9/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.013 r  O_OBUF[8]_inst_i_9/O
                         net (fo=2, routed)           0.417     3.430    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.051     3.481 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.530     4.012    N_1164
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.129     4.141 r  O_OBUF[10]_inst_i_6/O
                         net (fo=5, routed)           0.301     4.441    N_1415
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_10/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.043     4.484 r  O_OBUF[10]_inst_i_10/O
                         net (fo=2, routed)           0.431     4.915    N_1448
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.043     4.958 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.422     5.380    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     5.423 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.500     5.923    N_1949
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     5.966 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.434     6.400    N_1983
    SLICE_X2Y65                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X2Y65          LUT6 (Prop_lut6_I1_O)        0.043     6.443 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.411     7.853    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     9.689 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     9.689    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.627ns  (logic 2.962ns (30.769%)  route 6.665ns (69.231%))
  Logic Levels:           12  (IBUF=1 LUT4=3 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.637     2.970    N_883
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_9/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.013 r  O_OBUF[8]_inst_i_9/O
                         net (fo=2, routed)           0.417     3.430    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.051     3.481 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.530     4.012    N_1164
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.129     4.141 r  O_OBUF[10]_inst_i_6/O
                         net (fo=5, routed)           0.301     4.441    N_1415
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_10/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.043     4.484 r  O_OBUF[10]_inst_i_10/O
                         net (fo=2, routed)           0.431     4.915    N_1448
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.043     4.958 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.422     5.380    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     5.423 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.500     5.923    N_1949
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     5.966 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.432     6.398    N_1983
    SLICE_X2Y65                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X2Y65          LUT6 (Prop_lut6_I3_O)        0.043     6.441 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.334     7.775    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     9.627 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     9.627    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.624ns  (logic 2.943ns (30.582%)  route 6.681ns (69.418%))
  Logic Levels:           12  (IBUF=1 LUT4=4 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.637     2.970    N_883
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_9/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.013 r  O_OBUF[8]_inst_i_9/O
                         net (fo=2, routed)           0.417     3.430    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.051     3.481 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.530     4.012    N_1164
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.129     4.141 r  O_OBUF[10]_inst_i_6/O
                         net (fo=5, routed)           0.301     4.441    N_1415
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_10/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.043     4.484 r  O_OBUF[10]_inst_i_10/O
                         net (fo=2, routed)           0.431     4.915    N_1448
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.043     4.958 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.422     5.380    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     5.423 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.500     5.923    N_1949
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     5.966 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.417     6.383    N_1983
    SLICE_X2Y65                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X2Y65          LUT4 (Prop_lut4_I0_O)        0.043     6.426 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.366     7.792    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     9.624 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     9.624    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.167ns  (logic 3.007ns (32.802%)  route 6.160ns (67.198%))
  Logic Levels:           11  (IBUF=1 LUT4=4 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.637     2.970    N_883
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_9/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.013 r  O_OBUF[8]_inst_i_9/O
                         net (fo=2, routed)           0.417     3.430    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.051     3.481 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.530     4.012    N_1164
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.129     4.141 r  O_OBUF[10]_inst_i_6/O
                         net (fo=5, routed)           0.301     4.441    N_1415
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_10/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.043     4.484 r  O_OBUF[10]_inst_i_10/O
                         net (fo=2, routed)           0.431     4.915    N_1448
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.043     4.958 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.422     5.380    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     5.423 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.414     5.837    N_1949
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.053     5.890 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.347     7.237    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.929     9.167 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     9.167    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.123ns  (logic 2.909ns (31.890%)  route 6.213ns (68.110%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.637     2.970    N_883
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_9/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.013 r  O_OBUF[8]_inst_i_9/O
                         net (fo=2, routed)           0.417     3.430    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.051     3.481 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.530     4.012    N_1164
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.129     4.141 r  O_OBUF[10]_inst_i_6/O
                         net (fo=5, routed)           0.301     4.441    N_1415
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_10/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.043     4.484 r  O_OBUF[10]_inst_i_10/O
                         net (fo=2, routed)           0.431     4.915    N_1448
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.043     4.958 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.422     5.380    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     5.423 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.501     5.924    N_1949
    SLICE_X1Y64                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X1Y64          LUT6 (Prop_lut6_I2_O)        0.043     5.967 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.314     7.281    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     9.123 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     9.123    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.591ns  (logic 2.855ns (33.230%)  route 5.736ns (66.770%))
  Logic Levels:           10  (IBUF=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.637     2.970    N_883
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_9/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.013 r  O_OBUF[8]_inst_i_9/O
                         net (fo=2, routed)           0.417     3.430    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.051     3.481 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.530     4.012    N_1164
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.129     4.141 r  O_OBUF[10]_inst_i_6/O
                         net (fo=5, routed)           0.301     4.441    N_1415
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_10/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.043     4.484 r  O_OBUF[10]_inst_i_10/O
                         net (fo=2, routed)           0.431     4.915    N_1448
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I1
    SLICE_X0Y61          LUT4 (Prop_lut4_I1_O)        0.043     4.958 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.420     5.379    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     5.422 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.339     6.761    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     8.591 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     8.591    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.237ns  (logic 2.905ns (35.267%)  route 5.332ns (64.733%))
  Logic Levels:           9  (IBUF=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.637     2.970    N_883
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_9/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.013 r  O_OBUF[8]_inst_i_9/O
                         net (fo=2, routed)           0.417     3.430    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.051     3.481 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.534     4.016    N_1164
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_3/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.129     4.145 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.398     4.542    N_1664
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_2/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.049     4.591 r  O_OBUF[10]_inst_i_2/O
                         net (fo=3, routed)           0.421     5.012    N_1915
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.129     5.141 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.265     6.406    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     8.237 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     8.237    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[0]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.892ns  (logic 2.878ns (36.465%)  route 5.014ns (63.535%))
  Logic Levels:           9  (IBUF=1 LUT4=3 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.637     2.970    N_883
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_9/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.013 r  O_OBUF[8]_inst_i_9/O
                         net (fo=2, routed)           0.417     3.430    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.051     3.481 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.530     4.012    N_1164
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.129     4.141 r  O_OBUF[10]_inst_i_6/O
                         net (fo=5, routed)           0.498     4.639    N_1415
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_2/I2
    SLICE_X0Y60          LUT4 (Prop_lut4_I2_O)        0.043     4.682 r  O_OBUF[0]_inst_i_2/O
                         net (fo=1, routed)           0.147     4.829    N_1433
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X0Y60          LUT5 (Prop_lut5_I0_O)        0.054     4.883 r  O_OBUF[0]_inst_i_1/O
                         net (fo=1, routed)           1.124     6.007    O_OBUF[0]
    AH24                                                              r  O_OBUF[0]_inst/I
    AH24                 OBUF (Prop_obuf_I_O)         1.885     7.892 r  O_OBUF[0]_inst/O
                         net (fo=0)                   0.000     7.892    O[0]
    AH24                                                              r  O[0] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.665ns  (logic 2.757ns (35.974%)  route 4.907ns (64.026%))
  Logic Levels:           8  (IBUF=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.637     2.970    N_883
    SLICE_X2Y61                                                       r  O_OBUF[8]_inst_i_9/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     3.013 r  O_OBUF[8]_inst_i_9/O
                         net (fo=2, routed)           0.417     3.430    N_1149
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.051     3.481 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.534     4.016    N_1164
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_3/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.129     4.145 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.398     4.542    N_1664
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X0Y63          LUT4 (Prop_lut4_I1_O)        0.043     4.585 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.261     5.846    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.665 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.665    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.769ns  (logic 2.715ns (40.111%)  route 4.054ns (59.889%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=22, routed)          1.227     1.814    A_IBUF[6]
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.857 r  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.433     2.290    N_632
    SLICE_X2Y60                                                       r  O_OBUF[1]_inst_i_7/I5
    SLICE_X2Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.333 r  O_OBUF[1]_inst_i_7/O
                         net (fo=4, routed)           0.534     2.866    N_883
    SLICE_X2Y61                                                       r  O_OBUF[1]_inst_i_3/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.909 r  O_OBUF[1]_inst_i_3/O
                         net (fo=3, routed)           0.336     3.245    N_1148
    SLICE_X1Y62                                                       r  O_OBUF[1]_inst_i_1/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.052     3.297 r  O_OBUF[1]_inst_i_1/O
                         net (fo=3, routed)           0.255     3.553    O_OBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.131     3.684 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.952    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.769 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.769    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




