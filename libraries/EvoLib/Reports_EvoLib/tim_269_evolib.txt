Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 02:52:13 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_269_evolib.txt -name O
| Design       : mul8_269
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.655ns  (logic 2.945ns (34.030%)  route 5.710ns (65.970%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=16, routed)          1.353     1.939    A_IBUF[6]
    SLICE_X0Y60                                                       f  O_OBUF[15]_inst_i_21/I1
    SLICE_X0Y60          LUT2 (Prop_lut2_I1_O)        0.043     1.982 r  O_OBUF[15]_inst_i_21/O
                         net (fo=3, routed)           0.848     2.830    O_OBUF[15]_inst_i_21_n_0
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.873 r  O_OBUF[15]_inst_i_11/O
                         net (fo=2, routed)           0.226     3.100    N_948
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_15/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.045     3.145 r  O_OBUF[15]_inst_i_15/O
                         net (fo=3, routed)           0.299     3.444    N_1198
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.126     3.570 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.338     3.908    N_1448
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     3.951 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.409     4.360    N_1949
    SLICE_X3Y63                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X3Y63          LUT4 (Prop_lut4_I2_O)        0.051     4.411 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.506     4.917    N_1965
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.129     5.046 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.299     5.346    N_1999
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_1/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     5.389 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.431     6.819    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.655 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.655    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.651ns  (logic 2.942ns (34.010%)  route 5.709ns (65.990%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=16, routed)          1.353     1.939    A_IBUF[6]
    SLICE_X0Y60                                                       f  O_OBUF[15]_inst_i_21/I1
    SLICE_X0Y60          LUT2 (Prop_lut2_I1_O)        0.043     1.982 r  O_OBUF[15]_inst_i_21/O
                         net (fo=3, routed)           0.848     2.830    O_OBUF[15]_inst_i_21_n_0
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.873 r  O_OBUF[15]_inst_i_11/O
                         net (fo=2, routed)           0.226     3.100    N_948
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_15/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.045     3.145 r  O_OBUF[15]_inst_i_15/O
                         net (fo=3, routed)           0.299     3.444    N_1198
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.126     3.570 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.338     3.908    N_1448
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     3.951 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.409     4.360    N_1949
    SLICE_X3Y63                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X3Y63          LUT4 (Prop_lut4_I2_O)        0.051     4.411 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.506     4.917    N_1965
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.129     5.046 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.297     5.344    N_1999
    SLICE_X1Y63                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     5.387 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.432     6.819    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.651 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.651    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.394ns  (logic 2.908ns (34.647%)  route 5.486ns (65.353%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=16, routed)          1.353     1.939    A_IBUF[6]
    SLICE_X0Y60                                                       f  O_OBUF[15]_inst_i_21/I1
    SLICE_X0Y60          LUT2 (Prop_lut2_I1_O)        0.043     1.982 r  O_OBUF[15]_inst_i_21/O
                         net (fo=3, routed)           0.848     2.830    O_OBUF[15]_inst_i_21_n_0
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.873 r  O_OBUF[15]_inst_i_11/O
                         net (fo=2, routed)           0.226     3.100    N_948
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_15/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.045     3.145 r  O_OBUF[15]_inst_i_15/O
                         net (fo=3, routed)           0.299     3.444    N_1198
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.126     3.570 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.338     3.908    N_1448
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     3.951 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.409     4.360    N_1949
    SLICE_X3Y63                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X3Y63          LUT4 (Prop_lut4_I2_O)        0.051     4.411 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.593     5.004    N_1965
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X0Y62          LUT6 (Prop_lut6_I2_O)        0.129     5.133 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.419     6.552    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.394 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.394    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.196ns  (logic 2.908ns (35.477%)  route 5.289ns (64.523%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=4 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=16, routed)          1.353     1.939    A_IBUF[6]
    SLICE_X0Y60                                                       f  O_OBUF[15]_inst_i_21/I1
    SLICE_X0Y60          LUT2 (Prop_lut2_I1_O)        0.043     1.982 r  O_OBUF[15]_inst_i_21/O
                         net (fo=3, routed)           0.848     2.830    O_OBUF[15]_inst_i_21_n_0
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.873 r  O_OBUF[15]_inst_i_11/O
                         net (fo=2, routed)           0.226     3.100    N_948
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_15/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.045     3.145 r  O_OBUF[15]_inst_i_15/O
                         net (fo=3, routed)           0.299     3.444    N_1198
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.126     3.570 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.338     3.908    N_1448
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     3.951 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.409     4.360    N_1949
    SLICE_X3Y63                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X3Y63          LUT4 (Prop_lut4_I2_O)        0.051     4.411 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.402     4.813    N_1965
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.129     4.942 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.413     6.355    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.196 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.196    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.508ns  (logic 2.760ns (36.759%)  route 4.748ns (63.242%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=16, routed)          1.353     1.939    A_IBUF[6]
    SLICE_X0Y60                                                       f  O_OBUF[15]_inst_i_21/I1
    SLICE_X0Y60          LUT2 (Prop_lut2_I1_O)        0.043     1.982 r  O_OBUF[15]_inst_i_21/O
                         net (fo=3, routed)           0.848     2.830    O_OBUF[15]_inst_i_21_n_0
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.873 r  O_OBUF[15]_inst_i_11/O
                         net (fo=2, routed)           0.226     3.100    N_948
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_15/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.045     3.145 r  O_OBUF[15]_inst_i_15/O
                         net (fo=3, routed)           0.299     3.444    N_1198
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.126     3.570 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.338     3.908    N_1448
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X3Y62          LUT6 (Prop_lut6_I4_O)        0.043     3.951 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.409     4.360    N_1949
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.403 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.275     5.678    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.508 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.508    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.350ns  (logic 2.761ns (37.567%)  route 4.589ns (62.433%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=4 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 f  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              f  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 f  A_IBUF[6]_inst/O
                         net (fo=16, routed)          1.353     1.939    A_IBUF[6]
    SLICE_X0Y60                                                       f  O_OBUF[15]_inst_i_21/I1
    SLICE_X0Y60          LUT2 (Prop_lut2_I1_O)        0.043     1.982 r  O_OBUF[15]_inst_i_21/O
                         net (fo=3, routed)           0.848     2.830    O_OBUF[15]_inst_i_21_n_0
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I0
    SLICE_X2Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.873 r  O_OBUF[15]_inst_i_11/O
                         net (fo=2, routed)           0.226     3.100    N_948
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_15/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.045     3.145 r  O_OBUF[15]_inst_i_15/O
                         net (fo=3, routed)           0.299     3.444    N_1198
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.126     3.570 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.333     3.903    N_1448
    SLICE_X3Y62                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X3Y62          LUT4 (Prop_lut4_I1_O)        0.043     3.946 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.259     4.205    N_1698
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.043     4.248 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.271     5.518    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.350 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.350    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.694ns  (logic 2.718ns (40.610%)  route 3.976ns (59.390%))
  Logic Levels:           7  (IBUF=1 LUT4=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=16, routed)          1.362     1.948    A_IBUF[6]
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y61          LUT5 (Prop_lut5_I2_O)        0.043     1.991 r  O_OBUF[8]_inst_i_4/O
                         net (fo=7, routed)           0.435     2.427    N_664
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.470 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.335     2.804    N_1182
    SLICE_X3Y61                                                       r  O_OBUF[10]_inst_i_5/I3
    SLICE_X3Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.847 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.340     3.187    N_1432
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_3/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.052     3.239 r  O_OBUF[9]_inst_i_3/O
                         net (fo=1, routed)           0.225     3.464    N_1682
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X0Y62          LUT4 (Prop_lut4_I1_O)        0.132     3.596 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.279     4.875    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.694 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.694    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.472ns  (logic 2.665ns (41.176%)  route 3.807ns (58.824%))
  Logic Levels:           6  (IBUF=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=16, routed)          1.362     1.948    A_IBUF[6]
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y61          LUT5 (Prop_lut5_I2_O)        0.043     1.991 r  O_OBUF[8]_inst_i_4/O
                         net (fo=7, routed)           0.343     2.334    N_664
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_8/I2
    SLICE_X2Y61          LUT5 (Prop_lut5_I2_O)        0.049     2.383 r  O_OBUF[9]_inst_i_8/O
                         net (fo=5, routed)           0.501     2.885    N_1164
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_3/I4
    SLICE_X1Y62          LUT6 (Prop_lut6_I4_O)        0.127     3.012 r  O_OBUF[8]_inst_i_3/O
                         net (fo=1, routed)           0.327     3.339    N_1664
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_1/I2
    SLICE_X0Y63          LUT5 (Prop_lut5_I2_O)        0.043     3.382 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.274     4.656    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.472 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.472    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.976ns  (logic 2.628ns (43.976%)  route 3.348ns (56.024%))
  Logic Levels:           5  (IBUF=1 LUT3=2 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=16, routed)          1.362     1.948    A_IBUF[6]
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y61          LUT5 (Prop_lut5_I2_O)        0.043     1.991 r  O_OBUF[8]_inst_i_4/O
                         net (fo=7, routed)           0.513     2.504    N_664
    SLICE_X2Y63                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X2Y63          LUT3 (Prop_lut3_I0_O)        0.043     2.547 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.296     2.843    N_915
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X0Y63          LUT3 (Prop_lut3_I2_O)        0.050     2.893 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.177     4.071    O_OBUF[7]
    AK26                                                              r  O_OBUF[7]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.906     5.976 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.976    O[7]
    AK26                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.202ns  (logic 2.467ns (47.429%)  route 2.735ns (52.571%))
  Logic Levels:           4  (IBUF=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 f  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              f  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 f  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.256     1.835    A_IBUF[7]
    SLICE_X1Y63                                                       f  O_OBUF[5]_inst_i_2/I0
    SLICE_X1Y63          LUT5 (Prop_lut5_I0_O)        0.043     1.878 f  O_OBUF[5]_inst_i_2/O
                         net (fo=2, routed)           0.354     2.232    O_OBUF[5]_inst_i_2_n_0
    SLICE_X0Y63                                                       f  O_OBUF[5]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.275 r  O_OBUF[5]_inst_i_1/O
                         net (fo=1, routed)           1.125     3.400    O_OBUF[5]
    AJ26                                                              r  O_OBUF[5]_inst/I
    AJ26                 OBUF (Prop_obuf_I_O)         1.802     5.202 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.202    O[5]
    AJ26                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------




