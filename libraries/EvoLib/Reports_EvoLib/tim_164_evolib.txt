Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 21:46:13 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_164_evolib.txt -name O
| Design       : mul8_164
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.989ns  (logic 2.835ns (40.564%)  route 4.154ns (59.436%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.305     1.892    A_IBUF[6]
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_11/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.053     1.945 r  O_OBUF[11]_inst_i_11/O
                         net (fo=2, routed)           0.417     2.362    N_1232
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_6/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.131     2.493 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.351     2.844    N_1632
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.887 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.422     3.308    N_1781
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_3/I2
    SLICE_X2Y64          LUT5 (Prop_lut5_I2_O)        0.043     3.351 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.237     3.588    N_1973
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X2Y64          LUT6 (Prop_lut6_I1_O)        0.127     3.715 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.422     5.138    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     6.989 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     6.989    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.741ns  (logic 2.819ns (41.824%)  route 3.921ns (58.176%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.305     1.892    A_IBUF[6]
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_11/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.053     1.945 r  O_OBUF[11]_inst_i_11/O
                         net (fo=2, routed)           0.417     2.362    N_1232
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_6/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.131     2.493 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.351     2.844    N_1632
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.887 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.422     3.308    N_1781
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_3/I2
    SLICE_X2Y64          LUT5 (Prop_lut5_I2_O)        0.043     3.351 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.101     3.452    N_1973
    SLICE_X2Y64                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X2Y64          LUT6 (Prop_lut6_I4_O)        0.127     3.579 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.326     4.905    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     6.741 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     6.741    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.581ns  (logic 2.689ns (40.864%)  route 3.892ns (59.136%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.305     1.892    A_IBUF[6]
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_11/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.053     1.945 r  O_OBUF[11]_inst_i_11/O
                         net (fo=2, routed)           0.417     2.362    N_1232
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_6/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.131     2.493 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.351     2.844    N_1632
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.887 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.422     3.308    N_1781
    SLICE_X2Y64                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X2Y64          LUT5 (Prop_lut5_I1_O)        0.043     3.351 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.397     4.748    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.581 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.581    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.561ns  (logic 2.698ns (41.126%)  route 3.863ns (58.874%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.305     1.892    A_IBUF[6]
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_11/I2
    SLICE_X1Y63          LUT4 (Prop_lut4_I2_O)        0.053     1.945 r  O_OBUF[11]_inst_i_11/O
                         net (fo=2, routed)           0.417     2.362    N_1232
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_6/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.131     2.493 r  O_OBUF[11]_inst_i_6/O
                         net (fo=3, routed)           0.351     2.844    N_1632
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.887 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.440     3.326    N_1781
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X2Y64          LUT3 (Prop_lut3_I0_O)        0.043     3.369 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.350     4.719    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.561 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.561    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.028ns  (logic 2.553ns (42.360%)  route 3.474ns (57.640%))
  Logic Levels:           5  (IBUF=1 LUT3=2 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=9, routed)           1.138     1.733    A_IBUF[3]
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_8/I0
    SLICE_X1Y62          LUT3 (Prop_lut3_I0_O)        0.043     1.776 r  O_OBUF[11]_inst_i_8/O
                         net (fo=1, routed)           0.560     2.336    N_420
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y62          LUT5 (Prop_lut5_I3_O)        0.043     2.379 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.511     2.890    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X0Y64          LUT3 (Prop_lut3_I2_O)        0.043     2.933 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.265     4.198    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.028 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.028    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.001ns  (logic 2.655ns (44.252%)  route 3.345ns (55.748%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              f  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 f  A_IBUF[3]_inst/O
                         net (fo=9, routed)           1.225     1.819    A_IBUF[3]
    SLICE_X1Y63                                                       f  O_OBUF[2]_inst_i_5/I0
    SLICE_X1Y63          LUT2 (Prop_lut2_I0_O)        0.048     1.867 r  O_OBUF[2]_inst_i_5/O
                         net (fo=2, routed)           0.226     2.093    O_OBUF[2]_inst_i_5_n_0
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.129     2.222 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.624     2.846    N_1602
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X0Y64          LUT6 (Prop_lut6_I2_O)        0.043     2.889 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.271     4.159    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.001 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.001    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.887ns  (logic 2.623ns (44.554%)  route 3.264ns (55.446%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          0.993     1.580    A_IBUF[6]
    SLICE_X1Y61                                                       r  O_OBUF[2]_inst_i_4/I2
    SLICE_X1Y61          LUT4 (Prop_lut4_I2_O)        0.048     1.628 f  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.526     2.154    O_OBUF[2]_inst_i_4_n_0
    SLICE_X1Y63                                                       f  O_OBUF[11]_inst_i_5/I2
    SLICE_X1Y63          LUT5 (Prop_lut5_I2_O)        0.129     2.283 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.516     2.799    N_1425
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.842 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.229     4.071    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.887 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.887    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.800ns  (logic 2.628ns (45.314%)  route 3.172ns (54.686%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          0.993     1.580    A_IBUF[6]
    SLICE_X1Y61                                                       r  O_OBUF[2]_inst_i_4/I2
    SLICE_X1Y61          LUT4 (Prop_lut4_I2_O)        0.048     1.628 f  O_OBUF[2]_inst_i_4/O
                         net (fo=3, routed)           0.526     2.154    O_OBUF[2]_inst_i_4_n_0
    SLICE_X1Y63                                                       f  O_OBUF[11]_inst_i_5/I2
    SLICE_X1Y63          LUT5 (Prop_lut5_I2_O)        0.129     2.283 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.516     2.799    N_1425
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.842 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.136     3.979    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.800 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.800    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.622ns  (logic 2.635ns (46.877%)  route 2.986ns (53.123%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.061     1.650    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_2/I3
    SLICE_X0Y63          LUT5 (Prop_lut5_I3_O)        0.053     1.703 r  O_OBUF[9]_inst_i_2/O
                         net (fo=2, routed)           0.157     1.860    N_1172
    SLICE_X0Y63                                                       r  O_OBUF[1]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.131     1.991 r  O_OBUF[1]_inst_i_1/O
                         net (fo=2, routed)           0.501     2.492    O_OBUF[1]
    SLICE_X0Y64                                                       r  O_OBUF[8]_inst_i_1/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     2.535 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.268     3.803    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.622 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.622    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.526ns  (logic 2.547ns (46.092%)  route 2.979ns (53.908%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          0.993     1.580    A_IBUF[6]
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X1Y61          LUT4 (Prop_lut4_I2_O)        0.043     1.623 r  O_OBUF[11]_inst_i_7/O
                         net (fo=3, routed)           0.428     2.051    N_1186
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.094 r  O_OBUF[9]_inst_i_4/O
                         net (fo=1, routed)           0.292     2.386    N_1350
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.429 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.265     3.695    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.526 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.526    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------




