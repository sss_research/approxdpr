Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 06:18:40 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_339_evolib.txt -name O
| Design       : mul8_339
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.469ns  (logic 2.725ns (36.487%)  route 4.744ns (63.513%))
  Logic Levels:           7  (IBUF=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=30, routed)          1.945     2.532    A_IBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_11/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     2.575 r  O_OBUF[12]_inst_i_11/O
                         net (fo=2, routed)           0.167     2.742    O_OBUF[12]_inst_i_11_n_0
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_4/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     2.785 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.325     3.109    O_OBUF[11]_inst_i_4_n_0
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X2Y64          LUT5 (Prop_lut5_I0_O)        0.047     3.156 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.488     3.644    O_OBUF[11]_inst_i_2_n_0
    SLICE_X3Y66                                                       r  O_OBUF[15]_inst_i_4/I1
    SLICE_X3Y66          LUT6 (Prop_lut6_I1_O)        0.127     3.771 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.408     4.179    O_OBUF[15]_inst_i_4_n_0
    SLICE_X4Y63                                                       r  O_OBUF[14]_inst_i_1/I3
    SLICE_X4Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.222 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.412     5.633    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.469 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.469    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.437ns  (logic 2.741ns (36.854%)  route 4.696ns (63.146%))
  Logic Levels:           7  (IBUF=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=30, routed)          1.945     2.532    A_IBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_11/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     2.575 r  O_OBUF[12]_inst_i_11/O
                         net (fo=2, routed)           0.167     2.742    O_OBUF[12]_inst_i_11_n_0
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_4/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     2.785 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.325     3.109    O_OBUF[11]_inst_i_4_n_0
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X2Y64          LUT5 (Prop_lut5_I0_O)        0.047     3.156 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.488     3.644    O_OBUF[11]_inst_i_2_n_0
    SLICE_X3Y66                                                       r  O_OBUF[15]_inst_i_4/I1
    SLICE_X3Y66          LUT6 (Prop_lut6_I1_O)        0.127     3.771 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.281     4.052    O_OBUF[15]_inst_i_4_n_0
    SLICE_X4Y63                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X4Y63          LUT6 (Prop_lut6_I2_O)        0.043     4.095 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.491     5.586    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.437 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.437    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.401ns  (logic 2.722ns (36.785%)  route 4.678ns (63.215%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=30, routed)          1.945     2.532    A_IBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_11/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     2.575 r  O_OBUF[12]_inst_i_11/O
                         net (fo=2, routed)           0.167     2.742    O_OBUF[12]_inst_i_11_n_0
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_4/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     2.785 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.325     3.109    O_OBUF[11]_inst_i_4_n_0
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X2Y64          LUT5 (Prop_lut5_I0_O)        0.047     3.156 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.488     3.644    O_OBUF[11]_inst_i_2_n_0
    SLICE_X3Y66                                                       r  O_OBUF[15]_inst_i_4/I1
    SLICE_X3Y66          LUT6 (Prop_lut6_I1_O)        0.127     3.771 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.428     4.199    O_OBUF[15]_inst_i_4_n_0
    SLICE_X4Y65                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X4Y65          LUT3 (Prop_lut3_I2_O)        0.043     4.242 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.326     5.568    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.401 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.401    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.141ns  (logic 2.825ns (39.564%)  route 4.316ns (60.436%))
  Logic Levels:           7  (IBUF=1 LUT5=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=30, routed)          1.945     2.532    A_IBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_11/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     2.575 r  O_OBUF[12]_inst_i_11/O
                         net (fo=2, routed)           0.167     2.742    O_OBUF[12]_inst_i_11_n_0
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_4/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     2.785 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.325     3.109    O_OBUF[11]_inst_i_4_n_0
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X2Y64          LUT5 (Prop_lut5_I0_O)        0.047     3.156 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.371     3.527    O_OBUF[11]_inst_i_2_n_0
    SLICE_X3Y66                                                       r  O_OBUF[12]_inst_i_6/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.135     3.662 r  O_OBUF[12]_inst_i_6/O
                         net (fo=1, routed)           0.218     3.880    O_OBUF[12]_inst_i_6_n_0
    SLICE_X3Y66                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.129     4.009 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.291     5.299    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.141 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.141    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.804ns  (logic 2.589ns (38.048%)  route 4.215ns (61.952%))
  Logic Levels:           6  (IBUF=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=30, routed)          1.945     2.532    A_IBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_11/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     2.575 r  O_OBUF[12]_inst_i_11/O
                         net (fo=2, routed)           0.167     2.742    O_OBUF[12]_inst_i_11_n_0
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_4/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     2.785 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.316     3.101    O_OBUF[11]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     3.144 r  O_OBUF[10]_inst_i_5/O
                         net (fo=1, routed)           0.513     3.656    O_OBUF[10]_inst_i_5_n_0
    SLICE_X2Y64                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X2Y64          LUT6 (Prop_lut6_I3_O)        0.043     3.699 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.275     4.974    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.804 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.804    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.780ns  (logic 2.907ns (42.876%)  route 3.873ns (57.124%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=15, routed)          1.199     1.793    A_IBUF[1]
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_8/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     1.836 f  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.224     2.060    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       f  O_OBUF[7]_inst_i_7/I2
    SLICE_X0Y61          LUT5 (Prop_lut5_I2_O)        0.051     2.111 r  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.496     2.607    O_OBUF[7]_inst_i_7_n_0
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_3/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.137     2.744 f  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.347     3.091    O_OBUF[7]_inst_i_3_n_0
    SLICE_X1Y63                                                       f  O_OBUF[8]_inst_i_2/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.135     3.226 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.348     3.574    O_OBUF[8]_inst_i_2_n_0
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y63          LUT6 (Prop_lut6_I0_O)        0.129     3.703 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.258     4.961    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.780 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.780    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.764ns  (logic 2.688ns (39.738%)  route 4.076ns (60.262%))
  Logic Levels:           6  (IBUF=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=30, routed)          1.945     2.532    A_IBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_11/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     2.575 r  O_OBUF[12]_inst_i_11/O
                         net (fo=2, routed)           0.167     2.742    O_OBUF[12]_inst_i_11_n_0
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_4/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     2.785 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.325     3.109    O_OBUF[11]_inst_i_4_n_0
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X2Y64          LUT5 (Prop_lut5_I0_O)        0.047     3.156 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.371     3.527    O_OBUF[11]_inst_i_2_n_0
    SLICE_X3Y66                                                       r  O_OBUF[11]_inst_i_1/I3
    SLICE_X3Y66          LUT5 (Prop_lut5_I3_O)        0.127     3.654 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.923    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.764 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.764    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.686ns  (logic 2.819ns (42.157%)  route 3.868ns (57.843%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 f  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              f  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 f  A_IBUF[1]_inst/O
                         net (fo=15, routed)          1.199     1.793    A_IBUF[1]
    SLICE_X0Y61                                                       f  O_OBUF[6]_inst_i_8/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     1.836 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.224     2.060    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X0Y61          LUT5 (Prop_lut5_I2_O)        0.051     2.111 f  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.496     2.607    O_OBUF[7]_inst_i_7_n_0
    SLICE_X0Y62                                                       f  O_OBUF[7]_inst_i_3/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.137     2.744 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.347     3.091    O_OBUF[7]_inst_i_3_n_0
    SLICE_X1Y63                                                       r  O_OBUF[6]_inst_i_3/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.129     3.220 r  O_OBUF[6]_inst_i_3/O
                         net (fo=1, routed)           0.410     3.630    O_OBUF[6]_inst_i_3_n_0
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_1/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     3.673 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.191     4.864    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.686 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.686    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.612ns  (logic 2.684ns (40.594%)  route 3.928ns (59.406%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=30, routed)          1.671     2.257    A_IBUF[6]
    SLICE_X3Y65                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X3Y65          LUT6 (Prop_lut6_I0_O)        0.043     2.300 r  O_OBUF[12]_inst_i_12/O
                         net (fo=3, routed)           0.319     2.619    O_OBUF[12]_inst_i_12_n_0
    SLICE_X4Y64                                                       r  O_OBUF[11]_inst_i_7/I0
    SLICE_X4Y64          LUT5 (Prop_lut5_I0_O)        0.051     2.670 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.310     2.980    O_OBUF[11]_inst_i_7_n_0
    SLICE_X2Y64                                                       r  O_OBUF[10]_inst_i_2/I2
    SLICE_X2Y64          LUT3 (Prop_lut3_I2_O)        0.129     3.109 r  O_OBUF[10]_inst_i_2/O
                         net (fo=2, routed)           0.366     3.475    O_OBUF[10]_inst_i_2_n_0
    SLICE_X2Y64                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X2Y64          LUT3 (Prop_lut3_I0_O)        0.043     3.518 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.263     4.781    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.612 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.612    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.531ns  (logic 2.770ns (42.406%)  route 3.762ns (57.594%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 f  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              f  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 f  A_IBUF[1]_inst/O
                         net (fo=15, routed)          1.199     1.793    A_IBUF[1]
    SLICE_X0Y61                                                       f  O_OBUF[6]_inst_i_8/I4
    SLICE_X0Y61          LUT5 (Prop_lut5_I4_O)        0.043     1.836 r  O_OBUF[6]_inst_i_8/O
                         net (fo=2, routed)           0.224     2.060    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_7/I2
    SLICE_X0Y61          LUT5 (Prop_lut5_I2_O)        0.051     2.111 f  O_OBUF[7]_inst_i_7/O
                         net (fo=2, routed)           0.496     2.607    O_OBUF[7]_inst_i_7_n_0
    SLICE_X0Y62                                                       f  O_OBUF[7]_inst_i_3/I2
    SLICE_X0Y62          LUT3 (Prop_lut3_I2_O)        0.137     2.744 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.576     3.320    O_OBUF[7]_inst_i_3_n_0
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.129     3.449 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.266     4.715    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.531 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.531    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




