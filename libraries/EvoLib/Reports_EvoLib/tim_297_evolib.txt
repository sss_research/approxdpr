Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 04:14:46 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_297_evolib.txt -name O
| Design       : mul8_297
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.591ns  (logic 2.780ns (36.628%)  route 4.810ns (63.372%))
  Logic Levels:           8  (IBUF=1 LUT5=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=20, routed)          1.456     2.044    A_IBUF[5]
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_20/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.087 r  O_OBUF[13]_inst_i_20/O
                         net (fo=2, routed)           0.433     2.520    O_OBUF[13]_inst_i_20_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_5/I5
    SLICE_X4Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.563 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.418     2.981    O_OBUF[11]_inst_i_5_n_0
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.043     3.024 r  O_OBUF[10]_inst_i_6/O
                         net (fo=3, routed)           0.524     3.548    O_OBUF[10]_inst_i_6_n_0
    SLICE_X3Y64                                                       r  O_OBUF[6]_inst_i_1/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.591 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           0.334     3.926    O_OBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     3.969 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.235     4.203    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I0
    SLICE_X0Y65          LUT5 (Prop_lut5_I0_O)        0.054     4.257 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.409     5.667    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.924     7.591 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.591    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.512ns  (logic 2.697ns (35.903%)  route 4.815ns (64.097%))
  Logic Levels:           8  (IBUF=1 LUT5=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=20, routed)          1.456     2.044    A_IBUF[5]
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_20/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.087 r  O_OBUF[13]_inst_i_20/O
                         net (fo=2, routed)           0.433     2.520    O_OBUF[13]_inst_i_20_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_5/I5
    SLICE_X4Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.563 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.418     2.981    O_OBUF[11]_inst_i_5_n_0
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.043     3.024 r  O_OBUF[10]_inst_i_6/O
                         net (fo=3, routed)           0.524     3.548    O_OBUF[10]_inst_i_6_n_0
    SLICE_X3Y64                                                       r  O_OBUF[6]_inst_i_1/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.591 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           0.334     3.926    O_OBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_2/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     3.969 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.235     4.203    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.043     4.246 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.414     5.661    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.512 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.512    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.187ns  (logic 2.736ns (38.075%)  route 4.450ns (61.925%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=20, routed)          1.456     2.044    A_IBUF[5]
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_20/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.087 r  O_OBUF[13]_inst_i_20/O
                         net (fo=2, routed)           0.433     2.520    O_OBUF[13]_inst_i_20_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_5/I5
    SLICE_X4Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.563 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.418     2.981    O_OBUF[11]_inst_i_5_n_0
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.043     3.024 r  O_OBUF[10]_inst_i_6/O
                         net (fo=3, routed)           0.524     3.548    O_OBUF[10]_inst_i_6_n_0
    SLICE_X3Y64                                                       r  O_OBUF[6]_inst_i_1/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.591 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           0.331     3.922    O_OBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y65          LUT3 (Prop_lut3_I2_O)        0.049     3.971 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.288     5.259    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     7.187 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.187    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.134ns  (logic 2.635ns (36.940%)  route 4.499ns (63.060%))
  Logic Levels:           7  (IBUF=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=20, routed)          1.456     2.044    A_IBUF[5]
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_20/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.087 r  O_OBUF[13]_inst_i_20/O
                         net (fo=2, routed)           0.433     2.520    O_OBUF[13]_inst_i_20_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_5/I5
    SLICE_X4Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.563 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.418     2.981    O_OBUF[11]_inst_i_5_n_0
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.043     3.024 r  O_OBUF[10]_inst_i_6/O
                         net (fo=3, routed)           0.524     3.548    O_OBUF[10]_inst_i_6_n_0
    SLICE_X3Y64                                                       r  O_OBUF[6]_inst_i_1/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.591 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           0.331     3.922    O_OBUF[6]
    SLICE_X0Y65                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X0Y65          LUT5 (Prop_lut5_I1_O)        0.043     3.965 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.336     5.302    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.134 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.134    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.054ns  (logic 2.738ns (38.812%)  route 4.316ns (61.188%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=20, routed)          1.456     2.044    A_IBUF[5]
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_20/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.087 r  O_OBUF[13]_inst_i_20/O
                         net (fo=2, routed)           0.433     2.520    O_OBUF[13]_inst_i_20_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_5/I5
    SLICE_X4Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.563 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.418     2.981    O_OBUF[11]_inst_i_5_n_0
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.043     3.024 r  O_OBUF[10]_inst_i_6/O
                         net (fo=3, routed)           0.521     3.545    O_OBUF[10]_inst_i_6_n_0
    SLICE_X3Y64                                                       r  O_OBUF[11]_inst_i_4/I0
    SLICE_X3Y64          LUT5 (Prop_lut5_I0_O)        0.051     3.596 r  O_OBUF[11]_inst_i_4/O
                         net (fo=1, routed)           0.220     3.816    O_OBUF[11]_inst_i_4_n_0
    SLICE_X3Y64                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X3Y64          LUT3 (Prop_lut3_I2_O)        0.129     3.945 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.268     5.213    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.054 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.054    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.632ns  (logic 2.582ns (38.937%)  route 4.049ns (61.063%))
  Logic Levels:           6  (IBUF=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=20, routed)          1.456     2.044    A_IBUF[5]
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_20/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.087 r  O_OBUF[13]_inst_i_20/O
                         net (fo=2, routed)           0.433     2.520    O_OBUF[13]_inst_i_20_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_5/I5
    SLICE_X4Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.563 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.418     2.981    O_OBUF[11]_inst_i_5_n_0
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.043     3.024 r  O_OBUF[10]_inst_i_6/O
                         net (fo=3, routed)           0.524     3.548    O_OBUF[10]_inst_i_6_n_0
    SLICE_X3Y64                                                       r  O_OBUF[6]_inst_i_1/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.591 r  O_OBUF[6]_inst_i_1/O
                         net (fo=4, routed)           1.218     4.809    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.632 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.632    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.496ns  (logic 2.590ns (39.868%)  route 3.906ns (60.132%))
  Logic Levels:           6  (IBUF=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=20, routed)          1.456     2.044    A_IBUF[5]
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_20/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.087 r  O_OBUF[13]_inst_i_20/O
                         net (fo=2, routed)           0.433     2.520    O_OBUF[13]_inst_i_20_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_5/I5
    SLICE_X4Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.563 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.418     2.981    O_OBUF[11]_inst_i_5_n_0
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X3Y63          LUT5 (Prop_lut5_I4_O)        0.043     3.024 r  O_OBUF[10]_inst_i_6/O
                         net (fo=3, routed)           0.324     3.348    O_OBUF[10]_inst_i_6_n_0
    SLICE_X3Y64                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X3Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.391 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.274     4.666    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.496 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.496    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.324ns  (logic 2.599ns (41.100%)  route 3.725ns (58.900%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=12, routed)          1.205     1.801    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_11/I2
    SLICE_X1Y64          LUT6 (Prop_lut6_I2_O)        0.043     1.844 r  O_OBUF[8]_inst_i_11/O
                         net (fo=4, routed)           0.528     2.371    O_OBUF[8]_inst_i_11_n_0
    SLICE_X1Y65                                                       r  O_OBUF[8]_inst_i_3/I5
    SLICE_X1Y65          LUT6 (Prop_lut6_I5_O)        0.043     2.414 r  O_OBUF[8]_inst_i_3/O
                         net (fo=5, routed)           0.433     2.847    O_OBUF[8]_inst_i_3_n_0
    SLICE_X2Y65                                                       r  O_OBUF[10]_inst_i_2/I2
    SLICE_X2Y65          LUT6 (Prop_lut6_I2_O)        0.043     2.890 r  O_OBUF[10]_inst_i_2/O
                         net (fo=4, routed)           0.296     3.186    O_OBUF[10]_inst_i_2_n_0
    SLICE_X3Y64                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X3Y64          LUT4 (Prop_lut4_I0_O)        0.043     3.229 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.264     4.493    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.324 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.324    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.128ns  (logic 2.636ns (43.006%)  route 3.493ns (56.994%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=12, routed)          1.322     1.918    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_13/I4
    SLICE_X1Y64          LUT6 (Prop_lut6_I4_O)        0.043     1.961 r  O_OBUF[8]_inst_i_13/O
                         net (fo=3, routed)           0.420     2.380    O_OBUF[8]_inst_i_13_n_0
    SLICE_X1Y65                                                       r  O_OBUF[8]_inst_i_5/I4
    SLICE_X1Y65          LUT5 (Prop_lut5_I4_O)        0.049     2.429 r  O_OBUF[8]_inst_i_5/O
                         net (fo=2, routed)           0.530     2.959    O_OBUF[8]_inst_i_5_n_0
    SLICE_X2Y65                                                       r  O_OBUF[8]_inst_i_1/I3
    SLICE_X2Y65          LUT6 (Prop_lut6_I3_O)        0.129     3.088 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.221     4.310    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.128 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.128    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.790ns  (logic 2.541ns (43.886%)  route 3.249ns (56.114%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=12, routed)          1.322     1.918    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_13/I4
    SLICE_X1Y64          LUT6 (Prop_lut6_I4_O)        0.043     1.961 r  O_OBUF[8]_inst_i_13/O
                         net (fo=3, routed)           0.420     2.380    O_OBUF[8]_inst_i_13_n_0
    SLICE_X1Y65                                                       r  O_OBUF[7]_inst_i_2/I1
    SLICE_X1Y65          LUT5 (Prop_lut5_I1_O)        0.043     2.423 r  O_OBUF[7]_inst_i_2/O
                         net (fo=1, routed)           0.295     2.718    O_OBUF[7]_inst_i_2_n_0
    SLICE_X0Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.761 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.213     3.974    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.790 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.790    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




