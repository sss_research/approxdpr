Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 09:11:16 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_397_evolib.txt -name O
| Design       : mul8_397
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.406ns  (logic 3.022ns (35.954%)  route 5.384ns (64.046%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=11, routed)          1.524     2.120    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     2.163 r  O_OBUF[10]_inst_i_7/O
                         net (fo=3, routed)           0.344     2.507    N_1157
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_8/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.550 r  O_OBUF[8]_inst_i_8/O
                         net (fo=2, routed)           0.410     2.960    N_1394
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_2/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.048     3.008 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.448     3.456    N_1572
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X0Y63          LUT3 (Prop_lut3_I0_O)        0.134     3.590 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.534     4.124    N_1735
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.129     4.253 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.253     4.507    N_1929
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_4/I2
    SLICE_X1Y65          LUT5 (Prop_lut5_I2_O)        0.049     4.556 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.438     4.993    N_1957
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X1Y65          LUT6 (Prop_lut6_I2_O)        0.129     5.122 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.432     6.554    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.406 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.406    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.360ns  (logic 3.103ns (37.120%)  route 5.257ns (62.880%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT5=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=11, routed)          1.524     2.120    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     2.163 r  O_OBUF[10]_inst_i_7/O
                         net (fo=3, routed)           0.344     2.507    N_1157
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_8/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.550 r  O_OBUF[8]_inst_i_8/O
                         net (fo=2, routed)           0.410     2.960    N_1394
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_2/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.048     3.008 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.448     3.456    N_1572
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X0Y63          LUT3 (Prop_lut3_I0_O)        0.134     3.590 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.534     4.124    N_1735
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.129     4.253 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.253     4.507    N_1929
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_4/I2
    SLICE_X1Y65          LUT5 (Prop_lut5_I2_O)        0.049     4.556 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.335     4.891    N_1957
    SLICE_X1Y65                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X1Y65          LUT5 (Prop_lut5_I2_O)        0.138     5.029 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.408     6.437    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.924     8.360 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.360    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.134ns  (logic 3.004ns (36.924%)  route 5.131ns (63.076%))
  Logic Levels:           9  (IBUF=1 LUT3=3 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=11, routed)          1.524     2.120    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     2.163 r  O_OBUF[10]_inst_i_7/O
                         net (fo=3, routed)           0.344     2.507    N_1157
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_8/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.550 r  O_OBUF[8]_inst_i_8/O
                         net (fo=2, routed)           0.410     2.960    N_1394
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_2/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.048     3.008 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.448     3.456    N_1572
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X0Y63          LUT3 (Prop_lut3_I0_O)        0.134     3.590 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.534     4.124    N_1735
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.129     4.253 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.253     4.507    N_1929
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_4/I2
    SLICE_X1Y65          LUT5 (Prop_lut5_I2_O)        0.049     4.556 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.335     4.891    N_1957
    SLICE_X1Y65                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X1Y65          LUT3 (Prop_lut3_I1_O)        0.129     5.020 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.282     6.302    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.134 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.134    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.852ns  (logic 2.877ns (36.639%)  route 4.975ns (63.361%))
  Logic Levels:           8  (IBUF=1 LUT3=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=11, routed)          1.524     2.120    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     2.163 r  O_OBUF[10]_inst_i_7/O
                         net (fo=3, routed)           0.344     2.507    N_1157
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_8/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.550 r  O_OBUF[8]_inst_i_8/O
                         net (fo=2, routed)           0.410     2.960    N_1394
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_2/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.048     3.008 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.448     3.456    N_1572
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X0Y63          LUT3 (Prop_lut3_I0_O)        0.134     3.590 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.534     4.124    N_1735
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.129     4.253 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.440     4.694    N_1929
    SLICE_X1Y65                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X1Y65          LUT3 (Prop_lut3_I1_O)        0.043     4.737 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.274     6.011    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.852 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.852    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.662ns  (logic 2.877ns (37.556%)  route 4.784ns (62.444%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=11, routed)          1.524     2.120    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     2.163 r  O_OBUF[10]_inst_i_7/O
                         net (fo=3, routed)           0.344     2.507    N_1157
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_8/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.550 r  O_OBUF[8]_inst_i_8/O
                         net (fo=2, routed)           0.410     2.960    N_1394
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_2/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.048     3.008 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.448     3.456    N_1572
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X0Y63          LUT3 (Prop_lut3_I0_O)        0.134     3.590 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.534     4.124    N_1735
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.129     4.253 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.253     4.507    N_1929
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X1Y65          LUT5 (Prop_lut5_I1_O)        0.043     4.550 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.271     5.820    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.662 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.662    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.385ns  (logic 2.824ns (38.246%)  route 4.560ns (61.754%))
  Logic Levels:           7  (IBUF=1 LUT3=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=11, routed)          1.524     2.120    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     2.163 r  O_OBUF[10]_inst_i_7/O
                         net (fo=3, routed)           0.344     2.507    N_1157
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_8/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.550 r  O_OBUF[8]_inst_i_8/O
                         net (fo=2, routed)           0.410     2.960    N_1394
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_2/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.048     3.008 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.448     3.456    N_1572
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X0Y63          LUT3 (Prop_lut3_I0_O)        0.134     3.590 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.527     4.118    N_1735
    SLICE_X0Y65                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X0Y65          LUT3 (Prop_lut3_I0_O)        0.129     4.247 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.306     5.553    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.385 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.385    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.369ns  (logic 2.914ns (39.545%)  route 4.455ns (60.455%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=11, routed)          1.524     2.120    A_IBUF[2]
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     2.163 r  O_OBUF[10]_inst_i_7/O
                         net (fo=3, routed)           0.344     2.507    N_1157
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_8/I0
    SLICE_X1Y64          LUT6 (Prop_lut6_I0_O)        0.043     2.550 r  O_OBUF[8]_inst_i_8/O
                         net (fo=2, routed)           0.410     2.960    N_1394
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_2/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.048     3.008 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.448     3.456    N_1572
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X0Y63          LUT3 (Prop_lut3_I0_O)        0.134     3.590 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.534     4.124    N_1735
    SLICE_X0Y65                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y65          LUT5 (Prop_lut5_I1_O)        0.134     4.258 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.195     5.453    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.916     7.369 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.369    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.718ns  (logic 2.733ns (40.675%)  route 3.985ns (59.325%))
  Logic Levels:           7  (IBUF=1 LUT4=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 f  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              f  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 f  B_IBUF[1]_inst/O
                         net (fo=16, routed)          1.266     1.871    B_IBUF[1]
    SLICE_X1Y63                                                       f  O_OBUF[6]_inst_i_4/I1
    SLICE_X1Y63          LUT5 (Prop_lut5_I1_O)        0.043     1.914 r  O_OBUF[6]_inst_i_4/O
                         net (fo=3, routed)           0.312     2.226    N_48
    SLICE_X4Y63                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X4Y63          LUT6 (Prop_lut6_I3_O)        0.043     2.269 r  O_OBUF[6]_inst_i_2/O
                         net (fo=2, routed)           0.496     2.765    O_OBUF[6]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_2/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.051     2.816 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.420     3.236    N_1543
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_5/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.129     3.365 r  O_OBUF[8]_inst_i_5/O
                         net (fo=1, routed)           0.231     3.595    N_1721
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_1/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.043     3.638 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.261     4.899    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.718 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.718    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.361ns  (logic 2.687ns (42.247%)  route 3.673ns (57.753%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 f  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              f  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 f  B_IBUF[1]_inst/O
                         net (fo=16, routed)          1.266     1.871    B_IBUF[1]
    SLICE_X1Y63                                                       f  O_OBUF[6]_inst_i_4/I1
    SLICE_X1Y63          LUT5 (Prop_lut5_I1_O)        0.043     1.914 r  O_OBUF[6]_inst_i_4/O
                         net (fo=3, routed)           0.312     2.226    N_48
    SLICE_X4Y63                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X4Y63          LUT6 (Prop_lut6_I3_O)        0.043     2.269 r  O_OBUF[6]_inst_i_2/O
                         net (fo=2, routed)           0.496     2.765    O_OBUF[6]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_2/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.051     2.816 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.419     3.234    N_1543
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y63          LUT3 (Prop_lut3_I0_O)        0.129     3.363 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.181     4.544    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.361 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.361    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.810ns  (logic 2.556ns (43.998%)  route 3.254ns (56.002%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 f  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              f  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 f  B_IBUF[1]_inst/O
                         net (fo=16, routed)          1.266     1.871    B_IBUF[1]
    SLICE_X1Y63                                                       f  O_OBUF[6]_inst_i_4/I1
    SLICE_X1Y63          LUT5 (Prop_lut5_I1_O)        0.043     1.914 r  O_OBUF[6]_inst_i_4/O
                         net (fo=3, routed)           0.312     2.226    N_48
    SLICE_X4Y63                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X4Y63          LUT6 (Prop_lut6_I3_O)        0.043     2.269 r  O_OBUF[6]_inst_i_2/O
                         net (fo=2, routed)           0.496     2.765    O_OBUF[6]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.043     2.808 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.180     3.988    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     5.810 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.810    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




