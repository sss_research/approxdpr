Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 20:54:14 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_146_evolib.txt -name O
| Design       : mul8_146
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.685ns  (logic 2.869ns (37.334%)  route 4.816ns (62.666%))
  Logic Levels:           8  (IBUF=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.422     2.011    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_14/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.054 f  O_OBUF[11]_inst_i_14/O
                         net (fo=2, routed)           0.224     2.279    O_OBUF[11]_inst_i_14_n_0
    SLICE_X0Y63                                                       f  O_OBUF[11]_inst_i_10/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.051     2.330 r  O_OBUF[11]_inst_i_10/O
                         net (fo=2, routed)           0.498     2.828    O_OBUF[11]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y64          LUT5 (Prop_lut5_I3_O)        0.129     2.957 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.559     3.516    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y64                                                       f  O_OBUF[13]_inst_i_3/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.559 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.523     4.082    O_OBUF[13]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X1Y65          LUT6 (Prop_lut6_I2_O)        0.043     4.125 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.219     4.344    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I0
    SLICE_X0Y65          LUT5 (Prop_lut5_I0_O)        0.049     4.393 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.371     5.764    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.922     7.685 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.685    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.577ns  (logic 2.793ns (36.859%)  route 4.784ns (63.141%))
  Logic Levels:           8  (IBUF=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.422     2.011    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_14/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.054 f  O_OBUF[11]_inst_i_14/O
                         net (fo=2, routed)           0.224     2.279    O_OBUF[11]_inst_i_14_n_0
    SLICE_X0Y63                                                       f  O_OBUF[11]_inst_i_10/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.051     2.330 r  O_OBUF[11]_inst_i_10/O
                         net (fo=2, routed)           0.498     2.828    O_OBUF[11]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y64          LUT5 (Prop_lut5_I3_O)        0.129     2.957 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.559     3.516    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y64                                                       f  O_OBUF[13]_inst_i_3/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.559 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.523     4.082    O_OBUF[13]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X1Y65          LUT6 (Prop_lut6_I2_O)        0.043     4.125 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.219     4.344    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.043     4.387 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.339     5.726    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.577 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.577    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.215ns  (logic 2.832ns (39.255%)  route 4.383ns (60.745%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.422     2.011    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_14/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.054 f  O_OBUF[11]_inst_i_14/O
                         net (fo=2, routed)           0.224     2.279    O_OBUF[11]_inst_i_14_n_0
    SLICE_X0Y63                                                       f  O_OBUF[11]_inst_i_10/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.051     2.330 r  O_OBUF[11]_inst_i_10/O
                         net (fo=2, routed)           0.498     2.828    O_OBUF[11]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y64          LUT5 (Prop_lut5_I3_O)        0.129     2.957 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.559     3.516    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y64                                                       f  O_OBUF[13]_inst_i_3/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.559 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.410     3.968    O_OBUF[13]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X1Y65          LUT3 (Prop_lut3_I2_O)        0.049     4.017 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.270     5.287    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     7.215 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.215    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.180ns  (logic 2.731ns (38.039%)  route 4.449ns (61.961%))
  Logic Levels:           7  (IBUF=1 LUT5=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.422     2.011    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_14/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.054 f  O_OBUF[11]_inst_i_14/O
                         net (fo=2, routed)           0.224     2.279    O_OBUF[11]_inst_i_14_n_0
    SLICE_X0Y63                                                       f  O_OBUF[11]_inst_i_10/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.051     2.330 r  O_OBUF[11]_inst_i_10/O
                         net (fo=2, routed)           0.498     2.828    O_OBUF[11]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y64          LUT5 (Prop_lut5_I3_O)        0.129     2.957 f  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.559     3.516    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y64                                                       f  O_OBUF[13]_inst_i_3/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     3.559 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.410     3.968    O_OBUF[13]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X1Y65          LUT5 (Prop_lut5_I1_O)        0.043     4.011 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.336     5.348    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.180 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.180    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.704ns  (logic 2.788ns (41.587%)  route 3.916ns (58.413%))
  Logic Levels:           6  (IBUF=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 f  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              f  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 f  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.422     2.011    A_IBUF[4]
    SLICE_X0Y63                                                       f  O_OBUF[11]_inst_i_14/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.054 r  O_OBUF[11]_inst_i_14/O
                         net (fo=2, routed)           0.224     2.279    O_OBUF[11]_inst_i_14_n_0
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_10/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.051     2.330 f  O_OBUF[11]_inst_i_10/O
                         net (fo=2, routed)           0.498     2.828    O_OBUF[11]_inst_i_10_n_0
    SLICE_X1Y64                                                       f  O_OBUF[11]_inst_i_6/I3
    SLICE_X1Y64          LUT5 (Prop_lut5_I3_O)        0.134     2.962 r  O_OBUF[11]_inst_i_6/O
                         net (fo=2, routed)           0.503     3.465    O_OBUF[11]_inst_i_6_n_0
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_1/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.129     3.594 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.862    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.704 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.704    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.385ns  (logic 2.686ns (42.065%)  route 3.699ns (57.935%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.422     2.011    A_IBUF[4]
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_14/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.054 f  O_OBUF[11]_inst_i_14/O
                         net (fo=2, routed)           0.224     2.279    O_OBUF[11]_inst_i_14_n_0
    SLICE_X0Y63                                                       f  O_OBUF[11]_inst_i_10/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.051     2.330 r  O_OBUF[11]_inst_i_10/O
                         net (fo=2, routed)           0.498     2.828    O_OBUF[11]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y64          LUT5 (Prop_lut5_I3_O)        0.129     2.957 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.262     3.219    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X0Y64          LUT4 (Prop_lut4_I3_O)        0.043     3.262 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.293     4.555    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.385 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.385    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.276ns  (logic 2.687ns (42.815%)  route 3.589ns (57.185%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 f  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              f  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 f  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.422     2.011    A_IBUF[4]
    SLICE_X0Y63                                                       f  O_OBUF[11]_inst_i_14/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.054 r  O_OBUF[11]_inst_i_14/O
                         net (fo=2, routed)           0.224     2.279    O_OBUF[11]_inst_i_14_n_0
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_9/I4
    SLICE_X0Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.322 r  O_OBUF[11]_inst_i_9/O
                         net (fo=3, routed)           0.417     2.738    O_OBUF[11]_inst_i_9_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X1Y64          LUT5 (Prop_lut5_I3_O)        0.043     2.781 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.336     3.117    O_OBUF[11]_inst_i_5_n_0
    SLICE_X0Y64                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X0Y64          LUT3 (Prop_lut3_I0_O)        0.051     3.168 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.190     4.359    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.917     6.276 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.276    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.147ns  (logic 2.587ns (42.075%)  route 3.561ns (57.925%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.078     1.674    A_IBUF[2]
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_15/I1
    SLICE_X0Y62          LUT2 (Prop_lut2_I1_O)        0.043     1.717 r  O_OBUF[11]_inst_i_15/O
                         net (fo=1, routed)           0.482     2.199    O_OBUF[11]_inst_i_15_n_0
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_13/I5
    SLICE_X1Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.242 r  O_OBUF[11]_inst_i_13/O
                         net (fo=3, routed)           0.328     2.570    O_OBUF[11]_inst_i_13_n_0
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I1
    SLICE_X1Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.613 r  O_OBUF[8]_inst_i_4/O
                         net (fo=1, routed)           0.493     3.107    O_OBUF[8]_inst_i_4_n_0
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_1/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.150 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.179     4.329    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.147 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.147    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.851ns  (logic 2.623ns (44.827%)  route 3.228ns (55.173%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT3=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          0.968     1.555    A_IBUF[6]
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X1Y62          LUT2 (Prop_lut2_I1_O)        0.048     1.603 f  O_OBUF[7]_inst_i_7/O
                         net (fo=3, routed)           0.620     2.222    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y63                                                       f  O_OBUF[7]_inst_i_2/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.129     2.351 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.387     2.739    O_OBUF[7]_inst_i_2_n_0
    SLICE_X2Y64                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y64          LUT3 (Prop_lut3_I0_O)        0.043     2.782 r  O_OBUF[7]_inst_i_1/O
                         net (fo=3, routed)           1.253     4.035    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.851 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.851    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[3]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.556ns  (logic 2.718ns (48.923%)  route 2.838ns (51.077%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=10, routed)          1.078     1.674    A_IBUF[2]
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X0Y62          LUT3 (Prop_lut3_I1_O)        0.051     1.725 f  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.404     2.129    O_OBUF[8]_inst_i_6_n_0
    SLICE_X0Y63                                                       f  O_OBUF[3]_inst_i_2/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.129     2.258 r  O_OBUF[3]_inst_i_2/O
                         net (fo=3, routed)           0.307     2.565    O_OBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[3]_inst_i_1/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.053     2.618 r  O_OBUF[3]_inst_i_1/O
                         net (fo=1, routed)           1.049     3.667    O_OBUF[3]
    AJ27                                                              r  O_OBUF[3]_inst/I
    AJ27                 OBUF (Prop_obuf_I_O)         1.890     5.556 r  O_OBUF[3]_inst/O
                         net (fo=0)                   0.000     5.556    O[3]
    AJ27                                                              r  O[3] (OUT)
  -------------------------------------------------------------------    -------------------




