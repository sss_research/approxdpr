Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 04:05:52 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_294_evolib.txt -name O
| Design       : mul8_294
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.636ns  (logic 2.982ns (34.535%)  route 5.654ns (65.465%))
  Logic Levels:           11  (IBUF=1 LUT4=6 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.151     1.738    A_IBUF[5]
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_8/I3
    SLICE_X3Y61          LUT4 (Prop_lut4_I3_O)        0.043     1.781 r  O_OBUF[8]_inst_i_8/O
                         net (fo=5, routed)           0.270     2.051    N_882
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.094 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.424     2.517    N_915
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     2.560 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.416     2.976    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_5/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.127     3.103 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.438     3.541    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.584 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.414     3.998    N_1683
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.043     4.041 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.426     4.467    N_1699
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.510 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.323     4.833    N_1965
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X0Y64          LUT4 (Prop_lut4_I2_O)        0.043     4.876 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.454     5.331    N_1983
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y64          LUT4 (Prop_lut4_I0_O)        0.048     5.379 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.339     6.717    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.919     8.636 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.636    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.514ns  (logic 2.916ns (34.251%)  route 5.598ns (65.749%))
  Logic Levels:           11  (IBUF=1 LUT4=4 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.151     1.738    A_IBUF[5]
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_8/I3
    SLICE_X3Y61          LUT4 (Prop_lut4_I3_O)        0.043     1.781 r  O_OBUF[8]_inst_i_8/O
                         net (fo=5, routed)           0.471     2.253    N_882
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_9/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.043     2.296 r  O_OBUF[9]_inst_i_9/O
                         net (fo=3, routed)           0.372     2.667    N_914
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_8/I3
    SLICE_X2Y61          LUT4 (Prop_lut4_I3_O)        0.043     2.710 r  O_OBUF[10]_inst_i_8/O
                         net (fo=4, routed)           0.419     3.129    N_1165
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_8/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.172 r  O_OBUF[11]_inst_i_8/O
                         net (fo=2, routed)           0.224     3.396    N_1198
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_8/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.439 r  O_OBUF[15]_inst_i_8/O
                         net (fo=3, routed)           0.339     3.778    N_1449
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.049     3.827 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.422     4.248    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_7/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.127     4.375 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.431     4.807    N_1733
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_4/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.850 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.339     5.189    N_1749
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_1/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.043     5.232 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.431     6.662    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.514 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.514    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.469ns  (logic 2.900ns (34.245%)  route 5.569ns (65.755%))
  Logic Levels:           11  (IBUF=1 LUT4=4 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.151     1.738    A_IBUF[5]
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_8/I3
    SLICE_X3Y61          LUT4 (Prop_lut4_I3_O)        0.043     1.781 r  O_OBUF[8]_inst_i_8/O
                         net (fo=5, routed)           0.471     2.253    N_882
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_9/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.043     2.296 r  O_OBUF[9]_inst_i_9/O
                         net (fo=3, routed)           0.372     2.667    N_914
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_8/I3
    SLICE_X2Y61          LUT4 (Prop_lut4_I3_O)        0.043     2.710 r  O_OBUF[10]_inst_i_8/O
                         net (fo=4, routed)           0.419     3.129    N_1165
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_8/I1
    SLICE_X3Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.172 r  O_OBUF[11]_inst_i_8/O
                         net (fo=2, routed)           0.224     3.396    N_1198
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_8/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.439 r  O_OBUF[15]_inst_i_8/O
                         net (fo=3, routed)           0.339     3.778    N_1449
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.049     3.827 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.422     4.248    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_7/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.127     4.375 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.431     4.807    N_1733
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_4/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     4.850 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.340     5.190    N_1749
    SLICE_X0Y64                                                       r  O_OBUF[14]_inst_i_1/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     5.233 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.400     6.633    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.469 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.469    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.163ns  (logic 2.954ns (36.189%)  route 5.209ns (63.811%))
  Logic Levels:           10  (IBUF=1 LUT4=5 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.151     1.738    A_IBUF[5]
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_8/I3
    SLICE_X3Y61          LUT4 (Prop_lut4_I3_O)        0.043     1.781 r  O_OBUF[8]_inst_i_8/O
                         net (fo=5, routed)           0.270     2.051    N_882
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.094 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.424     2.517    N_915
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     2.560 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.416     2.976    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_5/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.127     3.103 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.438     3.541    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.584 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.414     3.998    N_1683
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.043     4.041 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.426     4.467    N_1699
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_2/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.510 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.323     4.833    N_1965
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X0Y64          LUT4 (Prop_lut4_I0_O)        0.052     4.885 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.348     6.234    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.930     8.163 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.163    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.999ns  (logic 2.857ns (35.715%)  route 5.142ns (64.285%))
  Logic Levels:           10  (IBUF=1 LUT4=6 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.151     1.738    A_IBUF[5]
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_8/I3
    SLICE_X3Y61          LUT4 (Prop_lut4_I3_O)        0.043     1.781 r  O_OBUF[8]_inst_i_8/O
                         net (fo=5, routed)           0.270     2.051    N_882
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.094 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.424     2.517    N_915
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     2.560 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.416     2.976    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_5/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.127     3.103 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.438     3.541    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.584 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.414     3.998    N_1683
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.043     4.041 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.525     4.567    N_1699
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.610 r  O_OBUF[11]_inst_i_3/O
                         net (fo=1, routed)           0.224     4.834    N_1714
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X0Y63          LUT4 (Prop_lut4_I1_O)        0.043     4.877 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.281     6.158    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.999 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.999    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.463ns  (logic 2.893ns (38.765%)  route 4.570ns (61.235%))
  Logic Levels:           9  (IBUF=1 LUT4=5 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.151     1.738    A_IBUF[5]
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_8/I3
    SLICE_X3Y61          LUT4 (Prop_lut4_I3_O)        0.043     1.781 r  O_OBUF[8]_inst_i_8/O
                         net (fo=5, routed)           0.270     2.051    N_882
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.094 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.424     2.517    N_915
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     2.560 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.416     2.976    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_5/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.127     3.103 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.438     3.541    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.584 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.414     3.998    N_1683
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.050     4.048 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.144     4.192    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y62          LUT4 (Prop_lut4_I1_O)        0.126     4.318 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.314     5.632    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.463 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.463    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.150ns  (logic 2.855ns (39.933%)  route 4.295ns (60.067%))
  Logic Levels:           8  (IBUF=1 LUT4=4 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.151     1.738    A_IBUF[5]
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_8/I3
    SLICE_X3Y61          LUT4 (Prop_lut4_I3_O)        0.043     1.781 r  O_OBUF[8]_inst_i_8/O
                         net (fo=5, routed)           0.270     2.051    N_882
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X2Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.094 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.424     2.517    N_915
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_9/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     2.560 r  O_OBUF[10]_inst_i_9/O
                         net (fo=4, routed)           0.416     2.976    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_5/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.127     3.103 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.438     3.541    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_3/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.051     3.592 r  O_OBUF[9]_inst_i_3/O
                         net (fo=1, routed)           0.290     3.882    N_1682
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.129     4.011 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.307     5.318    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.150 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.150    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.769ns  (logic 2.715ns (40.116%)  route 4.053ns (59.884%))
  Logic Levels:           7  (IBUF=1 LUT4=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.151     1.738    A_IBUF[5]
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_8/I3
    SLICE_X3Y61          LUT4 (Prop_lut4_I3_O)        0.043     1.781 r  O_OBUF[8]_inst_i_8/O
                         net (fo=5, routed)           0.464     2.246    N_882
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_7/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.289 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.343     2.631    N_1399
    SLICE_X3Y61                                                       r  O_OBUF[9]_inst_i_5/I0
    SLICE_X3Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.674 r  O_OBUF[9]_inst_i_5/O
                         net (fo=3, routed)           0.408     3.083    N_1414
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.051     3.134 r  O_OBUF[8]_inst_i_3/O
                         net (fo=1, routed)           0.432     3.566    N_1664
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.129     3.695 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.255     4.950    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.769 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.769    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.613ns  (logic 2.576ns (45.892%)  route 3.037ns (54.108%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.151     1.738    A_IBUF[5]
    SLICE_X3Y61                                                       r  O_OBUF[8]_inst_i_8/I3
    SLICE_X3Y61          LUT4 (Prop_lut4_I3_O)        0.043     1.781 r  O_OBUF[8]_inst_i_8/O
                         net (fo=5, routed)           0.232     2.013    N_882
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_4/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.043     2.056 r  O_OBUF[8]_inst_i_4/O
                         net (fo=3, routed)           0.303     2.359    N_1148
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_2/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     2.402 r  O_OBUF[7]_inst_i_2/O
                         net (fo=1, routed)           0.096     2.498    N_1648
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X0Y61          LUT3 (Prop_lut3_I2_O)        0.043     2.541 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.255     3.797    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.613 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.613    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.852ns  (logic 2.387ns (49.194%)  route 2.465ns (50.806%))
  Logic Levels:           3  (IBUF=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=4, routed)           1.186     1.707    A_IBUF[0]
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_1/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     1.750 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.279     3.029    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     4.852 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     4.852    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




