Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 17:04:14 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_070_evolib.txt -name O
| Design       : mul8_070
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.457ns  (logic 3.098ns (36.629%)  route 5.359ns (63.371%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT5=4 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.469     2.065    A_IBUF[2]
    SLICE_X2Y60                                                       r  O_OBUF[11]_inst_i_19/I4
    SLICE_X2Y60          LUT6 (Prop_lut6_I4_O)        0.043     2.108 r  O_OBUF[11]_inst_i_19/O
                         net (fo=2, routed)           0.334     2.442    N_509
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_24/I0
    SLICE_X2Y61          LUT5 (Prop_lut5_I0_O)        0.049     2.491 r  O_OBUF[11]_inst_i_24/O
                         net (fo=2, routed)           0.439     2.930    N_608
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_14/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.132     3.062 r  O_OBUF[11]_inst_i_14/O
                         net (fo=4, routed)           0.420     3.482    N_684
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X3Y62          LUT3 (Prop_lut3_I0_O)        0.140     3.622 r  O_OBUF[11]_inst_i_6/O
                         net (fo=4, routed)           0.422     4.044    N_753
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_15/I0
    SLICE_X3Y63          LUT5 (Prop_lut5_I0_O)        0.136     4.180 r  O_OBUF[15]_inst_i_15/O
                         net (fo=3, routed)           0.521     4.701    N_1378
    SLICE_X4Y63                                                       r  O_OBUF[13]_inst_i_4/I2
    SLICE_X4Y63          LUT3 (Prop_lut3_I2_O)        0.126     4.827 r  O_OBUF[13]_inst_i_4/O
                         net (fo=1, routed)           0.428     5.255    O_OBUF[13]_inst_i_4_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_1/I3
    SLICE_X2Y63          LUT5 (Prop_lut5_I3_O)        0.043     5.298 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.326     6.624    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.457 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.457    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.334ns  (logic 3.005ns (36.063%)  route 5.328ns (63.937%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT5=4 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.429     2.024    A_IBUF[2]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_27/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.067 r  O_OBUF[11]_inst_i_27/O
                         net (fo=2, routed)           0.312     2.380    N_503
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_26/I0
    SLICE_X2Y62          LUT5 (Prop_lut5_I0_O)        0.050     2.430 r  O_OBUF[11]_inst_i_26/O
                         net (fo=2, routed)           0.302     2.732    N_602
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.132     2.864 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     3.393    N_676
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_4/I0
    SLICE_X2Y62          LUT3 (Prop_lut3_I0_O)        0.126     3.519 r  O_OBUF[8]_inst_i_4/O
                         net (fo=6, routed)           0.541     4.060    N_746
    SLICE_X2Y64                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X2Y64          LUT5 (Prop_lut5_I2_O)        0.048     4.108 r  O_OBUF[13]_inst_i_2/O
                         net (fo=4, routed)           0.502     4.610    N_1060
    SLICE_X3Y63                                                       r  O_OBUF[14]_inst_i_6/I3
    SLICE_X3Y63          LUT5 (Prop_lut5_I3_O)        0.132     4.742 r  O_OBUF[14]_inst_i_6/O
                         net (fo=1, routed)           0.310     5.052    O_OBUF[14]_inst_i_6_n_0
    SLICE_X4Y63                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X4Y63          LUT6 (Prop_lut6_I5_O)        0.043     5.095 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.403     6.498    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.334 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.334    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.300ns  (logic 3.116ns (37.543%)  route 5.184ns (62.457%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT5=4 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.469     2.065    A_IBUF[2]
    SLICE_X2Y60                                                       r  O_OBUF[11]_inst_i_19/I4
    SLICE_X2Y60          LUT6 (Prop_lut6_I4_O)        0.043     2.108 r  O_OBUF[11]_inst_i_19/O
                         net (fo=2, routed)           0.334     2.442    N_509
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_24/I0
    SLICE_X2Y61          LUT5 (Prop_lut5_I0_O)        0.049     2.491 r  O_OBUF[11]_inst_i_24/O
                         net (fo=2, routed)           0.439     2.930    N_608
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_14/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.132     3.062 r  O_OBUF[11]_inst_i_14/O
                         net (fo=4, routed)           0.420     3.482    N_684
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X3Y62          LUT3 (Prop_lut3_I0_O)        0.140     3.622 r  O_OBUF[11]_inst_i_6/O
                         net (fo=4, routed)           0.422     4.044    N_753
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_15/I0
    SLICE_X3Y63          LUT5 (Prop_lut5_I0_O)        0.136     4.180 r  O_OBUF[15]_inst_i_15/O
                         net (fo=3, routed)           0.506     4.686    N_1378
    SLICE_X4Y63                                                       r  O_OBUF[15]_inst_i_6/I4
    SLICE_X4Y63          LUT6 (Prop_lut6_I4_O)        0.126     4.812 r  O_OBUF[15]_inst_i_6/O
                         net (fo=1, routed)           0.178     4.991    O_OBUF[15]_inst_i_6_n_0
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X2Y63          LUT5 (Prop_lut5_I4_O)        0.043     5.034 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.415     6.449    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.300 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.300    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.176ns  (logic 3.014ns (36.870%)  route 5.162ns (63.130%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.469     2.065    A_IBUF[2]
    SLICE_X2Y60                                                       r  O_OBUF[11]_inst_i_19/I4
    SLICE_X2Y60          LUT6 (Prop_lut6_I4_O)        0.043     2.108 r  O_OBUF[11]_inst_i_19/O
                         net (fo=2, routed)           0.334     2.442    N_509
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_24/I0
    SLICE_X2Y61          LUT5 (Prop_lut5_I0_O)        0.049     2.491 r  O_OBUF[11]_inst_i_24/O
                         net (fo=2, routed)           0.439     2.930    N_608
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_14/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.132     3.062 r  O_OBUF[11]_inst_i_14/O
                         net (fo=4, routed)           0.253     3.316    N_684
    SLICE_X3Y61                                                       r  O_OBUF[9]_inst_i_2/I5
    SLICE_X3Y61          LUT6 (Prop_lut6_I5_O)        0.132     3.448 r  O_OBUF[9]_inst_i_2/O
                         net (fo=6, routed)           0.614     4.062    N_828
    SLICE_X3Y64                                                       r  O_OBUF[15]_inst_i_12/I2
    SLICE_X3Y64          LUT5 (Prop_lut5_I2_O)        0.043     4.105 r  O_OBUF[15]_inst_i_12/O
                         net (fo=3, routed)           0.327     4.432    N_1146
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X3Y63          LUT4 (Prop_lut4_I3_O)        0.049     4.481 r  O_OBUF[12]_inst_i_4/O
                         net (fo=1, routed)           0.327     4.808    O_OBUF[12]_inst_i_4_n_0
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.129     4.937 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.397     6.334    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.176 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.176    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.797ns  (logic 2.968ns (38.064%)  route 4.829ns (61.936%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.429     2.024    A_IBUF[2]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_27/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.067 r  O_OBUF[11]_inst_i_27/O
                         net (fo=2, routed)           0.312     2.380    N_503
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_26/I0
    SLICE_X2Y62          LUT5 (Prop_lut5_I0_O)        0.050     2.430 r  O_OBUF[11]_inst_i_26/O
                         net (fo=2, routed)           0.302     2.732    N_602
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.132     2.864 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     3.393    N_676
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X2Y62          LUT3 (Prop_lut3_I0_O)        0.131     3.524 r  O_OBUF[12]_inst_i_12/O
                         net (fo=3, routed)           0.618     4.142    N_747
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_7/I3
    SLICE_X2Y64          LUT5 (Prop_lut5_I3_O)        0.132     4.274 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.319     4.593    N_1022
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_1/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.043     4.636 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.321     5.956    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.797 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.797    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.772ns  (logic 2.958ns (38.063%)  route 4.814ns (61.937%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT3=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.429     2.024    A_IBUF[2]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_27/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.067 r  O_OBUF[11]_inst_i_27/O
                         net (fo=2, routed)           0.312     2.380    N_503
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_26/I0
    SLICE_X2Y62          LUT5 (Prop_lut5_I0_O)        0.050     2.430 r  O_OBUF[11]_inst_i_26/O
                         net (fo=2, routed)           0.302     2.732    N_602
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.132     2.864 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     3.393    N_676
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X2Y62          LUT3 (Prop_lut3_I0_O)        0.131     3.524 r  O_OBUF[12]_inst_i_12/O
                         net (fo=3, routed)           0.618     4.142    N_747
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_7/I3
    SLICE_X2Y64          LUT5 (Prop_lut5_I3_O)        0.132     4.274 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.361     4.635    N_1022
    SLICE_X2Y64                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X2Y64          LUT2 (Prop_lut2_I1_O)        0.043     4.678 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.263     5.941    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.772 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.772    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.559ns  (logic 2.957ns (39.121%)  route 4.602ns (60.879%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.429     2.024    A_IBUF[2]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_27/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.067 r  O_OBUF[11]_inst_i_27/O
                         net (fo=2, routed)           0.312     2.380    N_503
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_26/I0
    SLICE_X2Y62          LUT5 (Prop_lut5_I0_O)        0.050     2.430 r  O_OBUF[11]_inst_i_26/O
                         net (fo=2, routed)           0.302     2.732    N_602
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.132     2.864 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     3.393    N_676
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_4/I0
    SLICE_X2Y62          LUT3 (Prop_lut3_I0_O)        0.126     3.519 r  O_OBUF[8]_inst_i_4/O
                         net (fo=6, routed)           0.541     4.060    N_746
    SLICE_X2Y64                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X2Y64          LUT5 (Prop_lut5_I2_O)        0.048     4.108 r  O_OBUF[13]_inst_i_2/O
                         net (fo=4, routed)           0.214     4.322    N_1060
    SLICE_X3Y64                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X3Y64          LUT4 (Prop_lut4_I2_O)        0.132     4.454 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.274     5.729    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.559 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.559    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.194ns  (logic 2.890ns (40.175%)  route 4.304ns (59.825%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.429     2.024    A_IBUF[2]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_27/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.067 r  O_OBUF[11]_inst_i_27/O
                         net (fo=2, routed)           0.312     2.380    N_503
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_26/I0
    SLICE_X2Y62          LUT5 (Prop_lut5_I0_O)        0.050     2.430 r  O_OBUF[11]_inst_i_26/O
                         net (fo=2, routed)           0.302     2.732    N_602
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.132     2.864 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     3.393    N_676
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_4/I0
    SLICE_X2Y62          LUT3 (Prop_lut3_I0_O)        0.126     3.519 r  O_OBUF[8]_inst_i_4/O
                         net (fo=6, routed)           0.543     4.062    N_746
    SLICE_X2Y64                                                       r  O_OBUF[7]_inst_i_1/I1
    SLICE_X2Y64          LUT3 (Prop_lut3_I1_O)        0.043     4.105 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.188     5.294    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.900     7.194 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     7.194    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.102ns  (logic 2.809ns (39.546%)  route 4.293ns (60.454%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.429     2.024    A_IBUF[2]
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_27/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.067 r  O_OBUF[11]_inst_i_27/O
                         net (fo=2, routed)           0.312     2.380    N_503
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_26/I0
    SLICE_X2Y62          LUT5 (Prop_lut5_I0_O)        0.050     2.430 r  O_OBUF[11]_inst_i_26/O
                         net (fo=2, routed)           0.302     2.732    N_602
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_6/I4
    SLICE_X2Y61          LUT5 (Prop_lut5_I4_O)        0.132     2.864 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.529     3.393    N_676
    SLICE_X2Y62                                                       r  O_OBUF[8]_inst_i_4/I0
    SLICE_X2Y62          LUT3 (Prop_lut3_I0_O)        0.126     3.519 r  O_OBUF[8]_inst_i_4/O
                         net (fo=6, routed)           0.541     4.060    N_746
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_1/I2
    SLICE_X2Y64          LUT4 (Prop_lut4_I2_O)        0.043     4.103 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.180     5.283    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.102 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.102    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.506ns  (logic 2.633ns (40.473%)  route 3.873ns (59.527%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.359     1.955    A_IBUF[2]
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_9/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.043     1.998 r  O_OBUF[6]_inst_i_9/O
                         net (fo=2, routed)           0.343     2.341    N_453
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_3/I1
    SLICE_X0Y61          LUT3 (Prop_lut3_I1_O)        0.043     2.384 r  O_OBUF[6]_inst_i_3/O
                         net (fo=4, routed)           0.431     2.815    N_564
    SLICE_X0Y63                                                       r  O_OBUF[5]_inst_i_3/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.858 r  O_OBUF[5]_inst_i_3/O
                         net (fo=2, routed)           0.416     3.274    N_664
    SLICE_X0Y64                                                       r  O_OBUF[6]_inst_i_7/I1
    SLICE_X0Y64          LUT6 (Prop_lut6_I1_O)        0.043     3.317 r  O_OBUF[6]_inst_i_7/O
                         net (fo=1, routed)           0.188     3.505    O_OBUF[6]_inst_i_7_n_0
    SLICE_X1Y64                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X1Y64          LUT6 (Prop_lut6_I5_O)        0.043     3.548 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.136     4.684    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.506 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.506    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




