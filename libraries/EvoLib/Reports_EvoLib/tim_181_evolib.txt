Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 22:35:28 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_181_evolib.txt -name O
| Design       : mul8_181
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.555ns  (logic 3.233ns (33.835%)  route 6.322ns (66.165%))
  Logic Levels:           12  (IBUF=1 LUT2=1 LUT4=4 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.472     2.940    N_883
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.983 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.334     3.317    N_915
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.051     3.368 r  O_OBUF[10]_inst_i_12/O
                         net (fo=3, routed)           0.446     3.813    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_14/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.129     3.942 r  O_OBUF[10]_inst_i_14/O
                         net (fo=2, routed)           0.471     4.413    N_1198
    SLICE_X5Y64                                                       r  O_OBUF[15]_inst_i_9/I5
    SLICE_X5Y64          LUT6 (Prop_lut6_I5_O)        0.043     4.456 r  O_OBUF[15]_inst_i_9/O
                         net (fo=3, routed)           0.502     4.959    N_1449
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_6/I3
    SLICE_X1Y65          LUT4 (Prop_lut4_I3_O)        0.051     5.010 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.500     5.509    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.129     5.638 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.253     5.892    N_1714
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     5.935 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.306     6.241    N_1983
    SLICE_X0Y65                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y65          LUT4 (Prop_lut4_I0_O)        0.051     6.292 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.344     7.636    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.919     9.555 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     9.555    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.548ns  (logic 3.157ns (33.070%)  route 6.390ns (66.930%))
  Logic Levels:           12  (IBUF=1 LUT2=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.472     2.940    N_883
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.983 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.334     3.317    N_915
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.051     3.368 r  O_OBUF[10]_inst_i_12/O
                         net (fo=3, routed)           0.446     3.813    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_14/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.129     3.942 r  O_OBUF[10]_inst_i_14/O
                         net (fo=2, routed)           0.471     4.413    N_1198
    SLICE_X5Y64                                                       r  O_OBUF[15]_inst_i_9/I5
    SLICE_X5Y64          LUT6 (Prop_lut6_I5_O)        0.043     4.456 r  O_OBUF[15]_inst_i_9/O
                         net (fo=3, routed)           0.502     4.959    N_1449
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_6/I3
    SLICE_X1Y65          LUT4 (Prop_lut4_I3_O)        0.051     5.010 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.500     5.509    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.129     5.638 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.253     5.892    N_1714
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_2/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     5.935 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.304     6.238    N_1983
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     6.281 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.415     7.697    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     9.548 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     9.548    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.376ns  (logic 3.142ns (33.508%)  route 6.234ns (66.492%))
  Logic Levels:           12  (IBUF=1 LUT2=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.472     2.940    N_883
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.983 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.334     3.317    N_915
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.051     3.368 r  O_OBUF[10]_inst_i_12/O
                         net (fo=3, routed)           0.376     3.744    N_932
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X3Y63          LUT4 (Prop_lut4_I1_O)        0.129     3.873 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.439     4.311    N_1182
    SLICE_X4Y64                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     4.354 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.423     4.778    N_1683
    SLICE_X5Y64                                                       r  O_OBUF[12]_inst_i_5/I2
    SLICE_X5Y64          LUT4 (Prop_lut4_I2_O)        0.051     4.829 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.533     5.361    N_1699
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_7/I3
    SLICE_X1Y65          LUT6 (Prop_lut6_I3_O)        0.129     5.490 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.256     5.746    N_1733
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X0Y65          LUT6 (Prop_lut6_I4_O)        0.043     5.789 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.350     6.140    N_1748
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X0Y65          LUT6 (Prop_lut6_I2_O)        0.043     6.183 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.358     7.541    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     9.376 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     9.376    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.175ns  (logic 3.104ns (33.836%)  route 6.070ns (66.164%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=4 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.472     2.940    N_883
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.983 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.334     3.317    N_915
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.051     3.368 r  O_OBUF[10]_inst_i_12/O
                         net (fo=3, routed)           0.446     3.813    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_14/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.129     3.942 r  O_OBUF[10]_inst_i_14/O
                         net (fo=2, routed)           0.471     4.413    N_1198
    SLICE_X5Y64                                                       r  O_OBUF[15]_inst_i_9/I5
    SLICE_X5Y64          LUT6 (Prop_lut6_I5_O)        0.043     4.456 r  O_OBUF[15]_inst_i_9/O
                         net (fo=3, routed)           0.502     4.959    N_1449
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_6/I3
    SLICE_X1Y65          LUT4 (Prop_lut4_I3_O)        0.051     5.010 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.500     5.509    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.129     5.638 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.382     6.020    N_1714
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X0Y64          LUT4 (Prop_lut4_I1_O)        0.043     6.063 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.271     7.333    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     9.175 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     9.175    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.056ns  (logic 3.105ns (34.284%)  route 5.951ns (65.716%))
  Logic Levels:           11  (IBUF=1 LUT2=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.472     2.940    N_883
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.983 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.334     3.317    N_915
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.051     3.368 r  O_OBUF[10]_inst_i_12/O
                         net (fo=3, routed)           0.446     3.813    N_932
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_14/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.129     3.942 r  O_OBUF[10]_inst_i_14/O
                         net (fo=2, routed)           0.471     4.413    N_1198
    SLICE_X5Y64                                                       r  O_OBUF[15]_inst_i_9/I5
    SLICE_X5Y64          LUT6 (Prop_lut6_I5_O)        0.043     4.456 r  O_OBUF[15]_inst_i_9/O
                         net (fo=3, routed)           0.502     4.959    N_1449
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_6/I3
    SLICE_X1Y65          LUT4 (Prop_lut4_I3_O)        0.051     5.010 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.500     5.509    N_1464
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.129     5.638 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.251     5.889    N_1714
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_1/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     5.932 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.282     7.214    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     9.056 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     9.056    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.112ns  (logic 2.956ns (36.443%)  route 5.156ns (63.557%))
  Logic Levels:           10  (IBUF=1 LUT2=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.472     2.940    N_883
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_13/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.983 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.334     3.317    N_915
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_12/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.051     3.368 r  O_OBUF[10]_inst_i_12/O
                         net (fo=3, routed)           0.376     3.744    N_932
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X3Y63          LUT4 (Prop_lut4_I1_O)        0.129     3.873 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.439     4.311    N_1182
    SLICE_X4Y64                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     4.354 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.423     4.778    N_1683
    SLICE_X5Y64                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X5Y64          LUT4 (Prop_lut4_I0_O)        0.043     4.821 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.226     5.046    N_1698
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X1Y64          LUT6 (Prop_lut6_I4_O)        0.043     5.089 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.193     6.282    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     8.112 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     8.112    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.030ns  (logic 2.919ns (36.345%)  route 5.112ns (63.655%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.344     2.811    N_883
    SLICE_X2Y63                                                       r  O_OBUF[7]_inst_i_11/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.854 r  O_OBUF[7]_inst_i_11/O
                         net (fo=3, routed)           0.421     3.275    N_1148
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_5/I0
    SLICE_X2Y64          LUT4 (Prop_lut4_I0_O)        0.043     3.318 r  O_OBUF[8]_inst_i_5/O
                         net (fo=3, routed)           0.411     3.730    N_1399
    SLICE_X3Y63                                                       r  O_OBUF[10]_inst_i_6/I2
    SLICE_X3Y63          LUT4 (Prop_lut4_I2_O)        0.053     3.783 r  O_OBUF[10]_inst_i_6/O
                         net (fo=4, routed)           0.446     4.228    N_1415
    SLICE_X4Y64                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X4Y64          LUT6 (Prop_lut6_I1_O)        0.131     4.359 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.518     4.877    N_1682
    SLICE_X1Y64                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X1Y64          LUT4 (Prop_lut4_I1_O)        0.043     4.920 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.279     6.199    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     8.030 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     8.030    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.190ns  (logic 2.765ns (38.457%)  route 4.425ns (61.543%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.344     2.811    N_883
    SLICE_X2Y63                                                       r  O_OBUF[7]_inst_i_11/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.854 r  O_OBUF[7]_inst_i_11/O
                         net (fo=3, routed)           0.421     3.275    N_1148
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_5/I0
    SLICE_X2Y64          LUT4 (Prop_lut4_I0_O)        0.043     3.318 r  O_OBUF[8]_inst_i_5/O
                         net (fo=3, routed)           0.407     3.725    N_1399
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     3.768 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.424     4.192    N_1664
    SLICE_X1Y66                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X1Y66          LUT4 (Prop_lut4_I1_O)        0.043     4.235 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.136     5.371    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.190 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.190    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.693ns  (logic 2.804ns (41.884%)  route 3.890ns (58.116%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.344     2.811    N_883
    SLICE_X2Y63                                                       r  O_OBUF[7]_inst_i_11/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.854 r  O_OBUF[7]_inst_i_11/O
                         net (fo=3, routed)           0.421     3.275    N_1148
    SLICE_X2Y64                                                       r  O_OBUF[7]_inst_i_3/I3
    SLICE_X2Y64          LUT4 (Prop_lut4_I3_O)        0.043     3.318 r  O_OBUF[7]_inst_i_3/O
                         net (fo=2, routed)           0.297     3.615    N_1398
    SLICE_X1Y66                                                       r  O_OBUF[7]_inst_i_1/I1
    SLICE_X1Y66          LUT6 (Prop_lut6_I1_O)        0.127     3.742 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.135     4.877    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.693 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.693    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[4]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.082ns  (logic 2.662ns (43.771%)  route 3.420ns (56.229%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=14, routed)          1.273     1.867    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X4Y64          LUT2 (Prop_lut2_I1_O)        0.051     1.918 f  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.421     2.339    O_OBUF[7]_inst_i_6_n_0
    SLICE_X3Y63                                                       f  O_OBUF[4]_inst_i_5/I0
    SLICE_X3Y63          LUT6 (Prop_lut6_I0_O)        0.129     2.468 r  O_OBUF[4]_inst_i_5/O
                         net (fo=4, routed)           0.356     2.824    N_883
    SLICE_X2Y63                                                       r  O_OBUF[4]_inst_i_2/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.043     2.867 r  O_OBUF[4]_inst_i_2/O
                         net (fo=2, routed)           0.229     3.096    N_1149
    SLICE_X2Y63                                                       r  O_OBUF[4]_inst_i_1/I2
    SLICE_X2Y63          LUT4 (Prop_lut4_I2_O)        0.043     3.139 r  O_OBUF[4]_inst_i_1/O
                         net (fo=4, routed)           1.140     4.280    O_OBUF[4]
    AJ26                                                              r  O_OBUF[4]_inst/I
    AJ26                 OBUF (Prop_obuf_I_O)         1.802     6.082 r  O_OBUF[4]_inst/O
                         net (fo=0)                   0.000     6.082    O[4]
    AJ26                                                              r  O[4] (OUT)
  -------------------------------------------------------------------    -------------------




