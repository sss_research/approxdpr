Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 23:59:38 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_210_evolib.txt -name O
| Design       : mul8_210
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.198ns  (logic 2.953ns (36.021%)  route 5.245ns (63.979%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT5=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=14, routed)          1.240     1.845    B_IBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[5]_inst_i_3/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.053     1.898 r  O_OBUF[5]_inst_i_3/O
                         net (fo=3, routed)           0.424     2.321    O_OBUF[5]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_15/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.131     2.452 r  O_OBUF[12]_inst_i_15/O
                         net (fo=1, routed)           0.505     2.957    O_OBUF[12]_inst_i_15_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_13/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.000 r  O_OBUF[12]_inst_i_13/O
                         net (fo=1, routed)           0.502     3.502    O_OBUF[12]_inst_i_13_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_10/I0
    SLICE_X2Y65          LUT5 (Prop_lut5_I0_O)        0.043     3.545 r  O_OBUF[12]_inst_i_10/O
                         net (fo=1, routed)           0.507     4.051    O_OBUF[12]_inst_i_10_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_4/I4
    SLICE_X2Y65          LUT6 (Prop_lut6_I4_O)        0.043     4.094 r  O_OBUF[12]_inst_i_4/O
                         net (fo=3, routed)           0.225     4.319    O_OBUF[12]_inst_i_4_n_0
    SLICE_X1Y66                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X1Y66          LUT6 (Prop_lut6_I2_O)        0.043     4.362 r  O_OBUF[15]_inst_i_2/O
                         net (fo=1, routed)           0.434     4.796    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y66                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y66          LUT5 (Prop_lut5_I4_O)        0.053     4.849 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.410     6.259    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.939     8.198 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.198    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.851ns  (logic 2.837ns (36.128%)  route 5.015ns (63.872%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=14, routed)          1.240     1.845    B_IBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[5]_inst_i_3/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.053     1.898 r  O_OBUF[5]_inst_i_3/O
                         net (fo=3, routed)           0.424     2.321    O_OBUF[5]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_15/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.131     2.452 r  O_OBUF[12]_inst_i_15/O
                         net (fo=1, routed)           0.505     2.957    O_OBUF[12]_inst_i_15_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_13/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.000 r  O_OBUF[12]_inst_i_13/O
                         net (fo=1, routed)           0.502     3.502    O_OBUF[12]_inst_i_13_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_10/I0
    SLICE_X2Y65          LUT5 (Prop_lut5_I0_O)        0.043     3.545 r  O_OBUF[12]_inst_i_10/O
                         net (fo=1, routed)           0.507     4.051    O_OBUF[12]_inst_i_10_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_4/I4
    SLICE_X2Y65          LUT6 (Prop_lut6_I4_O)        0.043     4.094 r  O_OBUF[12]_inst_i_4/O
                         net (fo=3, routed)           0.215     4.309    O_OBUF[12]_inst_i_4_n_0
    SLICE_X1Y66                                                       r  O_OBUF[14]_inst_i_5/I1
    SLICE_X1Y66          LUT3 (Prop_lut3_I1_O)        0.043     4.352 r  O_OBUF[14]_inst_i_5/O
                         net (fo=2, routed)           0.349     4.701    O_OBUF[14]_inst_i_5_n_0
    SLICE_X1Y66                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X1Y66          LUT3 (Prop_lut3_I2_O)        0.043     4.744 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.274     6.019    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.851 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.851    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.657ns  (logic 2.839ns (37.083%)  route 4.818ns (62.917%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=14, routed)          1.240     1.845    B_IBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[5]_inst_i_3/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.053     1.898 r  O_OBUF[5]_inst_i_3/O
                         net (fo=3, routed)           0.424     2.321    O_OBUF[5]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_15/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.131     2.452 r  O_OBUF[12]_inst_i_15/O
                         net (fo=1, routed)           0.505     2.957    O_OBUF[12]_inst_i_15_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_13/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.000 r  O_OBUF[12]_inst_i_13/O
                         net (fo=1, routed)           0.502     3.502    O_OBUF[12]_inst_i_13_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_10/I0
    SLICE_X2Y65          LUT5 (Prop_lut5_I0_O)        0.043     3.545 r  O_OBUF[12]_inst_i_10/O
                         net (fo=1, routed)           0.507     4.051    O_OBUF[12]_inst_i_10_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_4/I4
    SLICE_X2Y65          LUT6 (Prop_lut6_I4_O)        0.043     4.094 r  O_OBUF[12]_inst_i_4/O
                         net (fo=3, routed)           0.215     4.309    O_OBUF[12]_inst_i_4_n_0
    SLICE_X1Y66                                                       r  O_OBUF[14]_inst_i_5/I1
    SLICE_X1Y66          LUT3 (Prop_lut3_I1_O)        0.043     4.352 r  O_OBUF[14]_inst_i_5/O
                         net (fo=2, routed)           0.144     4.496    O_OBUF[14]_inst_i_5_n_0
    SLICE_X1Y66                                                       r  O_OBUF[14]_inst_i_1/I3
    SLICE_X1Y66          LUT6 (Prop_lut6_I3_O)        0.043     4.539 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.282     5.821    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.657 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.657    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.555ns  (logic 2.894ns (38.314%)  route 4.660ns (61.686%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 r  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              r  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 r  B_IBUF[1]_inst/O
                         net (fo=14, routed)          1.240     1.845    B_IBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[5]_inst_i_3/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.053     1.898 r  O_OBUF[5]_inst_i_3/O
                         net (fo=3, routed)           0.424     2.321    O_OBUF[5]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_15/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.131     2.452 r  O_OBUF[12]_inst_i_15/O
                         net (fo=1, routed)           0.505     2.957    O_OBUF[12]_inst_i_15_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_13/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.000 r  O_OBUF[12]_inst_i_13/O
                         net (fo=1, routed)           0.502     3.502    O_OBUF[12]_inst_i_13_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_10/I0
    SLICE_X2Y65          LUT5 (Prop_lut5_I0_O)        0.043     3.545 r  O_OBUF[12]_inst_i_10/O
                         net (fo=1, routed)           0.507     4.051    O_OBUF[12]_inst_i_10_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_4/I4
    SLICE_X2Y65          LUT6 (Prop_lut6_I4_O)        0.043     4.094 r  O_OBUF[12]_inst_i_4/O
                         net (fo=3, routed)           0.215     4.309    O_OBUF[12]_inst_i_4_n_0
    SLICE_X1Y66                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X1Y66          LUT3 (Prop_lut3_I2_O)        0.049     4.358 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.269     5.627    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     7.555 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.555    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.275ns  (logic 2.779ns (38.197%)  route 4.496ns (61.803%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              f  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[2]_inst/O
                         net (fo=14, routed)          1.188     1.784    A_IBUF[2]
    SLICE_X1Y62                                                       f  O_OBUF[8]_inst_i_6/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     1.827 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.409     2.236    O_OBUF[8]_inst_i_6_n_0
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_4/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.279 f  O_OBUF[7]_inst_i_4/O
                         net (fo=3, routed)           0.525     2.804    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y65                                                       f  O_OBUF[8]_inst_i_3/I0
    SLICE_X1Y65          LUT3 (Prop_lut3_I0_O)        0.043     2.847 r  O_OBUF[8]_inst_i_3/O
                         net (fo=4, routed)           0.531     3.378    O_OBUF[8]_inst_i_3_n_0
    SLICE_X2Y65                                                       r  O_OBUF[10]_inst_i_4/I2
    SLICE_X2Y65          LUT4 (Prop_lut4_I2_O)        0.043     3.421 f  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.348     3.770    O_OBUF[10]_inst_i_4_n_0
    SLICE_X3Y65                                                       f  O_OBUF[11]_inst_i_6/I2
    SLICE_X3Y65          LUT6 (Prop_lut6_I2_O)        0.127     3.897 r  O_OBUF[11]_inst_i_6/O
                         net (fo=1, routed)           0.224     4.121    O_OBUF[11]_inst_i_6_n_0
    SLICE_X3Y65                                                       r  O_OBUF[11]_inst_i_1/I4
    SLICE_X3Y65          LUT5 (Prop_lut5_I4_O)        0.043     4.164 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.270     5.434    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.275 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.275    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.033ns  (logic 2.725ns (38.746%)  route 4.308ns (61.254%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              f  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[2]_inst/O
                         net (fo=14, routed)          1.188     1.784    A_IBUF[2]
    SLICE_X1Y62                                                       f  O_OBUF[8]_inst_i_6/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     1.827 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.409     2.236    O_OBUF[8]_inst_i_6_n_0
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_4/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.279 f  O_OBUF[7]_inst_i_4/O
                         net (fo=3, routed)           0.525     2.804    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y65                                                       f  O_OBUF[8]_inst_i_3/I0
    SLICE_X1Y65          LUT3 (Prop_lut3_I0_O)        0.043     2.847 r  O_OBUF[8]_inst_i_3/O
                         net (fo=4, routed)           0.531     3.378    O_OBUF[8]_inst_i_3_n_0
    SLICE_X2Y65                                                       r  O_OBUF[10]_inst_i_4/I2
    SLICE_X2Y65          LUT4 (Prop_lut4_I2_O)        0.043     3.421 r  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.448     3.870    O_OBUF[10]_inst_i_4_n_0
    SLICE_X3Y65                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X3Y65          LUT6 (Prop_lut6_I2_O)        0.127     3.997 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.206     5.203    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.033 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.033    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.931ns  (logic 2.726ns (39.333%)  route 4.205ns (60.667%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              f  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[2]_inst/O
                         net (fo=14, routed)          1.188     1.784    A_IBUF[2]
    SLICE_X1Y62                                                       f  O_OBUF[8]_inst_i_6/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     1.827 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.409     2.236    O_OBUF[8]_inst_i_6_n_0
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_4/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.279 f  O_OBUF[7]_inst_i_4/O
                         net (fo=3, routed)           0.525     2.804    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y65                                                       f  O_OBUF[8]_inst_i_3/I0
    SLICE_X1Y65          LUT3 (Prop_lut3_I0_O)        0.043     2.847 r  O_OBUF[8]_inst_i_3/O
                         net (fo=4, routed)           0.531     3.378    O_OBUF[8]_inst_i_3_n_0
    SLICE_X2Y65                                                       r  O_OBUF[10]_inst_i_4/I2
    SLICE_X2Y65          LUT4 (Prop_lut4_I2_O)        0.043     3.421 r  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.350     3.772    O_OBUF[10]_inst_i_4_n_0
    SLICE_X3Y65                                                       r  O_OBUF[9]_inst_i_1/I4
    SLICE_X3Y65          LUT5 (Prop_lut5_I4_O)        0.127     3.899 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.201     5.100    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.931 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.931    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.404ns  (logic 2.587ns (40.389%)  route 3.818ns (59.611%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 f  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              f  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 f  A_IBUF[2]_inst/O
                         net (fo=14, routed)          1.188     1.784    A_IBUF[2]
    SLICE_X1Y62                                                       f  O_OBUF[8]_inst_i_6/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     1.827 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.409     2.236    O_OBUF[8]_inst_i_6_n_0
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_4/I4
    SLICE_X1Y63          LUT5 (Prop_lut5_I4_O)        0.043     2.279 f  O_OBUF[7]_inst_i_4/O
                         net (fo=3, routed)           0.525     2.804    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y65                                                       f  O_OBUF[8]_inst_i_3/I0
    SLICE_X1Y65          LUT3 (Prop_lut3_I0_O)        0.043     2.847 r  O_OBUF[8]_inst_i_3/O
                         net (fo=4, routed)           0.527     3.374    O_OBUF[8]_inst_i_3_n_0
    SLICE_X2Y65                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X2Y65          LUT4 (Prop_lut4_I1_O)        0.043     3.417 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.168     4.585    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.404 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.404    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.139ns  (logic 2.730ns (44.465%)  route 3.409ns (55.535%))
  Logic Levels:           5  (IBUF=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=13, routed)          1.450     2.044    A_IBUF[3]
    SLICE_X1Y64                                                       r  O_OBUF[6]_inst_i_2/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     2.087 r  O_OBUF[6]_inst_i_2/O
                         net (fo=3, routed)           0.319     2.406    O_OBUF[6]_inst_i_2_n_0
    SLICE_X1Y65                                                       r  O_OBUF[7]_inst_i_3/I0
    SLICE_X1Y65          LUT5 (Prop_lut5_I0_O)        0.049     2.455 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.440     2.895    O_OBUF[7]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[7]_inst_i_1/I1
    SLICE_X1Y65          LUT5 (Prop_lut5_I1_O)        0.139     3.034 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.200     4.234    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.904     6.139 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.139    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.908ns  (logic 2.562ns (43.365%)  route 3.346ns (56.635%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 f  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              f  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 f  A_IBUF[0]_inst/O
                         net (fo=7, routed)           1.350     1.872    A_IBUF[0]
    SLICE_X0Y62                                                       f  O_OBUF[5]_inst_i_4/I1
    SLICE_X0Y62          LUT2 (Prop_lut2_I1_O)        0.043     1.915 r  O_OBUF[5]_inst_i_4/O
                         net (fo=1, routed)           0.341     2.255    O_OBUF[5]_inst_i_4_n_0
    SLICE_X0Y62                                                       r  O_OBUF[5]_inst_i_2/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.298 r  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.507     2.805    O_OBUF[5]_inst_i_2_n_0
    SLICE_X0Y65                                                       r  O_OBUF[5]_inst_i_1/I0
    SLICE_X0Y65          LUT4 (Prop_lut4_I0_O)        0.050     2.855 r  O_OBUF[5]_inst_i_1/O
                         net (fo=1, routed)           1.148     4.003    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.905     5.908 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.908    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------




