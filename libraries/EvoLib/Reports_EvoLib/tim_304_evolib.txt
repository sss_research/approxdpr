Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 04:35:36 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_304_evolib.txt -name O
| Design       : mul8_304
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.692ns  (logic 2.782ns (32.007%)  route 5.910ns (67.993%))
  Logic Levels:           10  (IBUF=1 LUT5=1 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.233     1.820    A_IBUF[6]
    SLICE_X0Y59                                                       r  O_OBUF[8]_inst_i_10/I4
    SLICE_X0Y59          LUT6 (Prop_lut6_I4_O)        0.043     1.863 r  O_OBUF[8]_inst_i_10/O
                         net (fo=5, routed)           0.424     2.286    N_648
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.329 r  O_OBUF[0]_inst_i_9/O
                         net (fo=3, routed)           0.321     2.650    N_1165
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_7/I2
    SLICE_X0Y60          LUT5 (Prop_lut5_I2_O)        0.043     2.693 r  O_OBUF[0]_inst_i_7/O
                         net (fo=2, routed)           0.611     3.304    O_OBUF[0]_inst_i_7_n_0
    SLICE_X0Y63                                                       r  O_OBUF[0]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     3.347 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.758     4.105    O_OBUF[0]
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.148 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.438     4.586    N_1482
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_7/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     4.629 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.359     4.989    N_1733
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_2/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     5.032 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.440     5.472    N_1748
    SLICE_X2Y65                                                       r  O_OBUF[15]_inst_i_1/I0
    SLICE_X2Y65          LUT6 (Prop_lut6_I0_O)        0.043     5.515 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.326     6.840    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.692 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.692    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.679ns  (logic 2.766ns (31.872%)  route 5.913ns (68.128%))
  Logic Levels:           10  (IBUF=1 LUT5=1 LUT6=7 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.233     1.820    A_IBUF[6]
    SLICE_X0Y59                                                       r  O_OBUF[8]_inst_i_10/I4
    SLICE_X0Y59          LUT6 (Prop_lut6_I4_O)        0.043     1.863 r  O_OBUF[8]_inst_i_10/O
                         net (fo=5, routed)           0.424     2.286    N_648
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.329 r  O_OBUF[0]_inst_i_9/O
                         net (fo=3, routed)           0.321     2.650    N_1165
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_7/I2
    SLICE_X0Y60          LUT5 (Prop_lut5_I2_O)        0.043     2.693 r  O_OBUF[0]_inst_i_7/O
                         net (fo=2, routed)           0.611     3.304    O_OBUF[0]_inst_i_7_n_0
    SLICE_X0Y63                                                       r  O_OBUF[0]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     3.347 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.758     4.105    O_OBUF[0]
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.148 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.438     4.586    N_1482
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_7/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     4.629 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.359     4.989    N_1733
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_2/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     5.032 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.433     5.465    N_1748
    SLICE_X2Y65                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X2Y65          LUT6 (Prop_lut6_I5_O)        0.043     5.508 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.336     6.844    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.679 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.679    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.589ns  (logic 2.851ns (33.197%)  route 5.738ns (66.803%))
  Logic Levels:           10  (IBUF=1 LUT4=1 LUT5=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.233     1.820    A_IBUF[6]
    SLICE_X0Y59                                                       r  O_OBUF[8]_inst_i_10/I4
    SLICE_X0Y59          LUT6 (Prop_lut6_I4_O)        0.043     1.863 r  O_OBUF[8]_inst_i_10/O
                         net (fo=5, routed)           0.424     2.286    N_648
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.329 r  O_OBUF[0]_inst_i_9/O
                         net (fo=3, routed)           0.321     2.650    N_1165
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_7/I2
    SLICE_X0Y60          LUT5 (Prop_lut5_I2_O)        0.043     2.693 r  O_OBUF[0]_inst_i_7/O
                         net (fo=2, routed)           0.611     3.304    O_OBUF[0]_inst_i_7_n_0
    SLICE_X0Y63                                                       r  O_OBUF[0]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     3.347 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.758     4.105    O_OBUF[0]
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.148 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.438     4.586    N_1482
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_7/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     4.629 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.359     4.989    N_1733
    SLICE_X2Y64                                                       r  O_OBUF[15]_inst_i_2/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     5.032 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.228     5.260    N_1748
    SLICE_X2Y65                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X2Y65          LUT4 (Prop_lut4_I0_O)        0.047     5.307 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.366     6.672    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.917     8.589 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.589    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.154ns  (logic 2.823ns (34.626%)  route 5.330ns (65.374%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.233     1.820    A_IBUF[6]
    SLICE_X0Y59                                                       r  O_OBUF[8]_inst_i_10/I4
    SLICE_X0Y59          LUT6 (Prop_lut6_I4_O)        0.043     1.863 r  O_OBUF[8]_inst_i_10/O
                         net (fo=5, routed)           0.424     2.286    N_648
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.329 r  O_OBUF[0]_inst_i_9/O
                         net (fo=3, routed)           0.321     2.650    N_1165
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_7/I2
    SLICE_X0Y60          LUT5 (Prop_lut5_I2_O)        0.043     2.693 r  O_OBUF[0]_inst_i_7/O
                         net (fo=2, routed)           0.611     3.304    O_OBUF[0]_inst_i_7_n_0
    SLICE_X0Y63                                                       r  O_OBUF[0]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     3.347 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.559     3.906    O_OBUF[0]
    SLICE_X3Y64                                                       r  O_OBUF[12]_inst_i_6/I3
    SLICE_X3Y64          LUT4 (Prop_lut4_I3_O)        0.043     3.949 r  O_OBUF[12]_inst_i_6/O
                         net (fo=4, routed)           0.410     4.358    N_1464
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_3/I2
    SLICE_X1Y65          LUT6 (Prop_lut6_I2_O)        0.043     4.401 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.506     4.907    N_1965
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_1/I3
    SLICE_X2Y65          LUT4 (Prop_lut4_I3_O)        0.048     4.955 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.268     6.223    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.931     8.154 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.154    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.050ns  (logic 2.729ns (33.900%)  route 5.321ns (66.100%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.233     1.820    A_IBUF[6]
    SLICE_X0Y59                                                       r  O_OBUF[8]_inst_i_10/I4
    SLICE_X0Y59          LUT6 (Prop_lut6_I4_O)        0.043     1.863 r  O_OBUF[8]_inst_i_10/O
                         net (fo=5, routed)           0.424     2.286    N_648
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.329 r  O_OBUF[0]_inst_i_9/O
                         net (fo=3, routed)           0.321     2.650    N_1165
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_7/I2
    SLICE_X0Y60          LUT5 (Prop_lut5_I2_O)        0.043     2.693 r  O_OBUF[0]_inst_i_7/O
                         net (fo=2, routed)           0.611     3.304    O_OBUF[0]_inst_i_7_n_0
    SLICE_X0Y63                                                       r  O_OBUF[0]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     3.347 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           0.559     3.906    O_OBUF[0]
    SLICE_X3Y64                                                       r  O_OBUF[12]_inst_i_6/I3
    SLICE_X3Y64          LUT4 (Prop_lut4_I3_O)        0.043     3.949 r  O_OBUF[12]_inst_i_6/O
                         net (fo=4, routed)           0.418     4.367    N_1464
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X1Y64          LUT4 (Prop_lut4_I0_O)        0.043     4.410 r  O_OBUF[11]_inst_i_2/O
                         net (fo=1, routed)           0.487     4.897    N_1714
    SLICE_X0Y65                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y65          LUT6 (Prop_lut6_I0_O)        0.043     4.940 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.268     6.208    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.050 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.050    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.410ns  (logic 2.780ns (37.523%)  route 4.629ns (62.477%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.210     1.804    A_IBUF[3]
    SLICE_X0Y63                                                       r  O_OBUF[4]_inst_i_1/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.847 r  O_OBUF[4]_inst_i_1/O
                         net (fo=2, routed)           0.414     2.261    O_OBUF[4]
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_5/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.304 r  O_OBUF[8]_inst_i_5/O
                         net (fo=5, routed)           0.449     2.752    N_1382
    SLICE_X0Y62                                                       r  O_OBUF[0]_inst_i_4/I2
    SLICE_X0Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.795 r  O_OBUF[0]_inst_i_4/O
                         net (fo=6, routed)           0.610     3.405    N_1415
    SLICE_X0Y64                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X0Y64          LUT6 (Prop_lut6_I0_O)        0.043     3.448 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.297     3.745    N_1682
    SLICE_X0Y65                                                       r  O_OBUF[11]_inst_i_3/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.053     3.798 r  O_OBUF[11]_inst_i_3/O
                         net (fo=2, routed)           0.353     4.151    N_1933
    SLICE_X0Y65                                                       r  O_OBUF[10]_inst_i_1/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.131     4.282 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.298     5.580    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.410 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.410    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.902ns  (logic 2.641ns (38.257%)  route 4.262ns (61.743%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.210     1.804    A_IBUF[3]
    SLICE_X0Y63                                                       r  O_OBUF[4]_inst_i_1/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.847 r  O_OBUF[4]_inst_i_1/O
                         net (fo=2, routed)           0.414     2.261    O_OBUF[4]
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_5/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.304 r  O_OBUF[8]_inst_i_5/O
                         net (fo=5, routed)           0.449     2.752    N_1382
    SLICE_X0Y62                                                       r  O_OBUF[0]_inst_i_4/I2
    SLICE_X0Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.795 r  O_OBUF[0]_inst_i_4/O
                         net (fo=6, routed)           0.610     3.405    N_1415
    SLICE_X0Y64                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X0Y64          LUT6 (Prop_lut6_I0_O)        0.043     3.448 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.297     3.745    N_1682
    SLICE_X0Y65                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X0Y65          LUT4 (Prop_lut4_I0_O)        0.043     3.788 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.283     5.071    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.902 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.902    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.572ns  (logic 2.585ns (39.329%)  route 3.988ns (60.671%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.210     1.804    A_IBUF[3]
    SLICE_X0Y63                                                       r  O_OBUF[4]_inst_i_1/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.847 r  O_OBUF[4]_inst_i_1/O
                         net (fo=2, routed)           0.414     2.261    O_OBUF[4]
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_5/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.304 r  O_OBUF[8]_inst_i_5/O
                         net (fo=5, routed)           0.454     2.757    N_1382
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.800 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.697     3.498    N_1414
    SLICE_X1Y65                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y65          LUT6 (Prop_lut6_I0_O)        0.043     3.541 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.213     4.754    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.572 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.572    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.300ns  (logic 2.589ns (41.087%)  route 3.712ns (58.913%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.210     1.804    A_IBUF[3]
    SLICE_X0Y63                                                       r  O_OBUF[4]_inst_i_1/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.847 r  O_OBUF[4]_inst_i_1/O
                         net (fo=2, routed)           0.414     2.261    O_OBUF[4]
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_5/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.304 r  O_OBUF[8]_inst_i_5/O
                         net (fo=5, routed)           0.454     2.757    N_1382
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_2/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.800 r  O_OBUF[8]_inst_i_2/O
                         net (fo=3, routed)           0.334     3.135    N_1414
    SLICE_X2Y62                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.178 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.300     4.478    O_OBUF[3]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.300 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.300    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[0]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.224ns  (logic 2.556ns (41.064%)  route 3.668ns (58.936%))
  Logic Levels:           6  (IBUF=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.233     1.820    A_IBUF[6]
    SLICE_X0Y59                                                       r  O_OBUF[8]_inst_i_10/I4
    SLICE_X0Y59          LUT6 (Prop_lut6_I4_O)        0.043     1.863 r  O_OBUF[8]_inst_i_10/O
                         net (fo=5, routed)           0.424     2.286    N_648
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_9/I0
    SLICE_X0Y60          LUT6 (Prop_lut6_I0_O)        0.043     2.329 r  O_OBUF[0]_inst_i_9/O
                         net (fo=3, routed)           0.321     2.650    N_1165
    SLICE_X0Y60                                                       r  O_OBUF[0]_inst_i_7/I2
    SLICE_X0Y60          LUT5 (Prop_lut5_I2_O)        0.043     2.693 r  O_OBUF[0]_inst_i_7/O
                         net (fo=2, routed)           0.611     3.304    O_OBUF[0]_inst_i_7_n_0
    SLICE_X0Y63                                                       r  O_OBUF[0]_inst_i_1/I5
    SLICE_X0Y63          LUT6 (Prop_lut6_I5_O)        0.043     3.347 r  O_OBUF[0]_inst_i_1/O
                         net (fo=4, routed)           1.080     4.427    O_OBUF[0]
    AH24                                                              r  O_OBUF[0]_inst/I
    AH24                 OBUF (Prop_obuf_I_O)         1.797     6.224 r  O_OBUF[0]_inst/O
                         net (fo=0)                   0.000     6.224    O[0]
    AH24                                                              r  O[0] (OUT)
  -------------------------------------------------------------------    -------------------




