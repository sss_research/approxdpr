Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 11:33:06 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_443_evolib.txt -name O
| Design       : mul8_443
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.265ns  (logic 2.916ns (31.472%)  route 6.349ns (68.528%))
  Logic Levels:           11  (IBUF=1 LUT4=2 LUT5=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.239     1.826    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X1Y60          LUT5 (Prop_lut5_I1_O)        0.043     1.869 r  O_OBUF[7]_inst_i_8/O
                         net (fo=5, routed)           0.438     2.307    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.350 r  O_OBUF[15]_inst_i_13/O
                         net (fo=2, routed)           0.614     2.965    N_899
    SLICE_X4Y60                                                       r  O_OBUF[10]_inst_i_13/I1
    SLICE_X4Y60          LUT6 (Prop_lut6_I1_O)        0.043     3.008 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.436     3.443    N_932
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.043     3.486 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.603     4.089    N_1182
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     4.132 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.418     4.551    N_1683
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_5/I2
    SLICE_X0Y61          LUT4 (Prop_lut4_I2_O)        0.048     4.599 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.363     4.962    N_1699
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.129     5.091 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.323     5.414    N_1732
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_2/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.043     5.457 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.511     5.968    N_1983
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X0Y63          LUT6 (Prop_lut6_I3_O)        0.043     6.011 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.403     7.414    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     9.265 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     9.265    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.173ns  (logic 2.900ns (31.616%)  route 6.273ns (68.384%))
  Logic Levels:           11  (IBUF=1 LUT4=2 LUT5=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.239     1.826    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X1Y60          LUT5 (Prop_lut5_I1_O)        0.043     1.869 r  O_OBUF[7]_inst_i_8/O
                         net (fo=5, routed)           0.438     2.307    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.350 r  O_OBUF[15]_inst_i_13/O
                         net (fo=2, routed)           0.614     2.965    N_899
    SLICE_X4Y60                                                       r  O_OBUF[10]_inst_i_13/I1
    SLICE_X4Y60          LUT6 (Prop_lut6_I1_O)        0.043     3.008 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.436     3.443    N_932
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.043     3.486 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.603     4.089    N_1182
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     4.132 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.418     4.551    N_1683
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_5/I2
    SLICE_X0Y61          LUT4 (Prop_lut4_I2_O)        0.048     4.599 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.363     4.962    N_1699
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.129     5.091 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.323     5.414    N_1732
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_2/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.043     5.457 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.510     5.967    N_1983
    SLICE_X0Y63                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     6.010 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.328     7.338    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     9.173 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     9.173    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        9.170ns  (logic 2.989ns (32.600%)  route 6.180ns (67.400%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT5=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.239     1.826    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X1Y60          LUT5 (Prop_lut5_I1_O)        0.043     1.869 r  O_OBUF[7]_inst_i_8/O
                         net (fo=5, routed)           0.438     2.307    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.350 r  O_OBUF[15]_inst_i_13/O
                         net (fo=2, routed)           0.614     2.965    N_899
    SLICE_X4Y60                                                       r  O_OBUF[10]_inst_i_13/I1
    SLICE_X4Y60          LUT6 (Prop_lut6_I1_O)        0.043     3.008 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.436     3.443    N_932
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.043     3.486 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.603     4.089    N_1182
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     4.132 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.418     4.551    N_1683
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_5/I2
    SLICE_X0Y61          LUT4 (Prop_lut4_I2_O)        0.048     4.599 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.363     4.962    N_1699
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.129     5.091 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.323     5.414    N_1732
    SLICE_X0Y61                                                       r  O_OBUF[15]_inst_i_2/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.043     5.457 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.312     5.770    N_1983
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.049     5.819 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.432     7.251    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.919     9.170 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     9.170    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.859ns  (logic 2.772ns (31.289%)  route 6.087ns (68.711%))
  Logic Levels:           10  (IBUF=1 LUT4=3 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.239     1.826    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X1Y60          LUT5 (Prop_lut5_I1_O)        0.043     1.869 r  O_OBUF[7]_inst_i_8/O
                         net (fo=5, routed)           0.438     2.307    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.350 r  O_OBUF[15]_inst_i_13/O
                         net (fo=2, routed)           0.614     2.965    N_899
    SLICE_X4Y60                                                       r  O_OBUF[10]_inst_i_13/I1
    SLICE_X4Y60          LUT6 (Prop_lut6_I1_O)        0.043     3.008 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.436     3.443    N_932
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.043     3.486 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.603     4.089    N_1182
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     4.132 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.418     4.551    N_1683
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     4.594 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.422     5.015    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_2/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     5.058 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.497     5.555    N_1949
    SLICE_X0Y61                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     5.598 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.419     7.017    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.859 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.859    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.635ns  (logic 2.863ns (33.161%)  route 5.771ns (66.839%))
  Logic Levels:           10  (IBUF=1 LUT4=2 LUT5=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.239     1.826    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X1Y60          LUT5 (Prop_lut5_I1_O)        0.043     1.869 r  O_OBUF[7]_inst_i_8/O
                         net (fo=5, routed)           0.438     2.307    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.350 r  O_OBUF[15]_inst_i_13/O
                         net (fo=2, routed)           0.614     2.965    N_899
    SLICE_X4Y60                                                       r  O_OBUF[10]_inst_i_13/I1
    SLICE_X4Y60          LUT6 (Prop_lut6_I1_O)        0.043     3.008 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.436     3.443    N_932
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.043     3.486 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.603     4.089    N_1182
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     4.132 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.418     4.551    N_1683
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_5/I2
    SLICE_X0Y61          LUT4 (Prop_lut4_I2_O)        0.048     4.599 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.363     4.962    N_1699
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_4/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.129     5.091 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.326     5.417    N_1732
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X0Y61          LUT6 (Prop_lut6_I4_O)        0.043     5.460 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.333     6.793    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.635 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.635    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.976ns  (logic 2.718ns (34.077%)  route 5.258ns (65.923%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.239     1.826    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X1Y60          LUT5 (Prop_lut5_I1_O)        0.043     1.869 r  O_OBUF[7]_inst_i_8/O
                         net (fo=5, routed)           0.438     2.307    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.350 r  O_OBUF[15]_inst_i_13/O
                         net (fo=2, routed)           0.614     2.965    N_899
    SLICE_X4Y60                                                       r  O_OBUF[10]_inst_i_13/I1
    SLICE_X4Y60          LUT6 (Prop_lut6_I1_O)        0.043     3.008 r  O_OBUF[10]_inst_i_13/O
                         net (fo=3, routed)           0.436     3.443    N_932
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_7/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.043     3.486 r  O_OBUF[10]_inst_i_7/O
                         net (fo=4, routed)           0.603     4.089    N_1182
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_9/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     4.132 r  O_OBUF[10]_inst_i_9/O
                         net (fo=2, routed)           0.418     4.551    N_1683
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.043     4.594 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.227     4.820    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     4.863 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.282     6.146    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.976 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.976    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.582ns  (logic 2.859ns (37.707%)  route 4.723ns (62.293%))
  Logic Levels:           8  (IBUF=1 LUT4=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.239     1.826    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X1Y60          LUT5 (Prop_lut5_I1_O)        0.043     1.869 r  O_OBUF[7]_inst_i_8/O
                         net (fo=5, routed)           0.631     2.500    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y60                                                       r  O_OBUF[8]_inst_i_10/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.543 r  O_OBUF[8]_inst_i_10/O
                         net (fo=2, routed)           0.419     2.962    N_914
    SLICE_X2Y60                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.043     3.005 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.420     3.425    N_1164
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_3/I2
    SLICE_X1Y60          LUT6 (Prop_lut6_I2_O)        0.127     3.552 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.309     3.861    N_1664
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_2/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.054     3.915 r  O_OBUF[10]_inst_i_2/O
                         net (fo=3, routed)           0.420     4.335    N_1915
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.131     4.466 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.285     5.751    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.582 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.582    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.900ns  (logic 2.704ns (39.194%)  route 4.196ns (60.806%))
  Logic Levels:           7  (IBUF=1 LUT4=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=18, routed)          1.239     1.826    A_IBUF[6]
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X1Y60          LUT5 (Prop_lut5_I1_O)        0.043     1.869 r  O_OBUF[7]_inst_i_8/O
                         net (fo=5, routed)           0.631     2.500    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y60                                                       r  O_OBUF[8]_inst_i_10/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.543 r  O_OBUF[8]_inst_i_10/O
                         net (fo=2, routed)           0.419     2.962    N_914
    SLICE_X2Y60                                                       r  O_OBUF[8]_inst_i_6/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.043     3.005 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.420     3.425    N_1164
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_3/I2
    SLICE_X1Y60          LUT6 (Prop_lut6_I2_O)        0.127     3.552 r  O_OBUF[8]_inst_i_3/O
                         net (fo=2, routed)           0.309     3.861    N_1664
    SLICE_X0Y63                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X0Y63          LUT4 (Prop_lut4_I1_O)        0.043     3.904 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.177     5.081    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.900 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.900    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.279ns  (logic 2.675ns (42.601%)  route 3.604ns (57.399%))
  Logic Levels:           6  (IBUF=1 LUT2=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 f  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              f  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 f  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.113     1.702    A_IBUF[4]
    SLICE_X0Y61                                                       f  O_OBUF[7]_inst_i_5/I0
    SLICE_X0Y61          LUT2 (Prop_lut2_I0_O)        0.052     1.754 r  O_OBUF[7]_inst_i_5/O
                         net (fo=5, routed)           0.359     2.113    O_OBUF[7]_inst_i_5_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_4/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.131     2.244 r  O_OBUF[7]_inst_i_4/O
                         net (fo=3, routed)           0.409     2.653    N_1148
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X1Y60          LUT5 (Prop_lut5_I2_O)        0.043     2.696 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.456     3.152    N_1398
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.195 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.268     4.463    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.279 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.279    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[0]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.894ns  (logic 2.428ns (49.607%)  route 2.466ns (50.393%))
  Logic Levels:           3  (IBUF=1 LUT2=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=17, routed)          1.420     2.008    A_IBUF[5]
    SLICE_X0Y63                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X0Y63          LUT2 (Prop_lut2_I0_O)        0.043     2.051 r  O_OBUF[0]_inst_i_1/O
                         net (fo=1, routed)           1.046     3.097    O_OBUF[0]
    AH24                                                              r  O_OBUF[0]_inst/I
    AH24                 OBUF (Prop_obuf_I_O)         1.797     4.894 r  O_OBUF[0]_inst/O
                         net (fo=0)                   0.000     4.894    O[0]
    AH24                                                              r  O[0] (OUT)
  -------------------------------------------------------------------    -------------------




