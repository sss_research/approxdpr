Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 22:17:59 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_175_evolib.txt -name O
| Design       : mul8_175
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.829ns  (logic 2.820ns (36.023%)  route 5.009ns (63.977%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=11, routed)          1.091     1.670    A_IBUF[7]
    SLICE_X1Y61                                                       r  O_OBUF[15]_inst_i_11/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.713 r  O_OBUF[15]_inst_i_11/O
                         net (fo=3, routed)           0.510     2.223    N_948
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_7/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.266 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.405     2.671    N_1198
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X3Y63          LUT6 (Prop_lut6_I3_O)        0.043     2.714 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.415     3.129    N_1448
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.172 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.414     3.587    N_1949
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X3Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.630 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.327     3.957    N_1965
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X2Y63          LUT4 (Prop_lut4_I2_O)        0.043     4.000 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.498     4.498    N_1983
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_1/I0
    SLICE_X0Y64          LUT4 (Prop_lut4_I0_O)        0.053     4.551 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.348     5.899    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.930     7.829 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.829    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.812ns  (logic 2.716ns (34.769%)  route 5.096ns (65.231%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=11, routed)          1.091     1.670    A_IBUF[7]
    SLICE_X1Y61                                                       r  O_OBUF[15]_inst_i_11/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.713 r  O_OBUF[15]_inst_i_11/O
                         net (fo=3, routed)           0.510     2.223    N_948
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_7/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.266 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.405     2.671    N_1198
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X3Y63          LUT6 (Prop_lut6_I3_O)        0.043     2.714 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.415     3.129    N_1448
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.172 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.414     3.587    N_1949
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X3Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.630 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.327     3.957    N_1965
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X2Y63          LUT4 (Prop_lut4_I2_O)        0.043     4.000 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.505     4.505    N_1983
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.548 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.428     5.976    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.812 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.812    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.691ns  (logic 2.713ns (35.279%)  route 4.978ns (64.721%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=11, routed)          1.091     1.670    A_IBUF[7]
    SLICE_X1Y61                                                       r  O_OBUF[15]_inst_i_11/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.713 r  O_OBUF[15]_inst_i_11/O
                         net (fo=3, routed)           0.510     2.223    N_948
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_7/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.266 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.405     2.671    N_1198
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X3Y63          LUT6 (Prop_lut6_I3_O)        0.043     2.714 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.415     3.129    N_1448
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.172 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.414     3.587    N_1949
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X3Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.630 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.327     3.957    N_1965
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_2/I2
    SLICE_X2Y63          LUT4 (Prop_lut4_I2_O)        0.043     4.000 r  O_OBUF[15]_inst_i_2/O
                         net (fo=3, routed)           0.492     4.493    N_1983
    SLICE_X1Y64                                                       r  O_OBUF[14]_inst_i_1/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     4.536 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.323     5.858    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.691 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.691    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.288ns  (logic 2.776ns (38.084%)  route 4.513ns (61.916%))
  Logic Levels:           8  (IBUF=1 LUT4=1 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=11, routed)          1.091     1.670    A_IBUF[7]
    SLICE_X1Y61                                                       r  O_OBUF[15]_inst_i_11/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.043     1.713 r  O_OBUF[15]_inst_i_11/O
                         net (fo=3, routed)           0.510     2.223    N_948
    SLICE_X2Y61                                                       r  O_OBUF[11]_inst_i_7/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     2.266 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.405     2.671    N_1198
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X3Y63          LUT6 (Prop_lut6_I3_O)        0.043     2.714 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.415     3.129    N_1448
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_2/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.172 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.414     3.587    N_1949
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X3Y63          LUT6 (Prop_lut6_I2_O)        0.043     3.630 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.327     3.957    N_1965
    SLICE_X2Y63                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X2Y63          LUT4 (Prop_lut4_I0_O)        0.051     4.008 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.350     5.358    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.930     7.288 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.288    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.049ns  (logic 2.853ns (40.469%)  route 4.197ns (59.531%))
  Logic Levels:           8  (IBUF=1 LUT4=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          0.959     1.545    A_IBUF[6]
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_5/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     1.588 r  O_OBUF[9]_inst_i_5/O
                         net (fo=6, routed)           0.524     2.112    N_914
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_10/I1
    SLICE_X2Y61          LUT5 (Prop_lut5_I1_O)        0.043     2.155 r  O_OBUF[15]_inst_i_10/O
                         net (fo=2, routed)           0.244     2.399    N_1183
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_9/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.127     2.526 r  O_OBUF[15]_inst_i_9/O
                         net (fo=3, routed)           0.412     2.938    N_1214
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_5/I1
    SLICE_X3Y62          LUT4 (Prop_lut4_I1_O)        0.051     2.989 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.524     3.514    N_1464
    SLICE_X3Y63                                                       r  O_OBUF[11]_inst_i_3/I1
    SLICE_X3Y63          LUT4 (Prop_lut4_I1_O)        0.129     3.643 r  O_OBUF[11]_inst_i_3/O
                         net (fo=1, routed)           0.259     3.901    N_1714
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.043     3.944 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.275     5.219    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.049 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.049    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.856ns  (logic 2.811ns (41.001%)  route 4.045ns (58.999%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT4=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 f  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              f  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 f  A_IBUF[4]_inst/O
                         net (fo=17, routed)          1.231     1.820    A_IBUF[4]
    SLICE_X2Y63                                                       f  O_OBUF[9]_inst_i_6/I1
    SLICE_X2Y63          LUT2 (Prop_lut2_I1_O)        0.043     1.863 r  O_OBUF[9]_inst_i_6/O
                         net (fo=2, routed)           0.300     2.164    O_OBUF[9]_inst_i_6_n_0
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_3/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.127     2.291 r  O_OBUF[9]_inst_i_3/O
                         net (fo=3, routed)           0.421     2.711    N_1665
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.754 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.344     3.099    N_1683
    SLICE_X1Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.048     3.147 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.441     3.588    N_1698
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y63          LUT4 (Prop_lut4_I1_O)        0.129     3.717 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.307     5.025    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.856 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.856    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.024ns  (logic 2.577ns (42.785%)  route 3.447ns (57.215%))
  Logic Levels:           6  (IBUF=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          0.959     1.545    A_IBUF[6]
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_5/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     1.588 r  O_OBUF[9]_inst_i_5/O
                         net (fo=6, routed)           0.524     2.112    N_914
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I0
    SLICE_X2Y61          LUT5 (Prop_lut5_I0_O)        0.043     2.155 r  O_OBUF[6]_inst_i_2/O
                         net (fo=7, routed)           0.487     2.642    N_1182
    SLICE_X0Y62                                                       r  O_OBUF[9]_inst_i_2/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.685 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.220     2.905    N_1432
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.948 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.258     4.205    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.024 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.024    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.687ns  (logic 2.638ns (46.381%)  route 3.049ns (53.619%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=5, routed)           1.152     1.748    A_IBUF[2]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_8/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.791 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.351     2.142    O_OBUF[9]_inst_i_8_n_0
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X1Y62          LUT5 (Prop_lut5_I0_O)        0.048     2.190 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.333     2.523    N_1398
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.129     2.652 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.213     3.865    O_OBUF[6]
    AK27                                                              r  O_OBUF[6]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.687 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.687    O[6]
    AK27                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[0]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.678ns  (logic 2.605ns (45.880%)  route 3.073ns (54.120%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          0.959     1.545    A_IBUF[6]
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_5/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.043     1.588 r  O_OBUF[9]_inst_i_5/O
                         net (fo=6, routed)           0.524     2.112    N_914
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_2/I0
    SLICE_X2Y61          LUT5 (Prop_lut5_I0_O)        0.043     2.155 r  O_OBUF[6]_inst_i_2/O
                         net (fo=7, routed)           0.522     2.678    N_1182
    SLICE_X0Y63                                                       r  O_OBUF[0]_inst_i_1/I0
    SLICE_X0Y63          LUT3 (Prop_lut3_I0_O)        0.053     2.731 r  O_OBUF[0]_inst_i_1/O
                         net (fo=1, routed)           1.068     3.798    O_OBUF[0]
    AJ25                                                              r  O_OBUF[0]_inst/I
    AJ25                 OBUF (Prop_obuf_I_O)         1.879     5.678 r  O_OBUF[0]_inst/O
                         net (fo=0)                   0.000     5.678    O[0]
    AJ25                                                              r  O[0] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.669ns  (logic 2.632ns (46.432%)  route 3.037ns (53.568%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=5, routed)           1.152     1.748    A_IBUF[2]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_8/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.791 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.351     2.142    O_OBUF[9]_inst_i_8_n_0
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_3/I0
    SLICE_X1Y62          LUT5 (Prop_lut5_I0_O)        0.048     2.190 r  O_OBUF[8]_inst_i_3/O
                         net (fo=3, routed)           0.354     2.543    N_1398
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_1/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.129     2.672 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.180     3.853    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.669 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.669    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------




