Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 12:40:52 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_465_evolib.txt -name O
| Design       : mul8_465
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.793ns  (logic 3.054ns (34.736%)  route 5.739ns (65.264%))
  Logic Levels:           10  (IBUF=1 LUT4=4 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.559     2.585    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.628 r  O_OBUF[10]_inst_i_11/O
                         net (fo=1, routed)           0.246     2.874    N_1149
    SLICE_X3Y60                                                       r  O_OBUF[10]_inst_i_7/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.043     2.917 r  O_OBUF[10]_inst_i_7/O
                         net (fo=5, routed)           0.434     3.351    N_1165
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.394 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.423     3.816    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.050     3.866 r  O_OBUF[11]_inst_i_5/O
                         net (fo=5, routed)           0.520     4.386    N_1683
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.126     4.512 r  O_OBUF[15]_inst_i_11/O
                         net (fo=3, routed)           0.513     5.025    N_1715
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_4/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.053     5.078 r  O_OBUF[13]_inst_i_4/O
                         net (fo=2, routed)           0.304     5.382    N_1732
    SLICE_X0Y64                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X0Y64          LUT4 (Prop_lut4_I0_O)        0.139     5.521 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.344     6.865    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     8.793 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.793    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.782ns  (logic 2.856ns (32.524%)  route 5.926ns (67.476%))
  Logic Levels:           10  (IBUF=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.559     2.585    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.628 r  O_OBUF[10]_inst_i_11/O
                         net (fo=1, routed)           0.246     2.874    N_1149
    SLICE_X3Y60                                                       r  O_OBUF[10]_inst_i_7/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.043     2.917 r  O_OBUF[10]_inst_i_7/O
                         net (fo=5, routed)           0.434     3.351    N_1165
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.394 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.423     3.816    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.050     3.866 r  O_OBUF[11]_inst_i_5/O
                         net (fo=5, routed)           0.520     4.386    N_1683
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.126     4.512 r  O_OBUF[15]_inst_i_11/O
                         net (fo=3, routed)           0.513     5.025    N_1715
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_5/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     5.068 r  O_OBUF[15]_inst_i_5/O
                         net (fo=2, routed)           0.410     5.479    N_1733
    SLICE_X0Y62                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X0Y62          LUT6 (Prop_lut6_I5_O)        0.043     5.522 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.425     6.946    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.782 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.782    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.699ns  (logic 2.951ns (33.929%)  route 5.747ns (66.071%))
  Logic Levels:           10  (IBUF=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.559     2.585    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.628 r  O_OBUF[10]_inst_i_11/O
                         net (fo=1, routed)           0.246     2.874    N_1149
    SLICE_X3Y60                                                       r  O_OBUF[10]_inst_i_7/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.043     2.917 r  O_OBUF[10]_inst_i_7/O
                         net (fo=5, routed)           0.434     3.351    N_1165
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.394 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.423     3.816    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.050     3.866 r  O_OBUF[11]_inst_i_5/O
                         net (fo=5, routed)           0.520     4.386    N_1683
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.126     4.512 r  O_OBUF[15]_inst_i_11/O
                         net (fo=3, routed)           0.513     5.025    N_1715
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_4/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.053     5.078 r  O_OBUF[13]_inst_i_4/O
                         net (fo=2, routed)           0.299     5.377    N_1732
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_1/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.131     5.508 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.358     6.866    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.699 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.699    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.662ns  (logic 2.872ns (33.157%)  route 5.790ns (66.843%))
  Logic Levels:           10  (IBUF=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.559     2.585    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.628 r  O_OBUF[10]_inst_i_11/O
                         net (fo=1, routed)           0.246     2.874    N_1149
    SLICE_X3Y60                                                       r  O_OBUF[10]_inst_i_7/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.043     2.917 r  O_OBUF[10]_inst_i_7/O
                         net (fo=5, routed)           0.434     3.351    N_1165
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.394 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.423     3.816    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_5/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.050     3.866 r  O_OBUF[11]_inst_i_5/O
                         net (fo=5, routed)           0.520     4.386    N_1683
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_11/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.126     4.512 r  O_OBUF[15]_inst_i_11/O
                         net (fo=3, routed)           0.513     5.025    N_1715
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_5/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     5.068 r  O_OBUF[15]_inst_i_5/O
                         net (fo=2, routed)           0.296     5.364    N_1733
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X0Y62          LUT6 (Prop_lut6_I3_O)        0.043     5.407 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.403     6.810    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.662 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.662    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.033ns  (logic 2.729ns (33.970%)  route 5.304ns (66.030%))
  Logic Levels:           9  (IBUF=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.559     2.585    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.628 r  O_OBUF[10]_inst_i_11/O
                         net (fo=1, routed)           0.246     2.874    N_1149
    SLICE_X3Y60                                                       r  O_OBUF[10]_inst_i_7/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.043     2.917 r  O_OBUF[10]_inst_i_7/O
                         net (fo=5, routed)           0.534     3.452    N_1165
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_21/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.043     3.495 r  O_OBUF[15]_inst_i_21/O
                         net (fo=3, routed)           0.455     3.950    N_1198
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.993 r  O_OBUF[11]_inst_i_6/O
                         net (fo=5, routed)           0.504     4.497    N_1448
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_3/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     4.540 r  O_OBUF[11]_inst_i_3/O
                         net (fo=3, routed)           0.329     4.868    N_1949
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I3
    SLICE_X0Y63          LUT4 (Prop_lut4_I3_O)        0.043     4.911 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.281     6.192    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.033 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.033    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.911ns  (logic 2.718ns (34.354%)  route 5.193ns (65.646%))
  Logic Levels:           9  (IBUF=1 LUT4=4 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.559     2.585    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.628 r  O_OBUF[10]_inst_i_11/O
                         net (fo=1, routed)           0.246     2.874    N_1149
    SLICE_X3Y60                                                       r  O_OBUF[10]_inst_i_7/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.043     2.917 r  O_OBUF[10]_inst_i_7/O
                         net (fo=5, routed)           0.534     3.452    N_1165
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_21/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.043     3.495 r  O_OBUF[15]_inst_i_21/O
                         net (fo=3, routed)           0.455     3.950    N_1198
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_6/I0
    SLICE_X2Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.993 r  O_OBUF[11]_inst_i_6/O
                         net (fo=5, routed)           0.501     4.494    N_1448
    SLICE_X1Y63                                                       r  O_OBUF[10]_inst_i_2/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.537 r  O_OBUF[10]_inst_i_2/O
                         net (fo=1, routed)           0.217     4.753    N_1698
    SLICE_X1Y63                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.043     4.796 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.285     6.081    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.911 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.911    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.325ns  (logic 2.676ns (36.534%)  route 4.649ns (63.466%))
  Logic Levels:           8  (IBUF=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.559     2.585    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.628 r  O_OBUF[10]_inst_i_11/O
                         net (fo=1, routed)           0.246     2.874    N_1149
    SLICE_X3Y60                                                       r  O_OBUF[10]_inst_i_7/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.043     2.917 r  O_OBUF[10]_inst_i_7/O
                         net (fo=5, routed)           0.434     3.351    N_1165
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.394 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.423     3.816    N_1432
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_2/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     3.859 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.326     4.186    N_1682
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.043     4.229 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.265     5.493    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.325 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.325    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.635ns  (logic 2.620ns (39.496%)  route 4.014ns (60.504%))
  Logic Levels:           7  (IBUF=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.555     2.581    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[9]_inst_i_8/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.624 r  O_OBUF[9]_inst_i_8/O
                         net (fo=3, routed)           0.261     2.885    N_914
    SLICE_X3Y60                                                       r  O_OBUF[9]_inst_i_5/I1
    SLICE_X3Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.928 r  O_OBUF[9]_inst_i_5/O
                         net (fo=3, routed)           0.328     3.256    N_1414
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X1Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.299 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.217     3.516    N_1664
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.559 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.257     4.816    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.635 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.635    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.961ns  (logic 2.532ns (42.476%)  route 3.429ns (57.524%))
  Logic Levels:           5  (IBUF=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.463     2.489    O_OBUF[7]_inst_i_7_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_2/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.532 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.315     2.847    N_1398
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.890 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.254     4.145    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.961 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.961    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.901ns  (logic 2.628ns (44.537%)  route 3.273ns (55.463%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=17, routed)          1.397     1.983    A_IBUF[6]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_7/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.026 r  O_OBUF[7]_inst_i_7/O
                         net (fo=7, routed)           0.349     2.375    O_OBUF[7]_inst_i_7_n_0
    SLICE_X1Y60                                                       r  O_OBUF[6]_inst_i_2/I0
    SLICE_X1Y60          LUT4 (Prop_lut4_I0_O)        0.050     2.425 r  O_OBUF[6]_inst_i_2/O
                         net (fo=1, routed)           0.248     2.673    N_882
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_1/I4
    SLICE_X0Y60          LUT6 (Prop_lut6_I4_O)        0.126     2.799 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.279     4.079    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     5.901 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.901    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




