Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 02:11:10 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_255_evolib.txt -name O
| Design       : mul8_255
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.798ns  (logic 3.019ns (34.315%)  route 5.779ns (65.685%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.427     2.870    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.913 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.440     3.353    N_1415
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.048     3.401 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.446     3.847    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.132     3.979 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.429     4.409    N_1683
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.452 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.428     4.880    N_1949
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.048     4.928 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.201     5.129    N_1965
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.129     5.258 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.232     5.491    N_1999
    SLICE_X2Y62                                                       r  O_OBUF[15]_inst_i_1/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     5.534 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.413     6.947    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.798 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.798    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.792ns  (logic 3.003ns (34.159%)  route 5.789ns (65.841%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.427     2.870    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.913 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.440     3.353    N_1415
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.048     3.401 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.446     3.847    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.132     3.979 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.429     4.409    N_1683
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.452 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.428     4.880    N_1949
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.048     4.928 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.201     5.129    N_1965
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X1Y62          LUT6 (Prop_lut6_I3_O)        0.129     5.258 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.231     5.490    N_1999
    SLICE_X2Y62                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X2Y62          LUT6 (Prop_lut6_I4_O)        0.043     5.533 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.424     6.957    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.792 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.792    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.688ns  (logic 3.065ns (35.285%)  route 5.622ns (64.715%))
  Logic Levels:           10  (IBUF=1 LUT4=4 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.427     2.870    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.913 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.440     3.353    N_1415
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.048     3.401 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.446     3.847    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.132     3.979 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.429     4.409    N_1683
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.452 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.428     4.880    N_1949
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.048     4.928 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.260     5.188    N_1965
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.140     5.328 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.430     6.758    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.930     8.688 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.688    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.454ns  (logic 2.958ns (34.985%)  route 5.496ns (65.015%))
  Logic Levels:           10  (IBUF=1 LUT4=3 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.427     2.870    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.913 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.440     3.353    N_1415
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.048     3.401 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.446     3.847    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.132     3.979 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.429     4.409    N_1683
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.452 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.428     4.880    N_1949
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_2/I2
    SLICE_X0Y62          LUT4 (Prop_lut4_I2_O)        0.048     4.928 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.231     5.159    N_1965
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X0Y62          LUT6 (Prop_lut6_I2_O)        0.129     5.288 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.333     6.621    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.454 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.454    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.092ns  (logic 2.832ns (34.997%)  route 5.260ns (65.003%))
  Logic Levels:           9  (IBUF=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.427     2.870    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.913 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.440     3.353    N_1415
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.048     3.401 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.446     3.847    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X2Y63          LUT4 (Prop_lut4_I3_O)        0.132     3.979 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.429     4.409    N_1683
    SLICE_X0Y64                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.043     4.452 r  O_OBUF[11]_inst_i_2/O
                         net (fo=2, routed)           0.428     4.880    N_1949
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y62          LUT4 (Prop_lut4_I0_O)        0.043     4.923 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.328     6.251    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.092 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.092    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.874ns  (logic 2.727ns (34.636%)  route 5.146ns (65.364%))
  Logic Levels:           9  (IBUF=1 LUT4=4 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.427     2.870    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.913 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.440     3.353    N_1415
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_13/I2
    SLICE_X2Y61          LUT4 (Prop_lut4_I2_O)        0.043     3.396 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.400     3.797    N_1433
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X1Y61          LUT4 (Prop_lut4_I0_O)        0.043     3.840 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.600     4.440    N_1448
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X0Y64          LUT4 (Prop_lut4_I1_O)        0.043     4.483 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.224     4.707    N_1698
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y64          LUT4 (Prop_lut4_I1_O)        0.043     4.750 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.293     6.043    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.874 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.874    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.418ns  (logic 2.863ns (38.599%)  route 4.555ns (61.401%))
  Logic Levels:           8  (IBUF=1 LUT4=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.427     2.870    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.913 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.440     3.353    N_1415
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_5/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.048     3.401 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.446     3.847    N_1432
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_3/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.132     3.979 r  O_OBUF[9]_inst_i_3/O
                         net (fo=1, routed)           0.287     4.267    N_1682
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.127     4.394 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.193     5.587    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.418 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.418    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.680ns  (logic 2.721ns (40.726%)  route 3.960ns (59.274%))
  Logic Levels:           7  (IBUF=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.177     2.620    N_1399
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_3/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.663 r  O_OBUF[6]_inst_i_3/O
                         net (fo=4, routed)           0.430     3.093    N_1414
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.048     3.141 r  O_OBUF[8]_inst_i_3/O
                         net (fo=1, routed)           0.374     3.515    N_1664
    SLICE_X0Y65                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X0Y65          LUT4 (Prop_lut4_I1_O)        0.129     3.644 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.217     4.861    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.680 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.680    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.170ns  (logic 2.590ns (41.980%)  route 3.580ns (58.020%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.177     2.620    N_1399
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_3/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.663 r  O_OBUF[6]_inst_i_3/O
                         net (fo=4, routed)           0.424     3.087    N_1414
    SLICE_X0Y64                                                       r  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y64          LUT4 (Prop_lut4_I3_O)        0.043     3.130 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.218     4.348    O_OBUF[3]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.170 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.170    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[3]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.980ns  (logic 2.569ns (42.966%)  route 3.411ns (57.034%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=13, routed)          1.319     1.914    A_IBUF[2]
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_4/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     1.957 r  O_OBUF[8]_inst_i_4/O
                         net (fo=4, routed)           0.443     2.401    N_1383
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     2.444 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.177     2.620    N_1399
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_3/I0
    SLICE_X1Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.663 r  O_OBUF[6]_inst_i_3/O
                         net (fo=4, routed)           0.424     3.087    N_1414
    SLICE_X0Y64                                                       r  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y64          LUT4 (Prop_lut4_I3_O)        0.043     3.130 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.048     4.178    O_OBUF[3]
    AJ27                                                              r  O_OBUF[3]_inst/I
    AJ27                 OBUF (Prop_obuf_I_O)         1.802     5.980 r  O_OBUF[3]_inst/O
                         net (fo=0)                   0.000     5.980    O[3]
    AJ27                                                              r  O[3] (OUT)
  -------------------------------------------------------------------    -------------------




