Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 16:24:57 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_000_evolib.txt -name O
| Design       : mul8_000
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.309ns  (logic 2.824ns (33.992%)  route 5.484ns (66.008%))
  Logic Levels:           9  (IBUF=1 LUT3=3 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.823     2.413    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.456 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.420     2.876    O_OBUF[9]_inst_i_18_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.052     2.928 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.398     3.326    O_OBUF[9]_inst_i_10_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X5Y63          LUT3 (Prop_lut3_I0_O)        0.132     3.458 f  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.493     3.951    O_OBUF[9]_inst_i_3_n_0
    SLICE_X5Y64                                                       f  O_OBUF[11]_inst_i_4/I1
    SLICE_X5Y64          LUT3 (Prop_lut3_I1_O)        0.043     3.994 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.496     4.490    O_OBUF[11]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_3/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     4.533 r  O_OBUF[13]_inst_i_3/O
                         net (fo=4, routed)           0.231     4.764    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y63                                                       r  O_OBUF[14]_inst_i_2/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     4.807 r  O_OBUF[14]_inst_i_2/O
                         net (fo=1, routed)           0.295     5.102    O_OBUF[14]_inst_i_2_n_0
    SLICE_X0Y64                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y64          LUT5 (Prop_lut5_I4_O)        0.043     5.145 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.328     6.473    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.309 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.309    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.274ns  (logic 2.878ns (34.787%)  route 5.396ns (65.213%))
  Logic Levels:           8  (IBUF=1 LUT3=4 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.823     2.413    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.456 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.420     2.876    O_OBUF[9]_inst_i_18_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.052     2.928 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.398     3.326    O_OBUF[9]_inst_i_10_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X5Y63          LUT3 (Prop_lut3_I0_O)        0.132     3.458 f  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.493     3.951    O_OBUF[9]_inst_i_3_n_0
    SLICE_X5Y64                                                       f  O_OBUF[11]_inst_i_4/I1
    SLICE_X5Y64          LUT3 (Prop_lut3_I1_O)        0.043     3.994 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.496     4.490    O_OBUF[11]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_3/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     4.533 r  O_OBUF[13]_inst_i_3/O
                         net (fo=4, routed)           0.427     4.960    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X0Y63          LUT3 (Prop_lut3_I2_O)        0.048     5.008 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.339     6.346    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     8.274 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.274    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.187ns  (logic 2.840ns (34.687%)  route 5.347ns (65.313%))
  Logic Levels:           9  (IBUF=1 LUT3=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.823     2.413    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.456 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.420     2.876    O_OBUF[9]_inst_i_18_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.052     2.928 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.398     3.326    O_OBUF[9]_inst_i_10_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X5Y63          LUT3 (Prop_lut3_I0_O)        0.132     3.458 f  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.493     3.951    O_OBUF[9]_inst_i_3_n_0
    SLICE_X5Y64                                                       f  O_OBUF[11]_inst_i_4/I1
    SLICE_X5Y64          LUT3 (Prop_lut3_I1_O)        0.043     3.994 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.496     4.490    O_OBUF[11]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_3/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     4.533 r  O_OBUF[13]_inst_i_3/O
                         net (fo=4, routed)           0.227     4.760    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     4.803 f  O_OBUF[15]_inst_i_3/O
                         net (fo=1, routed)           0.167     4.970    O_OBUF[15]_inst_i_3_n_0
    SLICE_X0Y64                                                       f  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y64          LUT6 (Prop_lut6_I1_O)        0.043     5.013 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.323     6.336    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.187 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.187    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.175ns  (logic 2.778ns (33.987%)  route 5.397ns (66.013%))
  Logic Levels:           8  (IBUF=1 LUT3=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.823     2.413    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.456 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.420     2.876    O_OBUF[9]_inst_i_18_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.052     2.928 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.398     3.326    O_OBUF[9]_inst_i_10_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X5Y63          LUT3 (Prop_lut3_I0_O)        0.132     3.458 f  O_OBUF[9]_inst_i_3/O
                         net (fo=2, routed)           0.493     3.951    O_OBUF[9]_inst_i_3_n_0
    SLICE_X5Y64                                                       f  O_OBUF[11]_inst_i_4/I1
    SLICE_X5Y64          LUT3 (Prop_lut3_I1_O)        0.043     3.994 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.496     4.490    O_OBUF[11]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_3/I1
    SLICE_X1Y64          LUT6 (Prop_lut6_I1_O)        0.043     4.533 r  O_OBUF[13]_inst_i_3/O
                         net (fo=4, routed)           0.427     4.960    O_OBUF[13]_inst_i_3_n_0
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X0Y63          LUT5 (Prop_lut5_I1_O)        0.043     5.003 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.339     6.342    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.175 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.175    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.772ns  (logic 3.017ns (38.820%)  route 4.755ns (61.180%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.823     2.413    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.456 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.420     2.876    O_OBUF[9]_inst_i_18_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.052     2.928 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.398     3.326    O_OBUF[9]_inst_i_10_n_0
    SLICE_X5Y63                                                       r  O_OBUF[11]_inst_i_11/I1
    SLICE_X5Y63          LUT3 (Prop_lut3_I1_O)        0.140     3.466 r  O_OBUF[11]_inst_i_11/O
                         net (fo=4, routed)           0.519     3.986    O_OBUF[11]_inst_i_11_n_0
    SLICE_X5Y64                                                       r  O_OBUF[9]_inst_i_5/I2
    SLICE_X5Y64          LUT5 (Prop_lut5_I2_O)        0.139     4.125 r  O_OBUF[9]_inst_i_5/O
                         net (fo=1, routed)           0.339     4.463    O_OBUF[9]_inst_i_5_n_0
    SLICE_X5Y64                                                       r  O_OBUF[9]_inst_i_1/I3
    SLICE_X5Y64          LUT5 (Prop_lut5_I3_O)        0.136     4.599 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.255     5.854    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.917     7.772 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.772    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.673ns  (logic 2.932ns (38.210%)  route 4.741ns (61.790%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.823     2.413    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.456 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.420     2.876    O_OBUF[9]_inst_i_18_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.052     2.928 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.398     3.326    O_OBUF[9]_inst_i_10_n_0
    SLICE_X5Y63                                                       r  O_OBUF[11]_inst_i_11/I1
    SLICE_X5Y63          LUT3 (Prop_lut3_I1_O)        0.140     3.466 r  O_OBUF[11]_inst_i_11/O
                         net (fo=4, routed)           0.307     3.774    O_OBUF[11]_inst_i_11_n_0
    SLICE_X4Y64                                                       r  O_OBUF[11]_inst_i_3/I5
    SLICE_X4Y64          LUT6 (Prop_lut6_I5_O)        0.129     3.903 r  O_OBUF[11]_inst_i_3/O
                         net (fo=3, routed)           0.522     4.425    O_OBUF[11]_inst_i_3_n_0
    SLICE_X1Y64                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X1Y64          LUT5 (Prop_lut5_I1_O)        0.051     4.476 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.270     5.746    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.927     7.673 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.673    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.591ns  (logic 2.827ns (37.239%)  route 4.764ns (62.761%))
  Logic Levels:           7  (IBUF=1 LUT3=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.823     2.413    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.456 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.420     2.876    O_OBUF[9]_inst_i_18_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.052     2.928 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.398     3.326    O_OBUF[9]_inst_i_10_n_0
    SLICE_X5Y63                                                       r  O_OBUF[11]_inst_i_11/I1
    SLICE_X5Y63          LUT3 (Prop_lut3_I1_O)        0.140     3.466 r  O_OBUF[11]_inst_i_11/O
                         net (fo=4, routed)           0.307     3.774    O_OBUF[11]_inst_i_11_n_0
    SLICE_X4Y64                                                       r  O_OBUF[11]_inst_i_3/I5
    SLICE_X4Y64          LUT6 (Prop_lut6_I5_O)        0.129     3.903 r  O_OBUF[11]_inst_i_3/O
                         net (fo=3, routed)           0.522     4.425    O_OBUF[11]_inst_i_3_n_0
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X1Y64          LUT3 (Prop_lut3_I2_O)        0.043     4.468 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.293     5.761    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.591 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.591    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.308ns  (logic 2.815ns (38.526%)  route 4.492ns (61.474%))
  Logic Levels:           7  (IBUF=1 LUT3=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.823     2.413    A_IBUF[4]
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_18/I3
    SLICE_X2Y62          LUT6 (Prop_lut6_I3_O)        0.043     2.456 r  O_OBUF[9]_inst_i_18/O
                         net (fo=2, routed)           0.420     2.876    O_OBUF[9]_inst_i_18_n_0
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X2Y63          LUT3 (Prop_lut3_I2_O)        0.052     2.928 r  O_OBUF[9]_inst_i_10/O
                         net (fo=3, routed)           0.398     3.326    O_OBUF[9]_inst_i_10_n_0
    SLICE_X5Y63                                                       r  O_OBUF[11]_inst_i_11/I1
    SLICE_X5Y63          LUT3 (Prop_lut3_I1_O)        0.140     3.466 r  O_OBUF[11]_inst_i_11/O
                         net (fo=4, routed)           0.519     3.986    O_OBUF[11]_inst_i_11_n_0
    SLICE_X5Y64                                                       r  O_OBUF[8]_inst_i_2/I2
    SLICE_X5Y64          LUT3 (Prop_lut3_I2_O)        0.129     4.115 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.088     4.203    O_OBUF[8]_inst_i_2_n_0
    SLICE_X5Y64                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X5Y64          LUT6 (Prop_lut6_I0_O)        0.043     4.246 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.243     5.489    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.308 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.308    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.046ns  (logic 2.770ns (39.311%)  route 4.276ns (60.688%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.619     2.208    A_IBUF[4]
    SLICE_X4Y62                                                       r  O_OBUF[8]_inst_i_11/I4
    SLICE_X4Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.251 r  O_OBUF[8]_inst_i_11/O
                         net (fo=2, routed)           0.476     2.728    O_OBUF[8]_inst_i_11_n_0
    SLICE_X4Y65                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X4Y65          LUT3 (Prop_lut3_I0_O)        0.051     2.779 r  O_OBUF[8]_inst_i_6/O
                         net (fo=3, routed)           0.440     3.218    O_OBUF[8]_inst_i_6_n_0
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_6/I2
    SLICE_X4Y65          LUT3 (Prop_lut3_I2_O)        0.139     3.357 r  O_OBUF[7]_inst_i_6/O
                         net (fo=1, routed)           0.523     3.880    O_OBUF[7]_inst_i_6_n_0
    SLICE_X2Y65                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X2Y65          LUT6 (Prop_lut6_I4_O)        0.131     4.011 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.219     5.230    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     7.046 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     7.046    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.890ns  (logic 2.627ns (38.131%)  route 4.263ns (61.869%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=15, routed)          1.619     2.208    A_IBUF[4]
    SLICE_X4Y62                                                       r  O_OBUF[8]_inst_i_11/I4
    SLICE_X4Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.251 r  O_OBUF[8]_inst_i_11/O
                         net (fo=2, routed)           0.476     2.728    O_OBUF[8]_inst_i_11_n_0
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_10/I0
    SLICE_X4Y65          LUT3 (Prop_lut3_I0_O)        0.043     2.771 r  O_OBUF[7]_inst_i_10/O
                         net (fo=2, routed)           0.319     3.090    O_OBUF[7]_inst_i_10_n_0
    SLICE_X2Y65                                                       r  O_OBUF[5]_inst_i_5/I0
    SLICE_X2Y65          LUT6 (Prop_lut6_I0_O)        0.043     3.133 f  O_OBUF[5]_inst_i_5/O
                         net (fo=2, routed)           0.296     3.429    O_OBUF[5]_inst_i_5_n_0
    SLICE_X1Y65                                                       f  O_OBUF[7]_inst_i_2/I2
    SLICE_X1Y65          LUT4 (Prop_lut4_I2_O)        0.043     3.472 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.429     3.902    O_OBUF[7]_inst_i_2_n_0
    SLICE_X2Y65                                                       r  O_OBUF[6]_inst_i_1/I2
    SLICE_X2Y65          LUT3 (Prop_lut3_I2_O)        0.043     3.945 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.123     5.067    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.890 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.890    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




