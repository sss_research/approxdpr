Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 08:26:39 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_382_evolib.txt -name O
| Design       : mul8_382
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.870ns  (logic 2.904ns (32.745%)  route 5.965ns (67.255%))
  Logic Levels:           11  (IBUF=1 LUT4=4 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.068     1.656    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.699 r  O_OBUF[8]_inst_i_5/O
                         net (fo=6, routed)           0.667     2.365    N_882
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.408 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.436     2.845    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.888 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.342     3.230    N_1415
    SLICE_X3Y61                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.273 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.323     3.596    N_1433
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT4 (Prop_lut4_I0_O)        0.052     3.648 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.425     4.073    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.131     4.204 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.450     4.654    N_1699
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_7/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.697 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.418     5.115    N_1733
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X3Y63          LUT6 (Prop_lut6_I4_O)        0.043     5.158 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.421     5.579    N_1748
    SLICE_X1Y63                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X1Y63          LUT4 (Prop_lut4_I1_O)        0.043     5.622 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.415     7.037    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.870 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.870    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.860ns  (logic 2.907ns (32.812%)  route 5.953ns (67.188%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.068     1.656    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.699 r  O_OBUF[8]_inst_i_5/O
                         net (fo=6, routed)           0.667     2.365    N_882
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.408 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.436     2.845    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.888 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.342     3.230    N_1415
    SLICE_X3Y61                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.273 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.323     3.596    N_1433
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT4 (Prop_lut4_I0_O)        0.052     3.648 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.425     4.073    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.131     4.204 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.450     4.654    N_1699
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_7/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.697 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.418     5.115    N_1733
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X3Y63          LUT6 (Prop_lut6_I4_O)        0.043     5.158 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.418     5.576    N_1748
    SLICE_X1Y63                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X1Y63          LUT6 (Prop_lut6_I2_O)        0.043     5.619 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.406     7.025    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.860 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.860    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.805ns  (logic 2.923ns (33.198%)  route 5.882ns (66.802%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.068     1.656    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.699 r  O_OBUF[8]_inst_i_5/O
                         net (fo=6, routed)           0.667     2.365    N_882
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.408 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.436     2.845    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.888 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.342     3.230    N_1415
    SLICE_X3Y61                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.273 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.323     3.596    N_1433
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT4 (Prop_lut4_I0_O)        0.052     3.648 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.425     4.073    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.131     4.204 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.450     4.654    N_1699
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_7/I3
    SLICE_X3Y62          LUT6 (Prop_lut6_I3_O)        0.043     4.697 r  O_OBUF[15]_inst_i_7/O
                         net (fo=2, routed)           0.418     5.115    N_1733
    SLICE_X3Y63                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X3Y63          LUT6 (Prop_lut6_I4_O)        0.043     5.158 r  O_OBUF[15]_inst_i_3/O
                         net (fo=3, routed)           0.418     5.576    N_1748
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     5.619 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.335     6.953    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.805 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.805    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.399ns  (logic 2.967ns (35.326%)  route 5.432ns (64.674%))
  Logic Levels:           10  (IBUF=1 LUT4=5 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.068     1.656    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.699 r  O_OBUF[8]_inst_i_5/O
                         net (fo=6, routed)           0.667     2.365    N_882
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.408 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.436     2.845    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.888 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.342     3.230    N_1415
    SLICE_X3Y61                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.273 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.323     3.596    N_1433
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT4 (Prop_lut4_I0_O)        0.052     3.648 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.425     4.073    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_4/I3
    SLICE_X2Y62          LUT4 (Prop_lut4_I3_O)        0.131     4.204 r  O_OBUF[12]_inst_i_4/O
                         net (fo=4, routed)           0.434     4.638    N_1699
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X1Y61          LUT4 (Prop_lut4_I0_O)        0.043     4.681 r  O_OBUF[11]_inst_i_3/O
                         net (fo=1, routed)           0.323     5.004    N_1714
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.052     5.056 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.413     6.469    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.929     8.399 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.399    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.265ns  (logic 2.961ns (35.830%)  route 5.303ns (64.170%))
  Logic Levels:           10  (IBUF=1 LUT4=4 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.068     1.656    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.699 r  O_OBUF[8]_inst_i_5/O
                         net (fo=6, routed)           0.667     2.365    N_882
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.408 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.436     2.845    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.888 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.342     3.230    N_1415
    SLICE_X3Y61                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.273 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.323     3.596    N_1433
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_8/I2
    SLICE_X3Y62          LUT4 (Prop_lut4_I2_O)        0.043     3.639 r  O_OBUF[15]_inst_i_8/O
                         net (fo=3, routed)           0.224     3.862    N_1449
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X3Y62          LUT4 (Prop_lut4_I0_O)        0.051     3.913 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.430     4.344    N_1464
    SLICE_X1Y61                                                       r  O_OBUF[12]_inst_i_2/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.129     4.473 r  O_OBUF[12]_inst_i_2/O
                         net (fo=2, routed)           0.407     4.880    N_1965
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.051     4.931 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.406     6.337    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.928     8.265 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.265    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.886ns  (logic 2.910ns (36.901%)  route 4.976ns (63.099%))
  Logic Levels:           9  (IBUF=1 LUT4=4 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.068     1.656    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.699 r  O_OBUF[8]_inst_i_5/O
                         net (fo=6, routed)           0.667     2.365    N_882
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.408 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.436     2.845    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.888 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.342     3.230    N_1415
    SLICE_X3Y61                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X3Y61          LUT6 (Prop_lut6_I2_O)        0.043     3.273 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.323     3.596    N_1433
    SLICE_X3Y62                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y62          LUT4 (Prop_lut4_I0_O)        0.052     3.648 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.425     4.073    N_1448
    SLICE_X2Y62                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y62          LUT4 (Prop_lut4_I1_O)        0.136     4.209 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.400     4.609    N_1698
    SLICE_X0Y62                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y62          LUT4 (Prop_lut4_I1_O)        0.132     4.741 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.314     6.055    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.886 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.886    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.076ns  (logic 2.769ns (39.131%)  route 4.307ns (60.869%))
  Logic Levels:           8  (IBUF=1 LUT4=4 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.068     1.656    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.699 r  O_OBUF[8]_inst_i_5/O
                         net (fo=6, routed)           0.667     2.365    N_882
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.408 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.344     2.753    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_5/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.796 r  O_OBUF[9]_inst_i_5/O
                         net (fo=3, routed)           0.415     3.211    N_1414
    SLICE_X1Y61                                                       r  O_OBUF[10]_inst_i_4/I3
    SLICE_X1Y61          LUT4 (Prop_lut4_I3_O)        0.043     3.254 r  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.254     3.508    N_1665
    SLICE_X0Y61                                                       r  O_OBUF[9]_inst_i_3/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.049     3.557 r  O_OBUF[9]_inst_i_3/O
                         net (fo=1, routed)           0.294     3.850    N_1682
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X1Y62          LUT4 (Prop_lut4_I1_O)        0.129     3.979 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.266     5.245    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.076 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.076    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.996ns  (logic 2.712ns (38.770%)  route 4.284ns (61.230%))
  Logic Levels:           7  (IBUF=1 LUT4=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.068     1.656    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.699 r  O_OBUF[8]_inst_i_5/O
                         net (fo=6, routed)           0.667     2.365    N_882
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.408 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.344     2.753    N_1399
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_5/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.796 r  O_OBUF[9]_inst_i_5/O
                         net (fo=3, routed)           0.415     3.211    N_1414
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_3/I1
    SLICE_X1Y61          LUT4 (Prop_lut4_I1_O)        0.048     3.259 r  O_OBUF[8]_inst_i_3/O
                         net (fo=1, routed)           0.611     3.869    N_1664
    SLICE_X0Y62                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X0Y62          LUT4 (Prop_lut4_I1_O)        0.129     3.998 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.179     5.177    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.996 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.996    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.072ns  (logic 2.576ns (42.423%)  route 3.496ns (57.577%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=19, routed)          1.068     1.656    A_IBUF[5]
    SLICE_X1Y60                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X1Y60          LUT4 (Prop_lut4_I3_O)        0.043     1.699 r  O_OBUF[8]_inst_i_5/O
                         net (fo=6, routed)           0.498     2.197    N_882
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_3/I5
    SLICE_X0Y61          LUT6 (Prop_lut6_I5_O)        0.043     2.240 r  O_OBUF[7]_inst_i_3/O
                         net (fo=2, routed)           0.235     2.475    N_1148
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X0Y61          LUT5 (Prop_lut5_I2_O)        0.043     2.518 r  O_OBUF[7]_inst_i_2/O
                         net (fo=1, routed)           0.426     2.944    N_1648
    SLICE_X0Y63                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X0Y63          LUT3 (Prop_lut3_I2_O)        0.043     2.987 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.256    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.072 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.072    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        4.921ns  (logic 2.480ns (50.391%)  route 2.441ns (49.609%))
  Logic Levels:           3  (IBUF=1 LUT4=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=4, routed)           1.244     1.766    A_IBUF[0]
    SLICE_X0Y63                                                       r  O_OBUF[5]_inst_i_1/I1
    SLICE_X0Y63          LUT4 (Prop_lut4_I1_O)        0.051     1.817 r  O_OBUF[5]_inst_i_1/O
                         net (fo=1, routed)           1.197     3.014    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.908     4.921 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     4.921    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------




