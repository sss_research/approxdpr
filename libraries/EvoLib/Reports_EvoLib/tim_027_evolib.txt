Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 14:56:09 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_027_evolib.txt -name O
| Design       : mul8_027
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.347ns  (logic 3.006ns (36.020%)  route 5.340ns (63.980%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.500     2.096    A_IBUF[2]
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_12/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.139 r  O_OBUF[8]_inst_i_12/O
                         net (fo=3, routed)           0.238     2.376    O_OBUF[8]_inst_i_12_n_0
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_15/I0
    SLICE_X4Y64          LUT3 (Prop_lut3_I0_O)        0.049     2.425 r  O_OBUF[8]_inst_i_15/O
                         net (fo=5, routed)           0.445     2.870    O_OBUF[8]_inst_i_15_n_0
    SLICE_X5Y64                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X5Y64          LUT3 (Prop_lut3_I0_O)        0.139     3.009 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.540     3.549    O_OBUF[8]_inst_i_6_n_0
    SLICE_X2Y64                                                       r  O_OBUF[10]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.131     3.680 f  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.519     4.199    O_OBUF[10]_inst_i_4_n_0
    SLICE_X2Y65                                                       f  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y65          LUT6 (Prop_lut6_I1_O)        0.127     4.326 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.294     4.619    O_OBUF[12]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_5/I2
    SLICE_X1Y65          LUT6 (Prop_lut6_I2_O)        0.043     4.662 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.448     5.110    O_OBUF[15]_inst_i_5_n_0
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     5.153 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.358     6.511    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.347 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.347    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.312ns  (logic 3.022ns (36.358%)  route 5.290ns (63.642%))
  Logic Levels:           9  (IBUF=1 LUT3=2 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.500     2.096    A_IBUF[2]
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_12/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.139 r  O_OBUF[8]_inst_i_12/O
                         net (fo=3, routed)           0.238     2.376    O_OBUF[8]_inst_i_12_n_0
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_15/I0
    SLICE_X4Y64          LUT3 (Prop_lut3_I0_O)        0.049     2.425 r  O_OBUF[8]_inst_i_15/O
                         net (fo=5, routed)           0.445     2.870    O_OBUF[8]_inst_i_15_n_0
    SLICE_X5Y64                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X5Y64          LUT3 (Prop_lut3_I0_O)        0.139     3.009 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.540     3.549    O_OBUF[8]_inst_i_6_n_0
    SLICE_X2Y64                                                       r  O_OBUF[10]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.131     3.680 f  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.519     4.199    O_OBUF[10]_inst_i_4_n_0
    SLICE_X2Y65                                                       f  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y65          LUT6 (Prop_lut6_I1_O)        0.127     4.326 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.294     4.619    O_OBUF[12]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_5/I2
    SLICE_X1Y65          LUT6 (Prop_lut6_I2_O)        0.043     4.662 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.350     5.013    O_OBUF[15]_inst_i_5_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I3
    SLICE_X0Y65          LUT6 (Prop_lut6_I3_O)        0.043     5.056 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.405     6.461    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.312 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.312    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.117ns  (logic 3.004ns (37.001%)  route 5.114ns (62.999%))
  Logic Levels:           9  (IBUF=1 LUT3=3 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.500     2.096    A_IBUF[2]
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_12/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.139 r  O_OBUF[8]_inst_i_12/O
                         net (fo=3, routed)           0.238     2.376    O_OBUF[8]_inst_i_12_n_0
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_15/I0
    SLICE_X4Y64          LUT3 (Prop_lut3_I0_O)        0.049     2.425 r  O_OBUF[8]_inst_i_15/O
                         net (fo=5, routed)           0.445     2.870    O_OBUF[8]_inst_i_15_n_0
    SLICE_X5Y64                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X5Y64          LUT3 (Prop_lut3_I0_O)        0.139     3.009 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.540     3.549    O_OBUF[8]_inst_i_6_n_0
    SLICE_X2Y64                                                       r  O_OBUF[10]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.131     3.680 f  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.519     4.199    O_OBUF[10]_inst_i_4_n_0
    SLICE_X2Y65                                                       f  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y65          LUT6 (Prop_lut6_I1_O)        0.127     4.326 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.294     4.619    O_OBUF[12]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_5/I2
    SLICE_X1Y65          LUT6 (Prop_lut6_I2_O)        0.043     4.662 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.233     4.895    O_OBUF[15]_inst_i_5_n_0
    SLICE_X1Y65                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X1Y65          LUT3 (Prop_lut3_I2_O)        0.043     4.938 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.347     6.285    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.117 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.117    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.984ns  (logic 3.060ns (38.326%)  route 4.924ns (61.674%))
  Logic Levels:           8  (IBUF=1 LUT3=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.500     2.096    A_IBUF[2]
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_12/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.139 r  O_OBUF[8]_inst_i_12/O
                         net (fo=3, routed)           0.238     2.376    O_OBUF[8]_inst_i_12_n_0
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_15/I0
    SLICE_X4Y64          LUT3 (Prop_lut3_I0_O)        0.049     2.425 r  O_OBUF[8]_inst_i_15/O
                         net (fo=5, routed)           0.445     2.870    O_OBUF[8]_inst_i_15_n_0
    SLICE_X5Y64                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X5Y64          LUT3 (Prop_lut3_I0_O)        0.139     3.009 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.540     3.549    O_OBUF[8]_inst_i_6_n_0
    SLICE_X2Y64                                                       r  O_OBUF[10]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.131     3.680 f  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.519     4.199    O_OBUF[10]_inst_i_4_n_0
    SLICE_X2Y65                                                       f  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y65          LUT6 (Prop_lut6_I1_O)        0.127     4.326 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.415     4.740    O_OBUF[12]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X1Y65          LUT3 (Prop_lut3_I2_O)        0.048     4.788 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.269     6.057    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.927     7.984 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.984    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.895ns  (logic 2.969ns (37.610%)  route 4.926ns (62.390%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.500     2.096    A_IBUF[2]
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_12/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.139 r  O_OBUF[8]_inst_i_12/O
                         net (fo=3, routed)           0.238     2.376    O_OBUF[8]_inst_i_12_n_0
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_15/I0
    SLICE_X4Y64          LUT3 (Prop_lut3_I0_O)        0.049     2.425 r  O_OBUF[8]_inst_i_15/O
                         net (fo=5, routed)           0.445     2.870    O_OBUF[8]_inst_i_15_n_0
    SLICE_X5Y64                                                       r  O_OBUF[8]_inst_i_6/I0
    SLICE_X5Y64          LUT3 (Prop_lut3_I0_O)        0.139     3.009 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.540     3.549    O_OBUF[8]_inst_i_6_n_0
    SLICE_X2Y64                                                       r  O_OBUF[10]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.131     3.680 f  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.519     4.199    O_OBUF[10]_inst_i_4_n_0
    SLICE_X2Y65                                                       f  O_OBUF[12]_inst_i_3/I1
    SLICE_X2Y65          LUT6 (Prop_lut6_I1_O)        0.127     4.326 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.415     4.740    O_OBUF[12]_inst_i_3_n_0
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X1Y65          LUT5 (Prop_lut5_I1_O)        0.043     4.783 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.271     6.054    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.895 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.895    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.514ns  (logic 2.998ns (39.897%)  route 4.516ns (60.103%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.500     2.096    A_IBUF[2]
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_12/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.139 r  O_OBUF[8]_inst_i_12/O
                         net (fo=3, routed)           0.238     2.376    O_OBUF[8]_inst_i_12_n_0
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_15/I0
    SLICE_X4Y64          LUT3 (Prop_lut3_I0_O)        0.049     2.425 f  O_OBUF[8]_inst_i_15/O
                         net (fo=5, routed)           0.445     2.870    O_OBUF[8]_inst_i_15_n_0
    SLICE_X5Y64                                                       f  O_OBUF[8]_inst_i_6/I0
    SLICE_X5Y64          LUT3 (Prop_lut3_I0_O)        0.139     3.009 f  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.540     3.549    O_OBUF[8]_inst_i_6_n_0
    SLICE_X2Y64                                                       f  O_OBUF[10]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.131     3.680 r  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.512     4.192    O_OBUF[10]_inst_i_4_n_0
    SLICE_X2Y65                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X2Y65          LUT5 (Prop_lut5_I2_O)        0.127     4.319 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.283     5.601    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.913     7.514 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.514    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.455ns  (logic 2.916ns (39.119%)  route 4.539ns (60.881%))
  Logic Levels:           7  (IBUF=1 LUT3=3 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.500     2.096    A_IBUF[2]
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_12/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     2.139 r  O_OBUF[8]_inst_i_12/O
                         net (fo=3, routed)           0.238     2.376    O_OBUF[8]_inst_i_12_n_0
    SLICE_X4Y64                                                       r  O_OBUF[8]_inst_i_15/I0
    SLICE_X4Y64          LUT3 (Prop_lut3_I0_O)        0.049     2.425 f  O_OBUF[8]_inst_i_15/O
                         net (fo=5, routed)           0.445     2.870    O_OBUF[8]_inst_i_15_n_0
    SLICE_X5Y64                                                       f  O_OBUF[8]_inst_i_6/I0
    SLICE_X5Y64          LUT3 (Prop_lut3_I0_O)        0.139     3.009 f  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.540     3.549    O_OBUF[8]_inst_i_6_n_0
    SLICE_X2Y64                                                       f  O_OBUF[10]_inst_i_4/I4
    SLICE_X2Y64          LUT5 (Prop_lut5_I4_O)        0.131     3.680 r  O_OBUF[10]_inst_i_4/O
                         net (fo=3, routed)           0.512     4.192    O_OBUF[10]_inst_i_4_n_0
    SLICE_X2Y65                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X2Y65          LUT3 (Prop_lut3_I1_O)        0.127     4.319 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.305     5.623    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.455 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.455    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.236ns  (logic 2.764ns (38.192%)  route 4.472ns (61.808%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.292     1.888    A_IBUF[2]
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_9/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.931 r  O_OBUF[7]_inst_i_9/O
                         net (fo=2, routed)           0.338     2.268    O_OBUF[7]_inst_i_9_n_0
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_5/I2
    SLICE_X3Y62          LUT3 (Prop_lut3_I2_O)        0.048     2.316 r  O_OBUF[6]_inst_i_5/O
                         net (fo=3, routed)           0.331     2.647    O_OBUF[6]_inst_i_5_n_0
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_4/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.129     2.776 f  O_OBUF[6]_inst_i_4/O
                         net (fo=3, routed)           0.693     3.469    O_OBUF[6]_inst_i_4_n_0
    SLICE_X2Y65                                                       f  O_OBUF[7]_inst_i_2/I2
    SLICE_X2Y65          LUT3 (Prop_lut3_I2_O)        0.043     3.512 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.313     3.825    O_OBUF[7]_inst_i_2_n_0
    SLICE_X4Y65                                                       r  O_OBUF[8]_inst_i_7/I5
    SLICE_X4Y65          LUT6 (Prop_lut6_I5_O)        0.043     3.868 r  O_OBUF[8]_inst_i_7/O
                         net (fo=1, routed)           0.328     4.196    O_OBUF[8]_inst_i_7_n_0
    SLICE_X2Y64                                                       r  O_OBUF[8]_inst_i_1/I5
    SLICE_X2Y64          LUT6 (Prop_lut6_I5_O)        0.043     4.239 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.179     5.417    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.236 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.236    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.809ns  (logic 2.718ns (39.920%)  route 4.091ns (60.080%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.292     1.888    A_IBUF[2]
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_9/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.931 r  O_OBUF[7]_inst_i_9/O
                         net (fo=2, routed)           0.338     2.268    O_OBUF[7]_inst_i_9_n_0
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_5/I2
    SLICE_X3Y62          LUT3 (Prop_lut3_I2_O)        0.048     2.316 r  O_OBUF[6]_inst_i_5/O
                         net (fo=3, routed)           0.331     2.647    O_OBUF[6]_inst_i_5_n_0
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_4/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.129     2.776 f  O_OBUF[6]_inst_i_4/O
                         net (fo=3, routed)           0.693     3.469    O_OBUF[6]_inst_i_4_n_0
    SLICE_X2Y65                                                       f  O_OBUF[7]_inst_i_2/I2
    SLICE_X2Y65          LUT3 (Prop_lut3_I2_O)        0.043     3.512 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.282     3.794    O_OBUF[7]_inst_i_2_n_0
    SLICE_X4Y65                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X4Y65          LUT6 (Prop_lut6_I0_O)        0.043     3.837 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.156     4.993    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.809 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.809    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.552ns  (logic 2.775ns (42.361%)  route 3.776ns (57.639%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.292     1.888    A_IBUF[2]
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_9/I5
    SLICE_X3Y62          LUT6 (Prop_lut6_I5_O)        0.043     1.931 r  O_OBUF[7]_inst_i_9/O
                         net (fo=2, routed)           0.338     2.268    O_OBUF[7]_inst_i_9_n_0
    SLICE_X3Y62                                                       r  O_OBUF[6]_inst_i_5/I2
    SLICE_X3Y62          LUT3 (Prop_lut3_I2_O)        0.048     2.316 r  O_OBUF[6]_inst_i_5/O
                         net (fo=3, routed)           0.331     2.647    O_OBUF[6]_inst_i_5_n_0
    SLICE_X0Y62                                                       r  O_OBUF[6]_inst_i_4/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.129     2.776 r  O_OBUF[6]_inst_i_4/O
                         net (fo=3, routed)           0.693     3.469    O_OBUF[6]_inst_i_4_n_0
    SLICE_X2Y65                                                       r  O_OBUF[6]_inst_i_1/I2
    SLICE_X2Y65          LUT3 (Prop_lut3_I2_O)        0.048     3.517 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.123     4.640    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.912     6.552 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.552    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




