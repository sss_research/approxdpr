Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 19:27:00 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_117_evolib.txt -name O
| Design       : mul8_117
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.041ns  (logic 2.834ns (40.248%)  route 4.207ns (59.752%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=12, routed)          0.976     1.555    A_IBUF[7]
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_10/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.598 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.600     2.198    N_1203
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_8/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.241 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.496     2.737    N_1616
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_6/I1
    SLICE_X0Y63          LUT3 (Prop_lut3_I1_O)        0.051     2.788 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.395     3.183    N_1795
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.137     3.320 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.413     3.733    N_1973
    SLICE_X0Y64                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y64          LUT6 (Prop_lut6_I1_O)        0.129     3.862 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.328     5.190    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.041 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.041    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.869ns  (logic 2.818ns (41.024%)  route 4.051ns (58.976%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=12, routed)          0.976     1.555    A_IBUF[7]
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_10/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.598 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.600     2.198    N_1203
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_8/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.241 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.496     2.737    N_1616
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_6/I1
    SLICE_X0Y63          LUT3 (Prop_lut3_I1_O)        0.051     2.788 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.395     3.183    N_1795
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_3/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.137     3.320 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.171     3.491    N_1973
    SLICE_X0Y64                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.129     3.620 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.414     5.034    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     6.869 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     6.869    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.481ns  (logic 2.678ns (41.325%)  route 3.803ns (58.675%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=12, routed)          0.976     1.555    A_IBUF[7]
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_10/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.598 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.600     2.198    N_1203
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_8/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.241 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.496     2.737    N_1616
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_6/I1
    SLICE_X0Y63          LUT3 (Prop_lut3_I1_O)        0.051     2.788 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.395     3.183    N_1795
    SLICE_X0Y65                                                       r  O_OBUF[13]_inst_i_1/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.129     3.312 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.336     4.648    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.481 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.481    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.367ns  (logic 2.593ns (40.728%)  route 3.774ns (59.272%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=12, routed)          0.976     1.555    A_IBUF[7]
    SLICE_X1Y61                                                       r  O_OBUF[11]_inst_i_10/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.598 r  O_OBUF[11]_inst_i_10/O
                         net (fo=3, routed)           0.600     2.198    N_1203
    SLICE_X0Y62                                                       r  O_OBUF[13]_inst_i_8/I4
    SLICE_X0Y62          LUT6 (Prop_lut6_I4_O)        0.043     2.241 r  O_OBUF[13]_inst_i_8/O
                         net (fo=2, routed)           0.496     2.737    N_1616
    SLICE_X0Y63                                                       r  O_OBUF[13]_inst_i_4/I1
    SLICE_X0Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.780 r  O_OBUF[13]_inst_i_4/O
                         net (fo=3, routed)           0.432     3.213    N_1794
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X0Y65          LUT3 (Prop_lut3_I1_O)        0.043     3.256 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.270     4.525    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.367 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.367    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.083ns  (logic 2.648ns (43.539%)  route 3.434ns (56.461%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              f  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 f  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.245     1.839    A_IBUF[3]
    SLICE_X2Y63                                                       f  O_OBUF[2]_inst_i_5/I0
    SLICE_X2Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.882 r  O_OBUF[2]_inst_i_5/O
                         net (fo=2, routed)           0.416     2.298    O_OBUF[2]_inst_i_5_n_0
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.127     2.425 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.429     2.855    N_1602
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I2
    SLICE_X0Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.898 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.344     4.241    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.083 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.083    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.822ns  (logic 2.637ns (45.301%)  route 3.184ns (54.699%))
  Logic Levels:           5  (IBUF=1 LUT2=1 LUT3=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 f  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              f  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 f  A_IBUF[3]_inst/O
                         net (fo=8, routed)           1.245     1.839    A_IBUF[3]
    SLICE_X2Y63                                                       f  O_OBUF[2]_inst_i_5/I0
    SLICE_X2Y63          LUT2 (Prop_lut2_I0_O)        0.043     1.882 r  O_OBUF[2]_inst_i_5/O
                         net (fo=2, routed)           0.416     2.298    O_OBUF[2]_inst_i_5_n_0
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X1Y63          LUT6 (Prop_lut6_I3_O)        0.127     2.425 r  O_OBUF[11]_inst_i_4/O
                         net (fo=3, routed)           0.331     2.756    N_1602
    SLICE_X1Y64                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X1Y64          LUT3 (Prop_lut3_I1_O)        0.043     2.799 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.193     3.992    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.822 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.822    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[7]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.547ns  (logic 2.500ns (45.065%)  route 3.047ns (54.935%))
  Logic Levels:           5  (IBUF=1 LUT4=2 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AJ25                                              0.000     0.000 r  B[7] (IN)
                         net (fo=0)                   0.000     0.000    B[7]
    AJ25                                                              r  B_IBUF[7]_inst/I
    AJ25                 IBUF (Prop_ibuf_I_O)         0.554     0.554 r  B_IBUF[7]_inst/O
                         net (fo=19, routed)          1.153     1.707    B_IBUF[7]
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.043     1.750 f  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.328     2.078    N_52
    SLICE_X0Y63                                                       f  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y63          LUT5 (Prop_lut5_I3_O)        0.043     2.121 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.425     2.546    O_OBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.043     2.589 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.142     3.730    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.547 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.547    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.542ns  (logic 2.646ns (47.744%)  route 2.896ns (52.256%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=14, routed)          1.071     1.658    A_IBUF[5]
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_7/I0
    SLICE_X1Y62          LUT4 (Prop_lut4_I0_O)        0.053     1.711 r  O_OBUF[11]_inst_i_7/O
                         net (fo=3, routed)           0.396     2.107    N_1186
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X2Y63          LUT6 (Prop_lut6_I3_O)        0.131     2.238 r  O_OBUF[9]_inst_i_4/O
                         net (fo=1, routed)           0.152     2.390    N_1350
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_1/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.433 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.278     3.711    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     5.542 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.542    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[7]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.460ns  (logic 2.505ns (45.881%)  route 2.955ns (54.119%))
  Logic Levels:           5  (IBUF=1 LUT4=2 LUT5=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AJ25                                              0.000     0.000 r  B[7] (IN)
                         net (fo=0)                   0.000     0.000    B[7]
    AJ25                                                              r  B_IBUF[7]_inst/I
    AJ25                 IBUF (Prop_ibuf_I_O)         0.554     0.554 r  B_IBUF[7]_inst/O
                         net (fo=19, routed)          1.153     1.707    B_IBUF[7]
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.043     1.750 f  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.328     2.078    N_52
    SLICE_X0Y63                                                       f  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y63          LUT5 (Prop_lut5_I3_O)        0.043     2.121 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.425     2.546    O_OBUF[1]
    SLICE_X0Y65                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.043     2.589 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.050     3.638    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.460 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.460    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.385ns  (logic 2.584ns (47.987%)  route 2.801ns (52.013%))
  Logic Levels:           4  (IBUF=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=14, routed)          1.120     1.709    A_IBUF[4]
    SLICE_X1Y62                                                       r  O_OBUF[9]_inst_i_2/I3
    SLICE_X1Y62          LUT5 (Prop_lut5_I3_O)        0.050     1.759 r  O_OBUF[9]_inst_i_2/O
                         net (fo=2, routed)           0.412     2.171    N_1172
    SLICE_X1Y61                                                       r  O_OBUF[8]_inst_i_1/I4
    SLICE_X1Y61          LUT6 (Prop_lut6_I4_O)        0.126     2.297 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.270     3.567    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.385 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.385    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------




