Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 03:30:34 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_282_evolib.txt -name O
| Design       : mul8_282
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.880ns  (logic 2.783ns (35.323%)  route 5.096ns (64.677%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.156     1.750    A_IBUF[3]
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_18/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.793 r  O_OBUF[7]_inst_i_18/O
                         net (fo=2, routed)           0.427     2.221    O_OBUF[7]_inst_i_18_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_14/I2
    SLICE_X2Y60          LUT3 (Prop_lut3_I2_O)        0.043     2.264 r  O_OBUF[7]_inst_i_14/O
                         net (fo=4, routed)           0.471     2.735    O_OBUF[7]_inst_i_14_n_0
    SLICE_X4Y62                                                       r  O_OBUF[14]_inst_i_5/I3
    SLICE_X4Y62          LUT5 (Prop_lut5_I3_O)        0.050     2.785 r  O_OBUF[14]_inst_i_5/O
                         net (fo=4, routed)           0.576     3.361    O_OBUF[14]_inst_i_5_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_4/I2
    SLICE_X5Y63          LUT6 (Prop_lut6_I2_O)        0.126     3.487 f  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.518     4.005    O_OBUF[9]_inst_i_4_n_0
    SLICE_X6Y63                                                       f  O_OBUF[13]_inst_i_3/I3
    SLICE_X6Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.048 r  O_OBUF[13]_inst_i_3/O
                         net (fo=2, routed)           0.681     4.729    O_OBUF[13]_inst_i_3_n_0
    SLICE_X3Y65                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X3Y65          LUT2 (Prop_lut2_I1_O)        0.043     4.772 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.267     6.039    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.880 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.880    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.866ns  (logic 2.877ns (36.583%)  route 4.988ns (63.417%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.156     1.750    A_IBUF[3]
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_18/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.793 r  O_OBUF[7]_inst_i_18/O
                         net (fo=2, routed)           0.427     2.221    O_OBUF[7]_inst_i_18_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_14/I2
    SLICE_X2Y60          LUT3 (Prop_lut3_I2_O)        0.043     2.264 r  O_OBUF[7]_inst_i_14/O
                         net (fo=4, routed)           0.471     2.735    O_OBUF[7]_inst_i_14_n_0
    SLICE_X4Y62                                                       r  O_OBUF[14]_inst_i_5/I3
    SLICE_X4Y62          LUT5 (Prop_lut5_I3_O)        0.050     2.785 r  O_OBUF[14]_inst_i_5/O
                         net (fo=4, routed)           0.576     3.361    O_OBUF[14]_inst_i_5_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_4/I2
    SLICE_X5Y63          LUT6 (Prop_lut6_I2_O)        0.126     3.487 f  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.520     4.007    O_OBUF[9]_inst_i_4_n_0
    SLICE_X6Y63                                                       f  O_OBUF[15]_inst_i_3/I0
    SLICE_X6Y63          LUT3 (Prop_lut3_I0_O)        0.043     4.050 f  O_OBUF[15]_inst_i_3/O
                         net (fo=4, routed)           0.418     4.468    O_OBUF[15]_inst_i_3_n_0
    SLICE_X2Y65                                                       f  O_OBUF[15]_inst_i_1/I1
    SLICE_X2Y65          LUT6 (Prop_lut6_I1_O)        0.127     4.595 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.419     6.014    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.866 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.866    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.816ns  (logic 2.868ns (36.693%)  route 4.948ns (63.307%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.156     1.750    A_IBUF[3]
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_18/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.793 r  O_OBUF[7]_inst_i_18/O
                         net (fo=2, routed)           0.427     2.221    O_OBUF[7]_inst_i_18_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_14/I2
    SLICE_X2Y60          LUT3 (Prop_lut3_I2_O)        0.043     2.264 r  O_OBUF[7]_inst_i_14/O
                         net (fo=4, routed)           0.471     2.735    O_OBUF[7]_inst_i_14_n_0
    SLICE_X4Y62                                                       r  O_OBUF[14]_inst_i_5/I3
    SLICE_X4Y62          LUT5 (Prop_lut5_I3_O)        0.050     2.785 r  O_OBUF[14]_inst_i_5/O
                         net (fo=4, routed)           0.576     3.361    O_OBUF[14]_inst_i_5_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_4/I2
    SLICE_X5Y63          LUT6 (Prop_lut6_I2_O)        0.126     3.487 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.520     4.007    O_OBUF[9]_inst_i_4_n_0
    SLICE_X6Y63                                                       r  O_OBUF[15]_inst_i_3/I0
    SLICE_X6Y63          LUT3 (Prop_lut3_I0_O)        0.043     4.050 r  O_OBUF[15]_inst_i_3/O
                         net (fo=4, routed)           0.522     4.572    O_OBUF[15]_inst_i_3_n_0
    SLICE_X3Y65                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X3Y65          LUT6 (Prop_lut6_I4_O)        0.127     4.699 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.275     5.974    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.816 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.816    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.724ns  (logic 2.856ns (36.983%)  route 4.867ns (63.017%))
  Logic Levels:           8  (IBUF=1 LUT3=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.156     1.750    A_IBUF[3]
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_18/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.793 r  O_OBUF[7]_inst_i_18/O
                         net (fo=2, routed)           0.427     2.221    O_OBUF[7]_inst_i_18_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_14/I2
    SLICE_X2Y60          LUT3 (Prop_lut3_I2_O)        0.043     2.264 r  O_OBUF[7]_inst_i_14/O
                         net (fo=4, routed)           0.471     2.735    O_OBUF[7]_inst_i_14_n_0
    SLICE_X4Y62                                                       r  O_OBUF[14]_inst_i_5/I3
    SLICE_X4Y62          LUT5 (Prop_lut5_I3_O)        0.050     2.785 r  O_OBUF[14]_inst_i_5/O
                         net (fo=4, routed)           0.576     3.361    O_OBUF[14]_inst_i_5_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_4/I2
    SLICE_X5Y63          LUT6 (Prop_lut6_I2_O)        0.126     3.487 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.520     4.007    O_OBUF[9]_inst_i_4_n_0
    SLICE_X6Y63                                                       r  O_OBUF[15]_inst_i_3/I0
    SLICE_X6Y63          LUT3 (Prop_lut3_I0_O)        0.043     4.050 r  O_OBUF[15]_inst_i_3/O
                         net (fo=4, routed)           0.418     4.468    O_OBUF[15]_inst_i_3_n_0
    SLICE_X2Y65                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X2Y65          LUT3 (Prop_lut3_I1_O)        0.127     4.595 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.298     5.893    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.724 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.724    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.693ns  (logic 2.862ns (37.198%)  route 4.832ns (62.802%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.156     1.750    A_IBUF[3]
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_18/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.793 r  O_OBUF[7]_inst_i_18/O
                         net (fo=2, routed)           0.427     2.221    O_OBUF[7]_inst_i_18_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_14/I2
    SLICE_X2Y60          LUT3 (Prop_lut3_I2_O)        0.043     2.264 r  O_OBUF[7]_inst_i_14/O
                         net (fo=4, routed)           0.471     2.735    O_OBUF[7]_inst_i_14_n_0
    SLICE_X4Y62                                                       r  O_OBUF[14]_inst_i_5/I3
    SLICE_X4Y62          LUT5 (Prop_lut5_I3_O)        0.050     2.785 r  O_OBUF[14]_inst_i_5/O
                         net (fo=4, routed)           0.576     3.361    O_OBUF[14]_inst_i_5_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_4/I2
    SLICE_X5Y63          LUT6 (Prop_lut6_I2_O)        0.126     3.487 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.520     4.007    O_OBUF[9]_inst_i_4_n_0
    SLICE_X6Y63                                                       r  O_OBUF[15]_inst_i_3/I0
    SLICE_X6Y63          LUT3 (Prop_lut3_I0_O)        0.043     4.050 r  O_OBUF[15]_inst_i_3/O
                         net (fo=4, routed)           0.284     4.334    O_OBUF[15]_inst_i_3_n_0
    SLICE_X2Y65                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X2Y65          LUT6 (Prop_lut6_I5_O)        0.127     4.461 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.396     5.858    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.693 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.693    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.497ns  (logic 2.775ns (37.013%)  route 4.722ns (62.987%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.156     1.750    A_IBUF[3]
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_18/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.793 r  O_OBUF[7]_inst_i_18/O
                         net (fo=2, routed)           0.427     2.221    O_OBUF[7]_inst_i_18_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_14/I2
    SLICE_X2Y60          LUT3 (Prop_lut3_I2_O)        0.043     2.264 r  O_OBUF[7]_inst_i_14/O
                         net (fo=4, routed)           0.471     2.735    O_OBUF[7]_inst_i_14_n_0
    SLICE_X4Y62                                                       r  O_OBUF[14]_inst_i_5/I3
    SLICE_X4Y62          LUT5 (Prop_lut5_I3_O)        0.050     2.785 r  O_OBUF[14]_inst_i_5/O
                         net (fo=4, routed)           0.576     3.361    O_OBUF[14]_inst_i_5_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_4/I2
    SLICE_X5Y63          LUT6 (Prop_lut6_I2_O)        0.126     3.487 f  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.518     4.005    O_OBUF[9]_inst_i_4_n_0
    SLICE_X6Y63                                                       f  O_OBUF[13]_inst_i_3/I3
    SLICE_X6Y63          LUT6 (Prop_lut6_I3_O)        0.043     4.048 r  O_OBUF[13]_inst_i_3/O
                         net (fo=2, routed)           0.253     4.301    O_OBUF[13]_inst_i_3_n_0
    SLICE_X3Y64                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X3Y64          LUT6 (Prop_lut6_I1_O)        0.043     4.344 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.321     5.664    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.497 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.497    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.197ns  (logic 2.731ns (37.942%)  route 4.466ns (62.058%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.156     1.750    A_IBUF[3]
    SLICE_X2Y61                                                       r  O_OBUF[7]_inst_i_18/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.793 r  O_OBUF[7]_inst_i_18/O
                         net (fo=2, routed)           0.427     2.221    O_OBUF[7]_inst_i_18_n_0
    SLICE_X2Y60                                                       r  O_OBUF[7]_inst_i_14/I2
    SLICE_X2Y60          LUT3 (Prop_lut3_I2_O)        0.043     2.264 r  O_OBUF[7]_inst_i_14/O
                         net (fo=4, routed)           0.471     2.735    O_OBUF[7]_inst_i_14_n_0
    SLICE_X4Y62                                                       r  O_OBUF[14]_inst_i_5/I3
    SLICE_X4Y62          LUT5 (Prop_lut5_I3_O)        0.050     2.785 r  O_OBUF[14]_inst_i_5/O
                         net (fo=4, routed)           0.576     3.361    O_OBUF[14]_inst_i_5_n_0
    SLICE_X5Y63                                                       r  O_OBUF[9]_inst_i_4/I2
    SLICE_X5Y63          LUT6 (Prop_lut6_I2_O)        0.126     3.487 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.520     4.007    O_OBUF[9]_inst_i_4_n_0
    SLICE_X6Y63                                                       r  O_OBUF[9]_inst_i_1/I2
    SLICE_X6Y63          LUT4 (Prop_lut4_I2_O)        0.043     4.050 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.315     5.365    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.197 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.197    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.935ns  (logic 2.728ns (39.332%)  route 4.207ns (60.668%))
  Logic Levels:           7  (IBUF=1 LUT2=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.230     1.826    A_IBUF[2]
    SLICE_X4Y61                                                       r  O_OBUF[7]_inst_i_13/I4
    SLICE_X4Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.869 r  O_OBUF[7]_inst_i_13/O
                         net (fo=4, routed)           0.302     2.171    O_OBUF[7]_inst_i_13_n_0
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X3Y61          LUT3 (Prop_lut3_I1_O)        0.043     2.214 r  O_OBUF[7]_inst_i_8/O
                         net (fo=4, routed)           0.518     2.732    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_6/I0
    SLICE_X3Y62          LUT5 (Prop_lut5_I0_O)        0.053     2.785 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.448     3.234    O_OBUF[7]_inst_i_6_n_0
    SLICE_X6Y62                                                       r  O_OBUF[9]_inst_i_2/I3
    SLICE_X6Y62          LUT6 (Prop_lut6_I3_O)        0.131     3.365 r  O_OBUF[9]_inst_i_2/O
                         net (fo=4, routed)           0.443     3.807    O_OBUF[9]_inst_i_2_n_0
    SLICE_X6Y63                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X6Y63          LUT2 (Prop_lut2_I0_O)        0.043     3.850 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.266     5.116    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     6.935 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.935    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.439ns  (logic 2.633ns (40.894%)  route 3.806ns (59.106%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.148     1.744    A_IBUF[2]
    SLICE_X2Y60                                                       r  O_OBUF[6]_inst_i_11/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.043     1.787 r  O_OBUF[6]_inst_i_11/O
                         net (fo=2, routed)           0.416     2.202    O_OBUF[6]_inst_i_11_n_0
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_5/I2
    SLICE_X2Y61          LUT3 (Prop_lut3_I2_O)        0.043     2.245 r  O_OBUF[6]_inst_i_5/O
                         net (fo=4, routed)           0.237     2.483    O_OBUF[6]_inst_i_5_n_0
    SLICE_X0Y62                                                       r  O_OBUF[5]_inst_i_2/I0
    SLICE_X0Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.526 f  O_OBUF[5]_inst_i_2/O
                         net (fo=3, routed)           0.220     2.746    O_OBUF[5]_inst_i_2_n_0
    SLICE_X1Y61                                                       f  O_OBUF[6]_inst_i_6/I1
    SLICE_X1Y61          LUT6 (Prop_lut6_I1_O)        0.043     2.789 r  O_OBUF[6]_inst_i_6/O
                         net (fo=1, routed)           0.605     3.394    O_OBUF[6]_inst_i_6_n_0
    SLICE_X1Y62                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.437 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.180     4.617    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.439 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.439    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.364ns  (logic 2.627ns (41.283%)  route 3.737ns (58.717%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.230     1.826    A_IBUF[2]
    SLICE_X4Y61                                                       r  O_OBUF[7]_inst_i_13/I4
    SLICE_X4Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.869 r  O_OBUF[7]_inst_i_13/O
                         net (fo=4, routed)           0.302     2.171    O_OBUF[7]_inst_i_13_n_0
    SLICE_X3Y61                                                       r  O_OBUF[7]_inst_i_8/I1
    SLICE_X3Y61          LUT3 (Prop_lut3_I1_O)        0.043     2.214 r  O_OBUF[7]_inst_i_8/O
                         net (fo=4, routed)           0.518     2.732    O_OBUF[7]_inst_i_8_n_0
    SLICE_X3Y62                                                       r  O_OBUF[7]_inst_i_2/I0
    SLICE_X3Y62          LUT5 (Prop_lut5_I0_O)        0.043     2.775 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.420     3.195    O_OBUF[7]_inst_i_2_n_0
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_4/I4
    SLICE_X1Y62          LUT6 (Prop_lut6_I4_O)        0.043     3.238 r  O_OBUF[7]_inst_i_4/O
                         net (fo=1, routed)           0.089     3.327    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y62                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X1Y62          LUT6 (Prop_lut6_I2_O)        0.043     3.370 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.177     4.547    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.364 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.364    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




