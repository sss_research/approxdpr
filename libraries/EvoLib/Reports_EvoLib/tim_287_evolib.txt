Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 03:45:12 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_287_evolib.txt -name O
| Design       : mul8_287
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.247ns  (logic 2.643ns (36.474%)  route 4.604ns (63.526%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.398     1.984    A_IBUF[6]
    SLICE_X1Y67                                                       r  O_OBUF[13]_inst_i_8/I5
    SLICE_X1Y67          LUT6 (Prop_lut6_I5_O)        0.043     2.027 r  O_OBUF[13]_inst_i_8/O
                         net (fo=3, routed)           0.422     2.449    O_OBUF[13]_inst_i_8_n_0
    SLICE_X0Y68                                                       r  O_OBUF[7]_inst_i_4/I1
    SLICE_X0Y68          LUT6 (Prop_lut6_I1_O)        0.043     2.492 r  O_OBUF[7]_inst_i_4/O
                         net (fo=2, routed)           0.507     2.998    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X1Y64          LUT3 (Prop_lut3_I2_O)        0.043     3.041 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           0.591     3.633    O_OBUF[7]
    SLICE_X3Y65                                                       r  O_OBUF[13]_inst_i_5/I0
    SLICE_X3Y65          LUT5 (Prop_lut5_I0_O)        0.043     3.676 r  O_OBUF[13]_inst_i_5/O
                         net (fo=2, routed)           0.416     4.092    O_OBUF[13]_inst_i_5_n_0
    SLICE_X3Y66                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X3Y66          LUT3 (Prop_lut3_I2_O)        0.043     4.135 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.270     5.405    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.247 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.247    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.243ns  (logic 2.731ns (37.707%)  route 4.512ns (62.293%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.398     1.984    A_IBUF[6]
    SLICE_X1Y67                                                       r  O_OBUF[13]_inst_i_8/I5
    SLICE_X1Y67          LUT6 (Prop_lut6_I5_O)        0.043     2.027 r  O_OBUF[13]_inst_i_8/O
                         net (fo=3, routed)           0.422     2.449    O_OBUF[13]_inst_i_8_n_0
    SLICE_X0Y68                                                       r  O_OBUF[7]_inst_i_4/I1
    SLICE_X0Y68          LUT6 (Prop_lut6_I1_O)        0.043     2.492 r  O_OBUF[7]_inst_i_4/O
                         net (fo=2, routed)           0.507     2.998    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_6/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.051     3.049 r  O_OBUF[13]_inst_i_6/O
                         net (fo=3, routed)           0.612     3.661    O_OBUF[13]_inst_i_6_n_0
    SLICE_X2Y66                                                       r  O_OBUF[15]_inst_i_3/I5
    SLICE_X2Y66          LUT6 (Prop_lut6_I5_O)        0.129     3.790 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.294     4.084    O_OBUF[15]_inst_i_3_n_0
    SLICE_X0Y66                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y66          LUT6 (Prop_lut6_I4_O)        0.043     4.127 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.281     5.408    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.243 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.243    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.223ns  (logic 2.747ns (38.033%)  route 4.476ns (61.967%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.398     1.984    A_IBUF[6]
    SLICE_X1Y67                                                       r  O_OBUF[13]_inst_i_8/I5
    SLICE_X1Y67          LUT6 (Prop_lut6_I5_O)        0.043     2.027 r  O_OBUF[13]_inst_i_8/O
                         net (fo=3, routed)           0.422     2.449    O_OBUF[13]_inst_i_8_n_0
    SLICE_X0Y68                                                       r  O_OBUF[7]_inst_i_4/I1
    SLICE_X0Y68          LUT6 (Prop_lut6_I1_O)        0.043     2.492 r  O_OBUF[7]_inst_i_4/O
                         net (fo=2, routed)           0.507     2.998    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[13]_inst_i_6/I0
    SLICE_X1Y64          LUT3 (Prop_lut3_I0_O)        0.051     3.049 r  O_OBUF[13]_inst_i_6/O
                         net (fo=3, routed)           0.612     3.661    O_OBUF[13]_inst_i_6_n_0
    SLICE_X2Y66                                                       r  O_OBUF[15]_inst_i_3/I5
    SLICE_X2Y66          LUT6 (Prop_lut6_I5_O)        0.129     3.790 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.168     3.958    O_OBUF[15]_inst_i_3_n_0
    SLICE_X0Y66                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y66          LUT6 (Prop_lut6_I1_O)        0.043     4.001 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.371     5.371    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.223 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.223    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.045ns  (logic 2.634ns (37.391%)  route 4.411ns (62.609%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.398     1.984    A_IBUF[6]
    SLICE_X1Y67                                                       r  O_OBUF[13]_inst_i_8/I5
    SLICE_X1Y67          LUT6 (Prop_lut6_I5_O)        0.043     2.027 r  O_OBUF[13]_inst_i_8/O
                         net (fo=3, routed)           0.422     2.449    O_OBUF[13]_inst_i_8_n_0
    SLICE_X0Y68                                                       r  O_OBUF[7]_inst_i_4/I1
    SLICE_X0Y68          LUT6 (Prop_lut6_I1_O)        0.043     2.492 r  O_OBUF[7]_inst_i_4/O
                         net (fo=2, routed)           0.507     2.998    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X1Y64          LUT3 (Prop_lut3_I2_O)        0.043     3.041 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           0.591     3.633    O_OBUF[7]
    SLICE_X3Y65                                                       r  O_OBUF[13]_inst_i_5/I0
    SLICE_X3Y65          LUT5 (Prop_lut5_I0_O)        0.043     3.676 r  O_OBUF[13]_inst_i_5/O
                         net (fo=2, routed)           0.224     3.900    O_OBUF[13]_inst_i_5_n_0
    SLICE_X1Y66                                                       r  O_OBUF[13]_inst_i_1/I4
    SLICE_X1Y66          LUT6 (Prop_lut6_I4_O)        0.043     3.943 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.270     5.213    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.045 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.045    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.750ns  (logic 2.600ns (38.518%)  route 4.150ns (61.482%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.398     1.984    A_IBUF[6]
    SLICE_X1Y67                                                       r  O_OBUF[13]_inst_i_8/I5
    SLICE_X1Y67          LUT6 (Prop_lut6_I5_O)        0.043     2.027 r  O_OBUF[13]_inst_i_8/O
                         net (fo=3, routed)           0.422     2.449    O_OBUF[13]_inst_i_8_n_0
    SLICE_X0Y68                                                       r  O_OBUF[7]_inst_i_4/I1
    SLICE_X0Y68          LUT6 (Prop_lut6_I1_O)        0.043     2.492 r  O_OBUF[7]_inst_i_4/O
                         net (fo=2, routed)           0.507     2.998    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X1Y64          LUT3 (Prop_lut3_I2_O)        0.043     3.041 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           0.533     3.575    O_OBUF[7]
    SLICE_X2Y65                                                       r  O_OBUF[11]_inst_i_1/I4
    SLICE_X2Y65          LUT6 (Prop_lut6_I4_O)        0.043     3.618 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.291     4.909    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.750 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.750    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.713ns  (logic 2.874ns (42.812%)  route 3.839ns (57.188%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.355     1.944    A_IBUF[4]
    SLICE_X2Y64                                                       r  O_OBUF[11]_inst_i_10/I2
    SLICE_X2Y64          LUT6 (Prop_lut6_I2_O)        0.043     1.987 r  O_OBUF[11]_inst_i_10/O
                         net (fo=5, routed)           0.347     2.335    O_OBUF[11]_inst_i_10_n_0
    SLICE_X2Y65                                                       r  O_OBUF[11]_inst_i_11/I2
    SLICE_X2Y65          LUT3 (Prop_lut3_I2_O)        0.049     2.384 r  O_OBUF[11]_inst_i_11/O
                         net (fo=3, routed)           0.436     2.820    O_OBUF[11]_inst_i_11_n_0
    SLICE_X3Y65                                                       r  O_OBUF[11]_inst_i_5/I0
    SLICE_X3Y65          LUT5 (Prop_lut5_I0_O)        0.137     2.957 r  O_OBUF[11]_inst_i_5/O
                         net (fo=3, routed)           0.424     3.382    O_OBUF[11]_inst_i_5_n_0
    SLICE_X2Y65                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X2Y65          LUT4 (Prop_lut4_I0_O)        0.136     3.518 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.276     4.793    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.919     6.713 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.713    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.458ns  (logic 2.681ns (41.517%)  route 3.777ns (58.483%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=25, routed)          1.312     1.892    A_IBUF[7]
    SLICE_X0Y64                                                       r  O_OBUF[8]_inst_i_8/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     1.935 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.418     2.353    O_OBUF[8]_inst_i_8_n_0
    SLICE_X1Y65                                                       r  O_OBUF[8]_inst_i_5/I5
    SLICE_X1Y65          LUT6 (Prop_lut6_I5_O)        0.043     2.396 r  O_OBUF[8]_inst_i_5/O
                         net (fo=3, routed)           0.420     2.815    O_OBUF[8]_inst_i_5_n_0
    SLICE_X1Y66                                                       r  O_OBUF[11]_inst_i_2/I0
    SLICE_X1Y66          LUT3 (Prop_lut3_I0_O)        0.053     2.868 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.437     3.305    O_OBUF[11]_inst_i_2_n_0
    SLICE_X2Y65                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X2Y65          LUT3 (Prop_lut3_I1_O)        0.131     3.436 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.190     4.626    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.458 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.458    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.037ns  (logic 2.532ns (41.939%)  route 3.505ns (58.061%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=20, routed)          1.398     1.984    A_IBUF[6]
    SLICE_X1Y67                                                       r  O_OBUF[13]_inst_i_8/I5
    SLICE_X1Y67          LUT6 (Prop_lut6_I5_O)        0.043     2.027 r  O_OBUF[13]_inst_i_8/O
                         net (fo=3, routed)           0.422     2.449    O_OBUF[13]_inst_i_8_n_0
    SLICE_X0Y68                                                       r  O_OBUF[7]_inst_i_4/I1
    SLICE_X0Y68          LUT6 (Prop_lut6_I1_O)        0.043     2.492 r  O_OBUF[7]_inst_i_4/O
                         net (fo=2, routed)           0.507     2.998    O_OBUF[7]_inst_i_4_n_0
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X1Y64          LUT3 (Prop_lut3_I2_O)        0.043     3.041 r  O_OBUF[7]_inst_i_1/O
                         net (fo=4, routed)           1.180     4.221    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.037 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.037    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.814ns  (logic 2.527ns (43.468%)  route 3.287ns (56.532%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=25, routed)          1.312     1.892    A_IBUF[7]
    SLICE_X0Y64                                                       r  O_OBUF[8]_inst_i_8/I4
    SLICE_X0Y64          LUT6 (Prop_lut6_I4_O)        0.043     1.935 r  O_OBUF[8]_inst_i_8/O
                         net (fo=3, routed)           0.418     2.353    O_OBUF[8]_inst_i_8_n_0
    SLICE_X1Y65                                                       r  O_OBUF[8]_inst_i_5/I5
    SLICE_X1Y65          LUT6 (Prop_lut6_I5_O)        0.043     2.396 r  O_OBUF[8]_inst_i_5/O
                         net (fo=3, routed)           0.420     2.815    O_OBUF[8]_inst_i_5_n_0
    SLICE_X1Y66                                                       r  O_OBUF[8]_inst_i_1/I3
    SLICE_X1Y66          LUT4 (Prop_lut4_I3_O)        0.043     2.858 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.137     3.995    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     5.814 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     5.814    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[1]
                            (input port)
  Destination:            O[2]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.236ns  (logic 2.584ns (49.354%)  route 2.652ns (50.646%))
  Logic Levels:           4  (IBUF=1 LUT2=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP29                                              0.000     0.000 f  B[1] (IN)
                         net (fo=0)                   0.000     0.000    B[1]
    AP29                                                              f  B_IBUF[1]_inst/I
    AP29                 IBUF (Prop_ibuf_I_O)         0.605     0.605 f  B_IBUF[1]_inst/O
                         net (fo=10, routed)          1.330     1.935    B_IBUF[1]
    SLICE_X1Y67                                                       f  O_OBUF[2]_inst_i_2/I1
    SLICE_X1Y67          LUT2 (Prop_lut2_I1_O)        0.053     1.988 r  O_OBUF[2]_inst_i_2/O
                         net (fo=2, routed)           0.260     2.248    O_OBUF[2]_inst_i_2_n_0
    SLICE_X0Y67                                                       r  O_OBUF[2]_inst_i_1/I0
    SLICE_X0Y67          LUT6 (Prop_lut6_I0_O)        0.131     2.379 r  O_OBUF[2]_inst_i_1/O
                         net (fo=1, routed)           1.062     3.440    O_OBUF[2]
    AG25                                                              r  O_OBUF[2]_inst/I
    AG25                 OBUF (Prop_obuf_I_O)         1.795     5.236 r  O_OBUF[2]_inst/O
                         net (fo=0)                   0.000     5.236    O[2]
    AG25                                                              r  O[2] (OUT)
  -------------------------------------------------------------------    -------------------




