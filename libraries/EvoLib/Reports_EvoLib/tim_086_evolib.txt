Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 17:53:49 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_086_evolib.txt -name O
| Design       : mul8_086
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.658ns  (logic 3.096ns (35.758%)  route 5.562ns (64.242%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.471     2.065    A_IBUF[3]
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_13/I2
    SLICE_X2Y65          LUT6 (Prop_lut6_I2_O)        0.043     2.108 r  O_OBUF[12]_inst_i_13/O
                         net (fo=2, routed)           0.522     2.630    N_509
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_18/I0
    SLICE_X2Y65          LUT5 (Prop_lut5_I0_O)        0.043     2.673 r  O_OBUF[12]_inst_i_18/O
                         net (fo=2, routed)           0.343     3.016    N_608
    SLICE_X3Y65                                                       r  O_OBUF[12]_inst_i_9/I4
    SLICE_X3Y65          LUT5 (Prop_lut5_I4_O)        0.133     3.149 r  O_OBUF[12]_inst_i_9/O
                         net (fo=6, routed)           0.529     3.679    N_685
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X2Y66          LUT4 (Prop_lut4_I2_O)        0.134     3.813 r  O_OBUF[12]_inst_i_4/O
                         net (fo=9, routed)           0.564     4.377    N_834
    SLICE_X5Y66                                                       r  O_OBUF[11]_inst_i_2/I5
    SLICE_X5Y66          LUT6 (Prop_lut6_I5_O)        0.132     4.509 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.431     4.940    O_OBUF[11]_inst_i_2_n_0
    SLICE_X5Y66                                                       r  O_OBUF[14]_inst_i_5/I0
    SLICE_X5Y66          LUT4 (Prop_lut4_I0_O)        0.053     4.993 r  O_OBUF[14]_inst_i_5/O
                         net (fo=2, routed)           0.374     5.368    O_OBUF[14]_inst_i_5_n_0
    SLICE_X5Y65                                                       r  O_OBUF[13]_inst_i_1/I3
    SLICE_X5Y65          LUT6 (Prop_lut6_I3_O)        0.131     5.499 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.326     6.825    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.658 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.658    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.588ns  (logic 3.016ns (35.125%)  route 5.571ns (64.875%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.471     2.065    A_IBUF[3]
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_13/I2
    SLICE_X2Y65          LUT6 (Prop_lut6_I2_O)        0.043     2.108 r  O_OBUF[12]_inst_i_13/O
                         net (fo=2, routed)           0.522     2.630    N_509
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_18/I0
    SLICE_X2Y65          LUT5 (Prop_lut5_I0_O)        0.043     2.673 r  O_OBUF[12]_inst_i_18/O
                         net (fo=2, routed)           0.343     3.016    N_608
    SLICE_X3Y65                                                       r  O_OBUF[12]_inst_i_9/I4
    SLICE_X3Y65          LUT5 (Prop_lut5_I4_O)        0.133     3.149 r  O_OBUF[12]_inst_i_9/O
                         net (fo=6, routed)           0.529     3.679    N_685
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X2Y66          LUT4 (Prop_lut4_I2_O)        0.134     3.813 r  O_OBUF[12]_inst_i_4/O
                         net (fo=9, routed)           0.564     4.377    N_834
    SLICE_X5Y66                                                       r  O_OBUF[11]_inst_i_2/I5
    SLICE_X5Y66          LUT6 (Prop_lut6_I5_O)        0.132     4.509 r  O_OBUF[11]_inst_i_2/O
                         net (fo=3, routed)           0.431     4.940    O_OBUF[11]_inst_i_2_n_0
    SLICE_X5Y66                                                       r  O_OBUF[15]_inst_i_6/I0
    SLICE_X5Y66          LUT5 (Prop_lut5_I0_O)        0.043     4.983 r  O_OBUF[15]_inst_i_6/O
                         net (fo=1, routed)           0.409     5.393    O_OBUF[15]_inst_i_6_n_0
    SLICE_X5Y67                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X5Y67          LUT6 (Prop_lut6_I4_O)        0.043     5.436 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.301     6.737    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.588 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.588    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.558ns  (logic 3.000ns (35.050%)  route 5.558ns (64.950%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT4=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=22, routed)          1.550     2.136    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[6]_inst_i_14/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.179 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.228     2.407    N_502
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.047     2.454 r  O_OBUF[10]_inst_i_11/O
                         net (fo=2, routed)           0.634     3.088    N_571
    SLICE_X3Y66                                                       r  O_OBUF[9]_inst_i_9/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.132     3.220 r  O_OBUF[9]_inst_i_9/O
                         net (fo=5, routed)           0.548     3.768    N_676
    SLICE_X6Y66                                                       r  O_OBUF[9]_inst_i_2/I3
    SLICE_X6Y66          LUT4 (Prop_lut4_I3_O)        0.129     3.897 r  O_OBUF[9]_inst_i_2/O
                         net (fo=6, routed)           0.523     4.420    N_814
    SLICE_X3Y67                                                       r  O_OBUF[15]_inst_i_9/I0
    SLICE_X3Y67          LUT4 (Prop_lut4_I0_O)        0.053     4.473 r  O_OBUF[15]_inst_i_9/O
                         net (fo=2, routed)           0.299     4.772    N_1060
    SLICE_X5Y67                                                       r  O_OBUF[14]_inst_i_6/I4
    SLICE_X5Y67          LUT6 (Prop_lut6_I4_O)        0.131     4.903 r  O_OBUF[14]_inst_i_6/O
                         net (fo=1, routed)           0.411     5.314    O_OBUF[14]_inst_i_6_n_0
    SLICE_X5Y66                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X5Y66          LUT6 (Prop_lut6_I5_O)        0.043     5.357 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.365     6.722    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.558 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.558    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.277ns  (logic 2.963ns (35.802%)  route 5.314ns (64.198%))
  Logic Levels:           8  (IBUF=1 LUT4=1 LUT5=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=16, routed)          1.471     2.065    A_IBUF[3]
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_13/I2
    SLICE_X2Y65          LUT6 (Prop_lut6_I2_O)        0.043     2.108 r  O_OBUF[12]_inst_i_13/O
                         net (fo=2, routed)           0.522     2.630    N_509
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_18/I0
    SLICE_X2Y65          LUT5 (Prop_lut5_I0_O)        0.043     2.673 r  O_OBUF[12]_inst_i_18/O
                         net (fo=2, routed)           0.343     3.016    N_608
    SLICE_X3Y65                                                       r  O_OBUF[12]_inst_i_9/I4
    SLICE_X3Y65          LUT5 (Prop_lut5_I4_O)        0.133     3.149 r  O_OBUF[12]_inst_i_9/O
                         net (fo=6, routed)           0.529     3.679    N_685
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_4/I2
    SLICE_X2Y66          LUT4 (Prop_lut4_I2_O)        0.134     3.813 r  O_OBUF[12]_inst_i_4/O
                         net (fo=9, routed)           0.779     4.592    N_834
    SLICE_X6Y66                                                       r  O_OBUF[14]_inst_i_2/I2
    SLICE_X6Y66          LUT5 (Prop_lut5_I2_O)        0.132     4.724 r  O_OBUF[14]_inst_i_2/O
                         net (fo=3, routed)           0.411     5.135    N_1134
    SLICE_X4Y66                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X4Y66          LUT6 (Prop_lut6_I1_O)        0.043     5.178 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.258     6.436    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     8.277 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     8.277    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.092ns  (logic 3.041ns (37.578%)  route 5.051ns (62.422%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=22, routed)          1.550     2.136    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[6]_inst_i_14/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.179 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.228     2.407    N_502
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.047     2.454 r  O_OBUF[10]_inst_i_11/O
                         net (fo=2, routed)           0.634     3.088    N_571
    SLICE_X3Y66                                                       r  O_OBUF[9]_inst_i_9/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.132     3.220 r  O_OBUF[9]_inst_i_9/O
                         net (fo=5, routed)           0.548     3.768    N_676
    SLICE_X6Y66                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X6Y66          LUT3 (Prop_lut3_I0_O)        0.129     3.897 r  O_OBUF[12]_inst_i_12/O
                         net (fo=3, routed)           0.416     4.313    N_746
    SLICE_X3Y67                                                       r  O_OBUF[12]_inst_i_6/I4
    SLICE_X3Y67          LUT5 (Prop_lut5_I4_O)        0.133     4.446 r  O_OBUF[12]_inst_i_6/O
                         net (fo=1, routed)           0.419     4.865    N_1146
    SLICE_X4Y67                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X4Y67          LUT6 (Prop_lut6_I4_O)        0.129     4.994 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.256     6.250    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.092 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.092    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.667ns  (logic 2.938ns (38.325%)  route 4.729ns (61.675%))
  Logic Levels:           8  (IBUF=1 LUT3=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=22, routed)          1.550     2.136    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[6]_inst_i_14/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.179 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.228     2.407    N_502
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.047     2.454 r  O_OBUF[10]_inst_i_11/O
                         net (fo=2, routed)           0.634     3.088    N_571
    SLICE_X3Y66                                                       r  O_OBUF[9]_inst_i_9/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.132     3.220 r  O_OBUF[9]_inst_i_9/O
                         net (fo=5, routed)           0.548     3.768    N_676
    SLICE_X6Y66                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X6Y66          LUT3 (Prop_lut3_I0_O)        0.129     3.897 r  O_OBUF[12]_inst_i_12/O
                         net (fo=3, routed)           0.317     4.214    N_746
    SLICE_X2Y67                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X2Y67          LUT3 (Prop_lut3_I2_O)        0.127     4.341 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.221     4.562    O_OBUF[10]_inst_i_3_n_0
    SLICE_X3Y67                                                       r  O_OBUF[9]_inst_i_1/I2
    SLICE_X3Y67          LUT6 (Prop_lut6_I2_O)        0.043     4.605 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.230     5.836    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.667 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.667    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.648ns  (logic 2.937ns (38.405%)  route 4.711ns (61.595%))
  Logic Levels:           8  (IBUF=1 LUT3=3 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=22, routed)          1.550     2.136    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[6]_inst_i_14/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.179 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.228     2.407    N_502
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.047     2.454 r  O_OBUF[10]_inst_i_11/O
                         net (fo=2, routed)           0.634     3.088    N_571
    SLICE_X3Y66                                                       r  O_OBUF[9]_inst_i_9/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.132     3.220 r  O_OBUF[9]_inst_i_9/O
                         net (fo=5, routed)           0.548     3.768    N_676
    SLICE_X6Y66                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X6Y66          LUT3 (Prop_lut3_I0_O)        0.129     3.897 r  O_OBUF[12]_inst_i_12/O
                         net (fo=3, routed)           0.317     4.214    N_746
    SLICE_X2Y67                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X2Y67          LUT3 (Prop_lut3_I2_O)        0.127     4.341 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.222     4.563    O_OBUF[10]_inst_i_3_n_0
    SLICE_X3Y67                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X3Y67          LUT6 (Prop_lut6_I2_O)        0.043     4.606 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.211     5.818    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.648 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.648    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.585ns  (logic 2.926ns (38.572%)  route 4.659ns (61.428%))
  Logic Levels:           8  (IBUF=1 LUT3=3 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=22, routed)          1.550     2.136    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[6]_inst_i_14/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.179 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.228     2.407    N_502
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.047     2.454 r  O_OBUF[10]_inst_i_11/O
                         net (fo=2, routed)           0.634     3.088    N_571
    SLICE_X3Y66                                                       r  O_OBUF[9]_inst_i_9/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.132     3.220 r  O_OBUF[9]_inst_i_9/O
                         net (fo=5, routed)           0.548     3.768    N_676
    SLICE_X6Y66                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X6Y66          LUT3 (Prop_lut3_I0_O)        0.129     3.897 r  O_OBUF[12]_inst_i_12/O
                         net (fo=3, routed)           0.317     4.214    N_746
    SLICE_X2Y67                                                       r  O_OBUF[10]_inst_i_3/I2
    SLICE_X2Y67          LUT3 (Prop_lut3_I2_O)        0.127     4.341 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.253     4.594    O_OBUF[10]_inst_i_3_n_0
    SLICE_X3Y67                                                       r  O_OBUF[8]_inst_i_1/I2
    SLICE_X3Y67          LUT4 (Prop_lut4_I2_O)        0.043     4.637 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.130     5.766    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.585 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.585    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.400ns  (logic 2.887ns (39.020%)  route 4.512ns (60.980%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=22, routed)          1.550     2.136    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[6]_inst_i_14/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.179 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.228     2.407    N_502
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_11/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.047     2.454 r  O_OBUF[10]_inst_i_11/O
                         net (fo=2, routed)           0.634     3.088    N_571
    SLICE_X3Y66                                                       r  O_OBUF[9]_inst_i_9/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.132     3.220 r  O_OBUF[9]_inst_i_9/O
                         net (fo=5, routed)           0.548     3.768    N_676
    SLICE_X6Y66                                                       r  O_OBUF[9]_inst_i_2/I3
    SLICE_X6Y66          LUT4 (Prop_lut4_I3_O)        0.129     3.897 r  O_OBUF[9]_inst_i_2/O
                         net (fo=6, routed)           0.418     4.315    N_814
    SLICE_X2Y67                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X2Y67          LUT3 (Prop_lut3_I0_O)        0.050     4.365 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.134     5.499    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.900     7.400 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     7.400    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[4]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.626ns  (logic 2.581ns (38.945%)  route 4.046ns (61.055%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM26                                              0.000     0.000 r  B[4] (IN)
                         net (fo=0)                   0.000     0.000    B[4]
    AM26                                                              r  B_IBUF[4]_inst/I
    AM26                 IBUF (Prop_ibuf_I_O)         0.586     0.586 r  B_IBUF[4]_inst/O
                         net (fo=22, routed)          1.550     2.136    B_IBUF[4]
    SLICE_X2Y63                                                       r  O_OBUF[6]_inst_i_14/I5
    SLICE_X2Y63          LUT6 (Prop_lut6_I5_O)        0.043     2.179 r  O_OBUF[6]_inst_i_14/O
                         net (fo=2, routed)           0.228     2.407    N_502
    SLICE_X2Y63                                                       r  O_OBUF[6]_inst_i_12/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.450 r  O_OBUF[6]_inst_i_12/O
                         net (fo=4, routed)           0.626     3.076    N_570
    SLICE_X1Y66                                                       r  O_OBUF[6]_inst_i_4/I2
    SLICE_X1Y66          LUT6 (Prop_lut6_I2_O)        0.043     3.119 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.512     3.631    N_740
    SLICE_X0Y67                                                       r  O_OBUF[6]_inst_i_1/I2
    SLICE_X0Y67          LUT4 (Prop_lut4_I2_O)        0.043     3.674 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.129     4.804    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.626 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.626    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




