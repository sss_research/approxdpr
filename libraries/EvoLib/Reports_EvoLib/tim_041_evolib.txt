Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 15:38:08 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_041_evolib.txt -name O
| Design       : mul8_041
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.934ns  (logic 2.874ns (36.221%)  route 5.061ns (63.779%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.360     1.954    A_IBUF[3]
    SLICE_X6Y63                                                       r  O_OBUF[12]_inst_i_30/I5
    SLICE_X6Y63          LUT6 (Prop_lut6_I5_O)        0.043     1.997 r  O_OBUF[12]_inst_i_30/O
                         net (fo=3, routed)           0.522     2.519    O_OBUF[12]_inst_i_30_n_0
    SLICE_X4Y63                                                       r  O_OBUF[13]_inst_i_13/I0
    SLICE_X4Y63          LUT5 (Prop_lut5_I0_O)        0.048     2.567 r  O_OBUF[13]_inst_i_13/O
                         net (fo=4, routed)           0.231     2.798    O_OBUF[13]_inst_i_13_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_12/I4
    SLICE_X4Y63          LUT6 (Prop_lut6_I4_O)        0.129     2.927 r  O_OBUF[11]_inst_i_12/O
                         net (fo=4, routed)           0.509     3.436    O_OBUF[11]_inst_i_12_n_0
    SLICE_X3Y65                                                       r  O_OBUF[15]_inst_i_15/I5
    SLICE_X3Y65          LUT6 (Prop_lut6_I5_O)        0.043     3.479 r  O_OBUF[15]_inst_i_15/O
                         net (fo=4, routed)           0.717     4.196    O_OBUF[15]_inst_i_15_n_0
    SLICE_X3Y65                                                       r  O_OBUF[13]_inst_i_4/I1
    SLICE_X3Y65          LUT3 (Prop_lut3_I1_O)        0.053     4.249 r  O_OBUF[13]_inst_i_4/O
                         net (fo=2, routed)           0.432     4.682    O_OBUF[13]_inst_i_4_n_0
    SLICE_X4Y66                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X4Y66          LUT6 (Prop_lut6_I2_O)        0.131     4.813 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.289     6.102    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.934 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.934    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.897ns  (logic 2.790ns (35.328%)  route 5.107ns (64.672%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=18, routed)          1.497     2.086    A_IBUF[4]
    SLICE_X7Y63                                                       r  O_OBUF[12]_inst_i_24/I2
    SLICE_X7Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.129 f  O_OBUF[12]_inst_i_24/O
                         net (fo=5, routed)           0.435     2.564    O_OBUF[12]_inst_i_24_n_0
    SLICE_X5Y63                                                       f  O_OBUF[12]_inst_i_8/I4
    SLICE_X5Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.607 r  O_OBUF[12]_inst_i_8/O
                         net (fo=4, routed)           0.443     3.049    O_OBUF[12]_inst_i_8_n_0
    SLICE_X6Y64                                                       r  O_OBUF[13]_inst_i_9/I1
    SLICE_X6Y64          LUT3 (Prop_lut3_I1_O)        0.043     3.092 r  O_OBUF[13]_inst_i_9/O
                         net (fo=4, routed)           0.419     3.511    O_OBUF[13]_inst_i_9_n_0
    SLICE_X5Y64                                                       r  O_OBUF[13]_inst_i_3/I1
    SLICE_X5Y64          LUT3 (Prop_lut3_I1_O)        0.048     3.559 r  O_OBUF[13]_inst_i_3/O
                         net (fo=4, routed)           0.515     4.074    O_OBUF[13]_inst_i_3_n_0
    SLICE_X5Y66                                                       r  O_OBUF[15]_inst_i_3/I0
    SLICE_X5Y66          LUT4 (Prop_lut4_I0_O)        0.129     4.203 f  O_OBUF[15]_inst_i_3/O
                         net (fo=1, routed)           0.409     4.612    O_OBUF[15]_inst_i_3_n_0
    SLICE_X5Y65                                                       f  O_OBUF[15]_inst_i_1/I1
    SLICE_X5Y65          LUT6 (Prop_lut6_I1_O)        0.043     4.655 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.391     6.046    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.897 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.897    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.890ns  (logic 2.873ns (36.421%)  route 5.016ns (63.579%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.391     1.987    A_IBUF[2]
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_33/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.030 r  O_OBUF[12]_inst_i_33/O
                         net (fo=2, routed)           0.483     2.512    O_OBUF[12]_inst_i_33_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.043     2.555 r  O_OBUF[9]_inst_i_10/O
                         net (fo=5, routed)           0.388     2.943    O_OBUF[9]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[9]_inst_i_4/I4
    SLICE_X1Y64          LUT5 (Prop_lut5_I4_O)        0.051     2.994 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.552     3.546    O_OBUF[9]_inst_i_4_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_12/I3
    SLICE_X2Y65          LUT6 (Prop_lut6_I3_O)        0.129     3.675 f  O_OBUF[12]_inst_i_12/O
                         net (fo=2, routed)           0.419     4.094    O_OBUF[12]_inst_i_12_n_0
    SLICE_X2Y66                                                       f  O_OBUF[12]_inst_i_4/I2
    SLICE_X2Y66          LUT4 (Prop_lut4_I2_O)        0.043     4.137 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.502     4.639    O_OBUF[12]_inst_i_4_n_0
    SLICE_X5Y66                                                       r  O_OBUF[12]_inst_i_1/I2
    SLICE_X5Y66          LUT6 (Prop_lut6_I2_O)        0.127     4.766 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.282     6.048    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.890 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.890    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.887ns  (logic 2.870ns (36.388%)  route 5.017ns (63.612%))
  Logic Levels:           8  (IBUF=1 LUT4=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.360     1.954    A_IBUF[3]
    SLICE_X6Y63                                                       r  O_OBUF[12]_inst_i_30/I5
    SLICE_X6Y63          LUT6 (Prop_lut6_I5_O)        0.043     1.997 r  O_OBUF[12]_inst_i_30/O
                         net (fo=3, routed)           0.522     2.519    O_OBUF[12]_inst_i_30_n_0
    SLICE_X4Y63                                                       r  O_OBUF[13]_inst_i_13/I0
    SLICE_X4Y63          LUT5 (Prop_lut5_I0_O)        0.048     2.567 r  O_OBUF[13]_inst_i_13/O
                         net (fo=4, routed)           0.231     2.798    O_OBUF[13]_inst_i_13_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_12/I4
    SLICE_X4Y63          LUT6 (Prop_lut6_I4_O)        0.129     2.927 r  O_OBUF[11]_inst_i_12/O
                         net (fo=4, routed)           0.610     3.537    O_OBUF[11]_inst_i_12_n_0
    SLICE_X3Y65                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X3Y65          LUT4 (Prop_lut4_I0_O)        0.043     3.580 f  O_OBUF[11]_inst_i_3/O
                         net (fo=12, routed)          0.695     4.275    O_OBUF[11]_inst_i_3_n_0
    SLICE_X4Y66                                                       f  O_OBUF[14]_inst_i_6/I4
    SLICE_X4Y66          LUT5 (Prop_lut5_I4_O)        0.048     4.323 r  O_OBUF[14]_inst_i_6/O
                         net (fo=2, routed)           0.231     4.554    O_OBUF[14]_inst_i_6_n_0
    SLICE_X4Y66                                                       r  O_OBUF[14]_inst_i_1/I5
    SLICE_X4Y66          LUT6 (Prop_lut6_I5_O)        0.129     4.683 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.368     6.051    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     7.887 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.887    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.858ns  (logic 2.882ns (36.680%)  route 4.976ns (63.320%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT5=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=17, routed)          1.360     1.954    A_IBUF[3]
    SLICE_X6Y63                                                       r  O_OBUF[12]_inst_i_30/I5
    SLICE_X6Y63          LUT6 (Prop_lut6_I5_O)        0.043     1.997 r  O_OBUF[12]_inst_i_30/O
                         net (fo=3, routed)           0.522     2.519    O_OBUF[12]_inst_i_30_n_0
    SLICE_X4Y63                                                       r  O_OBUF[13]_inst_i_13/I0
    SLICE_X4Y63          LUT5 (Prop_lut5_I0_O)        0.048     2.567 r  O_OBUF[13]_inst_i_13/O
                         net (fo=4, routed)           0.231     2.798    O_OBUF[13]_inst_i_13_n_0
    SLICE_X4Y63                                                       r  O_OBUF[11]_inst_i_12/I4
    SLICE_X4Y63          LUT6 (Prop_lut6_I4_O)        0.129     2.927 r  O_OBUF[11]_inst_i_12/O
                         net (fo=4, routed)           0.509     3.436    O_OBUF[11]_inst_i_12_n_0
    SLICE_X3Y65                                                       r  O_OBUF[15]_inst_i_15/I5
    SLICE_X3Y65          LUT6 (Prop_lut6_I5_O)        0.043     3.479 r  O_OBUF[15]_inst_i_15/O
                         net (fo=4, routed)           0.717     4.196    O_OBUF[15]_inst_i_15_n_0
    SLICE_X3Y65                                                       r  O_OBUF[13]_inst_i_4/I1
    SLICE_X3Y65          LUT3 (Prop_lut3_I1_O)        0.053     4.249 r  O_OBUF[13]_inst_i_4/O
                         net (fo=2, routed)           0.430     4.679    O_OBUF[13]_inst_i_4_n_0
    SLICE_X2Y66                                                       r  O_OBUF[11]_inst_i_1/I4
    SLICE_X2Y66          LUT6 (Prop_lut6_I4_O)        0.131     4.810 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.207     6.017    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.858 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.858    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.700ns  (logic 2.862ns (37.170%)  route 4.838ns (62.830%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.391     1.987    A_IBUF[2]
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_33/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.030 r  O_OBUF[12]_inst_i_33/O
                         net (fo=2, routed)           0.483     2.512    O_OBUF[12]_inst_i_33_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.043     2.555 r  O_OBUF[9]_inst_i_10/O
                         net (fo=5, routed)           0.388     2.943    O_OBUF[9]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[9]_inst_i_4/I4
    SLICE_X1Y64          LUT5 (Prop_lut5_I4_O)        0.051     2.994 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.552     3.546    O_OBUF[9]_inst_i_4_n_0
    SLICE_X2Y65                                                       r  O_OBUF[12]_inst_i_12/I3
    SLICE_X2Y65          LUT6 (Prop_lut6_I3_O)        0.129     3.675 f  O_OBUF[12]_inst_i_12/O
                         net (fo=2, routed)           0.419     4.094    O_OBUF[12]_inst_i_12_n_0
    SLICE_X2Y66                                                       f  O_OBUF[12]_inst_i_4/I2
    SLICE_X2Y66          LUT4 (Prop_lut4_I2_O)        0.043     4.137 r  O_OBUF[12]_inst_i_4/O
                         net (fo=2, routed)           0.406     4.544    O_OBUF[12]_inst_i_4_n_0
    SLICE_X1Y66                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X1Y66          LUT3 (Prop_lut3_I1_O)        0.127     4.671 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.199     5.870    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.700 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.700    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.363ns  (logic 2.852ns (38.726%)  route 4.512ns (61.274%))
  Logic Levels:           8  (IBUF=1 LUT2=1 LUT3=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.391     1.987    A_IBUF[2]
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_33/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.030 r  O_OBUF[12]_inst_i_33/O
                         net (fo=2, routed)           0.483     2.512    O_OBUF[12]_inst_i_33_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.043     2.555 r  O_OBUF[9]_inst_i_10/O
                         net (fo=5, routed)           0.388     2.943    O_OBUF[9]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[9]_inst_i_4/I4
    SLICE_X1Y64          LUT5 (Prop_lut5_I4_O)        0.051     2.994 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.327     3.322    O_OBUF[9]_inst_i_4_n_0
    SLICE_X2Y65                                                       r  O_OBUF[14]_inst_i_12/I1
    SLICE_X2Y65          LUT3 (Prop_lut3_I1_O)        0.129     3.451 r  O_OBUF[14]_inst_i_12/O
                         net (fo=2, routed)           0.417     3.867    O_OBUF[14]_inst_i_12_n_0
    SLICE_X1Y65                                                       r  O_OBUF[9]_inst_i_7/I3
    SLICE_X1Y65          LUT6 (Prop_lut6_I3_O)        0.043     3.910 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.336     4.246    O_OBUF[9]_inst_i_7_n_0
    SLICE_X2Y66                                                       r  O_OBUF[8]_inst_i_1/I1
    SLICE_X2Y66          LUT2 (Prop_lut2_I1_O)        0.045     4.291 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.171     5.462    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.902     7.363 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.363    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.334ns  (logic 2.779ns (37.896%)  route 4.555ns (62.104%))
  Logic Levels:           8  (IBUF=1 LUT3=2 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.391     1.987    A_IBUF[2]
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_33/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.030 r  O_OBUF[12]_inst_i_33/O
                         net (fo=2, routed)           0.483     2.512    O_OBUF[12]_inst_i_33_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.043     2.555 r  O_OBUF[9]_inst_i_10/O
                         net (fo=5, routed)           0.388     2.943    O_OBUF[9]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[9]_inst_i_4/I4
    SLICE_X1Y64          LUT5 (Prop_lut5_I4_O)        0.051     2.994 r  O_OBUF[9]_inst_i_4/O
                         net (fo=5, routed)           0.327     3.322    O_OBUF[9]_inst_i_4_n_0
    SLICE_X2Y65                                                       r  O_OBUF[14]_inst_i_12/I1
    SLICE_X2Y65          LUT3 (Prop_lut3_I1_O)        0.129     3.451 r  O_OBUF[14]_inst_i_12/O
                         net (fo=2, routed)           0.417     3.867    O_OBUF[14]_inst_i_12_n_0
    SLICE_X1Y65                                                       r  O_OBUF[9]_inst_i_7/I3
    SLICE_X1Y65          LUT6 (Prop_lut6_I3_O)        0.043     3.910 r  O_OBUF[9]_inst_i_7/O
                         net (fo=2, routed)           0.338     4.248    O_OBUF[9]_inst_i_7_n_0
    SLICE_X3Y66                                                       r  O_OBUF[9]_inst_i_1/I5
    SLICE_X3Y66          LUT6 (Prop_lut6_I5_O)        0.043     4.291 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.211     5.502    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.334 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.334    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.956ns  (logic 2.627ns (37.767%)  route 4.329ns (62.233%))
  Logic Levels:           7  (IBUF=1 LUT3=2 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.391     1.987    A_IBUF[2]
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_33/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.030 r  O_OBUF[12]_inst_i_33/O
                         net (fo=2, routed)           0.483     2.512    O_OBUF[12]_inst_i_33_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.043     2.555 r  O_OBUF[9]_inst_i_10/O
                         net (fo=5, routed)           0.388     2.943    O_OBUF[9]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[12]_inst_i_17/I4
    SLICE_X1Y64          LUT5 (Prop_lut5_I4_O)        0.043     2.986 r  O_OBUF[12]_inst_i_17/O
                         net (fo=7, routed)           0.511     3.497    O_OBUF[12]_inst_i_17_n_0
    SLICE_X1Y65                                                       r  O_OBUF[12]_inst_i_6/I1
    SLICE_X1Y65          LUT6 (Prop_lut6_I1_O)        0.043     3.540 r  O_OBUF[12]_inst_i_6/O
                         net (fo=4, routed)           0.410     3.950    O_OBUF[12]_inst_i_6_n_0
    SLICE_X2Y66                                                       r  O_OBUF[7]_inst_i_1/I1
    SLICE_X2Y66          LUT3 (Prop_lut3_I1_O)        0.043     3.993 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.147     5.140    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.956 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.956    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.953ns  (logic 2.727ns (39.226%)  route 4.226ns (60.774%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=18, routed)          1.391     1.987    A_IBUF[2]
    SLICE_X1Y62                                                       r  O_OBUF[12]_inst_i_33/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     2.030 r  O_OBUF[12]_inst_i_33/O
                         net (fo=2, routed)           0.483     2.512    O_OBUF[12]_inst_i_33_n_0
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_10/I2
    SLICE_X1Y63          LUT3 (Prop_lut3_I2_O)        0.043     2.555 r  O_OBUF[9]_inst_i_10/O
                         net (fo=5, routed)           0.388     2.943    O_OBUF[9]_inst_i_10_n_0
    SLICE_X1Y64                                                       r  O_OBUF[12]_inst_i_17/I4
    SLICE_X1Y64          LUT5 (Prop_lut5_I4_O)        0.043     2.986 r  O_OBUF[12]_inst_i_17/O
                         net (fo=7, routed)           0.394     3.380    O_OBUF[12]_inst_i_17_n_0
    SLICE_X1Y65                                                       r  O_OBUF[6]_inst_i_5/I4
    SLICE_X1Y65          LUT5 (Prop_lut5_I4_O)        0.051     3.431 r  O_OBUF[6]_inst_i_5/O
                         net (fo=1, routed)           0.435     3.866    O_OBUF[6]_inst_i_5_n_0
    SLICE_X0Y65                                                       r  O_OBUF[6]_inst_i_1/I3
    SLICE_X0Y65          LUT4 (Prop_lut4_I3_O)        0.129     3.995 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.135     5.130    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.953 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.953    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




