Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 23:33:14 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_201_evolib.txt -name O
| Design       : mul8_201
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.004ns  (logic 2.646ns (37.778%)  route 4.358ns (62.222%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=14, routed)          1.172     1.751    A_IBUF[7]
    SLICE_X3Y61                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X3Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.794 r  O_OBUF[12]_inst_i_12/O
                         net (fo=2, routed)           0.531     2.326    N_1202
    SLICE_X3Y62                                                       r  O_OBUF[13]_inst_i_6/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.369 r  O_OBUF[13]_inst_i_6/O
                         net (fo=3, routed)           0.418     2.787    N_1603
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_2/I1
    SLICE_X3Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.830 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.419     3.249    N_1780
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_3/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.043     3.292 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.415     3.708    N_1973
    SLICE_X0Y63                                                       r  O_OBUF[15]_inst_i_1/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.043     3.751 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.402     5.152    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     7.004 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     7.004    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.937ns  (logic 2.721ns (39.226%)  route 4.216ns (60.774%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=14, routed)          1.172     1.751    A_IBUF[7]
    SLICE_X3Y61                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X3Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.794 r  O_OBUF[12]_inst_i_12/O
                         net (fo=2, routed)           0.526     2.321    N_1202
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.364 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.427     2.791    N_1602
    SLICE_X1Y63                                                       r  O_OBUF[15]_inst_i_6/I0
    SLICE_X1Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.834 r  O_OBUF[15]_inst_i_6/O
                         net (fo=2, routed)           0.414     3.247    N_1765
    SLICE_X3Y63                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X3Y63          LUT5 (Prop_lut5_I0_O)        0.051     3.298 r  O_OBUF[13]_inst_i_3/O
                         net (fo=1, routed)           0.352     3.650    N_1957
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X2Y63          LUT3 (Prop_lut3_I1_O)        0.129     3.779 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.326     5.105    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     6.937 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     6.937    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.909ns  (logic 2.630ns (38.067%)  route 4.279ns (61.933%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=14, routed)          1.172     1.751    A_IBUF[7]
    SLICE_X3Y61                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X3Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.794 r  O_OBUF[12]_inst_i_12/O
                         net (fo=2, routed)           0.531     2.326    N_1202
    SLICE_X3Y62                                                       r  O_OBUF[13]_inst_i_6/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.369 r  O_OBUF[13]_inst_i_6/O
                         net (fo=3, routed)           0.418     2.787    N_1603
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_2/I1
    SLICE_X3Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.830 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.419     3.249    N_1780
    SLICE_X2Y63                                                       r  O_OBUF[15]_inst_i_3/I1
    SLICE_X2Y63          LUT6 (Prop_lut6_I1_O)        0.043     3.292 r  O_OBUF[15]_inst_i_3/O
                         net (fo=2, routed)           0.416     3.708    N_1973
    SLICE_X0Y63                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.751 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.323     5.074    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     6.909 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     6.909    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.640ns  (logic 2.685ns (40.442%)  route 3.954ns (59.558%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=14, routed)          1.167     1.746    A_IBUF[7]
    SLICE_X3Y61                                                       r  O_OBUF[13]_inst_i_11/I0
    SLICE_X3Y61          LUT4 (Prop_lut4_I0_O)        0.049     1.795 r  O_OBUF[13]_inst_i_11/O
                         net (fo=2, routed)           0.506     2.301    N_1232
    SLICE_X2Y61                                                       r  O_OBUF[13]_inst_i_5/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.129     2.430 r  O_OBUF[13]_inst_i_5/O
                         net (fo=3, routed)           0.434     2.864    N_1632
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_6/I0
    SLICE_X3Y63          LUT3 (Prop_lut3_I0_O)        0.043     2.907 r  O_OBUF[12]_inst_i_6/O
                         net (fo=2, routed)           0.511     3.418    N_1781
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_1/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.461 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.337     4.798    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.640 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.640    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.492ns  (logic 2.593ns (39.934%)  route 3.900ns (60.066%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT4=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=14, routed)          1.172     1.751    A_IBUF[7]
    SLICE_X3Y61                                                       r  O_OBUF[12]_inst_i_12/I0
    SLICE_X3Y61          LUT6 (Prop_lut6_I0_O)        0.043     1.794 r  O_OBUF[12]_inst_i_12/O
                         net (fo=2, routed)           0.531     2.326    N_1202
    SLICE_X3Y62                                                       r  O_OBUF[13]_inst_i_6/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     2.369 r  O_OBUF[13]_inst_i_6/O
                         net (fo=3, routed)           0.418     2.787    N_1603
    SLICE_X3Y63                                                       r  O_OBUF[12]_inst_i_2/I1
    SLICE_X3Y63          LUT3 (Prop_lut3_I1_O)        0.043     2.830 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.432     3.262    N_1780
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     3.305 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.347     4.651    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.492 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.492    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.321ns  (logic 2.676ns (42.332%)  route 3.645ns (57.668%))
  Logic Levels:           6  (IBUF=1 LUT3=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=11, routed)          1.152     1.741    A_IBUF[4]
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_15/I2
    SLICE_X3Y62          LUT6 (Prop_lut6_I2_O)        0.043     1.784 r  O_OBUF[12]_inst_i_15/O
                         net (fo=3, routed)           0.358     2.142    N_1186
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_8/I2
    SLICE_X2Y62          LUT5 (Prop_lut5_I2_O)        0.043     2.185 r  O_OBUF[12]_inst_i_8/O
                         net (fo=3, routed)           0.422     2.607    N_1350
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X1Y63          LUT3 (Prop_lut3_I0_O)        0.127     2.734 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.445     3.179    N_1587
    SLICE_X1Y63                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.222 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.491    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     6.321 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     6.321    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.198ns  (logic 2.771ns (44.706%)  route 3.427ns (55.294%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=11, routed)          1.152     1.741    A_IBUF[4]
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_15/I2
    SLICE_X3Y62          LUT6 (Prop_lut6_I2_O)        0.043     1.784 r  O_OBUF[12]_inst_i_15/O
                         net (fo=3, routed)           0.358     2.142    N_1186
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_8/I2
    SLICE_X2Y62          LUT5 (Prop_lut5_I2_O)        0.043     2.185 r  O_OBUF[12]_inst_i_8/O
                         net (fo=3, routed)           0.422     2.607    N_1350
    SLICE_X1Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X1Y63          LUT3 (Prop_lut3_I0_O)        0.135     2.742 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.228     2.970    N_1586
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X1Y63          LUT3 (Prop_lut3_I1_O)        0.129     3.099 r  O_OBUF[9]_inst_i_1/O
                         net (fo=2, routed)           1.268     4.367    O_OBUF[1]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     6.198 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.198    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[1]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.939ns  (logic 2.729ns (45.951%)  route 3.210ns (54.049%))
  Logic Levels:           6  (IBUF=1 LUT3=2 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=11, routed)          1.152     1.741    A_IBUF[4]
    SLICE_X3Y62                                                       r  O_OBUF[12]_inst_i_15/I2
    SLICE_X3Y62          LUT6 (Prop_lut6_I2_O)        0.043     1.784 r  O_OBUF[12]_inst_i_15/O
                         net (fo=3, routed)           0.358     2.142    N_1186
    SLICE_X2Y62                                                       r  O_OBUF[12]_inst_i_8/I2
    SLICE_X2Y62          LUT5 (Prop_lut5_I2_O)        0.043     2.185 r  O_OBUF[12]_inst_i_8/O
                         net (fo=3, routed)           0.422     2.607    N_1350
    SLICE_X1Y63                                                       r  O_OBUF[10]_inst_i_3/I0
    SLICE_X1Y63          LUT3 (Prop_lut3_I0_O)        0.135     2.742 r  O_OBUF[10]_inst_i_3/O
                         net (fo=2, routed)           0.228     2.970    N_1586
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X1Y63          LUT3 (Prop_lut3_I1_O)        0.129     3.099 r  O_OBUF[9]_inst_i_1/O
                         net (fo=2, routed)           1.051     4.150    O_OBUF[1]
    AH25                                                              r  O_OBUF[1]_inst/I
    AH25                 OBUF (Prop_obuf_I_O)         1.789     5.939 r  O_OBUF[1]_inst/O
                         net (fo=0)                   0.000     5.939    O[1]
    AH25                                                              r  O[1] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[2]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.228ns  (logic 2.498ns (47.782%)  route 2.730ns (52.218%))
  Logic Levels:           4  (IBUF=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN27                                              0.000     0.000 r  A[2] (IN)
                         net (fo=0)                   0.000     0.000    A[2]
    AN27                                                              r  A_IBUF[2]_inst/I
    AN27                 IBUF (Prop_ibuf_I_O)         0.596     0.596 r  A_IBUF[2]_inst/O
                         net (fo=6, routed)           1.128     1.724    A_IBUF[2]
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_2/I4
    SLICE_X0Y61          LUT6 (Prop_lut6_I4_O)        0.043     1.767 r  O_OBUF[7]_inst_i_2/O
                         net (fo=2, routed)           0.348     2.114    N_1156
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_1/I0
    SLICE_X0Y61          LUT5 (Prop_lut5_I0_O)        0.043     2.157 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.254     3.412    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.228 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.228    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 B[5]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.097ns  (logic 2.478ns (48.624%)  route 2.618ns (51.376%))
  Logic Levels:           4  (IBUF=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AL26                                              0.000     0.000 r  B[5] (IN)
                         net (fo=0)                   0.000     0.000    B[5]
    AL26                                                              r  B_IBUF[5]_inst/I
    AL26                 IBUF (Prop_ibuf_I_O)         0.570     0.570 r  B_IBUF[5]_inst/O
                         net (fo=12, routed)          1.077     1.647    B_IBUF[5]
    SLICE_X0Y59                                                       r  O_OBUF[6]_inst_i_2/I3
    SLICE_X0Y59          LUT5 (Prop_lut5_I3_O)        0.043     1.690 r  O_OBUF[6]_inst_i_2/O
                         net (fo=1, routed)           0.271     1.961    O_OBUF[6]_inst_i_2_n_0
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X0Y61          LUT5 (Prop_lut5_I0_O)        0.043     2.004 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.271     3.275    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.097 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.097    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------




