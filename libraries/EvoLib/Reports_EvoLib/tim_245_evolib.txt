Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 01:41:58 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_245_evolib.txt -name O
| Design       : mul8_245
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.397ns  (logic 2.875ns (34.240%)  route 5.522ns (65.760%))
  Logic Levels:           10  (IBUF=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.076     1.664    A_IBUF[5]
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_8/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     1.707 r  O_OBUF[6]_inst_i_8/O
                         net (fo=3, routed)           0.437     2.144    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y60                                                       r  O_OBUF[9]_inst_i_12/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.187 r  O_OBUF[9]_inst_i_12/O
                         net (fo=3, routed)           0.507     2.694    O_OBUF[9]_inst_i_12_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.737 f  O_OBUF[9]_inst_i_7/O
                         net (fo=6, routed)           0.429     3.166    O_OBUF[9]_inst_i_7_n_0
    SLICE_X3Y62                                                       f  O_OBUF[11]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.209 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.304     3.514    O_OBUF[11]_inst_i_7_n_0
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_12/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.557 r  O_OBUF[15]_inst_i_12/O
                         net (fo=4, routed)           0.519     4.075    O_OBUF[15]_inst_i_12_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X1Y63          LUT6 (Prop_lut6_I0_O)        0.043     4.118 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.209     4.328    O_OBUF[12]_inst_i_3_n_0
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_6/I3
    SLICE_X1Y64          LUT4 (Prop_lut4_I3_O)        0.049     4.377 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.626     5.002    O_OBUF[15]_inst_i_6_n_0
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y65          LUT6 (Prop_lut6_I4_O)        0.129     5.131 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.414     6.545    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.397 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.397    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.284ns  (logic 2.859ns (34.514%)  route 5.425ns (65.486%))
  Logic Levels:           10  (IBUF=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.076     1.664    A_IBUF[5]
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_8/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     1.707 r  O_OBUF[6]_inst_i_8/O
                         net (fo=3, routed)           0.437     2.144    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y60                                                       r  O_OBUF[9]_inst_i_12/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.187 r  O_OBUF[9]_inst_i_12/O
                         net (fo=3, routed)           0.507     2.694    O_OBUF[9]_inst_i_12_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.737 f  O_OBUF[9]_inst_i_7/O
                         net (fo=6, routed)           0.429     3.166    O_OBUF[9]_inst_i_7_n_0
    SLICE_X3Y62                                                       f  O_OBUF[11]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.209 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.304     3.514    O_OBUF[11]_inst_i_7_n_0
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_12/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.557 r  O_OBUF[15]_inst_i_12/O
                         net (fo=4, routed)           0.519     4.075    O_OBUF[15]_inst_i_12_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X1Y63          LUT6 (Prop_lut6_I0_O)        0.043     4.118 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.209     4.328    O_OBUF[12]_inst_i_3_n_0
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_6/I3
    SLICE_X1Y64          LUT4 (Prop_lut4_I3_O)        0.049     4.377 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.621     4.997    O_OBUF[15]_inst_i_6_n_0
    SLICE_X0Y65                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X0Y65          LUT6 (Prop_lut6_I4_O)        0.129     5.126 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.323     6.449    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.284 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.284    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.048ns  (logic 2.856ns (35.492%)  route 5.192ns (64.508%))
  Logic Levels:           10  (IBUF=1 LUT4=2 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.076     1.664    A_IBUF[5]
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_8/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     1.707 r  O_OBUF[6]_inst_i_8/O
                         net (fo=3, routed)           0.437     2.144    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y60                                                       r  O_OBUF[9]_inst_i_12/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.187 r  O_OBUF[9]_inst_i_12/O
                         net (fo=3, routed)           0.507     2.694    O_OBUF[9]_inst_i_12_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.737 f  O_OBUF[9]_inst_i_7/O
                         net (fo=6, routed)           0.429     3.166    O_OBUF[9]_inst_i_7_n_0
    SLICE_X3Y62                                                       f  O_OBUF[11]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.209 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.304     3.514    O_OBUF[11]_inst_i_7_n_0
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_12/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.557 r  O_OBUF[15]_inst_i_12/O
                         net (fo=4, routed)           0.519     4.075    O_OBUF[15]_inst_i_12_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X1Y63          LUT6 (Prop_lut6_I0_O)        0.043     4.118 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.209     4.328    O_OBUF[12]_inst_i_3_n_0
    SLICE_X1Y64                                                       r  O_OBUF[15]_inst_i_6/I3
    SLICE_X1Y64          LUT4 (Prop_lut4_I3_O)        0.049     4.377 r  O_OBUF[15]_inst_i_6/O
                         net (fo=3, routed)           0.352     4.728    O_OBUF[15]_inst_i_6_n_0
    SLICE_X0Y64                                                       r  O_OBUF[13]_inst_i_1/I3
    SLICE_X0Y64          LUT6 (Prop_lut6_I3_O)        0.129     4.857 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.358     6.215    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.048 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.048    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.758ns  (logic 2.730ns (35.187%)  route 5.028ns (64.813%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.076     1.664    A_IBUF[5]
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_8/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     1.707 r  O_OBUF[6]_inst_i_8/O
                         net (fo=3, routed)           0.437     2.144    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y60                                                       r  O_OBUF[9]_inst_i_12/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.187 r  O_OBUF[9]_inst_i_12/O
                         net (fo=3, routed)           0.507     2.694    O_OBUF[9]_inst_i_12_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.737 f  O_OBUF[9]_inst_i_7/O
                         net (fo=6, routed)           0.429     3.166    O_OBUF[9]_inst_i_7_n_0
    SLICE_X3Y62                                                       f  O_OBUF[11]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.209 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.323     3.533    O_OBUF[11]_inst_i_7_n_0
    SLICE_X3Y62                                                       r  O_OBUF[15]_inst_i_11/I0
    SLICE_X3Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.576 r  O_OBUF[15]_inst_i_11/O
                         net (fo=4, routed)           0.501     4.077    O_OBUF[15]_inst_i_11_n_0
    SLICE_X1Y63                                                       r  O_OBUF[11]_inst_i_2/I3
    SLICE_X1Y63          LUT4 (Prop_lut4_I3_O)        0.043     4.120 r  O_OBUF[11]_inst_i_2/O
                         net (fo=1, routed)           0.410     4.530    O_OBUF[11]_inst_i_2_n_0
    SLICE_X0Y63                                                       r  O_OBUF[11]_inst_i_1/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     4.573 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.344     5.917    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.758 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.758    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.526ns  (logic 2.730ns (36.276%)  route 4.796ns (63.724%))
  Logic Levels:           9  (IBUF=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.076     1.664    A_IBUF[5]
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_8/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     1.707 r  O_OBUF[6]_inst_i_8/O
                         net (fo=3, routed)           0.437     2.144    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y60                                                       r  O_OBUF[9]_inst_i_12/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.187 r  O_OBUF[9]_inst_i_12/O
                         net (fo=3, routed)           0.507     2.694    O_OBUF[9]_inst_i_12_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.737 f  O_OBUF[9]_inst_i_7/O
                         net (fo=6, routed)           0.429     3.166    O_OBUF[9]_inst_i_7_n_0
    SLICE_X3Y62                                                       f  O_OBUF[11]_inst_i_7/I3
    SLICE_X3Y62          LUT4 (Prop_lut4_I3_O)        0.043     3.209 r  O_OBUF[11]_inst_i_7/O
                         net (fo=4, routed)           0.304     3.514    O_OBUF[11]_inst_i_7_n_0
    SLICE_X1Y62                                                       r  O_OBUF[15]_inst_i_12/I1
    SLICE_X1Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.557 r  O_OBUF[15]_inst_i_12/O
                         net (fo=4, routed)           0.519     4.075    O_OBUF[15]_inst_i_12_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_3/I0
    SLICE_X1Y63          LUT6 (Prop_lut6_I0_O)        0.043     4.118 r  O_OBUF[12]_inst_i_3/O
                         net (fo=2, routed)           0.209     4.328    O_OBUF[12]_inst_i_3_n_0
    SLICE_X1Y64                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X1Y64          LUT4 (Prop_lut4_I1_O)        0.043     4.371 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.314     5.685    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.526 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.526    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.095ns  (logic 2.676ns (37.713%)  route 4.419ns (62.287%))
  Logic Levels:           8  (IBUF=1 LUT4=2 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.076     1.664    A_IBUF[5]
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_8/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     1.707 r  O_OBUF[6]_inst_i_8/O
                         net (fo=3, routed)           0.238     1.945    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_5/I2
    SLICE_X0Y60          LUT4 (Prop_lut4_I2_O)        0.043     1.988 r  O_OBUF[6]_inst_i_5/O
                         net (fo=6, routed)           0.733     2.721    O_OBUF[6]_inst_i_5_n_0
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_4/I2
    SLICE_X2Y62          LUT6 (Prop_lut6_I2_O)        0.043     2.764 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.371     3.135    O_OBUF[9]_inst_i_4_n_0
    SLICE_X2Y62                                                       r  O_OBUF[11]_inst_i_5/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.178 r  O_OBUF[11]_inst_i_5/O
                         net (fo=2, routed)           0.496     3.675    O_OBUF[11]_inst_i_5_n_0
    SLICE_X1Y62                                                       r  O_OBUF[11]_inst_i_3/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.718 r  O_OBUF[11]_inst_i_3/O
                         net (fo=2, routed)           0.220     3.937    O_OBUF[11]_inst_i_3_n_0
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_1/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.043     3.980 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.285     5.265    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.095 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.095    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.866ns  (logic 2.732ns (39.793%)  route 4.134ns (60.207%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          1.076     1.664    A_IBUF[5]
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_8/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     1.707 r  O_OBUF[6]_inst_i_8/O
                         net (fo=3, routed)           0.437     2.144    O_OBUF[6]_inst_i_8_n_0
    SLICE_X0Y60                                                       r  O_OBUF[9]_inst_i_12/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.187 r  O_OBUF[9]_inst_i_12/O
                         net (fo=3, routed)           0.507     2.694    O_OBUF[9]_inst_i_12_n_0
    SLICE_X2Y61                                                       r  O_OBUF[9]_inst_i_7/I0
    SLICE_X2Y61          LUT6 (Prop_lut6_I0_O)        0.043     2.737 r  O_OBUF[9]_inst_i_7/O
                         net (fo=6, routed)           0.535     3.272    O_OBUF[9]_inst_i_7_n_0
    SLICE_X2Y62                                                       r  O_OBUF[9]_inst_i_2/I5
    SLICE_X2Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.315 r  O_OBUF[9]_inst_i_2/O
                         net (fo=3, routed)           0.309     3.624    O_OBUF[9]_inst_i_2_n_0
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_1/I0
    SLICE_X0Y63          LUT4 (Prop_lut4_I0_O)        0.053     3.677 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.946    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.919     6.866 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     6.866    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.732ns  (logic 2.813ns (41.791%)  route 3.919ns (58.209%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          0.895     1.482    A_IBUF[5]
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_3/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     1.525 r  O_OBUF[7]_inst_i_3/O
                         net (fo=6, routed)           0.600     2.126    O_OBUF[7]_inst_i_3_n_0
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.043     2.169 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.173     2.341    O_OBUF[6]_inst_i_4_n_0
    SLICE_X2Y60                                                       r  O_OBUF[6]_inst_i_2/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     2.384 r  O_OBUF[6]_inst_i_2/O
                         net (fo=3, routed)           0.387     2.771    O_OBUF[6]_inst_i_2_n_0
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_2/I4
    SLICE_X1Y62          LUT5 (Prop_lut5_I4_O)        0.051     2.822 r  O_OBUF[8]_inst_i_2/O
                         net (fo=1, routed)           0.606     3.428    O_OBUF[8]_inst_i_2_n_0
    SLICE_X1Y63                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y63          LUT4 (Prop_lut4_I0_O)        0.139     3.567 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.258     4.825    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.907     6.732 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.732    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.889ns  (logic 2.582ns (43.844%)  route 3.307ns (56.156%))
  Logic Levels:           6  (IBUF=1 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          0.895     1.482    A_IBUF[5]
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_3/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     1.525 r  O_OBUF[7]_inst_i_3/O
                         net (fo=6, routed)           0.600     2.126    O_OBUF[7]_inst_i_3_n_0
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_4/I1
    SLICE_X2Y61          LUT6 (Prop_lut6_I1_O)        0.043     2.169 r  O_OBUF[6]_inst_i_4/O
                         net (fo=2, routed)           0.173     2.341    O_OBUF[6]_inst_i_4_n_0
    SLICE_X2Y60                                                       r  O_OBUF[6]_inst_i_2/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.043     2.384 r  O_OBUF[6]_inst_i_2/O
                         net (fo=3, routed)           0.347     2.731    O_OBUF[6]_inst_i_2_n_0
    SLICE_X0Y63                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X0Y63          LUT6 (Prop_lut6_I0_O)        0.043     2.774 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.293     4.067    O_OBUF[3]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     5.889 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     5.889    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[5]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.725ns  (logic 2.623ns (45.820%)  route 3.102ns (54.180%))
  Logic Levels:           5  (IBUF=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN28                                              0.000     0.000 r  A[5] (IN)
                         net (fo=0)                   0.000     0.000    A[5]
    AN28                                                              r  A_IBUF[5]_inst/I
    AN28                 IBUF (Prop_ibuf_I_O)         0.588     0.588 r  A_IBUF[5]_inst/O
                         net (fo=18, routed)          0.895     1.482    A_IBUF[5]
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_3/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     1.525 r  O_OBUF[7]_inst_i_3/O
                         net (fo=6, routed)           0.436     1.961    O_OBUF[7]_inst_i_3_n_0
    SLICE_X0Y61                                                       r  O_OBUF[7]_inst_i_2/I1
    SLICE_X0Y61          LUT6 (Prop_lut6_I1_O)        0.043     2.004 r  O_OBUF[7]_inst_i_2/O
                         net (fo=3, routed)           0.510     2.514    O_OBUF[7]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X0Y62          LUT5 (Prop_lut5_I2_O)        0.050     2.564 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.261     3.825    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.899     5.725 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.725    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------




