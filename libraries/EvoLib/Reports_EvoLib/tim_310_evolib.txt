Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 04:53:16 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_310_evolib.txt -name O
| Design       : mul8_310
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.605ns  (logic 3.085ns (35.858%)  route 5.519ns (64.142%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.056     1.646    A_IBUF[4]
    SLICE_X1Y58                                                       r  O_OBUF[8]_inst_i_7/I5
    SLICE_X1Y58          LUT6 (Prop_lut6_I5_O)        0.043     1.689 r  O_OBUF[8]_inst_i_7/O
                         net (fo=4, routed)           0.427     2.116    N_899
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.159 r  O_OBUF[8]_inst_i_5/O
                         net (fo=4, routed)           0.404     2.563    N_1165
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X1Y60          LUT4 (Prop_lut4_I2_O)        0.049     2.612 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.420     3.032    N_1183
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.137     3.169 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.463     3.632    N_1198
    SLICE_X2Y60                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.129     3.761 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.481     4.242    N_1464
    SLICE_X1Y60                                                       r  O_OBUF[11]_inst_i_3/I3
    SLICE_X1Y60          LUT6 (Prop_lut6_I3_O)        0.043     4.285 r  O_OBUF[11]_inst_i_3/O
                         net (fo=2, routed)           0.341     4.626    N_1714
    SLICE_X0Y60                                                       r  O_OBUF[13]_inst_i_2/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.050     4.676 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.328     5.004    N_1965
    SLICE_X1Y61                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.126     5.130 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.175     5.305    N_1999
    SLICE_X2Y61                                                       r  O_OBUF[14]_inst_i_1/I4
    SLICE_X2Y61          LUT6 (Prop_lut6_I4_O)        0.043     5.348 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.424     6.772    O_OBUF[14]
    AK29                                                              r  O_OBUF[14]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     8.605 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.605    O[14]
    AK29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.585ns  (logic 3.088ns (35.970%)  route 5.497ns (64.030%))
  Logic Levels:           11  (IBUF=1 LUT4=3 LUT6=6 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.056     1.646    A_IBUF[4]
    SLICE_X1Y58                                                       r  O_OBUF[8]_inst_i_7/I5
    SLICE_X1Y58          LUT6 (Prop_lut6_I5_O)        0.043     1.689 r  O_OBUF[8]_inst_i_7/O
                         net (fo=4, routed)           0.427     2.116    N_899
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.159 r  O_OBUF[8]_inst_i_5/O
                         net (fo=4, routed)           0.404     2.563    N_1165
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X1Y60          LUT4 (Prop_lut4_I2_O)        0.049     2.612 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.420     3.032    N_1183
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.137     3.169 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.463     3.632    N_1198
    SLICE_X2Y60                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.129     3.761 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.481     4.242    N_1464
    SLICE_X1Y60                                                       r  O_OBUF[11]_inst_i_3/I3
    SLICE_X1Y60          LUT6 (Prop_lut6_I3_O)        0.043     4.285 r  O_OBUF[11]_inst_i_3/O
                         net (fo=2, routed)           0.341     4.626    N_1714
    SLICE_X0Y60                                                       r  O_OBUF[13]_inst_i_2/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.050     4.676 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.328     5.004    N_1965
    SLICE_X1Y61                                                       r  O_OBUF[15]_inst_i_4/I3
    SLICE_X1Y61          LUT6 (Prop_lut6_I3_O)        0.126     5.130 r  O_OBUF[15]_inst_i_4/O
                         net (fo=2, routed)           0.178     5.308    N_1999
    SLICE_X2Y61                                                       r  O_OBUF[15]_inst_i_1/I5
    SLICE_X2Y61          LUT6 (Prop_lut6_I5_O)        0.043     5.351 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.399     6.750    O_OBUF[15]
    AJ29                                                              r  O_OBUF[15]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.585 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.585    O[15]
    AJ29                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.503ns  (logic 3.148ns (37.021%)  route 5.355ns (62.979%))
  Logic Levels:           10  (IBUF=1 LUT4=4 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.056     1.646    A_IBUF[4]
    SLICE_X1Y58                                                       r  O_OBUF[8]_inst_i_7/I5
    SLICE_X1Y58          LUT6 (Prop_lut6_I5_O)        0.043     1.689 r  O_OBUF[8]_inst_i_7/O
                         net (fo=4, routed)           0.427     2.116    N_899
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.159 r  O_OBUF[8]_inst_i_5/O
                         net (fo=4, routed)           0.404     2.563    N_1165
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X1Y60          LUT4 (Prop_lut4_I2_O)        0.049     2.612 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.420     3.032    N_1183
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.137     3.169 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.463     3.632    N_1198
    SLICE_X2Y60                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.129     3.761 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.481     4.242    N_1464
    SLICE_X1Y60                                                       r  O_OBUF[11]_inst_i_3/I3
    SLICE_X1Y60          LUT6 (Prop_lut6_I3_O)        0.043     4.285 r  O_OBUF[11]_inst_i_3/O
                         net (fo=2, routed)           0.341     4.626    N_1714
    SLICE_X0Y60                                                       r  O_OBUF[13]_inst_i_2/I3
    SLICE_X0Y60          LUT4 (Prop_lut4_I3_O)        0.050     4.676 r  O_OBUF[13]_inst_i_2/O
                         net (fo=3, routed)           0.333     5.009    N_1965
    SLICE_X0Y61                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X0Y61          LUT4 (Prop_lut4_I0_O)        0.135     5.144 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.430     6.574    O_OBUF[12]
    AM31                                                              r  O_OBUF[12]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.929     8.503 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     8.503    O[12]
    AM31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.330ns  (logic 2.873ns (34.494%)  route 5.456ns (65.506%))
  Logic Levels:           10  (IBUF=1 LUT3=1 LUT4=2 LUT6=5 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.145     1.735    A_IBUF[4]
    SLICE_X2Y58                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X2Y58          LUT6 (Prop_lut6_I1_O)        0.043     1.778 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.414     2.192    N_898
    SLICE_X1Y58                                                       r  O_OBUF[7]_inst_i_3/I2
    SLICE_X1Y58          LUT3 (Prop_lut3_I2_O)        0.053     2.245 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.410     2.655    N_1148
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_7/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.131     2.786 r  O_OBUF[9]_inst_i_7/O
                         net (fo=5, routed)           0.384     3.169    N_1399
    SLICE_X1Y59                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X1Y59          LUT6 (Prop_lut6_I1_O)        0.043     3.212 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.410     3.623    N_1432
    SLICE_X0Y58                                                       r  O_OBUF[11]_inst_i_4/I3
    SLICE_X0Y58          LUT4 (Prop_lut4_I3_O)        0.043     3.666 r  O_OBUF[11]_inst_i_4/O
                         net (fo=4, routed)           0.512     4.177    N_1683
    SLICE_X2Y60                                                       r  O_OBUF[15]_inst_i_5/I3
    SLICE_X2Y60          LUT6 (Prop_lut6_I3_O)        0.043     4.220 r  O_OBUF[15]_inst_i_5/O
                         net (fo=3, routed)           0.334     4.554    N_1715
    SLICE_X2Y61                                                       r  O_OBUF[13]_inst_i_3/I0
    SLICE_X2Y61          LUT4 (Prop_lut4_I0_O)        0.043     4.597 r  O_OBUF[13]_inst_i_3/O
                         net (fo=3, routed)           0.520     5.117    N_1732
    SLICE_X0Y61                                                       r  O_OBUF[13]_inst_i_1/I3
    SLICE_X0Y61          LUT6 (Prop_lut6_I3_O)        0.043     5.160 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.328     6.488    O_OBUF[13]
    AL31                                                              r  O_OBUF[13]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     8.330 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.330    O[13]
    AL31                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.905ns  (logic 2.907ns (36.771%)  route 4.998ns (63.229%))
  Logic Levels:           9  (IBUF=1 LUT4=3 LUT6=4 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.056     1.646    A_IBUF[4]
    SLICE_X1Y58                                                       r  O_OBUF[8]_inst_i_7/I5
    SLICE_X1Y58          LUT6 (Prop_lut6_I5_O)        0.043     1.689 r  O_OBUF[8]_inst_i_7/O
                         net (fo=4, routed)           0.427     2.116    N_899
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.159 r  O_OBUF[8]_inst_i_5/O
                         net (fo=4, routed)           0.404     2.563    N_1165
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X1Y60          LUT4 (Prop_lut4_I2_O)        0.049     2.612 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.420     3.032    N_1183
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.137     3.169 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.463     3.632    N_1198
    SLICE_X2Y60                                                       r  O_OBUF[11]_inst_i_7/I2
    SLICE_X2Y60          LUT6 (Prop_lut6_I2_O)        0.129     3.761 r  O_OBUF[11]_inst_i_7/O
                         net (fo=2, routed)           0.481     4.242    N_1464
    SLICE_X1Y60                                                       r  O_OBUF[11]_inst_i_3/I3
    SLICE_X1Y60          LUT6 (Prop_lut6_I3_O)        0.043     4.285 r  O_OBUF[11]_inst_i_3/O
                         net (fo=2, routed)           0.341     4.626    N_1714
    SLICE_X0Y60                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.043     4.669 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.406     6.075    O_OBUF[11]
    AL29                                                              r  O_OBUF[11]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.905 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.905    O[11]
    AL29                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.664ns  (logic 2.908ns (37.943%)  route 4.756ns (62.057%))
  Logic Levels:           9  (IBUF=1 LUT4=5 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.056     1.646    A_IBUF[4]
    SLICE_X1Y58                                                       r  O_OBUF[8]_inst_i_7/I5
    SLICE_X1Y58          LUT6 (Prop_lut6_I5_O)        0.043     1.689 r  O_OBUF[8]_inst_i_7/O
                         net (fo=4, routed)           0.427     2.116    N_899
    SLICE_X2Y59                                                       r  O_OBUF[8]_inst_i_5/I3
    SLICE_X2Y59          LUT6 (Prop_lut6_I3_O)        0.043     2.159 r  O_OBUF[8]_inst_i_5/O
                         net (fo=4, routed)           0.404     2.563    N_1165
    SLICE_X1Y60                                                       r  O_OBUF[7]_inst_i_2/I2
    SLICE_X1Y60          LUT4 (Prop_lut4_I2_O)        0.049     2.612 r  O_OBUF[7]_inst_i_2/O
                         net (fo=4, routed)           0.420     3.032    N_1183
    SLICE_X3Y60                                                       r  O_OBUF[15]_inst_i_13/I0
    SLICE_X3Y60          LUT4 (Prop_lut4_I0_O)        0.137     3.169 r  O_OBUF[15]_inst_i_13/O
                         net (fo=3, routed)           0.467     3.636    N_1198
    SLICE_X2Y60                                                       r  O_OBUF[11]_inst_i_5/I1
    SLICE_X2Y60          LUT4 (Prop_lut4_I1_O)        0.129     3.765 r  O_OBUF[11]_inst_i_5/O
                         net (fo=4, routed)           0.430     4.195    N_1448
    SLICE_X0Y60                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.043     4.238 r  O_OBUF[10]_inst_i_3/O
                         net (fo=1, routed)           0.224     4.462    N_1698
    SLICE_X0Y60                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X0Y60          LUT4 (Prop_lut4_I1_O)        0.043     4.505 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.328     5.833    O_OBUF[10]
    AL30                                                              r  O_OBUF[10]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.664 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.664    O[10]
    AL30                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.289ns  (logic 2.858ns (39.213%)  route 4.431ns (60.787%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.145     1.735    A_IBUF[4]
    SLICE_X2Y58                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X2Y58          LUT6 (Prop_lut6_I1_O)        0.043     1.778 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.414     2.192    N_898
    SLICE_X1Y58                                                       r  O_OBUF[7]_inst_i_3/I2
    SLICE_X1Y58          LUT3 (Prop_lut3_I2_O)        0.053     2.245 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.410     2.655    N_1148
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_7/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.131     2.786 r  O_OBUF[9]_inst_i_7/O
                         net (fo=5, routed)           0.384     3.169    N_1399
    SLICE_X1Y59                                                       r  O_OBUF[10]_inst_i_5/I1
    SLICE_X1Y59          LUT6 (Prop_lut6_I1_O)        0.043     3.212 r  O_OBUF[10]_inst_i_5/O
                         net (fo=3, routed)           0.410     3.623    N_1432
    SLICE_X0Y58                                                       r  O_OBUF[9]_inst_i_3/I1
    SLICE_X0Y58          LUT4 (Prop_lut4_I1_O)        0.051     3.674 r  O_OBUF[9]_inst_i_3/O
                         net (fo=1, routed)           0.324     3.997    N_1682
    SLICE_X0Y59                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X0Y59          LUT4 (Prop_lut4_I1_O)        0.129     4.126 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.344     5.471    O_OBUF[9]
    AK28                                                              r  O_OBUF[9]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.289 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.289    O[9]
    AK28                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.807ns  (logic 2.719ns (39.946%)  route 4.088ns (60.054%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.145     1.735    A_IBUF[4]
    SLICE_X2Y58                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X2Y58          LUT6 (Prop_lut6_I1_O)        0.043     1.778 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.414     2.192    N_898
    SLICE_X1Y58                                                       r  O_OBUF[7]_inst_i_3/I2
    SLICE_X1Y58          LUT3 (Prop_lut3_I2_O)        0.053     2.245 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.410     2.655    N_1148
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_7/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.131     2.786 r  O_OBUF[9]_inst_i_7/O
                         net (fo=5, routed)           0.430     3.215    N_1399
    SLICE_X0Y59                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X0Y59          LUT4 (Prop_lut4_I0_O)        0.043     3.258 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.434     3.692    N_1414
    SLICE_X0Y61                                                       r  O_OBUF[8]_inst_i_1/I4
    SLICE_X0Y61          LUT6 (Prop_lut6_I4_O)        0.043     3.735 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.255     4.990    O_OBUF[8]
    AL28                                                              r  O_OBUF[8]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.807 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.807    O[8]
    AL28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.744ns  (logic 2.816ns (41.764%)  route 3.927ns (58.236%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.145     1.735    A_IBUF[4]
    SLICE_X2Y58                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X2Y58          LUT6 (Prop_lut6_I1_O)        0.043     1.778 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.414     2.192    N_898
    SLICE_X1Y58                                                       r  O_OBUF[7]_inst_i_3/I2
    SLICE_X1Y58          LUT3 (Prop_lut3_I2_O)        0.053     2.245 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.410     2.655    N_1148
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_7/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.131     2.786 r  O_OBUF[9]_inst_i_7/O
                         net (fo=5, routed)           0.430     3.215    N_1399
    SLICE_X0Y59                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X0Y59          LUT4 (Prop_lut4_I0_O)        0.043     3.258 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.235     3.493    N_1414
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X0Y61          LUT5 (Prop_lut5_I0_O)        0.049     3.542 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.294     4.836    O_OBUF[3]
    AK27                                                              r  O_OBUF[6]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.908     6.744 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.744    O[6]
    AK27                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[4]
                            (input port)
  Destination:            O[3]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.548ns  (logic 2.790ns (42.609%)  route 3.758ns (57.391%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP25                                              0.000     0.000 r  A[4] (IN)
                         net (fo=0)                   0.000     0.000    A[4]
    AP25                                                              r  A_IBUF[4]_inst/I
    AP25                 IBUF (Prop_ibuf_I_O)         0.590     0.590 r  A_IBUF[4]_inst/O
                         net (fo=12, routed)          1.145     1.735    A_IBUF[4]
    SLICE_X2Y58                                                       r  O_OBUF[7]_inst_i_6/I1
    SLICE_X2Y58          LUT6 (Prop_lut6_I1_O)        0.043     1.778 r  O_OBUF[7]_inst_i_6/O
                         net (fo=3, routed)           0.414     2.192    N_898
    SLICE_X1Y58                                                       r  O_OBUF[7]_inst_i_3/I2
    SLICE_X1Y58          LUT3 (Prop_lut3_I2_O)        0.053     2.245 r  O_OBUF[7]_inst_i_3/O
                         net (fo=3, routed)           0.410     2.655    N_1148
    SLICE_X1Y60                                                       r  O_OBUF[9]_inst_i_7/I5
    SLICE_X1Y60          LUT6 (Prop_lut6_I5_O)        0.131     2.786 r  O_OBUF[9]_inst_i_7/O
                         net (fo=5, routed)           0.430     3.215    N_1399
    SLICE_X0Y59                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X0Y59          LUT4 (Prop_lut4_I0_O)        0.043     3.258 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.235     3.493    N_1414
    SLICE_X0Y61                                                       r  O_OBUF[6]_inst_i_1/I0
    SLICE_X0Y61          LUT5 (Prop_lut5_I0_O)        0.049     3.542 r  O_OBUF[6]_inst_i_1/O
                         net (fo=2, routed)           1.124     4.666    O_OBUF[3]
    AG25                                                              r  O_OBUF[3]_inst/I
    AG25                 OBUF (Prop_obuf_I_O)         1.881     6.548 r  O_OBUF[3]_inst/O
                         net (fo=0)                   0.000     6.548    O[3]
    AG25                                                              r  O[3] (OUT)
  -------------------------------------------------------------------    -------------------




