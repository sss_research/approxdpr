Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 17:29:15 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_078_evolib.txt -name O
| Design       : mul8_078
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.130ns  (logic 2.865ns (35.232%)  route 5.266ns (64.768%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 f  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              f  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 f  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       f  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 r  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.439     2.513    N_34
    SLICE_X2Y63                                                       r  O_OBUF[8]_inst_i_9/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.556 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.367     2.923    N_876
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.966 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.415     3.382    N_1573
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X0Y64          LUT5 (Prop_lut5_I3_O)        0.051     3.433 r  O_OBUF[10]_inst_i_6/O
                         net (fo=2, routed)           0.518     3.950    N_1751
    SLICE_X0Y66                                                       r  O_OBUF[12]_inst_i_3/I4
    SLICE_X0Y66          LUT5 (Prop_lut5_I4_O)        0.129     4.079 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.330     4.410    N_1929
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_4/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.052     4.462 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.254     4.716    N_1957
    SLICE_X1Y65                                                       r  O_OBUF[15]_inst_i_1/I2
    SLICE_X1Y65          LUT6 (Prop_lut6_I2_O)        0.131     4.847 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.432     6.279    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.130 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.130    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.098ns  (logic 2.941ns (36.313%)  route 5.158ns (63.687%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT5=4 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 f  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              f  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 f  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       f  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 r  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.439     2.513    N_34
    SLICE_X2Y63                                                       r  O_OBUF[8]_inst_i_9/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.556 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.367     2.923    N_876
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.966 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.415     3.382    N_1573
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X0Y64          LUT5 (Prop_lut5_I3_O)        0.051     3.433 r  O_OBUF[10]_inst_i_6/O
                         net (fo=2, routed)           0.518     3.950    N_1751
    SLICE_X0Y66                                                       r  O_OBUF[12]_inst_i_3/I4
    SLICE_X0Y66          LUT5 (Prop_lut5_I4_O)        0.129     4.079 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.330     4.410    N_1929
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_4/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.052     4.462 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.222     4.683    N_1957
    SLICE_X1Y65                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X1Y65          LUT5 (Prop_lut5_I2_O)        0.137     4.820 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.357     6.177    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.922     8.098 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.098    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.983ns  (logic 2.846ns (35.649%)  route 5.137ns (64.351%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT4=1 LUT5=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 f  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              f  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 f  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       f  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 r  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.439     2.513    N_34
    SLICE_X2Y63                                                       r  O_OBUF[8]_inst_i_9/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.556 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.367     2.923    N_876
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.966 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.415     3.382    N_1573
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X0Y64          LUT5 (Prop_lut5_I3_O)        0.051     3.433 r  O_OBUF[10]_inst_i_6/O
                         net (fo=2, routed)           0.518     3.950    N_1751
    SLICE_X0Y66                                                       r  O_OBUF[12]_inst_i_3/I4
    SLICE_X0Y66          LUT5 (Prop_lut5_I4_O)        0.129     4.079 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.330     4.410    N_1929
    SLICE_X0Y65                                                       r  O_OBUF[15]_inst_i_4/I2
    SLICE_X0Y65          LUT5 (Prop_lut5_I2_O)        0.052     4.462 r  O_OBUF[15]_inst_i_4/O
                         net (fo=3, routed)           0.222     4.683    N_1957
    SLICE_X1Y65                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X1Y65          LUT3 (Prop_lut3_I1_O)        0.131     4.814 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.336     6.151    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.983 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.983    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.563ns  (logic 2.715ns (35.896%)  route 4.848ns (64.104%))
  Logic Levels:           8  (IBUF=1 LUT4=1 LUT5=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 f  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              f  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 f  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       f  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 r  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.439     2.513    N_34
    SLICE_X2Y63                                                       r  O_OBUF[8]_inst_i_9/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.556 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.367     2.923    N_876
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.966 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.415     3.382    N_1573
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X0Y64          LUT5 (Prop_lut5_I3_O)        0.051     3.433 r  O_OBUF[10]_inst_i_6/O
                         net (fo=2, routed)           0.518     3.950    N_1751
    SLICE_X0Y66                                                       r  O_OBUF[12]_inst_i_3/I4
    SLICE_X0Y66          LUT5 (Prop_lut5_I4_O)        0.129     4.079 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.330     4.410    N_1929
    SLICE_X0Y65                                                       r  O_OBUF[12]_inst_i_1/I1
    SLICE_X0Y65          LUT5 (Prop_lut5_I1_O)        0.043     4.453 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.269     5.721    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.563 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.563    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.304ns  (logic 2.714ns (37.163%)  route 4.590ns (62.837%))
  Logic Levels:           8  (IBUF=1 LUT3=1 LUT4=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 f  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              f  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 f  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       f  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 r  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.439     2.513    N_34
    SLICE_X2Y63                                                       r  O_OBUF[8]_inst_i_9/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.556 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.367     2.923    N_876
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.966 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.415     3.382    N_1573
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X0Y64          LUT5 (Prop_lut5_I3_O)        0.051     3.433 r  O_OBUF[10]_inst_i_6/O
                         net (fo=2, routed)           0.518     3.950    N_1751
    SLICE_X0Y66                                                       r  O_OBUF[12]_inst_i_3/I4
    SLICE_X0Y66          LUT5 (Prop_lut5_I4_O)        0.129     4.079 r  O_OBUF[12]_inst_i_3/O
                         net (fo=3, routed)           0.149     4.229    N_1929
    SLICE_X0Y66                                                       r  O_OBUF[11]_inst_i_1/I1
    SLICE_X0Y66          LUT3 (Prop_lut3_I1_O)        0.043     4.272 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.191     5.463    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.304 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.304    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.284ns  (logic 2.754ns (37.817%)  route 4.529ns (62.183%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=2 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 f  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              f  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 f  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       f  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 r  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.439     2.513    N_34
    SLICE_X2Y63                                                       r  O_OBUF[8]_inst_i_9/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.556 r  O_OBUF[8]_inst_i_9/O
                         net (fo=3, routed)           0.367     2.923    N_876
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_7/I2
    SLICE_X2Y63          LUT6 (Prop_lut6_I2_O)        0.043     2.966 r  O_OBUF[10]_inst_i_7/O
                         net (fo=2, routed)           0.415     3.382    N_1573
    SLICE_X0Y64                                                       r  O_OBUF[10]_inst_i_6/I3
    SLICE_X0Y64          LUT5 (Prop_lut5_I3_O)        0.051     3.433 r  O_OBUF[10]_inst_i_6/O
                         net (fo=2, routed)           0.518     3.950    N_1751
    SLICE_X0Y66                                                       r  O_OBUF[10]_inst_i_1/I4
    SLICE_X0Y66          LUT5 (Prop_lut5_I4_O)        0.137     4.087 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.280     5.367    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.916     7.284 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.284    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.214ns  (logic 2.568ns (35.592%)  route 4.646ns (64.408%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=2 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       r  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 f  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.502     2.576    N_34
    SLICE_X1Y61                                                       f  O_OBUF[6]_inst_i_3/I4
    SLICE_X1Y61          LUT5 (Prop_lut5_I4_O)        0.043     2.619 r  O_OBUF[6]_inst_i_3/O
                         net (fo=3, routed)           0.670     3.289    N_106
    SLICE_X1Y65                                                       r  O_OBUF[8]_inst_i_6/I3
    SLICE_X1Y65          LUT4 (Prop_lut4_I3_O)        0.043     3.332 f  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.334     3.666    O_OBUF[8]_inst_i_6_n_0
    SLICE_X0Y65                                                       f  O_OBUF[10]_inst_i_3/I0
    SLICE_X0Y65          LUT5 (Prop_lut5_I0_O)        0.043     3.709 r  O_OBUF[10]_inst_i_3/O
                         net (fo=3, routed)           0.420     4.130    N_1899
    SLICE_X0Y66                                                       r  O_OBUF[9]_inst_i_1/I1
    SLICE_X0Y66          LUT3 (Prop_lut3_I1_O)        0.043     4.173 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.210     5.383    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.214 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.214    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.843ns  (logic 2.606ns (38.083%)  route 4.237ns (61.917%))
  Logic Levels:           6  (IBUF=1 LUT4=2 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 f  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              f  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 f  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       f  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 r  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.502     2.576    N_34
    SLICE_X1Y61                                                       r  O_OBUF[6]_inst_i_3/I4
    SLICE_X1Y61          LUT5 (Prop_lut5_I4_O)        0.043     2.619 f  O_OBUF[6]_inst_i_3/O
                         net (fo=3, routed)           0.670     3.289    N_106
    SLICE_X1Y65                                                       f  O_OBUF[8]_inst_i_6/I3
    SLICE_X1Y65          LUT4 (Prop_lut4_I3_O)        0.043     3.332 r  O_OBUF[8]_inst_i_6/O
                         net (fo=2, routed)           0.334     3.666    O_OBUF[8]_inst_i_6_n_0
    SLICE_X0Y65                                                       r  O_OBUF[8]_inst_i_1/I4
    SLICE_X0Y65          LUT5 (Prop_lut5_I4_O)        0.051     3.717 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.221     4.938    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.905     6.843 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     6.843    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.798ns  (logic 2.510ns (36.918%)  route 4.288ns (63.082%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       r  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 f  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.502     2.576    N_34
    SLICE_X1Y61                                                       f  O_OBUF[6]_inst_i_3/I4
    SLICE_X1Y61          LUT5 (Prop_lut5_I4_O)        0.043     2.619 r  O_OBUF[6]_inst_i_3/O
                         net (fo=3, routed)           0.598     3.217    N_106
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_6/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     3.260 r  O_OBUF[7]_inst_i_6/O
                         net (fo=1, routed)           0.409     3.669    N_1869
    SLICE_X1Y63                                                       r  O_OBUF[7]_inst_i_1/I4
    SLICE_X1Y63          LUT6 (Prop_lut6_I4_O)        0.043     3.712 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.981    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     6.798 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     6.798    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[0]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.025ns  (logic 2.473ns (41.038%)  route 3.553ns (58.962%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AF24                                              0.000     0.000 r  A[0] (IN)
                         net (fo=0)                   0.000     0.000    A[0]
    AF24                                                              r  A_IBUF[0]_inst/I
    AF24                 IBUF (Prop_ibuf_I_O)         0.521     0.521 r  A_IBUF[0]_inst/O
                         net (fo=9, routed)           1.510     2.031    A_IBUF[0]
    SLICE_X3Y63                                                       r  O_OBUF[5]_inst_i_2/I0
    SLICE_X3Y63          LUT4 (Prop_lut4_I0_O)        0.043     2.074 f  O_OBUF[5]_inst_i_2/O
                         net (fo=8, routed)           0.502     2.576    N_34
    SLICE_X1Y61                                                       f  O_OBUF[6]_inst_i_3/I4
    SLICE_X1Y61          LUT5 (Prop_lut5_I4_O)        0.043     2.619 r  O_OBUF[6]_inst_i_3/O
                         net (fo=3, routed)           0.344     2.963    N_106
    SLICE_X1Y64                                                       r  O_OBUF[6]_inst_i_1/I3
    SLICE_X1Y64          LUT6 (Prop_lut6_I3_O)        0.043     3.006 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.197     4.203    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.025 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.025    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




