Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Tue Nov 14 01:24:16 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_239_evolib.txt -name O
| Design       : mul8_239
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.306ns  (logic 2.999ns (36.108%)  route 5.307ns (63.892%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT4=1 LUT5=3 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=20, routed)          1.433     2.026    A_IBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_22/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.069 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.337     2.405    N_503
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_21/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.448 r  O_OBUF[9]_inst_i_21/O
                         net (fo=2, routed)           0.332     2.781    N_602
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_12/I4
    SLICE_X2Y63          LUT5 (Prop_lut5_I4_O)        0.049     2.830 r  O_OBUF[9]_inst_i_12/O
                         net (fo=2, routed)           0.612     3.442    N_676
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.127     3.569 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.578     4.147    N_747
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X2Y64          LUT6 (Prop_lut6_I0_O)        0.043     4.190 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.319     4.509    N_1072
    SLICE_X2Y66                                                       r  O_OBUF[15]_inst_i_6/I2
    SLICE_X2Y66          LUT4 (Prop_lut4_I2_O)        0.047     4.556 r  O_OBUF[15]_inst_i_6/O
                         net (fo=2, routed)           0.408     4.964    O_OBUF[15]_inst_i_6_n_0
    SLICE_X1Y66                                                       r  O_OBUF[13]_inst_i_1/I1
    SLICE_X1Y66          LUT3 (Prop_lut3_I1_O)        0.135     5.099 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.288     6.387    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.919     8.306 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     8.306    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.301ns  (logic 2.924ns (35.222%)  route 5.377ns (64.778%))
  Logic Levels:           9  (IBUF=1 LUT4=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=20, routed)          1.433     2.026    A_IBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_22/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.069 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.337     2.405    N_503
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_21/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.448 r  O_OBUF[9]_inst_i_21/O
                         net (fo=2, routed)           0.332     2.781    N_602
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_12/I4
    SLICE_X2Y63          LUT5 (Prop_lut5_I4_O)        0.049     2.830 r  O_OBUF[9]_inst_i_12/O
                         net (fo=2, routed)           0.612     3.442    N_676
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.127     3.569 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.578     4.147    N_747
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X2Y64          LUT6 (Prop_lut6_I0_O)        0.043     4.190 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.319     4.509    N_1072
    SLICE_X2Y66                                                       r  O_OBUF[15]_inst_i_6/I2
    SLICE_X2Y66          LUT4 (Prop_lut4_I2_O)        0.047     4.556 r  O_OBUF[15]_inst_i_6/O
                         net (fo=2, routed)           0.408     4.964    O_OBUF[15]_inst_i_6_n_0
    SLICE_X1Y66                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X1Y66          LUT6 (Prop_lut6_I4_O)        0.127     5.091 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.358     6.449    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     8.301 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     8.301    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        8.148ns  (logic 2.820ns (34.607%)  route 5.328ns (65.393%))
  Logic Levels:           9  (IBUF=1 LUT3=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=20, routed)          1.433     2.026    A_IBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_22/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.069 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.337     2.405    N_503
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_21/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.448 r  O_OBUF[9]_inst_i_21/O
                         net (fo=2, routed)           0.332     2.781    N_602
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_12/I4
    SLICE_X2Y63          LUT5 (Prop_lut5_I4_O)        0.049     2.830 f  O_OBUF[9]_inst_i_12/O
                         net (fo=2, routed)           0.612     3.442    N_676
    SLICE_X1Y63                                                       f  O_OBUF[9]_inst_i_4/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.127     3.569 f  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.578     4.147    N_747
    SLICE_X2Y64                                                       f  O_OBUF[12]_inst_i_5/I0
    SLICE_X2Y64          LUT6 (Prop_lut6_I0_O)        0.043     4.190 f  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.319     4.509    N_1072
    SLICE_X2Y66                                                       f  O_OBUF[14]_inst_i_3/I1
    SLICE_X2Y66          LUT3 (Prop_lut3_I1_O)        0.043     4.552 r  O_OBUF[14]_inst_i_3/O
                         net (fo=1, routed)           0.352     4.904    O_OBUF[14]_inst_i_3_n_0
    SLICE_X2Y66                                                       r  O_OBUF[14]_inst_i_1/I2
    SLICE_X2Y66          LUT6 (Prop_lut6_I2_O)        0.043     4.947 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.366     6.313    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.836     8.148 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     8.148    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.871ns  (logic 2.782ns (35.350%)  route 5.089ns (64.650%))
  Logic Levels:           8  (IBUF=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=20, routed)          1.433     2.026    A_IBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_22/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.069 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.337     2.405    N_503
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_21/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.448 r  O_OBUF[9]_inst_i_21/O
                         net (fo=2, routed)           0.332     2.781    N_602
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_12/I4
    SLICE_X2Y63          LUT5 (Prop_lut5_I4_O)        0.049     2.830 r  O_OBUF[9]_inst_i_12/O
                         net (fo=2, routed)           0.612     3.442    N_676
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.127     3.569 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.578     4.147    N_747
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X2Y64          LUT6 (Prop_lut6_I0_O)        0.043     4.190 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.527     4.717    N_1072
    SLICE_X2Y65                                                       r  O_OBUF[11]_inst_i_1/I3
    SLICE_X2Y65          LUT6 (Prop_lut6_I3_O)        0.043     4.760 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.270     6.030    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     7.871 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     7.871    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[3]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.704ns  (logic 2.907ns (37.739%)  route 4.796ns (62.261%))
  Logic Levels:           9  (IBUF=1 LUT2=1 LUT3=2 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP26                                              0.000     0.000 r  A[3] (IN)
                         net (fo=0)                   0.000     0.000    A[3]
    AP26                                                              r  A_IBUF[3]_inst/I
    AP26                 IBUF (Prop_ibuf_I_O)         0.594     0.594 r  A_IBUF[3]_inst/O
                         net (fo=19, routed)          1.327     1.921    A_IBUF[3]
    SLICE_X4Y64                                                       r  O_OBUF[12]_inst_i_29/I4
    SLICE_X4Y64          LUT6 (Prop_lut6_I4_O)        0.043     1.964 r  O_OBUF[12]_inst_i_29/O
                         net (fo=2, routed)           0.343     2.307    N_515
    SLICE_X4Y64                                                       r  O_OBUF[12]_inst_i_28/I0
    SLICE_X4Y64          LUT5 (Prop_lut5_I0_O)        0.048     2.355 r  O_OBUF[12]_inst_i_28/O
                         net (fo=2, routed)           0.517     2.871    N_614
    SLICE_X4Y62                                                       r  O_OBUF[12]_inst_i_12/I2
    SLICE_X4Y62          LUT3 (Prop_lut3_I2_O)        0.129     3.000 r  O_OBUF[12]_inst_i_12/O
                         net (fo=5, routed)           0.425     3.426    N_691
    SLICE_X4Y65                                                       r  O_OBUF[11]_inst_i_4/I1
    SLICE_X4Y65          LUT3 (Prop_lut3_I1_O)        0.043     3.469 r  O_OBUF[11]_inst_i_4/O
                         net (fo=2, routed)           0.503     3.972    N_764
    SLICE_X3Y64                                                       r  O_OBUF[12]_inst_i_6/I0
    SLICE_X3Y64          LUT5 (Prop_lut5_I0_O)        0.048     4.020 r  O_OBUF[12]_inst_i_6/O
                         net (fo=3, routed)           0.184     4.204    N_1160
    SLICE_X3Y66                                                       r  O_OBUF[15]_inst_i_4/I4
    SLICE_X3Y66          LUT5 (Prop_lut5_I4_O)        0.129     4.333 r  O_OBUF[15]_inst_i_4/O
                         net (fo=4, routed)           0.299     4.632    N_1422
    SLICE_X1Y66                                                       r  O_OBUF[10]_inst_i_1/I1
    SLICE_X1Y66          LUT2 (Prop_lut2_I1_O)        0.043     4.675 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.199     5.874    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     7.704 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     7.704    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.669ns  (logic 2.783ns (36.288%)  route 4.886ns (63.712%))
  Logic Levels:           8  (IBUF=1 LUT5=3 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=20, routed)          1.433     2.026    A_IBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_22/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.069 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.337     2.405    N_503
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_21/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.448 r  O_OBUF[9]_inst_i_21/O
                         net (fo=2, routed)           0.332     2.781    N_602
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_12/I4
    SLICE_X2Y63          LUT5 (Prop_lut5_I4_O)        0.049     2.830 r  O_OBUF[9]_inst_i_12/O
                         net (fo=2, routed)           0.612     3.442    N_676
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.127     3.569 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.578     4.147    N_747
    SLICE_X2Y64                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X2Y64          LUT6 (Prop_lut6_I0_O)        0.043     4.190 r  O_OBUF[12]_inst_i_5/O
                         net (fo=4, routed)           0.320     4.510    N_1072
    SLICE_X2Y66                                                       r  O_OBUF[12]_inst_i_1/I3
    SLICE_X2Y66          LUT6 (Prop_lut6_I3_O)        0.043     4.553 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.275     5.827    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     7.669 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     7.669    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.306ns  (logic 2.730ns (37.362%)  route 4.576ns (62.638%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=20, routed)          1.433     2.026    A_IBUF[1]
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_22/I4
    SLICE_X0Y63          LUT6 (Prop_lut6_I4_O)        0.043     2.069 r  O_OBUF[9]_inst_i_22/O
                         net (fo=2, routed)           0.337     2.405    N_503
    SLICE_X0Y63                                                       r  O_OBUF[9]_inst_i_21/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.043     2.448 r  O_OBUF[9]_inst_i_21/O
                         net (fo=2, routed)           0.332     2.781    N_602
    SLICE_X2Y63                                                       r  O_OBUF[9]_inst_i_12/I4
    SLICE_X2Y63          LUT5 (Prop_lut5_I4_O)        0.049     2.830 r  O_OBUF[9]_inst_i_12/O
                         net (fo=2, routed)           0.612     3.442    N_676
    SLICE_X1Y63                                                       r  O_OBUF[9]_inst_i_4/I3
    SLICE_X1Y63          LUT5 (Prop_lut5_I3_O)        0.127     3.569 r  O_OBUF[9]_inst_i_4/O
                         net (fo=3, routed)           0.653     4.222    N_747
    SLICE_X1Y66                                                       r  O_OBUF[9]_inst_i_1/I2
    SLICE_X1Y66          LUT4 (Prop_lut4_I2_O)        0.043     4.265 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.210     5.475    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.831     7.306 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     7.306    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.249ns  (logic 2.808ns (38.729%)  route 4.442ns (61.271%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=20, routed)          1.869     2.463    A_IBUF[1]
    SLICE_X0Y64                                                       r  O_OBUF[2]_inst_i_2/I0
    SLICE_X0Y64          LUT5 (Prop_lut5_I0_O)        0.048     2.511 r  O_OBUF[2]_inst_i_2/O
                         net (fo=2, routed)           0.228     2.738    N_433
    SLICE_X0Y64                                                       r  O_OBUF[5]_inst_i_2/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.129     2.867 f  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.526     3.393    O_OBUF[5]_inst_i_2_n_0
    SLICE_X0Y62                                                       f  O_OBUF[8]_inst_i_5/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.436 r  O_OBUF[8]_inst_i_5/O
                         net (fo=1, routed)           0.235     3.671    O_OBUF[8]_inst_i_5_n_0
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.714 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.392     4.107    N_922
    SLICE_X1Y64                                                       r  O_OBUF[7]_inst_i_1/I2
    SLICE_X1Y64          LUT3 (Prop_lut3_I2_O)        0.049     4.156 r  O_OBUF[7]_inst_i_1/O
                         net (fo=1, routed)           1.191     5.347    O_OBUF[7]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.902     7.249 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     7.249    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[8]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.148ns  (logic 2.718ns (38.022%)  route 4.430ns (61.978%))
  Logic Levels:           7  (IBUF=1 LUT5=2 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=20, routed)          1.869     2.463    A_IBUF[1]
    SLICE_X0Y64                                                       r  O_OBUF[2]_inst_i_2/I0
    SLICE_X0Y64          LUT5 (Prop_lut5_I0_O)        0.048     2.511 r  O_OBUF[2]_inst_i_2/O
                         net (fo=2, routed)           0.228     2.738    N_433
    SLICE_X0Y64                                                       r  O_OBUF[5]_inst_i_2/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.129     2.867 f  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.526     3.393    O_OBUF[5]_inst_i_2_n_0
    SLICE_X0Y62                                                       f  O_OBUF[8]_inst_i_5/I1
    SLICE_X0Y62          LUT6 (Prop_lut6_I1_O)        0.043     3.436 r  O_OBUF[8]_inst_i_5/O
                         net (fo=1, routed)           0.235     3.671    O_OBUF[8]_inst_i_5_n_0
    SLICE_X1Y62                                                       r  O_OBUF[8]_inst_i_2/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.043     3.714 r  O_OBUF[8]_inst_i_2/O
                         net (fo=2, routed)           0.392     4.107    N_922
    SLICE_X1Y64                                                       r  O_OBUF[8]_inst_i_1/I0
    SLICE_X1Y64          LUT5 (Prop_lut5_I0_O)        0.043     4.150 r  O_OBUF[8]_inst_i_1/O
                         net (fo=1, routed)           1.180     5.330    O_OBUF[8]
    AK28                                                              r  O_OBUF[8]_inst/I
    AK28                 OBUF (Prop_obuf_I_O)         1.819     7.148 r  O_OBUF[8]_inst/O
                         net (fo=0)                   0.000     7.148    O[8]
    AK28                                                              r  O[8] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[1]
                            (input port)
  Destination:            O[6]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.755ns  (logic 2.679ns (39.656%)  route 4.076ns (60.344%))
  Logic Levels:           6  (IBUF=1 LUT5=1 LUT6=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AP27                                              0.000     0.000 r  A[1] (IN)
                         net (fo=0)                   0.000     0.000    A[1]
    AP27                                                              r  A_IBUF[1]_inst/I
    AP27                 IBUF (Prop_ibuf_I_O)         0.593     0.593 r  A_IBUF[1]_inst/O
                         net (fo=20, routed)          1.869     2.463    A_IBUF[1]
    SLICE_X0Y64                                                       r  O_OBUF[2]_inst_i_2/I0
    SLICE_X0Y64          LUT5 (Prop_lut5_I0_O)        0.048     2.511 r  O_OBUF[2]_inst_i_2/O
                         net (fo=2, routed)           0.228     2.738    N_433
    SLICE_X0Y64                                                       r  O_OBUF[5]_inst_i_2/I5
    SLICE_X0Y64          LUT6 (Prop_lut6_I5_O)        0.129     2.867 f  O_OBUF[5]_inst_i_2/O
                         net (fo=5, routed)           0.476     3.344    O_OBUF[5]_inst_i_2_n_0
    SLICE_X0Y61                                                       f  O_OBUF[6]_inst_i_7/I0
    SLICE_X0Y61          LUT6 (Prop_lut6_I0_O)        0.043     3.387 r  O_OBUF[6]_inst_i_7/O
                         net (fo=1, routed)           0.234     3.621    O_OBUF[6]_inst_i_7_n_0
    SLICE_X1Y62                                                       r  O_OBUF[6]_inst_i_1/I5
    SLICE_X1Y62          LUT6 (Prop_lut6_I5_O)        0.043     3.664 r  O_OBUF[6]_inst_i_1/O
                         net (fo=1, routed)           1.269     4.933    O_OBUF[6]
    AK26                                                              r  O_OBUF[6]_inst/I
    AK26                 OBUF (Prop_obuf_I_O)         1.823     6.755 r  O_OBUF[6]_inst/O
                         net (fo=0)                   0.000     6.755    O[6]
    AK26                                                              r  O[6] (OUT)
  -------------------------------------------------------------------    -------------------




