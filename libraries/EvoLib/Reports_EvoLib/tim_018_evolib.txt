Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2017.1 (win64) Build 1846317 Fri Apr 14 18:55:03 MDT 2017
| Date         : Mon Nov 13 14:29:33 2017
| Host         : cfaed-pdpc003 running 64-bit major release  (build 9200)
| Command      : report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_018_evolib.txt -name O
| Design       : mul8_018
| Device       : 7vx330t-ffg1157
| Speed File   : -3  PRODUCTION 1.11 2014-09-11
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[13]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.255ns  (logic 2.912ns (40.143%)  route 4.343ns (59.857%))
  Logic Levels:           7  (IBUF=1 LUT3=1 LUT4=1 LUT5=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.339     1.925    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.049     1.974 r  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.441     2.416    O_OBUF[10]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_6/I0
    SLICE_X2Y63          LUT5 (Prop_lut5_I0_O)        0.127     2.543 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.287     2.830    O_OBUF[13]_inst_i_6_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X1Y63          LUT5 (Prop_lut5_I2_O)        0.051     2.881 f  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.508     3.389    O_OBUF[12]_inst_i_2_n_0
    SLICE_X0Y62                                                       f  O_OBUF[13]_inst_i_4/I4
    SLICE_X0Y62          LUT5 (Prop_lut5_I4_O)        0.137     3.526 r  O_OBUF[13]_inst_i_4/O
                         net (fo=1, routed)           0.337     3.863    O_OBUF[13]_inst_i_4_n_0
    SLICE_X1Y62                                                       r  O_OBUF[13]_inst_i_1/I2
    SLICE_X1Y62          LUT3 (Prop_lut3_I2_O)        0.129     3.992 r  O_OBUF[13]_inst_i_1/O
                         net (fo=1, routed)           1.431     5.422    O_OBUF[13]
    AK29                                                              r  O_OBUF[13]_inst/I
    AK29                 OBUF (Prop_obuf_I_O)         1.833     7.255 r  O_OBUF[13]_inst/O
                         net (fo=0)                   0.000     7.255    O[13]
    AK29                                                              r  O[13] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[14]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        7.036ns  (logic 2.920ns (41.505%)  route 4.116ns (58.495%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.339     1.925    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.049     1.974 r  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.441     2.416    O_OBUF[10]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_6/I0
    SLICE_X2Y63          LUT5 (Prop_lut5_I0_O)        0.127     2.543 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.287     2.830    O_OBUF[13]_inst_i_6_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X1Y63          LUT5 (Prop_lut5_I2_O)        0.051     2.881 f  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.411     3.292    O_OBUF[12]_inst_i_2_n_0
    SLICE_X1Y62                                                       f  O_OBUF[15]_inst_i_2/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.129     3.421 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.222     3.643    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[14]_inst_i_1/I0
    SLICE_X0Y62          LUT5 (Prop_lut5_I0_O)        0.054     3.697 r  O_OBUF[14]_inst_i_1/O
                         net (fo=1, routed)           1.415     5.112    O_OBUF[14]
    AJ29                                                              r  O_OBUF[14]_inst/I
    AJ29                 OBUF (Prop_obuf_I_O)         1.924     7.036 r  O_OBUF[14]_inst/O
                         net (fo=0)                   0.000     7.036    O[14]
    AJ29                                                              r  O[14] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[15]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.941ns  (logic 2.837ns (40.874%)  route 4.104ns (59.126%))
  Logic Levels:           7  (IBUF=1 LUT4=1 LUT5=3 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.339     1.925    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.049     1.974 r  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.441     2.416    O_OBUF[10]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_6/I0
    SLICE_X2Y63          LUT5 (Prop_lut5_I0_O)        0.127     2.543 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.287     2.830    O_OBUF[13]_inst_i_6_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X1Y63          LUT5 (Prop_lut5_I2_O)        0.051     2.881 f  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.411     3.292    O_OBUF[12]_inst_i_2_n_0
    SLICE_X1Y62                                                       f  O_OBUF[15]_inst_i_2/I0
    SLICE_X1Y62          LUT6 (Prop_lut6_I0_O)        0.129     3.421 r  O_OBUF[15]_inst_i_2/O
                         net (fo=2, routed)           0.222     3.643    O_OBUF[15]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[15]_inst_i_1/I4
    SLICE_X0Y62          LUT5 (Prop_lut5_I4_O)        0.043     3.686 r  O_OBUF[15]_inst_i_1/O
                         net (fo=1, routed)           1.403     5.089    O_OBUF[15]
    AN30                                                              r  O_OBUF[15]_inst/I
    AN30                 OBUF (Prop_obuf_I_O)         1.851     6.941 r  O_OBUF[15]_inst/O
                         net (fo=0)                   0.000     6.941    O[15]
    AN30                                                              r  O[15] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[12]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.690ns  (logic 2.784ns (41.619%)  route 3.906ns (58.381%))
  Logic Levels:           6  (IBUF=1 LUT4=1 LUT5=3 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.339     1.925    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.049     1.974 r  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.441     2.416    O_OBUF[10]_inst_i_3_n_0
    SLICE_X2Y63                                                       r  O_OBUF[13]_inst_i_6/I0
    SLICE_X2Y63          LUT5 (Prop_lut5_I0_O)        0.127     2.543 r  O_OBUF[13]_inst_i_6/O
                         net (fo=2, routed)           0.287     2.830    O_OBUF[13]_inst_i_6_n_0
    SLICE_X1Y63                                                       r  O_OBUF[12]_inst_i_2/I2
    SLICE_X1Y63          LUT5 (Prop_lut5_I2_O)        0.051     2.881 r  O_OBUF[12]_inst_i_2/O
                         net (fo=3, routed)           0.508     3.389    O_OBUF[12]_inst_i_2_n_0
    SLICE_X0Y62                                                       r  O_OBUF[12]_inst_i_1/I0
    SLICE_X0Y62          LUT5 (Prop_lut5_I0_O)        0.129     3.518 r  O_OBUF[12]_inst_i_1/O
                         net (fo=1, routed)           1.330     4.848    O_OBUF[12]
    AL31                                                              r  O_OBUF[12]_inst/I
    AL31                 OBUF (Prop_obuf_I_O)         1.842     6.690 r  O_OBUF[12]_inst/O
                         net (fo=0)                   0.000     6.690    O[12]
    AL31                                                              r  O[12] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[11]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        6.566ns  (logic 2.880ns (43.861%)  route 3.686ns (56.139%))
  Logic Levels:           6  (IBUF=1 LUT4=2 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.051     1.638    A_IBUF[6]
    SLICE_X1Y62                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.051     1.689 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.315     2.004    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X0Y61          LUT5 (Prop_lut5_I0_O)        0.135     2.139 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.484     2.623    O_OBUF[10]_inst_i_4_n_0
    SLICE_X0Y63                                                       r  O_OBUF[12]_inst_i_5/I0
    SLICE_X0Y63          LUT5 (Prop_lut5_I0_O)        0.137     2.760 r  O_OBUF[12]_inst_i_5/O
                         net (fo=3, routed)           0.423     3.183    O_OBUF[12]_inst_i_5_n_0
    SLICE_X0Y62                                                       r  O_OBUF[11]_inst_i_1/I3
    SLICE_X0Y62          LUT4 (Prop_lut4_I3_O)        0.129     3.312 r  O_OBUF[11]_inst_i_1/O
                         net (fo=1, routed)           1.413     4.725    O_OBUF[11]
    AM31                                                              r  O_OBUF[11]_inst/I
    AM31                 OBUF (Prop_obuf_I_O)         1.841     6.566 r  O_OBUF[11]_inst/O
                         net (fo=0)                   0.000     6.566    O[11]
    AM31                                                              r  O[11] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[7]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.984ns  (logic 2.525ns (42.194%)  route 3.459ns (57.806%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.253     1.832    A_IBUF[7]
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_3/I0
    SLICE_X2Y61          LUT3 (Prop_lut3_I0_O)        0.043     1.875 r  O_OBUF[6]_inst_i_3/O
                         net (fo=1, routed)           0.419     2.295    O_OBUF[6]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_1/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.338 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.436     2.773    O_OBUF[1]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.816 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.351     4.168    O_OBUF[5]
    AL28                                                              r  O_OBUF[7]_inst/I
    AL28                 OBUF (Prop_obuf_I_O)         1.816     5.984 r  O_OBUF[7]_inst/O
                         net (fo=0)                   0.000     5.984    O[7]
    AL28                                                              r  O[7] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[7]
                            (input port)
  Destination:            O[5]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.897ns  (logic 2.530ns (42.907%)  route 3.367ns (57.093%))
  Logic Levels:           5  (IBUF=1 LUT3=1 LUT6=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AN25                                              0.000     0.000 r  A[7] (IN)
                         net (fo=0)                   0.000     0.000    A[7]
    AN25                                                              r  A_IBUF[7]_inst/I
    AN25                 IBUF (Prop_ibuf_I_O)         0.580     0.580 r  A_IBUF[7]_inst/O
                         net (fo=16, routed)          1.253     1.832    A_IBUF[7]
    SLICE_X2Y61                                                       r  O_OBUF[6]_inst_i_3/I0
    SLICE_X2Y61          LUT3 (Prop_lut3_I0_O)        0.043     1.875 r  O_OBUF[6]_inst_i_3/O
                         net (fo=1, routed)           0.419     2.295    O_OBUF[6]_inst_i_3_n_0
    SLICE_X0Y60                                                       r  O_OBUF[6]_inst_i_1/I1
    SLICE_X0Y60          LUT6 (Prop_lut6_I1_O)        0.043     2.338 r  O_OBUF[6]_inst_i_1/O
                         net (fo=3, routed)           0.436     2.773    O_OBUF[1]
    SLICE_X0Y60                                                       r  O_OBUF[7]_inst_i_1/I3
    SLICE_X0Y60          LUT6 (Prop_lut6_I3_O)        0.043     2.816 r  O_OBUF[7]_inst_i_1/O
                         net (fo=2, routed)           1.259     4.075    O_OBUF[5]
    AK27                                                              r  O_OBUF[5]_inst/I
    AK27                 OBUF (Prop_obuf_I_O)         1.822     5.897 r  O_OBUF[5]_inst/O
                         net (fo=0)                   0.000     5.897    O[5]
    AK27                                                              r  O[5] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[10]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.867ns  (logic 2.732ns (46.565%)  route 3.135ns (53.435%))
  Logic Levels:           5  (IBUF=1 LUT4=1 LUT5=2 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.051     1.638    A_IBUF[6]
    SLICE_X1Y62                                                       r  O_OBUF[2]_inst_i_3/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.051     1.689 r  O_OBUF[2]_inst_i_3/O
                         net (fo=4, routed)           0.315     2.004    O_OBUF[2]_inst_i_3_n_0
    SLICE_X0Y61                                                       r  O_OBUF[10]_inst_i_4/I0
    SLICE_X0Y61          LUT5 (Prop_lut5_I0_O)        0.135     2.139 r  O_OBUF[10]_inst_i_4/O
                         net (fo=2, routed)           0.484     2.623    O_OBUF[10]_inst_i_4_n_0
    SLICE_X0Y63                                                       r  O_OBUF[10]_inst_i_1/I2
    SLICE_X0Y63          LUT5 (Prop_lut5_I2_O)        0.129     2.752 r  O_OBUF[10]_inst_i_1/O
                         net (fo=1, routed)           1.285     4.037    O_OBUF[10]
    AL29                                                              r  O_OBUF[10]_inst/I
    AL29                 OBUF (Prop_obuf_I_O)         1.830     5.867 r  O_OBUF[10]_inst/O
                         net (fo=0)                   0.000     5.867    O[10]
    AL29                                                              r  O[10] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[9]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.665ns  (logic 2.641ns (46.623%)  route 3.024ns (53.377%))
  Logic Levels:           5  (IBUF=1 LUT4=2 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.051     1.638    A_IBUF[6]
    SLICE_X1Y62                                                       r  O_OBUF[10]_inst_i_7/I3
    SLICE_X1Y62          LUT4 (Prop_lut4_I3_O)        0.043     1.681 r  O_OBUF[10]_inst_i_7/O
                         net (fo=3, routed)           0.416     2.096    O_OBUF[10]_inst_i_7_n_0
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_2/I2
    SLICE_X1Y61          LUT6 (Prop_lut6_I2_O)        0.043     2.139 r  O_OBUF[9]_inst_i_2/O
                         net (fo=1, routed)           0.217     2.356    O_OBUF[9]_inst_i_2_n_0
    SLICE_X1Y61                                                       r  O_OBUF[9]_inst_i_1/I2
    SLICE_X1Y61          LUT4 (Prop_lut4_I2_O)        0.051     2.407 r  O_OBUF[9]_inst_i_1/O
                         net (fo=1, routed)           1.340     3.747    O_OBUF[9]
    AL30                                                              r  O_OBUF[9]_inst/I
    AL30                 OBUF (Prop_obuf_I_O)         1.917     5.665 r  O_OBUF[9]_inst/O
                         net (fo=0)                   0.000     5.665    O[9]
    AL30                                                              r  O[9] (OUT)
  -------------------------------------------------------------------    -------------------

Slack:                    inf
  Source:                 A[6]
                            (input port)
  Destination:            O[2]
                            (output port)
  Path Group:             (none)
  Path Type:              Max at Slow Process Corner
  Data Path Delay:        5.363ns  (logic 2.558ns (47.696%)  route 2.805ns (52.304%))
  Logic Levels:           4  (IBUF=1 LUT4=1 LUT6=1 OBUF=1)

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
    AM28                                              0.000     0.000 r  A[6] (IN)
                         net (fo=0)                   0.000     0.000    A[6]
    AM28                                                              r  A_IBUF[6]_inst/I
    AM28                 IBUF (Prop_ibuf_I_O)         0.587     0.587 r  A_IBUF[6]_inst/O
                         net (fo=19, routed)          1.339     1.925    A_IBUF[6]
    SLICE_X2Y63                                                       r  O_OBUF[10]_inst_i_3/I1
    SLICE_X2Y63          LUT4 (Prop_lut4_I1_O)        0.049     1.974 r  O_OBUF[10]_inst_i_3/O
                         net (fo=6, routed)           0.340     2.314    O_OBUF[10]_inst_i_3_n_0
    SLICE_X0Y63                                                       r  O_OBUF[2]_inst_i_1/I1
    SLICE_X0Y63          LUT6 (Prop_lut6_I1_O)        0.127     2.441 r  O_OBUF[2]_inst_i_1/O
                         net (fo=5, routed)           1.126     3.568    O_OBUF[2]
    AG25                                                              r  O_OBUF[2]_inst/I
    AG25                 OBUF (Prop_obuf_I_O)         1.795     5.363 r  O_OBUF[2]_inst/O
                         net (fo=0)                   0.000     5.363    O[2]
    AG25                                                              r  O[2] (OUT)
  -------------------------------------------------------------------    -------------------




