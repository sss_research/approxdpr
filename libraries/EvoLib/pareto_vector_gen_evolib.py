input_file = "C:\\Users\\sanjeev\\Desktop\\design_space_points_evolib.txt"
output_file = "C:\\Users\\sanjeev\\Desktop\\pareto_vector_evolib.txt"

tot_power = ""
dyn_power = ""
static_power = ""
delay = ""
pdp = ""
edp = ""
lut = ""
slices = ""
max_err_mag = ""
avg_err = ""
avg_rel_err = ""
err_occ = ""
max_err_occ = ""

tot_power_arr = []
dyn_power_arr = []
static_power_arr = []
delay_arr = []
pdp_arr = []
edp_arr = []
lut_arr = []
slices_arr = []
max_err_mag_arr = []
avg_err_arr = []
avg_rel_err_arr = []
err_occ_arr = []
max_err_occ_arr = []

with open(output_file, "w") as fh2:
	fh2.write("[")
	with open(input_file, "r") as fh1:
		position1 = fh1.seek(0,0)
		line1 = fh1.readline()
		while line1:
			length1 = len(line1)
			for i in range(length1):
				if line1[i] == "T" and line1[i+1] == "o" and line1[i+2] == "t" and line1[i+3] == "a" and line1[i+4] == "l":
					for j in range(14,length1-1):
						tot_power += str(line1[j])
					tot_power = str(tot_power)
					tot_power_arr.append(tot_power)
				if line1[i] == "D" and line1[i+1] == "y" and line1[i+2] == "n":
					for j in range(16,length1-1):
						dyn_power += str(line1[j])
					dyn_power = str(dyn_power)
					dyn_power_arr.append(dyn_power)	
				if line1[i] == "S" and line1[i+1] == "t" and line1[i+2] == "a":
					for j in range(15,length1-1):
						static_power += str(line1[j])
					static_power = str(static_power)
					static_power_arr.append(static_power)	
				if line1[i] == "D" and line1[i+1] == "e" and line1[i+2] == "l" and line1[i+3] == "a" and line1[i+4] == "y":
					for j in range(8,length1-1):
						delay += str(line1[j])
					delay = str(delay)
					delay_arr.append(delay)	
				if line1[i] == "P" and line1[i+1] == "D" and line1[i+2] == "P":
					for j in range(6,length1-1):
						pdp += str(line1[j])
					pdp = str(pdp)
					pdp_arr.append(pdp)	
				if line1[i] == "E" and line1[i+1] == "D" and line1[i+2] == "P":
					for j in range(6,length1-1):
						edp += str(line1[j])
					edp = str(edp)
					edp_arr.append(edp)
				if line1[i] == "L" and line1[i+1] == "U" and line1[i+2] == "T" and line1[i+3] == "s":
					for j in range(7,length1-1):
						lut += str(line1[j])
					lut = str(lut)
					lut_arr.append(lut)
				if line1[i] == "S" and line1[i+1] == "l" and line1[i+2] == "i" and line1[i+3] == "c" and line1[i+4] == "e" and line1[i+5] == "s":
					for j in range(9,length1-1):
						slices += str(line1[j])
					slices = str(slices)
					slices_arr.append(slices)
				if line1[i] == "M" and line1[i+1] == "a" and line1[i+2] == "x" and line1[i+3] == " " and line1[i+4] == "E" and line1[i+5] == "r" and line1[i+6] == "r" and line1[i+7] == "o" and line1[i+8] == "r" and line1[i+9] == " " and line1[i+10] == "M":
					for j in range(22,length1):
						max_err_mag += str(line1[j])
					max_err_mag = str(max_err_mag)
					max_err_mag_arr.append(max_err_mag)	
				if line1[i] == "A" and line1[i+1] == "v" and line1[i+2] == "g" and line1[i+3] == " " and line1[i+4] == "E":
					for j in range(12,length1):
						avg_err += str(line1[j])
					avg_err = str(avg_err)	
					avg_err_arr.append(avg_err)	
				if line1[i] == "A" and line1[i+1] == "v" and line1[i+2] == "g" and line1[i+3] == " " and line1[i+4] == "R":
					for j in range(21,length1-1):
						avg_rel_err += str(line1[j])
					avg_rel_err = str(avg_rel_err)	
					avg_rel_err_arr.append(avg_rel_err)
				if line1[i] == "E" and line1[i+1] == "r" and line1[i+2] == "r" and line1[i+3] == "o" and line1[i+4] == "r" and line1[i+5] == " " and line1[i+6] == "O" and line1[i-1] != " ":
					for j in range(19,length1-1):
						err_occ += str(line1[j])
					err_occ = str(err_occ)	
					err_occ_arr.append(err_occ)
				if line1[i] == "E" and line1[i+1] == "r" and line1[i+2] == "r" and line1[i+3] == "o" and line1[i+4] == "r" and line1[i+5] == " " and line1[i+6] == "O" and line1[i-1] == " ":
					for j in range(23,length1-1):
						max_err_occ += str(line1[j])
					max_err_occ = str(max_err_occ)	
					max_err_occ_arr.append(max_err_occ)	

					tot_power = ""
					dyn_power = ""
					static_power = ""
					delay = ""
					pdp = ""
					edp = ""
					lut = ""
					slices = ""
					max_err_mag = ""
					avg_err = ""
					avg_rel_err = ""
					err_occ = ""
					max_err_occ = ""	

			line1 = fh1.readline()
	
	#for i in range(len(edp_arr)):
		#fh2.write(delay_arr[i] + "," );#+ delay_arr[i] + "," + tot_power_arr[i] + ";")
	#	fh2.write(avg_rel_err_arr[i] + "," + lut_arr[i] + ";")# + delay_arr[i] + ";")
	#h2.write("]")	

	for i in range(len(avg_rel_err_arr)):
		fh2.write("[" + tot_power_arr[i] + "," + lut_arr[i] + "],")
	fh2.write("]")	
