reset_project
import_files -force -norecurse {C:/Users/sanjeev/Desktop/EvoLib/tb_mult_321.vhd C:/Users/sanjeev/Desktop/string_to_stdvec_helpers.vhd C:/Users/sanjeev/Desktop/EvoLib/mult_header.v C:/Users/sanjeev/Desktop/EvoLib/mult_321.v}
update_compile_order -fileset sources_1
update_compile_order -fileset sources_1
set_property used_in_synthesis false [get_files  C:/Users/sanjeev/Desktop/Sanjeev/Programs/Approximate_Computing/project_Testscript/project_Testscript.srcs/sources_1/imports/Desktop/Evolib/tb_mult_321.vhd]
update_compile_order -fileset sources_1
update_compile_order -fileset sources_1
set_property -name {xsim.simulate.log_all_signals} -value {true} -objects [get_filesets sim_1]
set_property -name {xsim.simulate.saif_all_signals} -value {true} -objects [get_filesets sim_1]
set_property -name {xsim.simulate.saif} -value {testscript.saif} -objects [get_filesets sim_1]
reset_run impl_1
launch_runs impl_1 -jobs 4
wait_on_run impl_1
open_run impl_1
report_timing -to O -delay_type max -max_paths 10 -sort_by group -input_pins -file C:/Users/sanjeev/Desktop/Reports_EvoLib/tim_321_evolib.txt -name O
report_utilization -file C:/Users/sanjeev/Desktop/Reports_EvoLib/util_321_evolib.txt -name utilization_1
set_property top mul8_321 [current_fileset]
update_compile_order -fileset sources_1
set_property top tb_mult_321 [get_filesets sim_1]
set_property top_lib xil_defaultlib [get_filesets sim_1]
update_compile_order -fileset sources_1
launch_simulation -mode post-implementation -type functional
run 65536 ns
read_saif {C:/Users/sanjeev/Desktop/Sanjeev/Programs/Approximate_Computing/project_Testscript/project_Testscript.sim/sim_1/impl/func/testscript.saif}
report_power -file C:/Users/sanjeev/Desktop/Reports_EvoLib/pow_321_evolib.txt -name {power_1}
file copy C:/Users/sanjeev/Desktop/Sanjeev/Programs/Approximate_Computing/project_Testscript/project_Testscript.sim/sim_1/impl/func/mult_8x8_321_evolib.txt C:/Users/sanjeev/Desktop/Simulation_Outputs_EvoLib/mult_8x8_321_evolib.txt
close_sim
remove_files  {{C:/Users/sanjeev/Desktop/Sanjeev/Programs/Approximate_Computing/project_Testscript/project_Testscript.srcs/sources_1/imports/Desktop/Evolib/tb_mult_321.vhd} {C:/Users/sanjeev/Desktop/Sanjeev/Programs/Approximate_Computing/project_Testscript/project_Testscript.srcs/sources_1/imports/Desktop/string_to_stdvec_helpers.vhd} {C:/Users/sanjeev/Desktop/Sanjeev/Programs/Approximate_Computing/project_Testscript/project_Testscript.srcs/sources_1/imports/Desktop/EvoLib/mult_header.v} {C:/Users/sanjeev/Desktop/Sanjeev/Programs/Approximate_Computing/project_Testscript/project_Testscript.srcs/sources_1/imports/Desktop/EvoLib/mult_321.v}}
