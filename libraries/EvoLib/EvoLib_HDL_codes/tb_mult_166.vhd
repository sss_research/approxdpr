library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use std.textio.all;
use work.all;
use work.str_to_stdvec_helpers.all;

entity tb_mult_166 is
end tb_mult_166;

architecture Behavioral of tb_mult_166 is
component mul8_166 is
		Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
			   B : in  STD_LOGIC_VECTOR (7 downto 0);
			   O : out  STD_LOGIC_VECTOR (15 downto 0));
end component;
signal a_sig, b_sig: STD_LOGIC_VECTOR (7 downto 0) :=(others => '0');
signal prod_sig: STD_LOGIC_VECTOR (15 downto 0);

begin
inst1_mult: mul8_166 port map(
A => a_sig,
B => b_sig,
O => prod_sig
);
stim: process

		file output_file : text;
		variable str_stimulus_out : string(16 downto 1);
		variable file_line_out : line;
begin
		file_open(output_file, "mult_8x8_166_evolib.txt", WRITE_MODE);
		for j in 0 to 255 loop
			for i in 0 to 255 loop
				a_sig <= std_logic_vector(to_unsigned(i, 8));
				b_sig <= std_logic_vector(to_unsigned(j, 8));
				wait for 1 ns;
				str_stimulus_out := stdvec_to_str(prod_sig);
				write(file_line_out, str_stimulus_out);
				writeline(output_file, file_line_out);
			end loop;
		end loop;
		wait;
		file_close(output_file);
end process;

end Behavioral;
