tot_power = ""
dyn_power = ""
static_power = ""
delay = ""
lut = ""
slices = ""
max_err_mag = ""
avg_err = ""
avg_rel_err = ""
err_occ = ""
max_err_occ = ""
output_file = "C:\\Users\\sanjeev\\Desktop\\design_space_points_evolib.txt"


with open(output_file, "w") as fh5:
	for i1 in range(0,5):
		for i2 in range (0,10):
			for i3 in range(0,10):
				s=str(i1)+str(i2)+str(i3)
				inputpow = "C:\\Users\\sanjeev\\Desktop\\Reports_EvoLib\\pow_" + s + "_evolib.txt"
				inputtim = "C:\\Users\\sanjeev\\Desktop\\Reports_EvoLib\\tim_" + s + "_evolib.txt"
				inputarea = "C:\\Users\\sanjeev\\Desktop\\Reports_EvoLib\\util_" + s + "_evolib.txt"
				inputerr = "C:\\Users\\sanjeev\\Desktop\\Error_Reports_EvoLib\\error_mult_8x8_" + s + "_evolib.txt"
				
				fh5.write("Multiplier " + s + "\n")

				with open(inputpow, "r") as fh1:
					position1 = fh1.seek(0,0)
					line1 = fh1.readline()
					while line1:
						length1 = len(line1)
						for i in range(length1):
							if line1[i] == "T" and line1[i+1] == "o" and line1[i+2] == "t" and line1[i+3] == "a" and line1[i+4] == "l" and line1[i+5] == " " and line1[i+6] ==  "O" and line1[i+7] == "n":
								tot_power = str(str(line1[i+27]) + str(line1[i+28]) + str(line1[i+29]) + str(line1[i+30]) + str(line1[i+31]))
							elif line1[i] == "D" and line1[i+1] == "y" and line1[i+2] == "n" and line1[i+3] == "a" and line1[i+4] == "m" and line1[i+5] == "i" and line1[i+6] ==  "c" and line1[i+7] == " " and line1[i+8] == "(" and line1[i+9] == "W" and line1[i+10] == ")":
								dyn_power = str(str(line1[i+27]) + str(line1[i+28]) + str(line1[i+29]) + str(line1[i+30]) + str(line1[i+31]))
							elif line1[i] == "D" and line1[i+1] == "e" and line1[i+2] == "v" and line1[i+3] == "i" and line1[i+4] == "c" and line1[i+5] == "e" and line1[i+6] ==  " " and line1[i+7] == "S" and line1[i+8] == "t":
								static_power = str(str(line1[i+27]) + str(line1[i+28]) + str(line1[i+29]) + str(line1[i+30]) + str(line1[i+31]))	
						line1 = fh1.readline()		
				fh5.write("Total Power : " + tot_power + "\n")	#print(tot_power)
				fh5.write("Dynamic Power : " + dyn_power + "\n")	#print(dyn_power)	
				fh5.write("Static Power : " + static_power + "\n")	#print(static_power)	
					
				with open(inputtim, "r") as fh2:
					position2 = fh2.seek(0,0)
					cnt = 0
					line2 = fh2.readline()
					while line2:
						length2 = len(line2)
						for i in range(length2):
							if line2[i] == "D" and line2[i+1] == "a" and line2[i+2] == "t" and line2[i+3] == "a" and cnt == 0:
								delay = str(str(line2[i+24]) + str(line2[i+25]) + str(line2[i+26]) + str(line2[i+27]) + str(line2[i+28]))
								cnt += 1
						line2 = fh2.readline()
				fh5.write("Delay : " + delay + "\n")	#print(delay)	

				pdp = float(tot_power) * float(delay)
				fh5.write("PDP : " + str(pdp) + "\n")	

				edp = float(pdp) * float(delay)
				fh5.write("EDP : "+ str(edp) + "\n")		

				with open(inputarea, "r") as fh3:
					position3 = fh3.seek(0,0)
					line3 = fh3.readline()
					while line3:
						length3 = len(line3)
						for i in range(length3):
							if line3[i] == "L" and line3[i+1] == "U" and line3[i+2] == "T" and line3[i+3] == "s":
								lut = str(str(line3[i+21]) + str(line3[i+22]) + str(line3[i+23]))
							elif line3[i] == "S" and line3[i+1] == "l" and line3[i+2] == "i" and line3[i+3] == "c" and line3[i+4] == "e" and line3[i+5] == " " and line3[i+6] == " ":
								slices = str(str(line3[i+29]) + str(line3[i+30]))
						line3 = fh3.readline()
				fh5.write("LUTs : " + lut + "\n")	#print(lut)				
				fh5.write("Slices : " + slices + "\n")	#print(slices)	
					
				with open(inputerr, "r") as fh4:
					max_err_mag = ""
					avg_err = ""
					avg_rel_err = ""
					err_occ = ""
					max_err_occ = ""
					position4 = fh4.seek(0,0)
					line4 = fh4.readline()
					while line4:
						length4 = len(line4)
						for i in range(length4):
							if line4[i] == "M" and line4[i+1] == "a" and line4[i+2] == "x" and line4[i+3] == "_" and line4[i+4] == "E":
								for j in range(22,length4):
									max_err_mag += str(line4[j])
								max_err_mag = str(max_err_mag)
							elif line4[i] == "A" and line4[i+1] == "v" and line4[i+2] == "g" and line4[i+3] == "_" and line4[i+4] == "E":
								for j in range(12,length4):
									avg_err += str(line4[j])
								avg_err = str(avg_err)
							elif line4[i] == "A" and line4[i+1] == "v" and line4[i+2] == "g" and line4[i+3] == "_" and line4[i+4] == "R":
								for j in range(16,length4):
									avg_rel_err += str(line4[j])
								avg_rel_err = str(avg_rel_err)
							elif line4[i] == "E" and line4[i+1] == "r" and line4[i+2] == "r" and line4[i+3] == "o" and line4[i+4] == "r" and line4[i+5] == " " and line4[i+6] == "O" and line4[i-1] != " ":
								for j in range(19,length4):
									err_occ += str(line4[j])
								err_occ = str(err_occ)
							elif line4[i] == "E" and line4[i+1] == "r" and line4[i+2] == "r" and line4[i+3] == "o" and line4[i+4] == "r" and line4[i+5] == " " and line4[i-1] == " ":
								for j in range(24,length4):
									max_err_occ += str(line4[j])
								max_err_occ = str(max_err_occ)	
						line4 = fh4.readline()
							
				fh5.write("Max Error Magnitude : " + max_err_mag)	#print(max_err_mag)	
				fh5.write("Avg Error : " + avg_err)	#print(avg_err)					
				fh5.write("Avg Relative Error : " + avg_rel_err)	#print(avg_rel_err)	
				fh5.write("Error Occurences : " + err_occ)	#print(err_occ)	
				fh5.write("Max Error Occurences : " + max_err_occ)	#print(max_err_occ)

				fh5.write("\n")		
#					with open(output_file, "w") as fh5:
#						fh5.write("Multiplier " + s + "\n")
#						fh5.write("Total Power : " + tot_power + "\n")
#						fh5.write("Dynamic Power : " + dyn_power + "\n")
#						fh5.write("Static Power : " + static_power + "\n")
#						fh5.write("Delay : " + delay + "\n")
#						fh5.write("LUTs : " + lut + "\n")
#						fh5.write("Slices : " + slices + "\n")
#						fh5.write("Max Error Magnitude : " + max_err_mag + "\n")
#						fh5.write("Avg Error : " + avg_err + "\n")
#						fh5.write("Avg Relative Error :  : " + avg_rel_err + "\n")
#						fh5.write("Error Occurences : " + err_occ + "\n")
#						fh5.write("Max Error Occurences : " + max_err_occ + "\n")
#						fh5.write("\n")







						

