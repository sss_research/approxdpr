##System Library imports
import numpy as np

import forSobel.SobelSupport as supSobel
import forSupport.supportFunctions as sup

sobel_X = supSobel.custom_sobel((7,7),axis=0);
sobel_Y = supSobel.custom_sobel((7,7),axis=1);

(sobelX_H,sobelX_V,sobelX_Err) = sup.conv2DKernelToXYSeparable(sobel_X)
print("Sobelx_H:\n",sobelX_H)
print("Sobelx_V:\n",sobelX_V)
print("Sobelx_Err:\n",sobelX_Err)

(sobelY_H,sobelY_V,sobelY_Err) = sup.conv2DKernelToXYSeparable(sobel_Y)
print("Sobely_H:\n",sobelY_H)
print("Sobely_V:\n",sobelY_V)
print("Sobely_Err:\n",sobelY_Err)
