import numpy as np
from scipy import signal

def gauss_kern(size, sizey=None):
    """ Returns a normalized 2D gauss kernel array for convolutions """
    size = int(size)
    if not sizey:
        sizey = size
    else:
        sizey = int(sizey)
    x, y = np.mgrid[-size:size+1, -sizey:sizey+1]
    g = np.exp(-(x**2/float(size)+y**2/float(sizey)))
    return g / g.sum()

def blur_image(im, n, ny=None) :
    """ blurs the image by convolving with a gaussian kernel of typical
        size n. The optional keyword argument ny allows for a different
        size in the y direction.
    """
    g = gauss_kern(n, sizey=ny)
    improc = signal.convolve(im,g, mode='valid')
    return(improc)

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])

def psnr(dataset1, dataset2, maximumDataValue, ignore=None):
   """
   title::
      psnr

   description::
      This method will compute the peak-signal-to-noise ratio (PSNR) between
      two provided data sets.  The PSNR will be computed for the ensemble
      data.  If the PSNR is desired for a particular slice of the provided
      data, then the data sets provided should represent those slices.

   attributes::
      dataset1
         An array-like object containing the first data set.
      dataset2
         An array-like object containing the second data set.
      maximumDataValue
         The maximum value that might be contained in the data set (this
         is not necessarily the maximum value in the data set, but
         rather it is the largest value that any member of the data set
         might take on).
      ignore
         A scalar value that will be ignored in the data sets.  This can
         be used to mask data in the provided data set from being
         included in the analysis. This value will be looked for in both
         of the provided data sets, and only an intersection of positions
         in the two data sets will be included in the computation. [default
         is None]

   author::
      Carl Salvaggio

   copyright::
      Copyright (C) 2015, Rochester Institute of Technology

   license::
      GPL

   version::
      1.0.0

   disclaimer::
      This source code is provided "as is" and without warranties as to
      performance or merchantability. The author and/or distributors of
      this source code may have made statements about this source code.
      Any such statements do not constitute warranties and shall not be
      relied on by the user in deciding whether to use this source code.

      This source code is provided without any express or implied warranties
      whatsoever. Because of the diversity of conditions and hardware under
      which this source code may be used, no warranty of fitness for a
      particular purpose is offered. The user is advised to test the source
      code thoroughly before relying on it. The user must assume the entire
      risk of using the source code.
   """

   # Make sure that the provided data sets are numpy ndarrays, if not
   # convert them and flatten te data sets for analysis
   if type(dataset1).__module__ != np.__name__:
      d1 = np.asarray(dataset1).flatten()
   else:
      d1 = dataset1.flatten()

   if type(dataset2).__module__ != np.__name__:
      d2 = np.asarray(dataset2).flatten()
   else:
      d2 = dataset2.flatten()

   # Make sure that the provided data sets are the same size
   if d1.size != d2.size:
      raise ValueError('Provided datasets must have the same size/shape')

   # Check if the provided data sets are identical, and if so, return an
   # infinite peak-signal-to-noise ratio
   if np.array_equal(d1, d2):
      return float('inf')

   # If specified, remove the values to ignore from the analysis and compute
   # the element-wise difference between the data sets
   if ignore is not None:
      index = np.intersect1d(np.where(d1 != ignore)[0],
                                np.where(d2 != ignore)[0])
      error = d1[index].astype(np.float64) - d2[index].astype(np.float64)
   else:
      error = d1.astype(np.float64)-d2.astype(np.float64)

   # Compute the mean-squared error
   meanSquaredError = np.sum(error**2) / error.size

   # Return the peak-signal-to-noise ratio
   return 10.0 * np.log10(maximumDataValue**2 / meanSquaredError)

def low_rank_approx(SVD=None, A=None, r=1):
    """
    Computes an r-rank approximation of a matrix
    given the component u, s, and v of it's SVD
    Requires: numpy
    """
    if not SVD:
        SVD = np.linalg.svd(A, full_matrices=False)
    u, s, v = SVD
    Ar = np.zeros((len(u), len(v)))
    for i in range(r):
        Ar += s[i] * np.outer(u.T[i], v[i])
    return Ar

def conv2DKernelToXYSeparable(inp2DArray):
    """
    Function to convert the given kernel into XY separable Kernels
    :param inp2DArray:      2D numpy array
    :return:2 numpy arrays- X1Dkernel, Y1DKernel and the error matrix
    """
    inpMatRank = np.linalg.matrix_rank(inp2DArray)
    print("Input array: ",np.shape(inp2DArray),"\n",inp2DArray)
    print("Test XY separability ...")
    print("Rank of Input Array: ",inpMatRank)
    # Compute the XY Separable forms
    u, s, vh = np.linalg.svd(inp2DArray, full_matrices=False)
    # assert np.allclose(inp2DArray, np.dot(u, np.dot(np.diag(s), vh)))
    #Validation
    if(inpMatRank > 1):
        print("INFO: Input 2D array is not XY separable since the rank is ",inpMatRank)
        print("INFO: Low-rank Approximation will be used")
        arry2D_XYSep = low_rank_approx((u, s, vh), r=1)
        u, s, vh = np.linalg.svd(arry2D_XYSep, full_matrices=False)
    else:
        arry2D_XYSep = inp2DArray
    V = np.array(u[:, 0] * np.sqrt(np.diag(s)[0, 0])).reshape(np.shape(arry2D_XYSep)[0], 1)
    H = np.array((vh.transpose()[:, 0]) * np.sqrt(np.diag(s)[0, 0])).reshape(1, np.shape(arry2D_XYSep)[1])
    print("V ", np.shape(V), "\n", V)
    print("H ", np.shape(H), "\n", H)
    print("Conv:\n", V * H)
    print("Difference: \n", inp2DArray - V * H)
    X1Dkernel = H;
    Y1DKernel = V.transpose();
    ErrMatrix = inp2DArray - V * H;
    return (X1Dkernel,Y1DKernel,ErrMatrix)



def noisy(noise_typ,image):
    if noise_typ == "gauss":
        row,col= image.shape
        mean = 0
        var = 0.2
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(row,col))
        gauss = gauss.reshape(row,col)
        noisy = image + gauss
        return noisy
    elif noise_typ == "s&p":
        row,col = image.shape
        s_vs_p = 0.5
        amount = 0.08
        out = np.copy(image)
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))for i in image.shape]
        out[coords] = 1
        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))for i in image.shape]
        out[coords] = 0
        return out
    elif noise_typ == "poisson":
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
        return noisy
    elif noise_typ =="speckle":
        row,col,ch = image.shape
        gauss = np.random.randn(row,col,ch)
        gauss = gauss.reshape(row,col,ch)
        noisy = image + image * gauss
        return noisy