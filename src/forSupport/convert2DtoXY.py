import numpy as np
from scipy import linalg

def low_rank_approx(SVD=None, A=None, r=1):
    """
    Computes an r-rank approximation of a matrix
    given the component u, s, and v of it's SVD
    Requires: numpy
    """
    if not SVD:
        SVD = np.linalg.svd(A, full_matrices=False)
    u, s, v = SVD
    Ar = np.zeros((len(u), len(v)))
    for i in range(r):
        Ar += s[i] * np.outer(u.T[i], v[i])
    return Ar
# Hello World program in Python
#sobel = np.array([[-1 ,0 ,1], [-2, 0 ,2],[ -1, 0 ,1]])
sobel = np.array([
    [ -5, -4,  0,   4,   5],
    [-8, -10,  0,  10,   8],
    [-10, -20,  0,  20,  10],
    [-8, -10,  0,  10,   8],
    [-5,  -4,  0,   4,   5]
                ])
print (np.shape(sobel))
print (sobel)
print (np.linalg.matrix_rank(sobel))
''' 
u, s, vh = linalg.svd(sobel, full_matrices=True)
assert np.allclose(sobel, np.dot(u, np.dot(np.diag(s), vh)))
new_Sobel = np.dot(u, np.dot(np.diag(s), vh))
'''
u, s, vh = linalg.svd(sobel, full_matrices=False)
D = linalg.diagsvd(s, np.shape(sobel)[0],np.shape(sobel)[1])
new_Sobel = np.dot(u, np.dot(u, np.dot(np.diag(s), vh)))
print (new_Sobel)
print ("u:\n",u)
print ("s:\n",s)
print ("vh:\n",vh)
print("D:\n",D)

D1 = D.copy()
D1[D1 < s[int(1)]] = 0.
print("Low rank minimization to : 1",)
#sobel_XYSep = np.dot(np.dot(u, D1), vh)
sobel_XYSep = low_rank_approx((u,s,vh), r=1)
print("Approximated Kernel: \n",sobel_XYSep)
print("Rank of Approximated Kernel: ",np.linalg.matrix_rank(sobel_XYSep))


V = np.array(u[:,0] * np.sqrt(np.diag(s)[0,0])).reshape(np.shape(sobel)[0],1)
H = np.array((vh.transpose()[:,0]) * np.sqrt(np.diag(s)[0,0])).reshape(1,np.shape(sobel)[1])
print ("V ",np.shape(V),"\n",V)
print ("H ",np.shape(H),"\n",H)
print ("Conv:\n",V*H)
print ("Difference: \n",sobel-V*H)