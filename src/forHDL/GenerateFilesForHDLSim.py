#!/usr/bin/env python3.5
import numpy as np
#import matplotlib.pyplot as plt
from numpy import genfromtxt
import subprocess as subproc
import random as rnd
from deap import base
from deap import creator
from deap import tools
from matplotlib import style
style.use("ggplot")
from PIL import Image as img
import threading as thr
import time as time
import timeit as timeit
from scipy import signal
import pylab as pylab
from astropy.convolution import Gaussian2DKernel
from astropy.convolution import Gaussian1DKernel
#User defined
import forFIRFilter.FIRFilter as firf
import forSupport.supportFunctions as support

NUM_SAMPLES = 1000
RANGE_LOW = -100.00
HIGH_RANGE = 100.00

GENERATED_INPUT_FILE_NAME = "inputForHDLTest.txt"
PYTHON_OUTPUT_FILE_NAME = "outFromBehavHigh.txt"

NUM_TAPS_FIR = 5
MULT_TYPES = [0, 0, 0, 0, 0]
MULT_WIDTH = 8
COMPUTATION_TYPE = 1    # 1 for table look-up based on multiplier type, 0 for python numpy multiplication
OPERAND_REP = 1         # 1 for fixed point , 0 for floating point
########## Input Test Signal #########
np.random.seed(100)
flt_TestSamples = np.random.uniform(low=RANGE_LOW, high=HIGH_RANGE, size=(NUM_SAMPLES,))
print(flt_TestSamples)
######################################
########## Generate FIR Filter coeficients #########
flt_CoeffsFIR = (Gaussian1DKernel(10,x_size=NUM_TAPS_FIR)).array
print(flt_CoeffsFIR)
######################################
#------------------------------------------------------------------------------------------------------
##---------------------- Load multiplier Lookup Table -----------------------------------
#------------------------------------------------------------------------------------------------------
print("Loading Multiplier Look-up table .... ")
MultLuT_8x8 = np.loadtxt(open("../libraries/PDTuD/LuT_8x8_Multiplier.csv", "rb"), delimiter=",",skiprows=1)
#------------------------------------------------------------------------------------------------------
inpFile = open(GENERATED_INPUT_FILE_NAME, 'w')
outFile = open(PYTHON_OUTPUT_FILE_NAME, 'w')
maxInpArray = np.amax(np.absolute(flt_TestSamples))
fltScaled_TestSamples = np.divide(flt_TestSamples,maxInpArray)
fxPnt_TestSamples = (np.round(fltScaled_TestSamples * (np.power(2,(MULT_WIDTH-1))-1))).astype(int)
FxdPnt_FIRFilter = firf.FIR_directForm(NUM_TAPS_FIR,flt_CoeffsFIR,arrMulTypes=MULT_TYPES,mulLuT=MultLuT_8x8)
FxdPnt_FIRFilter.maxAbsInp_ = np.amax(np.absolute(flt_TestSamples))
##Compute response
opSeqLen = NUM_TAPS_FIR + len(flt_TestSamples) - 1
#Append appropriate zeros at both ends of input sample sequence
padArray = np.zeros(NUM_TAPS_FIR - 1)
seqShiftVals = np.insert(flt_TestSamples,0,padArray)
seqShiftVals = np.append(seqShiftVals,padArray)
print(flt_TestSamples)
print(seqShiftVals)
#print("Shifting Sequence: ",seqShiftVals)
outSeq = np.zeros(opSeqLen)
#print("Assumption: The sequence starts at array[0]")
for op_cntr in range(opSeqLen):
    #print("Input Window: ",seqShiftVals[op_cntr:(op_cntr + self.numTaps_)])
    outSeq[op_cntr] = FxdPnt_FIRFilter.compOutputWithWindow(
                                seqShiftVals[op_cntr:(op_cntr + FxdPnt_FIRFilter.numTaps_)],
                                flagOpRepr=OPERAND_REP,
                                multWidth=MULT_WIDTH,
                                flagComputeType=COMPUTATION_TYPE
                            )
    if(op_cntr==0):
        for x in FxdPnt_FIRFilter.intCoeffs_:
            inpFile.write(str(x))
            inpFile.write("\n")
    outFile.write(str(FxdPnt_FIRFilter.intOutVal_))
    outFile.write("\n")
for ip_cntr in range(len(fxPnt_TestSamples)):
    inpFile.write(str(fxPnt_TestSamples[ip_cntr]))
    inpFile.write("\n")
inpFile.close()
outFile.close()