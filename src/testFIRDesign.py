#!/usr/bin/env python3.5
import numpy as np
#import matplotlib.pyplot as plt
from numpy import genfromtxt
import subprocess as subproc
import random as rnd
from deap import base
from deap import creator
from deap import tools
from matplotlib import style
style.use("ggplot")
from PIL import Image as img
import threading as thr
import time as time
import timeit as timeit
from scipy import signal
import pylab as pylab
#User defined
import forFIRFilter.FIRFilter as firf


def main():
    print("Testing FIR filter design")
    np.random.seed(10)
    ############################ Testing FIR filter design ##################
    #####   FIR Filter parameters ####
    arg_numTaps     = 10
    arg_nyqFreq     = 0.1
    #-----------------------
    #FIR filter instantiation
    argCoeffArray = signal.firwin(arg_numTaps, arg_nyqFreq)
    print("FIR Co-efficients:",argCoeffArray)
    myFIR = firf.FIR_directForm(arg_numTaps,argCoeffArray)
    myFIR.displayFIRDetails()
    ####################################
    #### Testing one window operation
    print("----------- Testing Window Operation -------- ")
    inp_args        = np.random.rand(arg_numTaps)
    windowOp        = myFIR.compOutputWithWindow(inp_args)
    print("Window Output: ", windowOp)
    print("From Computation: ",np.sum(np.multiply(inp_args,argCoeffArray)))
    ####################################
    ##### Testing for a sample sequence
    arg_numInSamples = 5
    print("----------- Testing Sample Sequence -------- ")
    inp_samples     = np.random.rand(arg_numInSamples)
    out_response    = myFIR.compOutputresponse(inp_samples,flagOpRepr=1)
    print("Output response: ", out_response)
    #return 0
    ####################################
    ##### Testing for a physical signals
    print("----------- Testing physical Sequence -------- ")
    #------------------------------------------------
   # Create a signal for demonstration.
   #------------------------------------------------
    sample_rate = 100.0
    nsamples = 1000
    t = pylab.arange(nsamples) / sample_rate
    x = np.cos(2*np.pi*0.5*t) + 0.2*np.sin(2*np.pi*2.5*t+0.1) + \
           0.2*np.sin(2*np.pi*15.3*t) + 0.1*np.sin(2*np.pi*16.7*t + 0.1) + \
               0.1*np.sin(2*np.pi*23.45*t+.8)
    #------------------------------------------------
    # Create a FIR filter and apply it to x.
    #------------------------------------------------
    # The Nyquist rate of the signal.
    nyq_rate = sample_rate / 2.0
    '''
    # The desired width of the transition from pass to stop,
    # relative to the Nyquist rate.  We'll design the filter
    # with a 5 Hz transition width.
    width = 5.0/nyq_rate

    # The desired attenuation in the stop band, in dB.
    ripple_db = 60.0

    # Compute the order and Kaiser parameter for the FIR filter.
    N, beta = signal.kaiserord(ripple_db, width)
    '''
    # The cutoff frequency of the filter.
    cutoff_hz       = 10.0
    arg_numTaps     = 32
    '''
    # Use firwin with a Kaiser window to create a lowpass FIR filter.
    taps = signal.firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))
    '''
    #FIR filter instantiation
    argCoeffArray = signal.firwin(arg_numTaps, cutoff_hz/nyq_rate)
    print("FIR Co-efficients:",argCoeffArray)
    myFIRPhy = firf.FIR_directForm(arg_numTaps,argCoeffArray)
    myFIRPhy.displayFIRDetails()
    # Use lfilter to filter x with the FIR filter.
    python_filtered_x   = signal.lfilter(argCoeffArray, 1.0, x)
    #print("Length Python filter output: ",len(python_filtered_x))
    myFIR_filtered_x    = myFIRPhy.compOutputresponse(x,flagOpRepr=1)
    #print("Pyfiltered:",python_filtered_x)
    #print("Myfiltered:",myFIR_filtered_x)
    ################## Test for approximation ######
    #Set num Taps
    aprxNumTaps = 32
    #Array of multiplier types
    arrayMuls   = np.random.randint(1,high=2,size=aprxNumTaps)
    ApproxCoeffArray = signal.firwin(aprxNumTaps, cutoff_hz/nyq_rate)
    print("FIR Co-efficients:",ApproxCoeffArray)
    #Load multiplier Lookup Table
    print("Loading Multiplier Look-up table .... ")
    MultLuT_8x8 = np.loadtxt(open("../libraries/PDTuD/LuT_8x8_Multiplier.csv", "rb"), delimiter=",",skiprows=1)
    approxFIRPhy = firf.FIR_directForm(aprxNumTaps,ApproxCoeffArray,arrMulTypes=arrayMuls,mulLuT=MultLuT_8x8)
    approxFIRPhy.displayFIRDetails()
    approxFIR_filtered_x    = approxFIRPhy.compOutputresponse(x,flagOpRepr=1,flagComputeType=1  )
    #print(approxFIR_filtered_x)

    # The phase delay of the filtered signal.
    delay = 0.5 * (arg_numTaps-1) / sample_rate
    #------------------------------------------------
    # Plot the original signal, python implemented and myFIR with fixed point filter filtered signals.
    #------------------------------------------------
    pylab.figure(1)
    # Plot the original signal.
    pylab.plot(t, x,'g-')
    # Plot the python-filtered signal.
    pylab.plot(t-delay, python_filtered_x, 'b-')
    # Plot the filtered signal, shifted to compensate for the phase delay.
    pylab.plot(t-delay, myFIR_filtered_x, 'r-')
    # Plot the approximated filtered signal, shifted to compensate for the phase delay.
    pylab.plot(t-delay, approxFIR_filtered_x, 'k-')
    # Plot just the "good" part of the filtered signal.  The first N-1
    # samples are "corrupted" by the initial conditions.
    #pylab.plot(t[arg_numTaps-1:]-delay, myFIR_filtered_x[arg_numTaps-1:], 'g', linewidth=4)
    pylab.xlabel('t')
    pylab.title('Python floating point and myFIR fixed point filtered signal')
    pylab.grid(True)
    ##Display all plots
    pylab.show()


if __name__== "__main__":
        main()

print ("Processing Completed...")
