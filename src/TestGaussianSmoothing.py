#!/usr/bin/env python3.5
import numpy as np
#import matplotlib.pyplot as plt
from numpy import genfromtxt
import subprocess as subproc
import random as rnd
from deap import base
from deap import creator
from deap import tools
from matplotlib import style
style.use("ggplot")
from PIL import Image as img
import threading as thr
import time as time
import timeit as timeit
from scipy import signal
from scipy import misc
import pylab as pylab
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import imageio as imgIO
#User defined

import forLibrary.MultDsgns as multDes
import forSupport.supportFunctions as support
import forGaussSmooth.GaussianSmoothing as gsmth

########## Input Test Image #########
imageFile = '../test/images/lena.jpeg'
######################################
######### Input FIR parameters #######
minRowsInWindow     = 7
maxRowsInWindow     = 7
minColsInWindow     = 7
maxColsInWindow     = 7
######################################
############### Genetic Algorithm Parameters ##############

############################################################
##Main function
def main():
    print("Genetic Algorithm based exploration for Approximate Filter parameterization")
    np.random.seed(10)
    #----------------- Plot Sample results -----------------------------------------------------------
    #-----------------------------------------------------------------
    #---------------- Plot the original Image.-----------------------
    testImage       = mpimg.imread(imageFile)
    testImageGray   = support.rgb2gray(testImage)
    #print(testImageGray)
    plt.figure(1)
    plt.title("Original Image")
    plt.imshow(testImageGray, cmap = plt.get_cmap('gray'),label='Original Image')
    #plt.show()
    #Window parameters
    numRowsInWindow = minRowsInWindow
    numColsInWindow = minColsInWindow
    #------------------------------------------------------------------------------------------------------
    ##------------------------ Python floating point filtering--------------------------
    #------------------------------------------------------------------------------------------------------
    fltPntSmthImg = gsmth.testGausSmth_python(testImageGray,numRowsInWindow,numColsInWindow)
    plt.figure(2)
    plt.imshow(fltPntSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
    print("Python based processing complete")
    plt.title("Python-based Gaussian Smoothing")
    #plt.show()
    #return 0
    #------------------------------------------------------------------------------------------------------
    ##---------------------- Load multiplier Lookup Table -----------------------------------
    #------------------------------------------------------------------------------------------------------
    print("Loading Multiplier Look-up table .... ")
    MultLuT_8x8 = np.loadtxt(open("../libraries/PDTuD/LuT_8x8_Multiplier.csv", "rb"), delimiter=",",skiprows=1)
    #------------------------------------------------------------------------------------------------------
    # --------------------- 32-bit Fixed point all Accurate (numpy integer multiplication) filtering 2-stage -----
    #------------------------------------------------------------------------------------------------------

    fxdPntSmthImg = gsmth.testGausSmth2Stage_xbitFxdPntFIR(testImageGray,numRowsInWindow,numColsInWindow,multWidth=32)
    plt.figure(3)
    plt.imshow(fxdPntSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
    print("2-stage  processing complete")
    plt.title("2-stage Gaussian Smoothing")
    #plt.show()
    #return 0
    #------------------------------------------------------------------------------------------------------
    # --------------------- 32-bit Fixed point all Accurate 2 dimensional array processing  -----
    #------------------------------------------------------------------------------------------------------
    fxdPntSmthImg = gsmth.testGausSmth2D_xbitFxdPntFIR(testImageGray,numRowsInWindow,numColsInWindow,multWidth=32)
    plt.figure(4)
    plt.imshow(fxdPntSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
    print("2D processing complete")
    plt.title("2D Gaussian Smoothing")
    #plt.show()
    #return 0
    #------------------------------------------------------------------------------------------------------
    # --------------------- 8-bit Fixed point all Accurate (lookup Table based) filtering  ----------------------------------
    #------------------------------------------------------------------------------------------------------
    #arrayMuls   = np.random.randint(0,high=1,size=minTaps_forFxdDSE)


    #------------------------------------------------------------------------------------------------------
    # --------------------- 8-bit Fixed point all Approximate filtering  ---------------------------
    #------------------------------------------------------------------------------------------------------
    #arrayMuls   = np.random.randint(1,high=2,size=minTaps_forFxdDSE)

    #------------------------------------------------------------------------------------------------------
    #pylab.legend(loc='upper left', shadow=True)
    ##Display all plots
    plt.show()
    print("Sample display Done !!!")
    #------------------------------------------------------------------------------------------------------
    ## ----------------------   DSE for approximate multipliers-based FIR filter design -------------------
    #------------------------------------------------------------------------------------------------------
    #Parse Multiplier Design Metrics
    listMultDsgns = multDes.parseMultDsgnPoints("../libraries/PDTuD/DesignPointsMetrics.csv")




if __name__== "__main__":
        main()

print ("Processing Completed...")
