#!/usr/bin/env bash
arrayImages=("4.1.01.tiff"  "4.1.03.tiff"  "4.1.05.tiff"  "4.1.07.tiff"  "4.2.01.tiff"  "4.2.05.tiff"  "4.2.07.tiff"  "5.1.14.tiff"  "5.2.10.tiff"  "boat.512.tiff"  "house.tiff"
"4.1.02.tiff"  "4.1.04.tiff"  "4.1.06.tiff"  "4.1.08.tiff"  "4.2.03.tiff"  "4.2.06.tiff"  "5.1.10.tiff"  "5.2.08.tiff"  "5.3.01.tiff" )
#arrayImages=("4.1.01.tiff"  "4.1.03.tiff" )
touch ../outputs/res2DAll_3_3.csv
touch ../outputs/res2SAll_3_3.csv
for i in "${arrayImages[@]}"
do
    echo $i
    python3 CompareAllAccVivAllAccCustAllApp2D.py ${i} 3 3 "Noisy"  # "NoNoise" "Noisy"
    cat ../outputs/resCompare2D${i}_.csv >>  ../outputs/res2DAll_3_3.csv
    python3 CompareAllAccVivAllAccCustAllApp2S.py ${i} 3 3 "Noisy"  # "NoNoise" "Noisy"
    cat ../outputs/resCompare2S${i}_.csv >>  ../outputs/res2SAll_3_3.csv
done

touch ../outputs/res2DAll_5_5.csv
touch ../outputs/res2SAll_5_5.csv
for i in "${arrayImages[@]}"
do
    echo $i
    python3 CompareAllAccVivAllAccCustAllApp2D.py ${i} 5 5 "Noisy"  # "NoNoise" "Noisy"
    cat ../outputs/resCompare2D${i}_.csv >>  ../outputs/res2DAll_5_5.csv
    python3 CompareAllAccVivAllAccCustAllApp2S.py ${i} 5 5 "Noisy"  # "NoNoise" "Noisy"
    cat ../outputs/resCompare2S${i}_.csv >>  ../outputs/res2SAll_5_5.csv
done

touch ../outputs/res2DAll_7_7.csv
touch ../outputs/res2SAll_7_7.csv
for i in "${arrayImages[@]}"
do
    echo $i
    python3 CompareAllAccVivAllAccCustAllApp2D.py ${i} 7 7 "Noisy"  # "NoNoise" "Noisy"
    cat ../outputs/resCompare2D${i}_.csv >>  ../outputs/res2DAll_7_7.csv
    python3 CompareAllAccVivAllAccCustAllApp2S.py ${i} 7 7 "Noisy"  # "NoNoise" "Noisy"
    cat ../outputs/resCompare2S${i}_.csv >>  ../outputs/res2SAll_7_7.csv
done