#System Libraries
import numpy as np
import random


#User-defined Libraries

'''
Function to compute the result of element-wise multiplication of two number arrays,
with different type of multiplier used for each element.
'''
def compArrayProd(arrOp1,maxAbsOp1,arrOp2,maxAbsOp2,
                    flagOpRepr=None,multWidth=None,flagComputeType=None,arrMultTypes=None,multLuT=[]):
    """
    Input Arguments:
    arrOp1,arrOp2       : Numpy arrays for element-wise multiplication
    maxAbsOp1,maxAbsOp1 : Maximum values of the two array of operands
    flagOpRepr          : Floating point (default,0) or fixed-point(1)
    multWidth           : Width of fixed point multipliers used
    flagComputeType     : Computation Type: 0 for computation, 1 for Look-up Table based
    arrMultTypes        : Array of multiplierTypes. If supplied length should equal to the length of the operand arrays
                          Default value to None, i.e. exact,floating point
    multLuT             : The look-Up-table used to get the multiplier output values
    """
    #print("Flag Operand Repr: ",flagOpRepr)
    if(flagOpRepr==None):
        flagOpRepr=0
    ##multWidth : Width of multiplier (default 8-bit)
    if(multWidth==None):
        multWidth=8
    ##flagComputeType : Compute (default, 0) or Lookup(1)
    if(flagComputeType==None):
        flagComputeType=0
    '''
    #Print the values the parameters are set to for the operation
    print("Computing multiplier-specific element-wise multiplication of two arrays.")
    print("Computation type: " ,flagComputeType )
    print("Length Mult Types: " ,len(arrMultTypes) )
    print("Flag Operand Repr: ",flagOpRepr)
    #'''
    #Case-based computation

    #Case for floating point python-based array multiplication
    if(np.sum(arrMultTypes) == 0 and flagOpRepr==0 ):
        #print("Computing floating point multiplications")
        ##All exact floating point multiplications
        fltPnt_prod = np.multiply(arrOp1,arrOp2)
        #print(fltPnt_prod)
        return fltPnt_prod
    #Case for fixed-point python-based array multiplication of specified width
    if(np.sum(arrMultTypes) == 0 and flagOpRepr==1 and flagComputeType==0):
        #print("Computing fixed point python. Mult Width: ",multWidth)
        ################## Input Range Scaling ##########################
        ##Input-range Scaling: to limit inputs between within max(inputs)
        arrOp1_fltPntScaled = np.divide(arrOp1,maxAbsOp1)
        #print("Input-range Scaled Op1:",arrOp1_fltPntScaled)
        arrOp2_fltPntScaled = np.divide(arrOp2,maxAbsOp2)
        #print("Input-range Scaled Op2:",arrOp2_fltPntScaled)
        ################## Conversion to fixed point ##########################
        #Convert operands to width specific-bit fixed point
        arrOp1_fxPnt = (np.round(arrOp1_fltPntScaled * (np.power(2,(multWidth-1))-1))).astype(int)
        #print("Fixed point Scaled Op1:",arrOp1_fxPnt)
        arrOp2_fxPnt = (np.round(arrOp2_fltPntScaled * (np.power(2,(multWidth-1))-1))).astype(int)
        #print("Fixed point Scaled Op2:",arrOp2_fxPnt)
        ################## Integer Multiplication ##########################
        #print("Computing fixed point numeric multiplications")
        fxdPnt_prod = np.multiply(arrOp1_fxPnt,arrOp2_fxPnt)
        #print("Fixed Point product:",fxdPnt_prod)
        ################## Input Descaling of result Array ##########################
        #fxdPnt_prod_Scaled = np.multiply(fxdPnt_prod,(maxAbsOp1*maxAbsOp2)) / (np.power(2,(multWidth*2 - 2)))
        ################## Return result array ##########################
        #return fxdPnt_prod_Scaled
        return fxdPnt_prod

    #Case for fixed-point lookUpTable-based array multiplication of specified width
    if(flagComputeType==1):
        #print("Computing fixed point lookup")
        ################## Input Range Scaling ##########################
        ##Input-range Scaling: to limit inputs within max(inputs)
        arrOp1_fltPntScaled = np.divide(arrOp1,maxAbsOp1)
        #print("Input-range Scaled Op1:",arrOp1_fltPntScaled)
        arrOp2_fltPntScaled = np.divide(arrOp2,maxAbsOp2)
        #print("Input-range Scaled Op2:",arrOp2_fltPntScaled)
        ################## Conversion to fixed point ##########################
        #Convert operands to width specific-bit fixed point
        arrOp1_fxPnt = (np.round(arrOp1_fltPntScaled * (np.power(2,(multWidth-1))-1))).astype(int)
        #print("Fixed point Scaled Op1:",arrOp1_fxPnt)
        arrOp2_fxPnt = (np.round(arrOp2_fltPntScaled * (np.power(2,(multWidth-1))-1))).astype(int)
        #print("Fixed point Scaled Op2:",arrOp2_fxPnt)
        ################## Look-up-Table-based Integer Multiplication ##########################
        #Compute row indices for looking up multiplication values
        rowIndices =  np.multiply(
                            (np.add(arrOp1_fxPnt,np.power(2,(multWidth-1)))),
                            np.power(2,multWidth)
                        )   +\
                      np.array(
                            np.add(
                                arrOp2_fxPnt,
                                np.power(2,(multWidth-1))
                            )
                        )
        #print("Row Indices: ",np.shape(rowIndices),rowIndices)
        #print("MultType Indices:",np.shape(arrMultTypes),arrMultTypes)
        #'''
        def lookupMulTable(row,col):
            return multLuT[row,col]
        vecLookUp = np.vectorize(lookupMulTable)
        fxdPnt_prod = vecLookUp(rowIndices,arrMultTypes)
        #'''
        #print("Fixed Point product:",fxdPnt_prod)
        ################## Input Descaling of result Array ##########################
        #fxdPnt_prod_Scaled = np.multiply(fxdPnt_prod,(maxAbsOp1*maxAbsOp2)) / (np.power(2,(multWidth*2 - 2)))
        #Return result
        #return fxdPnt_prod_Scaled
        return fxdPnt_prod

'''
Function to compute the sum of the elements of an array.
Currently set to perform accurate, floating-point addition.
Similar to the multiplier-specific element-wise array multiplication, 
this addition operation can be modified to simulate the behavior 
of adder-specific accumulation.
'''
def compCumulSum(arrayOps,flagOpRepr=None,adderWidth=None,flagComputeType=None,arrAdderTypes=None):
    """
    Input Arguments:
    arrayOps        : a numpy array containing values whose cumulative sum is to be computed
    flagOpRepr      : Floating point (default,0) or fixed-point(1)
    adderWidth      : Width of fixed point or floating point adders used in the design
    flagComputeType : Computation Type: 0 for computation, 1 for Look-up Table based
    arrAdderTypes   : Array of adderTypes. Default value to None, i.e. exact,floating point
    adderLUT        : Look-up Table to get addition values (future)
    """
    if(flagOpRepr==None):
        flagOpRepr=0
    ##adderWidth : Width of adder (default 16-bit)
    if(adderWidth==None):
        adderWidth=16
    ##flagComputeType : Compute (default, 0) or Lookup(1)
    if(flagComputeType==None):
        flagComputeType=0
    #Case-based computation
    if(len(arrAdderTypes) == 0 and flagOpRepr==0 and flagComputeType==0):
        ##All exact floating point multiplications
        #print("Computing floating point additions")
        fltPnt_Sum = np.sum(arrayOps)
        #print(fltPnt_Sum)
        return fltPnt_Sum
    if(len(arrAdderTypes) == 0 and flagOpRepr==1 and flagComputeType==0):
        #All exact fixed point multiplications
        #print("Computing fixed point additions")
        fxdPnt_accum = int(np.sum(arrayOps))
        return fxdPnt_accum
    if(len(arrAdderTypes) == 0 and flagOpRepr==1 and flagComputeType==1):
        #All exact fixed point multiplications
        #print("Computing fixed point additions")
        fxdPnt_accum = int(np.sum(arrayOps))
        return fxdPnt_accum
