#!/usr/bin/env python3.5
#System Libraries
import numpy as np
from matplotlib import style
style.use("ggplot")
import sys
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from skimage.measure import compare_ssim as ssim
#User defined

import forLibrary.MultDsgns as multDes
import forSupport.supportFunctions as support
import forGaussSmooth.GaussSmooth2StageDsgn as gs2s

print("Image Name: {}".format(sys.argv[1]))
print("Window length: {}".format(int(sys.argv[2])))
print("Window height: {}".format(int(sys.argv[3])))
# print("Multipliers type: {}".format(sys.argv[4]))  # "Vivado"  "Booth"
print("Noise type: {}".format(sys.argv[4]))  # "Noisy"  "NoNoise"

imageFName = sys.argv[1]
########## Settings ################
MULT_WIDTH          = 8
MULT_LUT            = "../libraries/PDTuD/LuT_8x8_Multiplier.csv"
MULT_DSGN_METRICS   = "../libraries/PDTuD/DesignPointsMetrics.csv"
####################################
INPUT_IMAGE         = "../test/images/" + imageFName
ROWS_WINDOW         = int(sys.argv[2])
COLS_WINDOW         = int(sys.argv[3])
SSIM_EVAL_WINDOW    = 3
TEST_MULT_CONFIG = [0,1,1,2]
#####################################

##---------------------- Load multiplier Lookup Table -----------------------------------
print("Loading Multiplier Look-up table ...")
MultLuT_8x8 = np.loadtxt(open(MULT_LUT, "rb"), delimiter=",",skiprows=1)
##----------------------Parse Multiplier Design Metrics----------------------------------
print("Loading Multiplier designs point metrics ...")
listMultDsgns = multDes.parseMultDsgnPoints(MULT_DSGN_METRICS)
#---------------- Read original Image.-----------------------
testImage       = mpimg.imread(INPUT_IMAGE)
testImageGray   = support.rgb2gray(testImage)
if sys.argv[4] == "Noisy":
    noisyImage = support.noisy("s&p",testImageGray)
elif sys.argv[4] == "NoNoise":
    noisyImage = testImageGray
else:
    print("ERROR: Undefined Noisy options ...  (Noisy, NoNoise)")
    exit(code=-1)
#'''
#---------------- Plot the original Image.-----------------------
plt.figure(1)
plt.title("Noisy Image")
plt.imshow(noisyImage, cmap = plt.get_cmap('gray'),label='Original Image')
plt.axis('off')
plt.savefig("../outputs/"+imageFName+ "_original.pdf",bbox_inches='tight')
#'''
###########################################################################################
# --------------------- 8-bit Fixed point all Accurate 2-stage Gaussian Smoothing   -----
###########################################################################################
print("--------------------- 8-bit Fixed point all Accurate 2-stage Gaussian Smoothing   ------------------")
##Instantiate 2-Stage Gaussian Smoothing Design
gSmthDsgn = gs2s.GaussSmth2StageDsgn(
                                        rowsInWindow = ROWS_WINDOW,
                                        colsInWindow = COLS_WINDOW,
                                        multWidth=MULT_WIDTH,
                                        flagComputeType=1
                                    )
arrayAllAccVivMuls   = np.random.randint(0,high=1,size=(ROWS_WINDOW+COLS_WINDOW))
allAccGaussSmthImg = gSmthDsgn.processImage(img2DArray=noisyImage,multLUT=MultLuT_8x8)
(allAccUtilLUTs,allAccUtilSlices,allAccUtilCCs)=gSmthDsgn.estimateDesignUtilMetrics(listMultDsgns)
print("Resource Util: All Accurate Vivado")
print(allAccUtilLUTs,allAccUtilSlices,allAccUtilCCs)
vivadoAllAccLUTs = allAccUtilLUTs
arrayAllAccCustMuls   = np.random.randint(1,high=2,size=(ROWS_WINDOW+COLS_WINDOW))
gSmthDsgn.updateMultTypes(arrayAllAccCustMuls)
(allAccUtilLUTs,allAccUtilSlices,allAccUtilCCs)=gSmthDsgn.estimateDesignUtilMetrics(listMultDsgns)
print("Resource Util: All Accurate Custom")
print(allAccUtilLUTs,allAccUtilSlices,allAccUtilCCs)
boothAllAccLUTs = allAccUtilLUTs
gSmthDsgn.displayGS2SDsgnDetails()
psnr_NoisyOrig = support.psnr(testImageGray,noisyImage,255)
psnr_AccAcc = support.psnr(allAccGaussSmthImg,allAccGaussSmthImg,255)
ssim_AccAcc = ssim(allAccGaussSmthImg,allAccGaussSmthImg,win_size=SSIM_EVAL_WINDOW)
psnr_AccOrig = support.psnr(testImageGray,allAccGaussSmthImg,255)
#---------------- Plot Gaussian Smoothed Image with all accurate multipliers -----------------------
#'''
plt.figure(2)
plt.imshow(allAccGaussSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
titleStr = "2-stage Gaussian Smoothing (All Accurate)"
plt.title(titleStr)
plt.axis('off')
plt.savefig("../outputs/"+imageFName+ "_allAccurate2S.pdf",bbox_inches='tight')
#'''
############################################################################################
# --------------------- 8-bit Fixed point All Approximate 2-stage Gaussian Smoothing   -----
############################################################################################
print("--------------------- 8-bit Fixed point all Approximate 2-stage Gaussian Smoothing   ------------------")
arrayAllApproxMuls   = np.random.randint(2,high=3,size=(ROWS_WINDOW+COLS_WINDOW))
gSmthDsgn.updateMultTypes(arrayAllApproxMuls)
gSmthDsgn.displayGS2SDsgnDetails()
allApproxGaussSmthImg = gSmthDsgn.processImage(img2DArray=noisyImage,multLUT=MultLuT_8x8)
psnr_AppOrig = support.psnr(testImageGray,allApproxGaussSmthImg,255)
(allAppUtilLUTs,allAppUtilSlices,allAppUtilCCs)=gSmthDsgn.estimateDesignUtilMetrics(listMultDsgns)
print("Resource Util: All Approximate Custom")
print(allAppUtilLUTs,allAppUtilSlices,allAppUtilCCs)
boothAllAppLUTs = allAppUtilLUTs
psnr_AccApp = support.psnr(allApproxGaussSmthImg,allAccGaussSmthImg,255)
ssim_AccApp = ssim(allAccGaussSmthImg,allApproxGaussSmthImg,win_size=SSIM_EVAL_WINDOW)
#---------------- Plot Gaussian Smoothed Image with all Approximate multipliers -----------------------
plt.figure(3)
plt.imshow(allApproxGaussSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
titleStr = "2-stage Gaussian Smoothing (All Approx)"
plt.title(titleStr)
plt.axis('off')
plt.savefig("../outputs/"+imageFName+ "_allApprox2S.pdf",bbox_inches='tight')
############################################################################################
# ----------- 8-bit Fixed point 2-stage Gaussian Smoothing with specified Mult Types   -----
############################################################################################
diffImage = np.abs(allApproxGaussSmthImg - allAccGaussSmthImg )
plt.figure(4)
plt.imshow(diffImage, cmap = plt.get_cmap('gray'),label='Smoothed Image')
titleStr = "Difference Image"
plt.title(titleStr)
plt.axis('off')
plt.savefig("../outputs/"+imageFName+ "_diffImage2S.pdf",bbox_inches='tight')
print("PSNRs:",psnr_NoisyOrig,psnr_AccOrig,psnr_AppOrig)

if(len(TEST_MULT_CONFIG)== (ROWS_WINDOW + COLS_WINDOW) ):
    print("--------------------- 8-bit Fixed point 2-stage Gaussian Smoothing (Test Mult Config)   ------------------")
    gSmthDsgn.updateMultTypes(TEST_MULT_CONFIG)
    gSmthDsgn.displayGS2SDsgnDetails()
    testGaussSmthImg = gSmthDsgn.processImage(img2DArray=noisyImage,multLUT=MultLuT_8x8)
    (allAccUtilLUTs, allAccUtilSlices, allAccUtilCCs) = gSmthDsgn.estimateDesignUtilMetrics(listMultDsgns)
    print("Resource Util: Test Config Approximate Custom")
    print(allAccUtilLUTs, allAccUtilSlices, allAccUtilCCs)
    psnr_AccTest = support.psnr(testGaussSmthImg,allAccGaussSmthImg,255)
    ssim_AccTest = ssim(allAccGaussSmthImg,testGaussSmthImg,win_size=SSIM_EVAL_WINDOW)
    print("PSNRs(AccAcc,AccApp,AccTest): ",psnr_AccAcc,", ",psnr_AccApp,", ",psnr_AccTest)
    print("SSIMs(AccAcc,AccApp,AccTest): ",ssim_AccAcc,", ",ssim_AccApp,", ",ssim_AccTest)
    #---------------- Plot Gaussian Smoothed Image with all Approximate multipliers -----------------------
    plt.figure(5)
    plt.imshow(testGaussSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
    titleStr = "2-stage Gaussian Smoothing (Test Multiplier Config)\n"+"SSIM = "+str(round(ssim_AccTest,4))+"   PSNR = "+str(round(psnr_AccTest,4))
    plt.title(titleStr)
    plt.axis('off')
    plt.savefig("../outputs/"+imageFName+ "_testConfig2S.pdf", bbox_inches='tight')
else:
    print("Length of test config array not matching the sum of rows and columns of Smoothing window")
    print("PSNRs: ", psnr_AccAcc, ", ", psnr_AccApp)
    print("SSIMs: ", ssim_AccAcc, ", ", ssim_AccApp)
##Display all plots
#plt.show()
# Write outputs about psnr, ssim and resource util
print("LUTs Util:\n VivadoAllAccurate: {}, BoothAllAccurate: {}, BoothAllApproximate: {}".format(
    vivadoAllAccLUTs,boothAllAccLUTs,boothAllAppLUTs))
print("PSNR :\n AllAccurate: {}, AllApproximate: {}".format(psnr_AccAcc,psnr_AccApp))
print("SSIM :\n AllAccurate: {}, AllApproximate: {}".format(ssim_AccAcc,ssim_AccApp))

resFName = "../outputs/" + "resCompare2S" + imageFName + "_.csv"
with open(resFName, 'w') as out_file:
    out_file.write("{},{},{},{},{},{},{},\n".format(
        vivadoAllAccLUTs, boothAllAccLUTs, boothAllAppLUTs,
        psnr_AccAcc, psnr_AccApp, ssim_AccAcc,ssim_AccApp
    ))