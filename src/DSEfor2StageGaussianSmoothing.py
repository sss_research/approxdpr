#!/usr/bin/env python3.5
import numpy as np
import random as rnd
from deap import base
from deap import creator
from deap import tools
from matplotlib import style
import sys
style.use("ggplot")

print("Image Name: {}".format(sys.argv[1]))
print("Window length: {}".format(int(sys.argv[2])))
print("Window height: {}".format(int(sys.argv[3])))
print("Multipliers type: {}".format(sys.argv[4])) ## "Vivado"  "Booth"


#User defined
import forGaussSmooth.settingsDSE2StageGaussSmth as settings
import forGaussSmooth.supportGAgs2s as supGA

########## Input Test Image #########
imageFile = '../test/images/' + sys.argv[1]
######################################
######### Input FIR parameters #######
if sys.argv[4] == "Vivado":
    multOpts = [0,2]
elif sys.argv[4] == "Booth":
    multOpts = [1, 2]
else:
    print("ERROR: Undefined multiplier options ...  (Vivado, Booth)")
    exit(code=-1)
rowsInWindow     = int(sys.argv[2])
colsInWindow     = int(sys.argv[3])
multWidth       = 8
flagComputeType = 1         #All computations set to lookup table based
######################################
######### Multiplier Designs #######
multLUTFile = "../libraries/PDTuD/LuT_8x8_Multiplier.csv"
multDsgnMetricFile = "../libraries/PDTuD/DesignPointsMetrics.csv"
################################### GA Options #################################################
#DSE parameters that affect the DSE time
startPop= 100            #Starting population Size
maxGens= 25                  #Number of generation for evolution
#GA parameters
mutationProb = 0.4
tournSize = 3
ssimEvalWin = 3
#MultiObjective Settings
weightUtilRsrcs = -1    # -ve for minimizing area
weightSSIM      = 1     # +ve for maximizing PSNR/SSIM
################################################################################################
##Main function
def main():
    print("DSE for 2Stage FIR Filter based Gaussian Smoothing")
    np.random.seed(10)
    #------------------------------------------------------------------------------------------------------
    settings.initGS2SDsgn(imageFile, ssimEvalWin, rowsInWindow, colsInWindow, multWidth, flagComputeType, multLUTFile, multDsgnMetricFile,multOpts)
    #return
    ##GA-related data
    creator.create("FitnessMulti", base.Fitness, weights=(weightUtilRsrcs,weightSSIM)) 
    creator.create("Individual", list, fitness=creator.FitnessMulti)
    toolbox = base.Toolbox()
    toolbox.register("arrayMultTypes", supGA.CreateRandomChromosome)
    toolbox.register("individual", tools.initIterate, creator.Individual,toolbox.arrayMultTypes)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("evaluate",  supGA.ComputeRewardForChromosome)
    toolbox.register("mate", tools.cxTwoPoint)
    #toolbox.register("mutate", tools.mutUniformInt,low=settings.lbMut,up=settings.ubMut,indpb=mutationProb)
    toolbox.register("mutate", settings.mutateChromosome,choices=settings.multDsgnOptions,indpb=mutationProb)
    toolbox.register("select", tools.selTournament, tournsize=tournSize)
    paretoFront = tools.ParetoFront()
    rnd.seed(64)
    # create an initial population of n individuals (where
    # each individual is a list of integers)
    pop = toolbox.population(n=startPop)
    print("The population:", pop)

    # CXPB  is the probability with which two individuals
    #       are crossed
    #
    # MUTPB is the probability for mutating an individual
    CXPB, MUTPB = 0.5, 0.1
    print("Start of evolution")
    # Evaluate the entire population
    fitnesses = list(map(toolbox.evaluate, pop))
    #print(fitnesses)
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit
        print("Individual: ", ind)
        print("Fitness:", fit)
    print("  Evaluated %i individuals" % len(pop))
    # Extracting all the fitnesses of
    fits = [ind.fitness.values[0] for ind in pop]
    # Variable keeping track of the number of generations
    g = 0
    paretoFront.update(pop)
    # Begin the evolution
    #while max(fits) < startPop and g < maxGens:
    while g < maxGens:
        # A new generation
        g = g + 1
        print("-- Generation %i --" % g)
        # Select the next generation individuals
        offspring = toolbox.select(pop, len(pop))
        # Clone the selected individuals
        offspring = list(map(toolbox.clone, offspring))
        # Apply crossover and mutation on the offspring
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            # cross two individuals with probability CXPB
            if rnd.random() < CXPB:
                toolbox.mate(child1, child2)
                # fitness values of the children
                # must be recalculated later
                del child1.fitness.values
                del child2.fitness.values
        for mutant in offspring:
            # mutate an individual with probability MUTPB
            if rnd.random() < MUTPB:
                print("Mutating")
                toolbox.mutate(mutant)
                del mutant.fitness.values
        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit
        print("  Evaluated %i individuals" % len(invalid_ind))
        # The population is entirely replaced by the offspring
        pop[:] = offspring
        # Gather all the fitnesses in one list and print the stats
        fits = [ind.fitness.values[0] for ind in pop]
        length = len(pop)
        mean = sum(fits) / length
        sum2 = sum(x*x for x in fits)
        std = abs(sum2 / length - mean**2)**0.5
        print("The population:",pop)
        paretoFront.update(pop)
        '''
        print("  Min %s" % min(fits))
        print("  Max %s" % max(fits))
        print("  Avg %s" % mean)
        print("  Std %s" % std)
        '''
    print("-- End of (successful) evolution --")
    best_ind = tools.selBest(pop, 1)[0]
    print("Best reliability Metrics:")
    reward = supGA.ComputeRewardForChromosome(best_ind)
    print("Best individual is %s, %s" % (best_ind, best_ind.fitness.values))
    print("Pareto Front:")
    outfileName = "../outputs/DSE_2S_" + sys.argv[1] + "_" + sys.argv[2] + "_" + \
                  sys.argv[3] + "_" + sys.argv[4] + "_" + ".csv"
    with open(outfileName, 'w') as out_file:
        for p in paretoFront:
            # print("%s, %s" % (p, p.fitness.values))
            out_file.write("{},{}\n".format(p, p.fitness.values))

if __name__== "__main__":
        main()

print ("Done.")
