#!/usr/bin/env bash
python3 DSEfor2DWinGaussianSmoothing.py  "cameraman.jpg" 3 3 "Vivado"
python3 DSEfor2DWinGaussianSmoothing.py  "cameraman.jpg" 3 3 "Booth"

python3 DSEfor2DWinGaussianSmoothing.py  "cameraman.jpg" 5 5 "Vivado"
python3 DSEfor2DWinGaussianSmoothing.py  "cameraman.jpg" 5 5 "Booth"

python3 DSEfor2DWinGaussianSmoothing.py  "cameraman.jpg" 7 7 "Vivado"
python3 DSEfor2DWinGaussianSmoothing.py  "cameraman.jpg" 7 7 "Booth"
