import numpy as np
##User-defined libraries

class MultDesign:
    ##Constructor
    def __init__(self,dsgnID,dsgnDesc,utilLUTs,utilSlices,utilCCs):
        self.dsgnID_        = dsgnID
        self.dsgnDesc_      = dsgnDesc
        self.utilLUTs_      = utilLUTs
        self.utilSlices_    = utilSlices
        self.utilCCs_       = utilCCs
    ##Display design details
    def DisplayDsgnDetails(self):
        print("--------- Multiplier Design Details -----------")
        print("Design ID: ", self.dsgnID_)
        print("Design Type: ", self.dsgnDesc_)
        print("LUTs used: ", self.utilLUTs_)
        print("Slices used: ",self.utilSlices_)
        print("CCs used: ",self.utilCCs_)
        print("-----------------------------------------------")


def parseMultDsgnPoints(fName_DsgnPntMets):
    list_MultDsgns  = []
    csvDsgnPntsData = np.genfromtxt(fName_DsgnPntMets,\
                                    dtype=None,\
                                    delimiter=',',\
                                    names=True)
    #print(csvDsgnPntsData)
    #print("Size:",np.shape(csvDsgnPntsData))
    #print("Sample entry:",len(csvDsgnPntsData))
    for dsgnCntr in range(0,len(csvDsgnPntsData)):
        list_MultDsgns.append(MultDesign(\
                            csvDsgnPntsData[dsgnCntr]["dsgnID"],\
                            (csvDsgnPntsData[dsgnCntr]["remark"]).decode("utf-8"),\
                            csvDsgnPntsData[dsgnCntr]["useLUTs"],\
                            csvDsgnPntsData[dsgnCntr]["useSlices"],\
                            csvDsgnPntsData[dsgnCntr]["useCCs"],\
                            ))
    '''
    for mulDsgn in list_MultDsgns:
        mulDsgn.DisplayDsgnDetails()
    '''
    return list_MultDsgns
