#!/usr/bin/env python3.5
#System Libraries
import numpy as np
from matplotlib import style
style.use("ggplot")
import matplotlib.image as mpimg
import random as rnd
#User defined

import forLibrary.MultDsgns as multDes
import forSupport.supportFunctions as support
import forGaussSmooth.GaussianSmooth2DWinDsgn as gs2d

def initGS2dDsgn(imageFile,ssimEvalWin,rowsInWindow,colsInWindow,multWidth,flagComputeType,multLUTFile,multDsgnMetricFile,multOpts):
    global numRowsInWindow
    numRowsInWindow = rowsInWindow
    global numColsInWindow
    numColsInWindow = colsInWindow
    global multDsgnOptions
    multDsgnOptions = multOpts
    global lbMut,ubMut
    lbMut = np.random.randint(np.min(multDsgnOptions),\
                                high=np.min(multDsgnOptions)+1,\
                                size=rowsInWindow*colsInWindow)
    ubMut = np.random.randint(np.max(multDsgnOptions),\
                                high=np.max(multDsgnOptions)+1,\
                                size=rowsInWindow*colsInWindow)
    global ssimEvalWinSize
    ssimEvalWinSize = ssimEvalWin
    ##---------------------- Load multiplier Lookup Table -----------------------------------
    #------------------------------------------------------------------------------------------------------
    print("Loading Multiplier Look-up table ...")
    global MultLuT_8x8
    MultLuT_8x8 = np.loadtxt(open(multLUTFile, "rb"), delimiter=",",skiprows=1)
    #-----------------------------------------------------------------
    #Parse Multiplier Design Metrics
    print("Loading Multiplier designs point metrics ...")
    global listMultDsgns
    listMultDsgns = multDes.parseMultDsgnPoints(multDsgnMetricFile)
    #------------------------------------------------------------------------------------------------------
    #---------------- Read original Image.-----------------------
    testImage       = mpimg.imread(imageFile)
    global testImageGray
    testImageGray   = support.rgb2gray(testImage)
    #print(testImageGray)
    '''
    #---------------- Plot the original Image.-----------------------
    plt.figure(1)
    plt.title("Original Image")
    plt.imshow(testImageGray, cmap = plt.get_cmap('gray'),label='Original Image')
    '''
    #-------------------------------------------------------------------------------------------------------------
    # --------------------- 8-bit Fixed point all Accurate 2-stage Gaussian Smoothing   -----
    #-------------------------------------------------------------------------------------------------------------
    ##Instantiate 2-Stage Gaussian Smoothing Design
    global gSmthDsgn
    gSmthDsgn = gs2d.GaussSmth2DWinDsgn(   rowsInWindow = rowsInWindow,\
                                            colsInWindow = colsInWindow,\
                                            multWidth=8,\
                                            flagComputeType=1)
    global allAccGaussSmthImg
    allAccGaussSmthImg = gSmthDsgn.processImage(img2DArray=testImageGray,multLUT=MultLuT_8x8)
    global allAccUtilLUTs,allAccUtilSlices,allAccUtilCCs
    (allAccUtilLUTs,allAccUtilSlices,allAccUtilCCs)=gSmthDsgn.estimateDesignUtilMetrics(listMultDsgns)
    print(allAccUtilLUTs,allAccUtilSlices,allAccUtilCCs)
    #gSmthDsgn.displayGS2SDsgnDetails()
    '''
    #---------------- Plot Gaussian Smoothed Image with all accurate multipliers -----------------------
    plt.figure(2)
    plt.imshow(fxdPnt8bitAllAccSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
    plt.title("2-stage Gaussian Smoothing All Accurate")
    psnr_AccAcc = support.psnr(fxdPnt8bitAllAccSmthImg,fxdPnt8bitAllAccSmthImg,255)
    ssim_AccAcc = ssim(fxdPnt8bitAllAccSmthImg,fxdPnt8bitAllAccSmthImg,win_size=minRowsInWindow)
    #All Approximate
    arrayAllApproxMuls   = np.random.randint(2,high=3,size=(minRowsInWindow+minColsInWindow))
    gSmthDsgn.updateMultTypes(arrayAllApproxMuls)
    #gSmthDsgn.displayGS2SDsgnDetails()
    fxdPnt8bitAllApproxSmthImg = gSmthDsgn.processImage(img2DArray=testImageGray,multLUT=MultLuT_8x8)
    psnr_AccApp = support.psnr(fxdPnt8bitAllApproxSmthImg,fxdPnt8bitAllAccSmthImg,255)
    ssim_AccApp = ssim(fxdPnt8bitAllAccSmthImg,fxdPnt8bitAllApproxSmthImg,win_size=minRowsInWindow)
    print("PSNRs: ",psnr_AccAcc,", ",psnr_AccApp)
    print("SSIMs: ",ssim_AccAcc,", ",ssim_AccApp)
    #---------------- Plot Gaussian Smoothed Image with updated multiplier types -----------------------
    plt.figure(3)
    plt.imshow(fxdPnt8bitAllApproxSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
    plt.title("2-stage Gaussian Smoothing All Approx")
    ##Display all plots
    plt.show()
    '''

def mutateChromosome(arg_indChrome,choices,indpb):
    for selLoc in range(len(arg_indChrome)):
        if rnd.random() < indpb:
            posChoices = list(choices)
            currChoice = arg_indChrome[selLoc]
            posChoices.remove(currChoice)
            if(len(posChoices) > 0):
                arg_indChrome[selLoc] = np.random.choice(posChoices)
    return  arg_indChrome




