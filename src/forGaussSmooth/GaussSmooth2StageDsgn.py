##System Library imports
import numpy as np
from matplotlib import style
style.use("ggplot")
from astropy.convolution import Gaussian1DKernel
from multiprocessing import Pool

#Import user-defined libraries
import forFIRFilter.FIRFilter as firf

'''
Class for evaluating 2-stage Gaussian Smoothing design
It instantiates 2 FIR Filters as its member variables
'''

class GaussSmth2StageDsgn:

    def __init__(self,rowsInWindow,colsInWindow,multWidth,sd_X=5,sd_Y=5,arrMulTypes=[],maxInpVal=None,flagComputeType=0):
        """
        Constructor
        :param rowsInWindow:    Number of rows in the window for Gaussian smoothing operation
        :param colsInWindow:    Number of rows in the window for Gaussian smoothing operation
        :param multWidth:       Width of the multiplier
        :param sd_X:            Standard deviation in X-axis for generating Gaussian co-efficients
        :param sd_Y:            Standard deviation in Y-axis for generating Gaussian co-efficients
        :param arrMulTypes:     Configuration of the type of multipliers in the design
        :param maxInpVal:       Maximum expected input value: defaults to 255
        :param flagComputeType: Type of computation. Default is 0 i.e. computation by floating point
                                                     Value of 1 means using LookUp Tables
        """
        self.numTaps_XFilter_   = rowsInWindow
        self.numTaps_YFilter_   = colsInWindow
        self.multWidth_         = multWidth
        self.computeType_       = flagComputeType
        if(maxInpVal == None):
            self.maxInpVal_     = 255
        else:
            self.maxInpVal_     = maxInpVal
        if(len(arrMulTypes)==0):        #If type is not specified, set everything to 0 i.e. accurate Vivado
            self.arrMultTypes_  = np.zeros(rowsInWindow + colsInWindow,dtype=int)
        else:
            self.arrMultTypes_  = arrMulTypes
        ##Instantiate Stage 1 (X) FIR Filter
        gauss1Dkern_x = (Gaussian1DKernel(sd_X,x_size=rowsInWindow)).array
        gauss1DkernNorm_x = gauss1Dkern_x / gauss1Dkern_x.sum()
        self.FxdPnt_FIRFilterX_ = firf.FIR_directForm(  rowsInWindow,\
                                                        gauss1DkernNorm_x,\
                                                        arrMulTypes=self.arrMultTypes_[0:rowsInWindow])
        self.FxdPnt_FIRFilterX_.maxAbsInp_ = self.maxInpVal_
        ##Instantiate Stage 2 (Y) FIR Filter
        gauss1Dkern_y = (Gaussian1DKernel(sd_Y,x_size=colsInWindow)).array
        gauss1DkernNorm_y = gauss1Dkern_y / gauss1Dkern_y.sum()
        self.FxdPnt_FIRFilterY_ = firf.FIR_directForm(  colsInWindow,\
                                                        gauss1DkernNorm_y,\
                                                        arrMulTypes=self.arrMultTypes_[rowsInWindow:(rowsInWindow+colsInWindow)])
        self.FxdPnt_FIRFilterY_.maxAbsInp_ = self.maxInpVal_

    def displayGS2SDsgnDetails(self):
        """
        Display the Gaussian Smooting 2-Stage Design details
        :return:
        """
        print("--------  Gaussian Smoothing 2-Stage Design Details------------ ")
        if self.computeType_ == 0:
            print("Computation Type: Numpy Integer Multiplication")
        else:
            print("Computation Type: LookUp Table-based Integer Multiplication")
        print("Stage 1 (X) FIR Filter Details:")
        self.FxdPnt_FIRFilterX_.displayFIRDetails()
        print("Stage 2 (Y) FIR Filter Details:")
        self.FxdPnt_FIRFilterY_.displayFIRDetails()

    def updateMultTypes(self,arrMultTypes):
        """
        Update the multiplier types in the design
        :param arrMultTypes: New multiplier type configuration to be used in the design.Used during DSE.
        :return:
        """
        if(len(arrMultTypes)!= (self.numTaps_XFilter_+self.numTaps_YFilter_)):
            print("ERROR: Inavlid array size")
            exit()
        self.arrMultTypes_ = arrMultTypes
        self.FxdPnt_FIRFilterX_.MultTypes_ = arrMultTypes[0:self.numTaps_XFilter_]
        self.FxdPnt_FIRFilterY_.MultTypes_ = arrMultTypes[self.numTaps_XFilter_:(self.numTaps_XFilter_ + self.numTaps_YFilter_)]

    def processImage(self,img2DArray,multLUT=[]):
        """
        Process Input image using the design
        :param img2DArray   : image Data to be processed
        :param multLUT      : LookUp Table to be used for computing multiplier-specific values
        :return             : Processed Image
        """
        tmpInpArrayX = np.zeros(np.shape(img2DArray))
        tmpInpArrayY = np.zeros(np.shape(img2DArray))
        pool = Pool(processes = 5)
        if(self.computeType_ == 1):
            print("Setting up multiplication Lookup Table")
            self.FxdPnt_FIRFilterX_.multLuT    = np.array(multLUT)
            self.FxdPnt_FIRFilterY_.multLuT    = np.array(multLUT)
            print("Image Processing...")
        '''
        for rowCntr in range(0,(np.shape(img2DArray))[0]):
            tmpInpArrayX[rowCntr,:] = self.FxdPnt_FIRFilterX_.compOutputresponse1D(img2DArray[rowCntr,:],\
                                                    multWidth=self.multWidth_,flagOpRepr=1,\
                                                    flagComputeType=self.computeType_)

        tmpInpArrayX = np.apply_along_axis(\
                    self.FxdPnt_FIRFilterX_.compOutputresponse1D,\
                    1,\
                    img2DArray,\
                    multWidth=self.multWidth_,flagOpRepr=1,\
                    flagComputeType=self.computeType_\
                    )
        '''

        def rowWiseProcess(inpRowIdx):
            tmpInpArrayX[inpRowIdx,:] =self.FxdPnt_FIRFilterX_.compOutputresponse1D(img2DArray[inpRowIdx,:],\
                                                    multWidth=self.multWidth_,flagOpRepr=1,\
                                                    flagComputeType=self.computeType_)
            return 0
        vecRowProcess   = np.vectorize(rowWiseProcess)
        tmp1    = vecRowProcess(range(0,(np.shape(img2DArray))[0]))
        '''
        for colCntr in range(0,(np.shape(img2DArray))[1]):
            tmpInpArrayY[:,colCntr] = self.FxdPnt_FIRFilterY_.compOutputresponse1D(tmpInpArrayX[:,colCntr],\
                                                    multWidth=self.multWidth_,flagOpRepr=1,\
                                                    flagComputeType=self.computeType_)

        tmpInpArrayY = np.apply_along_axis(\
                    self.FxdPnt_FIRFilterY_.compOutputresponse1D,\
                    0,\
                    tmpInpArrayX,\
                    multWidth=self.multWidth_,flagOpRepr=1,\
                    flagComputeType=self.computeType_\
                    )
        '''

        def colWiseProcess(inpColIdx):
            tmpInpArrayY[:,inpColIdx] =self.FxdPnt_FIRFilterY_.compOutputresponse1D(tmpInpArrayX[:,inpColIdx],\
                                                    multWidth=self.multWidth_,flagOpRepr=1,\
                                                    flagComputeType=self.computeType_)
            return 0
        vecColProcess   = np.vectorize(colWiseProcess)
        tmp2    = vecColProcess(range(0,(np.shape(img2DArray))[1]))
        pool.close()
        return tmpInpArrayY

    def estimateDesignUtilMetrics(self,listMultDsgnPnts):
        """
        Compute the design-specific metrics (resource utilization )
        :param listMultDsgnPnts: List of resource utilization of each design point
        :return:
        """
        utilLUTs=0;utilSlices=0;utilCCs=0;
        for multType in self.arrMultTypes_:
            utilLUTs = utilLUTs + listMultDsgnPnts[multType].utilLUTs_
            utilSlices = utilSlices + listMultDsgnPnts[multType].utilSlices_
            utilCCs = utilCCs + listMultDsgnPnts[multType].utilCCs_
        return (utilLUTs,utilSlices,utilCCs)
