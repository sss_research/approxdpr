##System Library imports
import numpy as np
from numpy import genfromtxt
import subprocess as subproc
import random as rnd
from deap import base
from deap import creator
from deap import tools
from matplotlib import style
style.use("ggplot")
from PIL import Image as img
import threading as thr
import time as time
import timeit as timeit
from scipy import signal
import pylab as pylab
from astropy.convolution import Gaussian2DKernel
from astropy.convolution import Gaussian1DKernel
##User-defined library imports
import forFIRFilter.FIRFilter as firf
import forSupport.supportFunctions as support

def testGausSmth_python(inp2DArr,rowsInWindow,colsInWindow):
    gauss2Dkern = (Gaussian2DKernel(5,x_size=rowsInWindow,y_size=colsInWindow)).array
    gauss2Dkern_norm = gauss2Dkern / gauss2Dkern.sum()
    out2DArr = signal.convolve(inp2DArr,gauss2Dkern_norm,mode='valid')
    #out2DArr = support.blur_image(inp2DArr,rowsInWindow,colsInWindow)
    return out2DArr

def testGausSmth2Stage_xbitFxdPntFIR(inp2DArr,rowsInWindow,colsInWindow,flagComputeType=0,mulLuT=[],multTypeArrayX=[],multTypeArrayY=[],multWidth=None):
    numRows = np.shape(inp2DArr)[0]
    numCols = np.shape(inp2DArr)[1]
    #print("input shape: " , np.shape(inp2DArr))
    tmpInpArrayX = np.zeros(np.shape(inp2DArr))
    tmpInpArrayY = np.zeros(np.shape(inp2DArr))
    #Get maximum absolute value of input 2D array
    maxAbsInpVal = np.amax(inp2DArr)   #for image
    # Get gaussian window coefficients
    ## For row level processing
    gauss1Dkern_x = (Gaussian1DKernel(5,x_size=rowsInWindow)).array
    gauss1DkernNorm_x = gauss1Dkern_x / gauss1Dkern_x.sum()
    #print(gauss1DkernNorm_x)
    ## For column level processing
    gauss1Dkern_y = (Gaussian1DKernel(5,x_size=colsInWindow)).array
    gauss1DkernNorm_y = gauss1Dkern_y / gauss1Dkern_y.sum()
    #print(gauss1DkernNorm_y)
    #Instantiate Gaussian_X filter
    FxdPnt_FIRFilter_X = firf.FIR_directForm(rowsInWindow,gauss1DkernNorm_x,arrMulTypes=multTypeArrayX,mulLuT=mulLuT)
    FxdPnt_FIRFilter_X.maxAbsInp_ = maxAbsInpVal
    #FxdPnt_FIRFilter_X.displayFIRDetails()
    #Generate X-axis filtered data
    for rowCntr in range(0,(np.shape(inp2DArr))[0]):
        tmpInpArrayX[rowCntr,:] = FxdPnt_FIRFilter_X.compOutputresponse1D(inp2DArr[rowCntr,:],\
                                                multWidth=multWidth,flagOpRepr=1,\
                                                flagComputeType=flagComputeType)
    #return tmpInpArrayX
    #Instantiate Gaussian_Y filter
    FxdPnt_FIRFilter_Y = firf.FIR_directForm(colsInWindow,gauss1DkernNorm_y,arrMulTypes=multTypeArrayY,mulLuT=mulLuT)
    FxdPnt_FIRFilter_Y.maxAbsInp_ = maxAbsInpVal
    #FxdPnt_FIRFilter_Y.displayFIRDetails()
    #Generate Y-axis filtered data
    for colCntr in range(0,(np.shape(inp2DArr))[1]):
        tmpInpArrayY[:,colCntr] = FxdPnt_FIRFilter_Y.compOutputresponse1D(tmpInpArrayX[:,colCntr],\
                                                multWidth=multWidth,flagOpRepr=1,\
                                                flagComputeType=flagComputeType)
    #Get filtered image
    prcImg = tmpInpArrayY
    #print("processed shape: " , np.shape(prcImg))
    return prcImg

def testGausSmth2D_xbitFxdPntFIR(inp2DArr,rowsInWindow,colsInWindow,flagComputeType=0,mulLuT=[],multTypeArrayX=[],multTypeArrayY=[],multWidth=None):
    gauss2Dkern = (Gaussian2DKernel(5,x_size=rowsInWindow,y_size=colsInWindow)).array
    gauss2Dkern_norm = gauss2Dkern / gauss2Dkern.sum()
    gausKernelFlattened_norm = gauss2Dkern_norm.flatten()
    #print(gausKernelFlattened_norm)
    #print("window rows and columns: ",rowsInWindow,", ", colsInWindow)
    #Get maximum absolute value of input 2D array
    maxAbsInpVal = np.amax(inp2DArr)   #for image
    #Instantiate FIR Filter
    FxdPnt_FIRFilter = firf.FIR_directForm((rowsInWindow*colsInWindow),gausKernelFlattened_norm,arrMulTypes=multTypeArrayX,mulLuT=mulLuT)
    FxdPnt_FIRFilter.maxAbsInp_ = maxAbsInpVal
    #FxdPnt_FIRFilter.displayFIRDetails()
    prcImage = FxdPnt_FIRFilter.compOutputresponse2D(inp2DArr,rowsInWindow,colsInWindow,flagOpRepr=1,multWidth=multWidth,flagComputeType=flagComputeType)
    return prcImage
