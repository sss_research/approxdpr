##System Library imports
import numpy as np
from matplotlib import style
style.use("ggplot")
from astropy.convolution import Gaussian1DKernel
from astropy.convolution import Gaussian2DKernel
from multiprocessing import Pool

#Import user-defined libraries
import forFIRFilter.FIRFilter as firf

'''
Class for evaluating 2-stage Gaussian Smoothing design
It instantiates 2 FIR Filters as its member variables
'''

class GaussSmth2DWinDsgn:

    def __init__(self,rowsInWindow,colsInWindow,multWidth,sd_X=5,sd_Y=5,arrMulTypes=[],maxInpVal=None,flagComputeType=0):
        """
        Constructor
        :param rowsInWindow:    Number of rows in the window for Gaussian smoothing operation
        :param colsInWindow:    Number of rows in the window for Gaussian smoothing operation
        :param multWidth:       Width of the multiplier
        :param sd_X:            Standard deviation in X-axis for generating Gaussian co-efficients
        :param sd_Y:            Standard deviation in Y-axis for generating Gaussian co-efficients
        :param arrMulTypes:     Configuration of the type of multipliers in the design
        :param maxInpVal:       Maximum expected input value: defaults to 255
        :param flagComputeType: Type of computation. Default is 0 i.e. computation by floating point
                                                     Value of 1 means using LookUp Tables
        """
        self.lengthX_ = rowsInWindow
        self.lengthY_ = colsInWindow
        self.numTaps_Filter_   = rowsInWindow*colsInWindow
        self.multWidth_         = multWidth
        self.computeType_       = flagComputeType
        if(maxInpVal == None):
            self.maxInpVal_     = 255
        else:
            self.maxInpVal_     = maxInpVal
        if(len(arrMulTypes)==0):        #If type is not specified, set everything to 0 i.e. accurate Vivado
            self.arrMultTypes_  = np.zeros(rowsInWindow * colsInWindow,dtype=int)
        else:
            self.arrMultTypes_  = arrMulTypes
        ##Instantiate Stage 1 (X) FIR Filter
        gauss2Dkern = (Gaussian2DKernel(sd_X, x_size=rowsInWindow, y_size=colsInWindow)).array
        gauss2DkernNorm = gauss2Dkern / gauss2Dkern.sum()
        coeffArray = gauss2DkernNorm.flatten()
        self.FxdPnt_FIRFilter_ = firf.FIR_directForm(  rowsInWindow*colsInWindow, \
                                                       coeffArray,\
                                                        arrMulTypes=self.arrMultTypes_)
        self.FxdPnt_FIRFilter_.maxAbsInp_ = self.maxInpVal_

    def displayGS2DDsgnDetails(self):
        """
        Display the Gaussian Smooting 2-Stage Design details
        :return:
        """
        print("--------  Gaussian Smoothing 2D Design Details------------ ")
        if self.computeType_ == 0:
            print("Computation Type: Numpy Integer Multiplication")
        else:
            print("Computation Type: LookUp Table-based Integer Multiplication")
        print("2D FIR Filter Details:")
        self.FxdPnt_FIRFilter_.displayFIRDetails()

    def updateMultTypes(self,arrMultTypes):
        """
        Update the multiplier types in the design
        :param arrMultTypes: New multiplier type configuration to be used in the design.Used during DSE.
        :return:
        """
        if(len(arrMultTypes)!= self.numTaps_Filter_):
            print("ERROR: Inavlid array size")
            exit()
        self.arrMultTypes_ = arrMultTypes
        self.FxdPnt_FIRFilter_.MultTypes_ = arrMultTypes

    def processImage(self,img2DArray,multLUT=[]):
        """
        Process Input image using the design
        :param img2DArray   : image Data to be processed
        :param multLUT      : LookUp Table to be used for computing multiplier-specific values
        :return             : Processed Image
        """
        # Zero Padding
        numZP_eachSideX = (np.round((self.lengthX_ - 1) / 2)).astype(int)
        numZP_eachSideY = (np.round((self.lengthY_ - 1) / 2)).astype(int)
        inp2DArr_ZPadded = np.lib.pad(
            img2DArray,
            (
                (numZP_eachSideY, numZP_eachSideY),
                (numZP_eachSideX, numZP_eachSideX)
            ),
            'constant', constant_values=(0)
        )
        # print("ZeroPadded Input:\n",inp2DArr_ZPadded)
        ##Compute response
        out2DArray = np.zeros(np.shape(img2DArray))
        # print("Assumption: The sequence starts at array[0]")
        ##For each pixel of input image, create the window size from zero padded array
        for rowCntr in range(0, (np.shape(img2DArray)[0])):
            for colCntr in range(0, (np.shape(img2DArray)[1])):
                ##Get starting co-ordinates of window
                rowIdx_Window = rowCntr
                colIdx_Window = colCntr
                matWindow = inp2DArr_ZPadded[
                            rowIdx_Window:rowIdx_Window + self.lengthX_,
                            colIdx_Window:colIdx_Window + self.lengthY_]
                arrWindow = matWindow.flatten()
                out2DArray[rowCntr, colCntr] = \
                    self.FxdPnt_FIRFilter_.compOutputWithWindow(
                            arrWindow,
                            flagOpRepr=1,
                            multWidth=self.multWidth_,
                            adderWidth=None,
                            flagComputeType=self.computeType_,
                        multLUTArr=multLUT
                        )
        return out2DArray

    def estimateDesignUtilMetrics(self,listMultDsgnPnts):
        """
        Compute the design-specific metrics (resource utilization )
        :param listMultDsgnPnts: List of resource utilization of each design point
        :return:
        """
        utilLUTs=0;utilSlices=0;utilCCs=0;
        for multType in self.arrMultTypes_:
            utilLUTs = utilLUTs + listMultDsgnPnts[multType].utilLUTs_
            utilSlices = utilSlices + listMultDsgnPnts[multType].utilSlices_
            utilCCs = utilCCs + listMultDsgnPnts[multType].utilCCs_
        return (utilLUTs,utilSlices,utilCCs)
