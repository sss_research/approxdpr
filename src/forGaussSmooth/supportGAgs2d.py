#!/usr/bin/env python3.5
#System Libraries
import numpy as np
from matplotlib import style
style.use("ggplot")
from skimage.measure import compare_ssim as ssim

#User defined
import forSupport.supportFunctions as support
import forGaussSmooth.settingsDSE2dGaussSmth as settings

#Create a random individual chromosome
def CreateRandomChromosome():
    numOptions = settings.gSmthDsgn.numTaps_Filter_
    choices = settings.multDsgnOptions
    chromosome = np.random.choice(choices,numOptions)
    print(chromosome)
    return chromosome

#Compute the rewards for the individual chromosome
def ComputeRewardForChromosome(individual):
    #Update multiplier types ffor the gs2s design
    arrayMultTypes = individual
    settings.gSmthDsgn.updateMultTypes(arrayMultTypes)
    #Process test image
    gsmthImage = settings.gSmthDsgn.processImage(\
                                    img2DArray=settings.testImageGray,\
                                    multLUT=settings.MultLuT_8x8\
                                    )
    #Compute PSNR
    psnrVal = support.psnr(\
                    gsmthImage,\
                    settings.allAccGaussSmthImg,\
                    255\
                    )
    #Compute SSIM
    ssimVal = ssim(\
                settings.allAccGaussSmthImg,\
                gsmthImage,\
                win_size=settings.ssimEvalWinSize\
                )
    #Compute Design Rsrc Utilization (LUTs)
    (totalLUTs,totalSlices,totalCCs) = settings.gSmthDsgn.estimateDesignUtilMetrics(settings.listMultDsgns)
    return (totalLUTs/settings.allAccUtilLUTs),\
            (ssimVal)
