#!/usr/bin/env python3.5
import numpy as np
#import matplotlib.pyplot as plt
from numpy import genfromtxt
import subprocess as subproc
import random as rnd
from deap import base
from deap import creator
from deap import tools
from matplotlib import style
style.use("ggplot")
from PIL import Image as img
import threading as thr
import time as time
import timeit as timeit
from scipy import signal
import pylab as pylab
#User defined
import forFIRFilter.supportFIR_1D as supFIR1
import forLibrary.MultDsgns as multDes

########## Input Test Signal #########
sample_rate     = 100.0
nsamples        = 1000
t               = pylab.arange(nsamples) / sample_rate
testSignal      = np.cos(2*np.pi*0.5*t) + 0.2*np.sin(2*np.pi*2.5*t+0.1) + \
                    0.2*np.sin(2*np.pi*15.3*t) + 0.1*np.sin(2*np.pi*16.7*t + 0.1) + \
                    0.1*np.sin(2*np.pi*23.45*t+.8)
cutoff_hz       = 10.0
nyq_rate        = sample_rate / 2.0
######################################
######### Input FIR parameters #######
numTaps_AccfxdPnt = 16
numTaps_AccfltPnt = 16
minTaps_forFxdDSE = 16
maxTaps_forFxdDSE = 32
######################################
############### Genetic Algorithm Parameters ##############

############################################################
##Main function
def main():
    print("Genetic Algorithm based exploration for Approximate Filter parameterization")
    np.random.seed(10)
    #----------------- Plot Sample results -----------------------------------------------------------
    pylab.figure(1)
    pylab.xlabel('t')
    pylab.title('Plotting data from DSE for approximate multipliers-based FIR filter design')
    pylab.grid(True)
    pylab.ylim(-2.0, 2.0)
    #-----------------------------------------------------------------
    #---------------- Plot the original signal.-----------------------
    pylab.plot(t, testSignal,'g-',label='I/P Signal')
    #------------------------------------------------------------------------------------------------------
    ##------------------------ Python floating point filtering--------------------------
    #------------------------------------------------------------------------------------------------------
    (outAccFltPnt_FIRFiltered,psnr_Flt_AccFlt) = supFIR1.testFilter_FltPntAccFIR_1D(\
                                            testSignal,sample_rate,nyq_rate,cutoff_hz,numTaps_AccfltPnt)
    delay_fltPnt = 0.5 * (numTaps_AccfltPnt - 1) / sample_rate
    # Plot the floating point filtered
    labelStr = '32-bit FltPntAcc; PSNR: ' + str('%6.2f'%psnr_Flt_AccFlt)
    pylab.plot(t-delay_fltPnt, outAccFltPnt_FIRFiltered, 'b-',label=labelStr)
    print("PSNR (floating vs floating point): ",psnr_Flt_AccFlt)
    #------------------------------------------------------------------------------------------------------
    ##---------------------- Load multiplier Lookup Table -----------------------------------
    #------------------------------------------------------------------------------------------------------
    print("Loading Multiplier Look-up table .... ")
    MultLuT_8x8 = np.loadtxt(open("../libraries/PDTuD/LuT_8x8_Multiplier.csv", "rb"), delimiter=",",skiprows=1)
    #------------------------------------------------------------------------------------------------------
    # --------------------- 32-bit Fixed point all Accurate (numpy integer multiplication) filtering  ----------------------------------
    #------------------------------------------------------------------------------------------------------
    (outFxdPnt_FIRFiltered,psnr_Flt_Fxd) = supFIR1.testFilter_xbitFxdPntFIR_1D(
                                                testSignal,sample_rate,nyq_rate,cutoff_hz,
                                                minTaps_forFxdDSE,
                                                flagComputeType=0,
                                                multWidth=31
                                        )
    delay_fxdPnt = 0.5 * (minTaps_forFxdDSE - 1) / sample_rate
    # Plot the floating point filtered
    labelStr = '31-bit FxdPntAcc; PSNR: ' + str('%6.2f'%psnr_Flt_Fxd)
    pylab.plot(t-delay_fxdPnt, outFxdPnt_FIRFiltered, 'k-',label=labelStr)
    print("PSNR (Floating vs Accurate Fixed(multipliers only)): ",psnr_Flt_Fxd)
    #return 0
    #------------------------------------------------------------------------------------------------------
    # --------------------- 8-bit Fixed point all Accurate (lookup Table based) filtering  ----------------------------------
    #------------------------------------------------------------------------------------------------------
    arrayMuls   = np.random.randint(0,high=1,size=minTaps_forFxdDSE)
    (outFxdPnt_FIRFiltered,psnr_Flt_Fxd) = supFIR1.testFilter_xbitFxdPntFIR_1D(\
                                    testSignal,sample_rate,nyq_rate,cutoff_hz,\
                                    minTaps_forFxdDSE,\
                                    flagComputeType=1,\
                                    mulLuT=MultLuT_8x8,multTypeArray=arrayMuls,\
                                    multWidth=8)
    delay_fxdPnt = 0.5 * (minTaps_forFxdDSE - 1) / sample_rate
    # Plot the floating point filtered
    labelStr = '8-bit FxdPntAccLuT; PSNR: ' + str('%6.2f'%psnr_Flt_Fxd)
    pylab.plot(t-delay_fxdPnt, outFxdPnt_FIRFiltered, 'r-',label=labelStr)
    print("PSNR (Floating vs Accurate Fixed(multipliers only)): ",psnr_Flt_Fxd)

    #------------------------------------------------------------------------------------------------------
    # --------------------- 8-bit Fixed point all Approximate filtering  ---------------------------
    #------------------------------------------------------------------------------------------------------
    arrayMuls   = np.random.randint(2,high=3,size=minTaps_forFxdDSE)
    (outFxdPnt_FIRFiltered,psnr_Flt_Fxd) = supFIR1.testFilter_xbitFxdPntFIR_1D(\
                                    testSignal,sample_rate,nyq_rate,cutoff_hz,\
                                    minTaps_forFxdDSE,\
                                    flagComputeType=1,\
                                    mulLuT=MultLuT_8x8,multTypeArray=arrayMuls,\
                                    multWidth=8)
    delay_fxdPnt = 0.5 * (minTaps_forFxdDSE - 1) / sample_rate
    # Plot the floating point filtered
    labelStr = '8-bit FxdPntApproxLuT; PSNR: ' + str('%6.2f'%psnr_Flt_Fxd)
    pylab.plot(t-delay_fxdPnt, outFxdPnt_FIRFiltered, 'm-',label=labelStr)
    print("PSNR (Floating vs Approx Fixed(multipliers only)): ",psnr_Flt_Fxd)
    #------------------------------------------------------------------------------------------------------
    pylab.legend(loc='upper left', shadow=True)
    ##Display all plots
    pylab.show()
    print("Sample display Done !!!")
    return 0



if __name__== "__main__":
        main()

print ("Processing Completed...")
