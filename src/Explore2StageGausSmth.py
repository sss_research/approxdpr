#!/usr/bin/env python3.5
#System Libraries
import numpy as np
from matplotlib import style
style.use("ggplot")
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from skimage.measure import compare_ssim as ssim
#User defined

import forLibrary.MultDsgns as multDes
import forSupport.supportFunctions as support
import forGaussSmooth.GaussSmooth2StageDsgn as gs2s


########## Settings ################
MULT_WIDTH          = 8
MULT_LUT            = "../libraries/PDTuD/LuT_8x8_Multiplier.csv"
MULT_DSGN_METRICS   = "../libraries/PDTuD/DesignPointsMetrics.csv"
####################################
INPUT_IMAGE         = "../test/images/lena.jpeg"
ROWS_WINDOW         = 3
COLS_WINDOW         = 3
SSIM_EVAL_WINDOW    = 3
#vary this to change multiplier configuration.
#Make sure the length of the array is equal to
#the sum of ROWS_WINDOW and COLS_WINDOW
TEST_MULT_CONFIG    = [1, 1, 1, 1, 1, 1 ]
#####################################

##---------------------- Load multiplier Lookup Table -----------------------------------
print("Loading Multiplier Look-up table ...")
MultLuT_8x8 = np.loadtxt(open(MULT_LUT, "rb"), delimiter=",",skiprows=1)
##----------------------Parse Multiplier Design Metrics----------------------------------
print("Loading Multiplier designs point metrics ...")
listMultDsgns = multDes.parseMultDsgnPoints(MULT_DSGN_METRICS)
#---------------- Read original Image.-----------------------
testImage       = mpimg.imread(INPUT_IMAGE)
testImageGray   = support.rgb2gray(testImage)
#'''
#---------------- Plot the original Image.-----------------------
plt.figure(1)
plt.title("Original Image")
plt.imshow(testImageGray, cmap = plt.get_cmap('gray'),label='Original Image')
#'''
###########################################################################################
# --------------------- 8-bit Fixed point all Accurate 2-stage Gaussian Smoothing   -----
###########################################################################################
print("--------------------- 8-bit Fixed point all Accurate 2-stage Gaussian Smoothing   ------------------")
##Instantiate 2-Stage Gaussian Smoothing Design
gSmthDsgn = gs2s.GaussSmth2StageDsgn(
                                        rowsInWindow = ROWS_WINDOW,
                                        colsInWindow = COLS_WINDOW,
                                        multWidth=MULT_WIDTH,\
                                        flagComputeType=1
                                    )
allAccGaussSmthImg = gSmthDsgn.processImage(img2DArray=testImageGray,multLUT=MultLuT_8x8)
(allAccUtilLUTs,allAccUtilSlices,allAccUtilCCs)=gSmthDsgn.estimateDesignUtilMetrics(listMultDsgns)
print(allAccUtilLUTs,allAccUtilSlices,allAccUtilCCs)
gSmthDsgn.displayGS2SDsgnDetails()
psnr_AccAcc = support.psnr(allAccGaussSmthImg,allAccGaussSmthImg,255)
ssim_AccAcc = ssim(allAccGaussSmthImg,allAccGaussSmthImg,win_size=SSIM_EVAL_WINDOW)
#---------------- Plot Gaussian Smoothed Image with all accurate multipliers -----------------------
#'''
plt.figure(2)
plt.imshow(allAccGaussSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
plt.title("2-stage Gaussian Smoothing All Accurate")
plt.axis('off')
plt.savefig("../outputs/allAccurate.pdf",bbox_inches='tight')
#'''
############################################################################################
# --------------------- 8-bit Fixed point All Approximate 2-stage Gaussian Smoothing   -----
############################################################################################
print("--------------------- 8-bit Fixed point all Approximate 2-stage Gaussian Smoothing   ------------------")
arrayAllApproxMuls   = np.random.randint(2,high=3,size=(ROWS_WINDOW+COLS_WINDOW))
gSmthDsgn.updateMultTypes(arrayAllApproxMuls)
gSmthDsgn.displayGS2SDsgnDetails()
allApproxGaussSmthImg = gSmthDsgn.processImage(img2DArray=testImageGray,multLUT=MultLuT_8x8)
psnr_AccApp = support.psnr(allApproxGaussSmthImg,allAccGaussSmthImg,255)
ssim_AccApp = ssim(allAccGaussSmthImg,allApproxGaussSmthImg,win_size=SSIM_EVAL_WINDOW)
#---------------- Plot Gaussian Smoothed Image with all Approximate multipliers -----------------------
plt.figure(3)
plt.imshow(allApproxGaussSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
plt.title("2-stage Gaussian Smoothing All Approx")
plt.axis('off')
plt.savefig("../outputs/allApprox.pdf",bbox_inches='tight')
############################################################################################
# ----------- 8-bit Fixed point 2-stage Gaussian Smoothing with specified Mult Types   -----
############################################################################################
if(len(TEST_MULT_CONFIG)== (ROWS_WINDOW + COLS_WINDOW) ):
    print("--------------------- 8-bit Fixed point 2-stage Gaussian Smoothing (Test Mult Config)   ------------------")
    gSmthDsgn.updateMultTypes(TEST_MULT_CONFIG)
    gSmthDsgn.displayGS2SDsgnDetails()
    testGaussSmthImg = gSmthDsgn.processImage(img2DArray=testImageGray,multLUT=MultLuT_8x8)
    psnr_AccTest = support.psnr(testGaussSmthImg,allAccGaussSmthImg,255)
    ssim_AccTest = ssim(allAccGaussSmthImg,testGaussSmthImg,win_size=SSIM_EVAL_WINDOW)
    print("PSNRs(AccAcc,AccApp,AccTest): ",psnr_AccAcc,", ",psnr_AccApp,", ",psnr_AccTest)
    print("SSIMs(AccAcc,AccApp,AccTest): ",ssim_AccAcc,", ",ssim_AccApp,", ",ssim_AccTest)
    #---------------- Plot Gaussian Smoothed Image with all Approximate multipliers -----------------------
    plt.figure(3)
    plt.imshow(testGaussSmthImg, cmap = plt.get_cmap('gray'),label='Smoothed Image')
    plt.title("2-stage Gaussian Smoothing with Test Multiplier Config")
    plt.axis('off')
    plt.savefig("../outputs/testConfig.pdf", bbox_inches='tight')
else:
    print("Length of test config array not matching the sum of rows and columns of Smoothing window")
    print("PSNRs: ", psnr_AccAcc, ", ", psnr_AccApp)
    print("SSIMs: ", ssim_AccAcc, ", ", ssim_AccApp)
##Display all plots
#plt.show()