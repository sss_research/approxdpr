#!/usr/bin/env bash
python3 DSEfor2StageGaussianSmoothing.py  "lena.jpeg" 3 3 "Vivado"
python3 DSEfor2StageGaussianSmoothing.py  "lena.jpeg" 3 3 "Booth"

python3 DSEfor2StageGaussianSmoothing.py  "lena.jpeg" 5 5 "Vivado"
python3 DSEfor2StageGaussianSmoothing.py  "lena.jpeg" 5 5 "Booth"

python3 DSEfor2StageGaussianSmoothing.py  "lena.jpeg" 7 7 "Vivado"
python3 DSEfor2StageGaussianSmoothing.py  "lena.jpeg" 7 7 "Booth"
