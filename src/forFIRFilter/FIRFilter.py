#System Libraries
import numpy as np
import random

#User-defined libraries
import forApproximation.ApproximateLibrary as approxLib


'''
Class definition ofr creating an FIR filter
'''
class FIR_directForm:
    ##Constructor
    def __init__(self,numTaps,arrCoeffs,arrMulTypes=[],arrAddTypes=[],mulLuT=[]):
        self.numTaps_   = numTaps           #Number of Taps in the filter
        self.numMuls_   = numTaps           #Number of multipliers in the FIR (equivalent to the number of taps)
        self.numAdds_   = numTaps-1         #Number of adders in the design
        self.FIRCoeffs_ = arrCoeffs         #FIR filter co-efficients
        self.intOutVal_ = 0
        self.maxAbsFIRCoeff_ = np.amax(np.absolute(self.FIRCoeffs_)) #Maximum value of the filter coefficients.
                                                                    # This is used during scaling to find out the
                                                                    #appropriate fixed point values used in the design
        if(len(arrCoeffs) != numTaps):          #Sanity check
            print("ERROR:: Incorrect Arguments: Number of co-efficients and number of taps are not equal")
            exit()
        '''
        ##Initialize the type of adders
        #Default is floating point operation.
        This means the assumption of accurate adders
        '''
        if(len(arrAddTypes) == 0):
            self.AdderTypes_ = []
        else:
            self.AdderTypes_ = arrAddTypes
        '''
        ##Initialize the type of multipliers
        #Default is floating point operation
        '''
        if(len(arrMulTypes) == 0):
            self.MultTypes_ = []
        else:
            self.MultTypes_ = arrMulTypes
        self.maxAbsInp_ = np.nan    #Maximum expected value of the inputs.
                                    #This is used during scaling to find out the
                                    #appropriate fixed point values used in the design
        self.multLuT    = np.array(mulLuT)  #Table to get the values from, in the case
                                            #of using look-up table based approximation value determination
        self.rsrcLUTs   = np.nan            #Estimated resource usage LUTs
        self.rsrcSlices = np.nan            #Estimated resource usage Slices
    ##Display the instantiated FIR details
    def displayFIRDetails(self):
        print("--------  FIR Filter Details------------ ")
        print("Architecture: Direct Form (refer: DSP with FPGAs page no. 166)" )
        print("# Taps: ", str(self.numTaps_))
        print("Adder Types : ")
        print(self.AdderTypes_)
        print("Multiplier Types : ")
        print(self.MultTypes_)
        print("Multiplication LuT size: ",np.shape(self.multLuT))
        print("Maximum Input Value: ",self.maxAbsInp_)
    ##Compute the FIR filter output for an input window of length equal to numTaps
    def compOutputWithWindow(self,inputVals,flagOpRepr=None,multWidth=None,adderWidth=None,flagComputeType=None,multLUTArr=[]):
        ##inputVals : a numpy array of values which will be computed
        ##flagOpRepr : Floating point (default,0) or fixed-point(1)
        if(flagOpRepr==None):
            flagOpRepr=0
        ##multWidth : Width of multiplier (default 8-bit)
        if(multWidth==None):
            multWidth=8
        ##adderWidth : Width of multiplier (default 16-bit)
        if(adderWidth==None):
            adderWidth=16
        ##flagComputeType : Compute (default, 0) or Lookup(1)
        if(flagComputeType==None):
            flagComputeType=0
        if(len(multLUTArr)!=0):
            self.multLuT = multLUTArr
        #print("Shape of Mult LUT: ",np.shape(self.multLuT))
        #Compute the multiplier-specific element-wise multiplication of two arrays of numbers
        tmpProds  = approxLib.compArrayProd(self.FIRCoeffs_,self.maxAbsFIRCoeff_,
                                            inputVals,self.maxAbsInp_,
                                            flagOpRepr=flagOpRepr,
                                            multWidth=multWidth,
                                            flagComputeType=flagComputeType,
                                            arrMultTypes=self.MultTypes_,
                                            multLuT=self.multLuT)
        #Add the products (The accumulate operation)
        accumSum  = approxLib.compCumulSum(tmpProds,flagOpRepr,adderWidth,flagComputeType,self.AdderTypes_)
        self.intCoeffs_ = (np.round(
                                (np.divide(self.FIRCoeffs_,self.maxAbsFIRCoeff_)) *
                                (np.power(2,(multWidth-1))-1))
                            ).astype(int)
        self.intOutVal_ = accumSum      #To enable reading the integer valued FIR Filter output for each cycle
        if(flagOpRepr !=0  ):      #Fixed point
            #print("Descaling outputs:",multWidth)
            accumSum =  np.multiply(accumSum, (self.maxAbsFIRCoeff_ * self.maxAbsInp_)) / (np.power(2, (multWidth * 2 - 2)))
        '''
        Bug Alert!!!!
        May be appropriate for 1D-signal processing only ??
        #'''
        return accumSum
    ## Compute the output response for convolution of a sequence of samples
    def compOutputresponseConv(self,inSampleSeq,flagOpRepr=None,multWidth=None,adderWidth=None,flagComputeType=None):
        ##inSampleSeq : a numpy array of sequence of samples over which the moving window will be operated
        ##flagOpRepr : Floating point (default,0) or fixed-point(1)
        if(flagOpRepr==None):
            flagOpRepr=0
        ##multWidth : Width of multiplier (default 8-bit)
        if(multWidth==None):
            multWidth=8
        ##adderWidth : Width of multiplier (default 16-bit)
        if(adderWidth==None):
            adderWidth=16
        ##flagComputeType : Compute (default, 0) or Lookup(1)
        if(flagComputeType==None):
            flagComputeType=0
        #print("Flag Operand Repr: ",flagOpRepr)
        ##Compute response
        opSeqLen = self.numTaps_ + len(inSampleSeq) - 1
        #Append appropriate zeros at both ends of input sample sequence
        padArray = np.zeros(self.numTaps_ - 1)
        seqShiftVals = np.insert(inSampleSeq,0,padArray)
        seqShiftVals = np.append(seqShiftVals,padArray)
        #print("Shifting Sequence: ",seqShiftVals)
        outSeq = np.zeros(opSeqLen)
        #print("Assumption: The sequence starts at array[0]")
        for op_cntr in range(opSeqLen):
            #print("Input Window: ",seqShiftVals[op_cntr:(op_cntr + self.numTaps_)])
            outSeq[op_cntr] = self.compOutputWithWindow(
                                        seqShiftVals[op_cntr:(op_cntr + self.numTaps_)],
                                        flagOpRepr=flagOpRepr,
                                        multWidth=multWidth,
                                        adderWidth=adderWidth,
                                        flagComputeType=flagComputeType
                                    )
            '''
            Explanation for arguments passed:
            seqShiftVals[op_cntr:(op_cntr + self.numTaps_)]: This is like a moving window over the input sequence
                                                            Starting from op_cntr entry for a length equivalent 
                                                            to the length of the number of taps.
            '''
        outRetSeq =  outSeq[0:len(inSampleSeq)]
        return outRetSeq
    '''
    ## Compute the output response for a 1D sequence of samples
    ###Similar to the convolution. But includes the appropriate zero padding at the ends to account for a moing window
    ###that does not lose information at the edges. 
    '''
    def compOutputresponse1D(self,inSampleSeq,flagOpRepr=None,multWidth=None,adderWidth=None,flagComputeType=None):
        ##inSampleSeq : a numpy array of sequence of samples over which the moving window will be operated
        ##flagOpRepr : Floating point (default,0) or fixed-point(1)
        if(flagOpRepr==None):
            flagOpRepr=0
        ##multWidth : Width of multiplier (default 8-bit)
        if(multWidth==None):
            multWidth=8
        ##adderWidth : Width of multiplier (default 16-bit)
        if(adderWidth==None):
            adderWidth=16
        ##flagComputeType : Compute (default, 0) or Lookup(1)
        if(flagComputeType==None):
            flagComputeType=0
        #print("Flag Operand Repr: ",flagOpRepr)
        ##Compute response
        opSeqLen = len(inSampleSeq)
        outSeq = np.zeros(opSeqLen)
        #Zero Padding
        numZP_eachSide = (np.round((self.numTaps_ - 1)/2)).astype(int)
        ZP_arr = np.zeros(numZP_eachSide)
        tmp_ZPinp = np.insert(inSampleSeq,0,ZP_arr)
        proc_ZPinp = np.append(tmp_ZPinp,ZP_arr)
        for op_cntr in range(opSeqLen):
            #print("Input Window: ",seqShiftVals[op_cntr:(op_cntr + self.numTaps_)])
            outSeq[op_cntr] = self.compOutputWithWindow(proc_ZPinp[op_cntr:(op_cntr + self.numTaps_)],\
                                            flagOpRepr=flagOpRepr,multWidth=multWidth,adderWidth=adderWidth,flagComputeType=flagComputeType)
        return outSeq

    '''
    ## Compute the output response for a 2D numpy array
    Can be used to estimate the behavior, in case the FIR Filter is used for a non X-Y separable operation
    '''
    def compOutputresponse2D(self,in2DArray,rowsInWindow,colsInWindow,flagOpRepr=None,multWidth=None,adderWidth=None,flagComputeType=None):
        ##inSampleSeq : a numpy array of sequence of samples over which the moving window will be operated
        ##flagOpRepr : Floating point (default,0) or fixed-point(1)
        if(flagOpRepr==None):
            flagOpRepr=0
        ##multWidth : Width of multiplier (default 8-bit)
        if(multWidth==None):
            multWidth=8
        ##adderWidth : Width of multiplier (default 16-bit)
        if(adderWidth==None):
            adderWidth=16
        ##flagComputeType : Compute (default, 0) or Lookup(1)
        if(flagComputeType==None):
            flagComputeType=0
        #print("Flag Operand Repr: ",flagOpRepr)
        ##Zero padding (number of rows and columns on each side)
        numRows_ZPad    = (np.round((rowsInWindow - 1)/2)).astype(int)
        numCols_ZPad    = (np.round((colsInWindow - 1)/2)).astype(int)
        inpArr_ZPadded  = np.lib.pad(in2DArray,
                            ((numRows_ZPad,numRows_ZPad),(numCols_ZPad,numCols_ZPad))
                            ,'constant',constant_values=(0))
        ##Compute response
        out2DArray = np.zeros(np.shape(in2DArray))
        #print("Assumption: The sequence starts at array[0]")
        ##For each pixel of input image, create the window size from zero padded array
        for rowCntr in range(0,(np.shape(in2DArray)[0])):
            for colCntr in range(0,(np.shape(in2DArray)[1])):
                ##Get starting co-ordinates of window
                rowIdx_Window   = rowCntr
                colIdx_Window   = colCntr
                matWindow       = inpArr_ZPadded[
                                        rowIdx_Window:rowIdx_Window+rowsInWindow,
                                        colIdx_Window:colIdx_Window+colsInWindow]
                arrWindow       = matWindow.flatten()
                out2DArray[rowCntr,colCntr] = self.compOutputWithWindow(arrWindow,
                                                flagOpRepr=flagOpRepr,multWidth=multWidth,adderWidth=adderWidth,flagComputeType=flagComputeType)
        return out2DArray
    '''
    #Update the FIR design metrics based on the Config of multipliers
    It is used to update the resource usage 
    during DSE due to a change in the type of multipliers being used 
    for the same FIR Filter object, rather than instantiating a new 
    FIR Filter object
    '''
    def updateDsgnPntMetrics(self,listMultDsgns):
        temp_rsrcLUTs   = 0
        for multType in self.MultTypes_:
            temp_rsrcLUTs   = temp_rsrcLUTs + listMultDsgns[multType].utilLUTs
        temp_rsrcSlices   = 0
        for multType in self.MultTypes_:
            temp_rsrcSlices   = temp_rsrcSlices + listMultDsgns[multType].utilSlices
        self.rsrcSlices = temp_rsrcSlices
        self.rsrcLUTs = temp_rsrcLUTs
