##System Library imports
import numpy as np
from numpy import genfromtxt
import subprocess as subproc
import random as rnd
from deap import base
from deap import creator
from deap import tools
from matplotlib import style
style.use("ggplot")
from PIL import Image as img
import threading as thr
import time as time
import timeit as timeit
from scipy import signal
import pylab as pylab
##User-defined library imports
import forFIRFilter.FIRFilter as firf
import forSupport.supportFunctions as support

def testFilter_FltPntAccFIR_1D(inpSignal,inp_samplerate,inp_NyqRate,inp_cutoffHz,numTaps_AccfltPnt):
    ##Instantiate Accurate fixed point FIR filter
    AccFltPnt_CoeffArray = signal.firwin(numTaps_AccfltPnt, inp_cutoffHz/inp_NyqRate)
    #print("Accurate FltPnt FIR Co-efficients:",AccFltPnt_CoeffArray)
    AccFltPnt_FIRFiltered_x           = signal.lfilter(AccFltPnt_CoeffArray, 1.0, inpSignal)
    #global psnr_Flt_AccFxd
    psnr_Flt_AccFlt = support.psnr(AccFltPnt_FIRFiltered_x,AccFltPnt_FIRFiltered_x,np.max(inpSignal))
    return (AccFltPnt_FIRFiltered_x,psnr_Flt_AccFlt)

def testFilter_xbitFxdPntFIR_1D(
        inpSignal,
        inp_samplerate, inp_NyqRate, inp_cutoffHz,
        numTaps_FxdPnt,
        flagComputeType=0,
        mulLuT=[],multTypeArray=[],
        multWidth=None):
    ##Instantiate Accurate fixed point FIR filter
    FxdPnt_CoeffArray = signal.firwin(numTaps_FxdPnt,inp_cutoffHz/inp_NyqRate)
    #print("FxdPnt FIR Co-efficients:",FxdPnt_CoeffArray)
    FxdPnt_FIRFilter = firf.FIR_directForm(numTaps_FxdPnt,FxdPnt_CoeffArray,arrMulTypes=multTypeArray,mulLuT=mulLuT)
    FxdPnt_FIRFilter.maxAbsInp_ = np.amax(np.absolute(inpSignal))
    FxdPnt_FIRFilter.displayFIRDetails()
    python_filtered_x           = signal.lfilter(FxdPnt_CoeffArray, 1.0, inpSignal)
    FxdPnt_FIRFiltered_x        = FxdPnt_FIRFilter.compOutputresponseConv(
                                        inpSignal,
                                        multWidth=multWidth,
                                        flagOpRepr=1,
                                        flagComputeType=flagComputeType
                                    )
    psnr_Flt_Fxd = support.psnr(python_filtered_x,FxdPnt_FIRFiltered_x,np.max(inpSignal))
    #print("PSNR (floating vs Accurate Fixed): ",psnr_Flt_AccFxd)
    return (FxdPnt_FIRFiltered_x,psnr_Flt_Fxd)
