#------------------------------------------------
# Create a signal for demonstration.
#------------------------------------------------
sample_rate = 100.0
nsamples = 1000
t = pylab.arange(nsamples) / sample_rate
x = np.cos(2*np.pi*0.5*t) + 0.2*np.sin(2*np.pi*2.5*t+0.1) + \
       0.2*np.sin(2*np.pi*15.3*t) + 0.1*np.sin(2*np.pi*16.7*t + 0.1) + \
           0.1*np.sin(2*np.pi*23.45*t+.8)
#------------------------------------------------
# Create a FIR filter and apply it to x.
#------------------------------------------------
# The Nyquist rate of the signal.
nyq_rate = sample_rate / 2.0
'''
# The desired width of the transition from pass to stop,
# relative to the Nyquist rate.  We'll design the filter
# with a 5 Hz transition width.
width = 5.0/nyq_rate

# The desired attenuation in the stop band, in dB.
ripple_db = 60.0

# Compute the order and Kaiser parameter for the FIR filter.
N, beta = signal.kaiserord(ripple_db, width)
'''
# The cutoff frequency of the filter.
cutoff_hz       = 10.0
arg_numTaps     = 100
'''
# Use firwin with a Kaiser window to create a lowpass FIR filter.
taps = signal.firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))
'''
#FIR filter instantiation
argCoeffArray = signal.firwin(arg_numTaps, cutoff_hz/nyq_rate)
# Use lfilter to filter x with the FIR filter.
python_filtered_x   = signal.lfilter(argCoeffArray, 1.0, x)
#------------------------------------------------
# Plot the FIR filter coefficients.
#------------------------------------------------
pylab.figure(1)
pylab.plot(argCoeffArray, 'bo-', linewidth=2)
pylab.title('Filter Coefficients (%d taps)' % arg_numTaps)
pylab.grid(True)

#------------------------------------------------
# Plot the magnitude response of the filter.
#------------------------------------------------
pylab.figure(2)
pylab.clf()
w, h = signal.freqz(argCoeffArray, worN=8000)
pylab.plot((w/np.pi)*nyq_rate, pylab.absolute(h), linewidth=2)
pylab.xlabel('Frequency (Hz)')
pylab.ylabel('Gain')
pylab.title('Frequency Response')
pylab.ylim(-0.05, 1.05)
pylab.grid(True)

# Upper inset plot.
ax1 = pylab.axes([0.42, 0.6, .45, .25])
pylab.plot((w/np.pi)*nyq_rate, pylab.absolute(h), linewidth=2)
pylab.xlim(0,8.0)
pylab.ylim(0.9985, 1.001)
pylab.grid(True)

# Lower inset plot
ax2 = pylab.axes([0.42, 0.25, .45, .25])
pylab.plot((w/np.pi)*nyq_rate, pylab.absolute(h), linewidth=2)
pylab.xlim(12.0, 20.0)
pylab.ylim(0.0, 0.0025)
pylab.grid(True)


# The phase delay of the filtered signal.
delay = 0.5 * (arg_numTaps-1) / sample_rate
#------------------------------------------------
# Plot the original and python filtered signals.
#------------------------------------------------
pylab.figure(3)
# Plot the original signal.
pylab.plot(t, x)
# Plot the filtered signal, shifted to compensate for the phase delay.
pylab.plot(t-delay, python_filtered_x, 'r-')
# Plot just the "good" part of the filtered signal.  The first N-1
# samples are "corrupted" by the initial conditions.
pylab.plot(t[arg_numTaps-1:]-delay, python_filtered_x[arg_numTaps-1:], 'g', linewidth=4)

pylab.xlabel('t')
pylab.title('Original and python filtered signal')
pylab.grid(True)
