----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.04.2018 19:50:28
-- Design Name: 
-- Module Name: tb_FIRFilter_sss - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;
use ieee.STD_LOGIC_SIGNED.all;
use ieee.STD_LOGIC_ARITH.all;
use std.textio.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_FIRFilter_sss is
--  Port ( );
end tb_FIRFilter_sss;

architecture Behavioral of tb_FIRFilter_sss is
    component FIRFilter_sss 
        Port ( clk : in STD_LOGIC;
               rst : in STD_LOGIC;
               dataIN : in STD_LOGIC_VECTOR (7 downto 0);
               dataOUT : out STD_LOGIC_VECTOR (18 downto 0);
               coeffs_flat : in std_logic_vector(8*5-1 downto 0) );
    end component;
    component new_filter 
        Port( 
            clk : in std_logic;
            din : in std_logic_vector(7 downto 0);
            dout : out std_logic_vector(18 downto 0)
        ); 
    end component; 
    signal dataIN : STD_LOGIC_VECTOR (7 downto 0);
    signal clk : STD_LOGIC;
    signal rst : STD_LOGIC;
    signal dataOUT : STD_LOGIC_VECTOR (18 downto 0);
    constant clk_period : time := 10 ns;
    constant NUM_COL                : integer := 1;   -- number of column of file
    type t_integer_array       is array(4 downto 0)  of integer;
    signal coeffs_flat : std_logic_vector(8*5-1 downto 0);
    
begin
--    uut: FIRFilter_sss port map (
--                rst => rst,
--                clk => clk ,
--                dataIN => dataIN,
--                dataOUT => dataOUT
--            );
--    uut: new_filter port map (
--               clk => clk ,
--               din  => dataIN,
--               dout => dataOUT
--           );
    uut: FIRFilter_sss port map (
            rst => rst,
            clk => clk ,
            dataIN => dataIN,
            dataOUT => dataOUT,
            coeffs_flat => coeffs_flat
        );
    clk_process :process
       begin
            clk <= '0';
            wait for clk_period/2;  --for 0.5 ns signal is '0'.
            clk <= '1';
            wait for clk_period/2;  --for next 0.5 ns signal is '1'.
       end process;
     
     stim_proc: process
        file inputfile : TEXT open READ_MODE is "/home/ssatyendras/WORK/ApproxDPR/approxdpr/src/inputForHDLTest.txt";
        file outputfile : TEXT open WRITE_MODE is "/home/ssatyendras/WORK/ApproxDPR/approxdpr/src/outputFromHDLTest.txt";
        variable Lr, Lw : line;
        --variable rdData : STD_LOGIC_VECTOR (dataIN' range);
        variable rdData : integer;
        variable coeffVals : t_integer_array; 
        --variable v_data_read : t_integer_array(1 to NUM_COL);
        begin
        wait for 10 ns;
        rst <= '1';
        wait for 50 ns;
        rst <= '0';
        for tapCntr in 4 downto 0 loop
            readline (inputfile, Lr);
            read(Lr, coeffVals(tapCntr));  
        end loop;
        coeffs_flat <=  conv_std_logic_vector(coeffVals(4),8) &
                        conv_std_logic_vector(coeffVals(3),8) &
                        conv_std_logic_vector(coeffVals(2),8) &
                        conv_std_logic_vector(coeffVals(1),8) &
                        conv_std_logic_vector(coeffVals(0),8);
        while not endfile(inputfile) loop
--            for kk in 1 to NUM_COL loop
--                read(Lr,v_data_read(kk));
--            end loop;
            readline (inputfile, Lr);
            read(Lr, rdData);
            --dataIN <= conv_std_logic_vector(v_data_read(1),8);
            dataIN <= conv_std_logic_vector(rdData,8);
            wait for clk_period;
            --write(Lw, string'(" res = "));
            write(Lw, conv_integer(dataOUT));
            writeline(outputfile, Lw);
        end loop;
        for i in 100 downto 0 loop
            wait for clk_period;
            --write(Lw, string'(" res = "));
            write(Lw, conv_integer(dataOUT));
            writeline(outputfile, Lw);
        end loop;
        file_close(inputfile);
        file_close(outputfile);
        wait for 1000 ns;
        --wait;
    end process;

end Behavioral;
