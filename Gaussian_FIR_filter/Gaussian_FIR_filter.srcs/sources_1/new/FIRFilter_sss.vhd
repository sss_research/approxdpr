----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.04.2018 19:49:23
-- Design Name: 
-- Module Name: FIRFilter_sss - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FIRFilter_sss is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           dataIN : in STD_LOGIC_VECTOR (7 downto 0);
           dataOUT : out STD_LOGIC_VECTOR (18 downto 0);
           coeffs_flat : in std_logic_vector(8*5-1 downto 0) );
end FIRFilter_sss;

architecture Behavioral of FIRFilter_sss is
    type delayElement_Type is array(4 downto 0) of std_logic_vector(dataIN'range);
    type coeff_Type is array(4 downto 0) of std_logic_vector(dataIN'range);
    type mulOUT_Type is array(4 downto 0) of std_logic_vector(15 downto 0);
    type addIN_fromMUL_Type is array(4 downto 0) of std_logic_vector(18 downto 0);
    type addIN_fromADD_Type is array(3 downto 0) of std_logic_vector(18 downto 0);
    type addOUT_type is array(3 downto 0) of std_logic_vector(18 downto 0);
--    constant coeffs : coeff_Type := (
--                                   --76543210
--                                    "00001000", 
--                                    "01000000", 
--                                    "01111111", 
--                                    "01000000", 
--                                    "00001000"
--                                    );
    signal coeffs : coeff_Type;
    signal delayArray :  delayElement_Type; 
    signal mulOUT : mulOUT_type;
    signal addOUT : addOUT_type;
    signal addIN_fromMUL  : addIN_fromMUL_Type;
    signal addIN_fromADD  : addIN_fromADD_Type;     

begin
    splitCoeffs:for i in 0 to 4 generate
           coeffs(i)(7 downto 0) <= coeffs_flat(i*8+7 downto i*8 );
        end generate splitCoeffs;
    ----------------------------------------
    --Creating delayed inputs
    ----------------------------------------      
    -- For delay
    fir_process:process(clk) 
    begin
        if (rst = '1') then
            for i in 4 downto 0 loop
                delayArray(i) <= (others => '0');
            end loop;
        elsif(rising_edge(clk)) then
            for i in 4 downto 0 loop
                if i = 0 then
                     delayArray(i) <= dataIN;
                 else
                    delayArray(i) <=  delayArray(i-1);
                end if;
            end loop;
        end if;
    end process;
    ----------------------------------------
    -- Multiplication of delayed input
    ----------------------------------------   
    g_GENERATE_Multiplication: for ii in 4 downto 0 generate
            mulOUT(ii) <= std_logic_vector(signed(coeffs(ii)) * signed(delayArray(ii)));
        end generate g_GENERATE_Multiplication;
    ----------------------------------------
    --Rounding and/or Truncation and/or SignExtension
    ----------------------------------------        
    --For multiplier outputs 
    g_GENERATE_MULOut_Rounding: for ii in 4 downto 0 generate
            addIN_fromMUL(ii) <= mulOUT(ii)(15) & mulOUT(ii)(15) & mulOUT(ii)(15) & mulOUT(ii);
        end generate g_GENERATE_MULOut_Rounding;
    --For adder outputs 
    g_GENERATE_ADDOut_Rounding: for ii in 3 downto 0 generate
            addIN_fromADD(ii) <= addOUT(ii);
        end generate g_GENERATE_ADDOut_Rounding;
    ----------------------------------------
    --Accumulation of products
    ----------------------------------------      
    --For Accumulation
    addOUT(0) <= std_logic_vector(signed(addIN_fromMUL(0)) + signed(addIN_fromMUL(1))) ;
    g_GENERATE_Accum: for j in 3 downto 1 generate
        addOUT(j) <= std_logic_vector(signed(addIN_fromMUL(j+1)) + signed(addIN_fromADD(j-1))) ;
    end generate g_GENERATE_Accum;
    ----------------------------------------
    --Data Output
    ----------------------------------------      
    --For Data Out
    dataOUT <= addIN_fromADD(3);

end Behavioral;
