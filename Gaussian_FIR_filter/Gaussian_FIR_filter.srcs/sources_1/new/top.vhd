----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/31/2017 11:07:18 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
port( 
top_x : in std_logic_vector(7 downto 0);
top_coeff : in std_logic_vector(((1+1)*8-1) downto 0); --4 = N
top_y : out std_logic_vector(1+15 downto 0); --4 = N
top_clk : in std_logic;
top_reset : in std_logic
);
end top;

architecture Behavioral of top is

begin

filter : entity work.filter
port map(
    x => top_x,
    coeff => top_coeff,
    y => top_y,
    clk => top_clk,
    reset => top_reset
);
    
end Behavioral;
