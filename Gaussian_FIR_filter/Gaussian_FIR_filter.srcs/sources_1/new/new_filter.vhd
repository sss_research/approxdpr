----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/26/2018 10:21:20 AM
-- Design Name: 
-- Module Name: new_filter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity new_filter is
Port(
    clk : in std_logic;
    din : in std_logic_vector(7 downto 0);
    dout : out std_logic_vector(18 downto 0)
);    
end new_filter;

architecture Behavioral of new_filter is

    signal dinr : std_logic_vector(din'range);
    type coeff_type is array(4 downto 0) of std_logic_vector(7 downto 0);
    constant coeffs : coeff_type := (
        "01110101", "01111100", "01111111", "01111100", "01110101");
    type mul_type is array(4 downto 0) of std_logic_vector(15 downto 0);
    signal mul : mul_type;
    type add_type is array(4 downto 0) of std_logic_vector(18 downto 0);
    signal add : add_type;
    constant zero : std_logic_vector(18 downto 0) := (others => '0');

begin

process(clk) 
begin
    if clk'event and clk='1' then
        dinr <= din;
        for i in 4 downto 0 loop
            mul(i) <= std_logic_vector(signed(dinr) * signed(coeffs(4-i)));
            if i = 0 then
                --add(i) <= std_logic_vector(signed(zero) + signed(mul(0)));
                add(i) <= zero + (mul(0)(15) & mul(0)(15) & mul(0)(15) & mul(0));
            else
                --add(i) <= std_logic_vector(signed(mul(i)(15) & mul(i)(15) & mul(i)) + signed(add(i-1)));
                add(i) <= (mul(i)(15) & mul(i)(15) & mul(i)(15) & mul(i)) + add(i-1);
            end if;
         end loop;
         dout <= add(4);
        end if;
     end process;

end Behavioral;
