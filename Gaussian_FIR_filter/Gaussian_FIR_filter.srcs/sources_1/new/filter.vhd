-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


-- entity "uses" the package   


package mytypes_pkg is

     type my_array_t is array (0 to 1) of integer range 0 to 1;

end package mytypes_pkg;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;

use UNISIM.VComponents.all;

use work.mytypes_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity filter is
 
generic (
N : integer := 1;
NN : integer := 8;
MM : integer := 8;
--mult_arr : my_array_t := (1, 2, 3 ,3, 2, 1, 1, 2, 3, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
mult_arr : my_array_t := (1, 1)
);

Port (
  x : in std_logic_vector (7 downto 0);
  coeff : in std_logic_vector (((N+1)*8-1) downto 0);
   y : out std_logic_vector (N+15 downto 0); 
  clk : in std_logic;
  reset : in std_logic
 
 );
end filter;

architecture Behavioral of filter is

--component mul8_WallaceTreeMultiplier_Using_CarryLookaheadAdder is
--Port ( 
--A : in  STD_LOGIC_VECTOR (7 downto 0);
--B : in  STD_LOGIC_VECTOR (7 downto 0);
--O : out STD_LOGIC_VECTOR (15 downto 0)); 
--end component;
--component mul8_012 is
--Port (
--A : in STD_LOGIC_VECTOR (7 downto 0);
--B : in STD_LOGIC_VECTOR (7 downto 0);
--O : out STD_LOGIC_VECTOR (15 downto 0));
--end component;
--component mult_mod_8_aaaa is
--Port (
--A : in STD_LOGIC_VECTOR (7 downto 0);
--B : in STD_LOGIC_VECTOR (7 downto 0);
--PROD : out STD_LOGIC_VECTOR (15 downto 0));
--end component;
signal q : std_logic_vector ((N+1)*8-1 downto 0);
--signal a_sig, b_sig : std_logic_vector (7 downto 0);
signal prod_sig : std_logic_vector ((N+1)*16-1 downto 0);
--shared variable a_sig1, b_sig1: std_logic_vector (7 downto 0);
shared variable p : std_logic_vector (N+15 downto 0);
begin
generate_mul: for i in 0 to N generate
    begin
    generate_mul_acc : if  mult_arr(i) = 0 generate
    begin
       prod_sig((i*16+15) downto (i*16)) <= std_logic_vector(signed(q((i*8+7) downto (i*8))) * signed(coeff((i*8+7) downto (i*8))));
    end generate generate_mul_acc;
    
    generate_mul_appr: if mult_arr(i) = 1 generate
    begin     
        mul : entity work.multiplier
        generic map(
            N => NN,
            M => MM
        )    
        port map(
            a => q((i*8+7) downto (i*8)),
            b => coeff((i*8+7) downto (i*8)),
            d => (others => '0'),
            p => prod_sig((i*16+15) downto (i*16))
        );
    end generate generate_mul_appr;
end generate generate_mul;
     

--generate_mult: for i in 0 to N generate
--begin
--  generate_mult_accurate: if mult_arr(i) = 1 generate
--   begin
---- p := (others=> std_logic ' ('0'));


--    inst1_mult: mul8_WallaceTreeMultiplier_Using_CarryLookaheadAdder port map(
--            A => q((i*8+7) downto (i*8)),
--            B => coeff((i*8+7) downto (i*8)),
--            O => prod_sig((i*16+15) downto (i*16))
--            );
--    end generate generate_mult_accurate;
--    generate_mult_approx_1: if mult_arr(i) = 2 generate
--       begin
--    -- p := (others=> std_logic ' ('0'));
--        inst2_mult: mul8_012  port map(
--                A => q((i*8+7) downto (i*8)),
--                B => coeff((i*8+7) downto (i*8)),
--                O => prod_sig((i*16+15) downto (i*16))
--                );
--        end generate generate_mult_approx_1 ;
----        generate_mult_approx_2: if mult_arr(i) = 3 generate
----           begin
----        -- p := (others=> std_logic ' ('0'));
----            inst3_mult: mult_mod_8_aaaa port map(
----                    A => q((i*8+7) downto (i*8)),
----                    B => coeff((i*8+7) downto (i*8)),
----                    PROD => prod_sig((i*16+15) downto (i*16))
----                    );
----            end generate generate_mult_approx_2;
--            --p := p + prod_sig((i*8+7) downto (i*8));
--end generate generate_mult;
-- mult1 : process(clk, reset, coeff, x)
-- begin 
                              
--a_sig <= a_sig1;
--b_sig <= b_sig1;
--end process mult1;
p_input : process (reset,clk)
begin
  if(rising_edge(clk)) then
        if(reset='1') then
            q <= (others=> std_logic ' ('0'));
        else
            q (7 downto 0) <= x (7 downto 0);
            loop1:
            for i in 0 to N-1 loop
                q((i+1)*8+7 downto (i+1)*8) <= q((i*8)+7 downto (i*8));    
            end loop loop1;    
        end if;
    end if;        
 end process p_input;   
 
p_2: process (reset,clk,coeff,x)

variable mult : std_logic_vector (15 downto 0);

begin

        p := "0000000000000000"&prod_sig(15 downto 0);
        loop2:
        for i in 1 to N loop
           
          --a_sig1 := q((i*8+7) downto (i*8));
          -- b_sig1 := coeff((i*8+7) downto (i*8));
           
           --mult := ;
           p := p + prod_sig((i*16+15) downto (i*16));   
        end loop loop2; 
        y <= p;
         
end process p_2;     
end Behavioral;