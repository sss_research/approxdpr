----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/31/2017 11:09:01 AM
-- Design Name: 
-- Module Name: top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

--use IEEE.MATH_REAL.ALL;
use STD.TEXTIO.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
-- library UNISIM;
-- use UNISIM.VComponents.all;

entity top_tb is 
end top_tb;

architecture Behavioral of top_tb is
constant N : integer := 1;

--signal x : std_logic_vector(7 downto 0) := (others => '0');
--signal coeff: std_logic_vector(((N+1)*8-1) downto 0) := (others => '0');
--signal y : std_logic_vector(N+15 downto 0) := (others => '0');
--signal clk : std_logic := '0';
--signal reset : std_logic := '0';

signal clk : std_logic := '0';
signal din : std_logic_vector(7 downto 0);
signal dout : std_logic_vector(18 downto 0);

signal running : std_logic := '1';
constant T_PERIOD : time := 20 ns;

file file_output : text;
file file_input : text;

type input is array(4 downto 0) of std_logic_vector(7 downto 0);
signal inputs : input;

begin

clk <= clk xor running after T_PERIOD/2;

DUT : entity work.new_filter
port map(
    clk => clk,
    din => din,
    dout => dout
);

stimuli : process
   
    variable oline : line;
    variable iline : line;
    --variable v_input : std_logic_vector(7 downto 0);
    --variable space : character;
    variable read_input : std_logic_vector(7 downto 0);
    variable input_space : character;
    
begin

--file_open(file_input, "/afs/pd.inf.tu-dresden.de/users/s1780256/tmp/project_FIR_2/data_in.txt", read_mode);
--file_open(file_output, "/afs/pd.inf.tu-dresden.de/users/s1780256/tmp/project_FIR_2/data_out.txt", write_mode);

--readline(file_input, iline);

--read(iline, read_input);
--inputs(4) <= read_input;

--read(iline, input_space);

--read(iline, read_input);
--inputs(3) <= read_input;

--read(iline, input_space);

--read(iline, read_input);
--inputs(2) <= read_input;

--read(iline, input_space);

--read(iline, read_input);
--inputs(1) <= read_input;

--read(iline, input_space);

--read(iline, read_input);
--inputs(0) <= read_input;

wait for 1 ns;

din <= inputs(4);

wait until rising_edge(clk);

din <= inputs(3);
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);
 
din <= inputs(2); 
write(oline, dout);
write(oline, string'(" "));
 
wait until rising_edge(clk);

din <= inputs(1); 
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk); 

din <= inputs(0); 
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "00001111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "00001111";

write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "00001111";

write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111110";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "00000000";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

din <= "01111111";
write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

write(oline, dout);
write(oline, string'(" "));

wait until rising_edge(clk);

write(oline, dout);
write(oline, string'(" "));
writeline(file_output, oline);

running <= '0';

wait;
end process;

end Behavioral;