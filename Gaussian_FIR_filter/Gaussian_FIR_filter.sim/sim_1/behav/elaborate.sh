#!/bin/bash -f
xv_path="/afs/pd.inf.tu-dresden.de/sw/Vivado-17.1/Vivado/2017.1"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto a54ba299327b41b1a5d5f10e9ebd52b9 -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot top_tb_behav xil_defaultlib.top_tb -log elaborate.log
